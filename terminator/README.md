### Installation

```
# sublime wrtapper 
ln -s plugins/PLUGINNAME ~/.config/terminator/plugins/
cd  /usr/local/bin && sudo ln -s ~/Workspace/kerenv/terminator/plugins/sublime_wrapper_url.sh .
#.zshrc 
# add terminator sublime_wrapper_url.sh
export PATH=$PATH:/usr/local/bin
```

### Reference

```
https://terminator-gtk3.readthedocs.io/en/latest/plugins.html
```