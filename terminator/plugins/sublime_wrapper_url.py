#!/usr/bin/env python3

# -*- coding: utf-8 -*-
# Terminator GCC url handler (provides clickable links in gcc error messages) by Florent Dutrech


# INSTALL
# mkdir ~/.config/terminator/plugins
# Important!
# For this plugin to work, the terminator python lib must be modified a bit
# In /usr/lib/python2.7/site-packages/terminatorlib/terminal.py, line 1417,
#
# Replace
# newurl = urlplugin.callback(url)
# with
# newurl = urlplugin.callback(url, self)

# Every plugin you want Terminator to load *must* be listed in 'AVAILABLE'
#
# https://lazka.github.io/pgi-docs/Vte-2.91/classes/Terminal.html
# If more than one regular expression has been set with vte_terminal_match_add(), then expressions are checked in the order in which they were added.
#


# https://terminator-gtk3.readthedocs.io/en/latest/plugins.html

# 
# TO DEBUG :
#         - Open terminator from commandline : terminator -d -u
#         - >> tail -f  /tmp/sublime_terminator_url.log
#

# https://github.com/mchelem/terminator-editor-plugin/blob/master/editor_plugin.py

AVAILABLE = ['PDFURLHandler', 'PythonExceptionURLHandler', 'PytestURLHandler', 'GolangURLHandler', 'GccFileURLHandler',  'AnyFileURLHandler', 'GitDiffURLHandler', 'TpURLHandler']
AVAILABLE.remove('PDFURLHandler')
AVAILABLE.remove('GccFileURLHandler')
# AVAILABLE = ['PythonExceptionURLHandler', 'PytestURLHandler', 'GolangURLHandler', 'GccFileURLHandler',  'AnyFileURLHandler', 'GitDiffURLHandler', 'TpURLHandler']
# AVAILABLE = ['PythonExceptionURLHandler', 'PytestURLHandler', 'GolangURLHandler', 'GccFileURLHandler',  'GitDiffURLHandler']

VERSION = "1.2.0"

import os, sys, re, inspect
import json
import logging
import traceback
import subprocess
import terminatorlib.plugin as plugin
import re
from pathlib import Path

logging.basicConfig(level=logging.INFO)

LOGGER_NAME = "sublime_terminator_url"

LOGGER = logging.getLogger(LOGGER_NAME)
LOGGER.propagate = False

# logFormatter = logging.Formatter("[%(asctime)s][%(pathname)s:%(lineno)d][%(levelname)-5.5s] %(message)s")
logFormatter = logging.Formatter("[%(asctime)s][%(levelname)s] %(message)s")

fileHandler = logging.FileHandler(f"/tmp/{LOGGER_NAME}.log")
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
LOGGER.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.INFO)
LOGGER.addHandler(consoleHandler)

userchars = "-A-Za-z0-9"
pathchars = userchars + "_$.+!*(),;@&=?/~#%'\""
# pathchars = userchars + "_$.+!*(),;:@&=?/~#%"

# https://developer.gnome.org/vte/unstable/VteRegex.html
# https://developer.gnome.org/glib/stable/glib-regex-syntax.html


#
# VERY GOOD MODEL : https://github.com/mchelem/terminator-editor-plugin/blob/master/editor_plugin.py
#

# /usr/lib/python2.7/site-packages/terminatorlib/terminal.py
# line 315 : self.matches[name] = self.vte.match_add(match)
# line 1416 : if match == self.matches[urlplugin.handler_name]:

# https://github.com/martyr-deepin/python-vte/search?q=match_add&unscoped_q=match_add
# (define-method match_add
#   (of-object "VteTerminal")
#   (c-name "vte_terminal_match_add")
#   (return-type "int")

# See man:pcre2pattern(3) for information about the supported regex language.
# https://www.pcre.org/current/doc/html/pcre2pattern.html

# prevent python exceptions to be matched
# ANY_MATCH = '[' + pathchars + ']+\.\w+(:[0-9]+){0,2}'


# ANY_MATCH = '(^|[\r\n\t\f()=\'])[^: \[\]\r\n\t\f()=\']+(:[0-9]+)?(:[0-9]+)?'


# NO_PY_MATCH = '(^File ")'
# ANY_MATCH = NO_PY_MATCH + ANY_MATCH



import terminatorlib

LOGGER.info(f"{sys.version=}")
LOGGER.info(f"{VERSION=}")
LOGGER.debug(f"{__file__=}")
LOGGER.debug(f"{terminatorlib.__file__=}")

class PDFURLHandler(plugin.URLHandler):

    handler_name = 'pdf_url_handler'
    # match = '"[' + pathchars + ']+\.pdf'
    # match = '\b.*\.pdf\b'
    match = '[^: \[\]\r\n\t\f()=\'"]\.pdf'
    def callback(self, url):
        print("PFD url :", url)
        return url


def linux_find(filename, root):
    """
    Use linux 'find' command to serach filename in ATEME_WORKSPACE directory

    Input:
        filename: stem.suffix:line:column

    Output
        filename: PATH/stem.suffix:line:column if any match, None otherwise
    """

    LOG_PREFIX = "[linux_find]"

    LOGGER.info(f"{LOG_PREFIX} linux_find({filename=})")

    LOGGER.info(f"{LOG_PREFIX} {root=}")

    if not root or not os.path.exists(root):
        LOGGER.info(f"{LOG_PREFIX} Unknown root - exit")
        return None

    path = Path(filename)

    # remove :line:column suffix if any
    extension, suffix = re.match('([^:]+)(:?.*)', path.suffix).groups()

    cmd = f'find {root} -name "{path.stem + extension}"'

    LOGGER.info(f"{LOG_PREFIX} {cmd=}")
    ps = subprocess.run(cmd, shell=True, capture_output=True, text=True)
    LOGGER.info(ps)

    filenames=ps.stdout.strip('\n').split("\n")
    LOGGER.info(f"{LOG_PREFIX} matches[%d] =\n\t - %s"%(len(filenames), '\n\t - '.join(filenames)))

    if not filenames:
        return None

    return filenames[0] + suffix


def fzf_search(filename):

    LOGGER.info(f"fzf_search({filename=})")

    # remove line/column: filename.ext:line:col
    if ":" in filename:
        filename = filename.split(":", 1)[0]

    #
    FZF_ROOT = os.environ.get("ATEME_WORKSPACE", None)

    LOGGER.info(f"{FZF_ROOT=}")

    if not FZF_ROOT or not os.path.exists(FZF_ROOT):
        LOGGER.info(f"Unknown FZF_ROOT - exit")
        return None

    fzf_filename = Path(filename).name
    fzf_extension = Path(filename).suffix
    cmd = f'find %s -name "*%s" | fzf --no-sort --filter %s'%(FZF_ROOT, fzf_filename, fzf_extension)

    # cmd = f'find {FZF_ROOT} -name {fzf_filename} | fzf --no-sort'
    # cmd = f'find {FZF_ROOT} -name {fzf_filename} | fzf --no-sort'

    LOGGER.info("FZF cmd=%s"%(cmd))
    ps = subprocess.run(cmd, shell=True, capture_output=True, text=True)

    # LOGGER.info(ps)
    LOGGER.info(ps.stderr)
    LOGGER.info(ps.stdout)
    # ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # stdout, stderr = ps.communicate()
    # if stderr:
    #     LOGGER.info("Stderr:")
    #     LOGGER.info(stderr)

    # LOGGER.info("Stdout:")
    # LOGGER.info(stdout)

    filenames=ps.stdout.strip('\n').split("\n")
    LOGGER.info("FZF matches[%d] =\n\t - %s"%(len(filenames), '\n\t - '.join(filenames)))

    if not filenames:
        return None

    if len(filenames)==1:
        return filenames[0]

    # Multiple FZF match, select best one...
    exact_filenames=[f for f in filenames if Path(f).stem == path.stem]

    LOGGER.info(f"{exact_filenames=}")
    if exact_filenames:
        LOGGER.info("Exact matche : filenames[%d]=%s"%(len(exact_filenames), str(exact_filenames)))
        filename3 = exact_filenames[0]

    return filenames[0]




class SublimeURLHandler(plugin.URLHandler):

    """
    1. terminator matches filenames using plugin.URLHandler.match regex
    2. terminator calls plugin.URLHandler.callback, which must return parsed filename
    3. terminator open parsed-filename using its MIME type

    Because we would like to open filename with line and column encoded, the parsed-filename will not have valid MIME-type (eg /tmp/file.cpp:56:12 or /tmp/file.py - line 65)

    The trick here is :
     - always return the same parsed-filename, with a custom extension (.st3)
     - write within the wrapper file the filename with line and column encoded
     - create a script to open this new extension
        
        '''
        #! /usr/bin/bash
        if [ $# == 1 ]; then
            subl $(cat $1)
        fi
        '''

     - create new MIME type (use right click on dumb .st3 file, choose this script to open by default, or see detail description on kerenv/docs)

    """

    capabilities = ['url_handler']

    # TO BE DEFINED 
    handler_name = None
    match = None

    CONFIG = None
    @staticmethod
    def config(key=None, default_value=None):
        if SublimeURLHandler.CONFIG is None:
            path = os.path.join(os.path.expanduser("~"), ".config/terminator/plugins", os.path.basename(__file__).replace(".py", ".json") )
            if os.path.exists(path):
                LOGGER.info("Found %s config file"%path)
                with open(path) as f:
                    SublimeURLHandler.CONFIG=json.load(f)
                    LOGGER.info("SublimeURLHandler.CONFIG=%s"%str(SublimeURLHandler.CONFIG))
            else:
                LOGGER.info("No %s config file found - return empty"%path)
                SublimeURLHandler.CONFIG={}

        if key is None:
            return SublimeURLHandler.CONFIG
        else:
            return SublimeURLHandler.CONFIG.get(key, default_value)

    def __init__(self):
        self.filename = None
        self.page = None
        self.cmd = None

    def get_terminal(self):
        # HACK: Because the current working directory is not available to
        # plugins, we need to use the inspect module to climb up the stack to
        # the Terminal object and call get_cwd() from there.
        for frameinfo in inspect.stack():
            frameobj = frameinfo[0].f_locals.get('self')
            if frameobj and frameobj.__class__.__name__ == 'Terminal':
                return frameobj

    def get_cwd(self):
        """ Return current working directory. """
        term = self.get_terminal()
        if term:
            return term.get_cwd()

    def smart_open_one(self, filename, line=None, column=None, program=None):

        LOGGER.info(f"smart_open_one({filename=}, {line=}, {column=}, {program=})")

        if not filename:
            LOGGER.warning(f"Empty {filename=}")
            return False

        if not os.path.exists(filename):
            LOGGER.warning(f"File does not exists '{filename}'")
            return False

        if program is None:
            if filename.endswith(".pdf"):
                program = "evince"
            else:
                program = "subl"

        if program == "subl":

            cmd = program + " " + filename
            if line is not None:
                cmd += ":" + line
                if column is not None:
                    cmd += ":" + column

        elif program == "evince":

            cmd = program + " " + filename

            # page
            if line is not None:
                cmd += " -i " + line
            cmd += " &"


        else:
            LOGGER.error(f"File does not exists '{filename}'")
            return False

        return cmd

    def smart_open(self, filename, line=None, column=None, program=None):
        LOGGER.info(f"smart_open({filename=}, {line=}, {column=}, {program=})")

        try:

            while 1:

                #
                # NOMINAL CASE
                #
                cmd = self.smart_open_one(filename, line=line, column=column, program=program)
                if cmd is not False:
                    break

                #
                # BASE PATH
                #
                for base_path_src, base_path_dst in SublimeURLHandler.config("base_path", {}).items():
                    cmd = self.smart_open_one(filename.replace(base_path_src, base_path_dst, 1), line=line, column=column, program=program)
                    if cmd is not False:
                        break
                if cmd is not False:
                    break

                #
                # CWD
                #
                cwd = self.get_cwd()

                LOGGER.info(f"Try with cwd={cwd}")
                filename2 = (cwd + filename[1:]) if filename.startswith("./") else (cwd + '/' + filename)

                cmd = self.smart_open_one(filename2, line=line, column=column, program=program)
                if cmd is not False:
                    break

                #
                # FIND
                #
                for root in SublimeURLHandler.config("find_root", []):
                    cmd = self.smart_open_one(linux_find(filename, root), line=line, column=column, program=program)
                    if cmd is not False:
                        break
                if cmd is not False:
                    break


                #
                # FZF
                #
                cmd = self.smart_open_one(fzf_search(filename), line=line, column=column, program=program)
                if cmd is not False:
                    break


                else:
                    LOGGER.info("fzf_search Failed")


                break


        except Exception as e:
            LOGGER.exception(e)

        if cmd is not False:
            LOGGER.info(cmd)
            os.system(cmd)

        # return dummy args for xdg-open which will be called by terminator
        return '--version'

    def callback(self, url):

        # TO READ :
        # https://github.com/mchelem/terminator-editor-plugin/blob/master/editor_plugin.py

        try:

            cwd = None
            relative_url = None

            LOGGER.info(" * handler_name : %s"%(str(self.handler_name) ))
            LOGGER.info(" * regex match: %s (%s)"%(repr(self.match), str(type(self.match)) ))
            LOGGER.info(" * url : %s"%(repr(url) ))

            match = re.match(self.match, url)
            if match is not None:
                LOGGER.info(" * regex groups : %s"%str(match.groups()) )



  # def callback(self, url):
  #   if not url.startswith("~") and not url.startswith("/"):
  #     for terminal in self.terminator.terminals:
  #       if terminal is self.terminator.last_focused_term:
  #         return terminal.get_cwd() + "/" + url
  #   else:
  #     return os.path.expanduser(url)








            #
            # git diff
            #
            # --- a/units/TitanRtpPassthrough/tests/pipelines/gateway.yaml
            # +++ b/units/TitanRtpPassthrough/tests/pipelines/gateway.yaml
            #
            if url.startswith("a/") or url.startswith("b/") :
                LOGGER.info("git diff url detected (%s) - try without '[ab]/' prefix..."%url)
                url = url[2:]

            #
            # if file is on another machine, replace path
            #
            SUBLIME_URL_HANDLER=self.config("SUBLIME_URL_HANDLER")
            if SUBLIME_URL_HANDLER:
                LOGGER.info("#")
                LOGGER.info("# SUBLIME_URL_HANDLER : %s "%str(SUBLIME_URL_HANDLER))
                LOGGER.info("#")
                if type(SUBLIME_URL_HANDLER) is str:
                    SUBLIME_URL_HANDLER = SUBLIME_URL_HANDLER.split(",")
                for pair in SUBLIME_URL_HANDLER:
                    first,second=pair.split(":")
                    if url.startswith(first):
                        LOGGER.info("%s matches %s"%(url,pair))
                        url=url.replace(first,second,1)
                        LOGGER.info("Update url=%s"%url)
            else:
                LOGGER.info("# SUBLIME_URL_HANDLER not defined, skip")

            #
            # if file is not absolute, append terminator CWD
            #
            filename=url.split(":")[0]
            if not os.path.exists(filename):
                if not url.startswith('/'):
                    LOGGER.info("#")
                    LOGGER.info("# CWD")
                    LOGGER.info("#")
                    for i,frameinfo in enumerate(inspect.stack()):
                        frameobj = frameinfo[0].f_locals['self']
                        if frameobj and frameobj.__class__.__name__ == "Terminal":
                            cwd = frameobj.get_cwd()
                            break

                    if cwd is not None:
                        LOGGER.info("cwd : %s"%(str(cwd)))
                        cwd_filename=os.path.join(cwd, filename)
                        if os.path.exists(cwd_filename):
                            relative_url = url
                            url = url.replace(filename, cwd_filename)
                            LOGGER.info("Apply 'cwd' : url='%s'"%url)
                        else:
                            LOGGER.info("Do not apply 'cwd' since joined path '%s' does not exists"%cwd_filename)
                    else:
                        LOGGER.info("could not found 'cwd'")

                    # LOGGER.info("absolute url : %s (%s) "%(str(url), str(type(url)) ))

                    # LOGGER.info(self.match)
                    # LOGGER.info(type(self.match))


            # export SUBLIME_URL_HANDLER=from:to,from2:to2

            # if re.search(self.match, url):
            #     #
            #     # DEBUG
            #     #
            #     # with open("/tmp/debug.txt", 'w') as f:
            #     #     f.write( "url : %s (%s)\n"%(str(url), str(type(url)) ))
            #     #     f.write( "python version : %s\n"%(str(sys.version) ))
            #     #
            #     # write wrapper file
            #     #
            #     with open(SublimeURLHandler.filename, 'w') as f:
            #         f.write(url)
            # else:


            filename=url.split(":")[0]

            #
            # FZF
            #

            if not os.path.exists(filename):

                SUBLIME_URL_HANDLER_FZF_ROOT = self.config("SUBLIME_URL_HANDLER_FZF_ROOT", [])
                if type(SUBLIME_URL_HANDLER_FZF_ROOT) is str:
                    SUBLIME_URL_HANDLER_FZF_ROOT=[SUBLIME_URL_HANDLER_FZF_ROOT]

                LOGGER.info("#")
                LOGGER.info("# FZF (%s)"%(str(SUBLIME_URL_HANDLER_FZF_ROOT)))
                LOGGER.info("#")

                for root in SUBLIME_URL_HANDLER_FZF_ROOT + [cwd]:
                    if root and os.path.exists(root):
                        extension = os.path.splitext(filename)[1]
                        # cmd = 'find %s -name "*%s" | fzf --no-sort --filter \"%s\"'%(root, extension, filename)

                        # my_env = os.environ.copy()
                        # my_env["PATH"] = "/usr/sbin:/sbin:" + my_env["PATH"]
                        # subprocess.Popen(my_command, env=my_env)

                        cmd = 'find %s -name "*%s" | /home/fdutrech/Workspace/third_parties/fzf/bin/fzf --no-sort --filter %s'%(root, extension, filename)
                        LOGGER.info("cmd=%s"%(cmd))
                        ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        stdout, stderr = ps.communicate()
                        if stderr:
                            LOGGER.info("Stderr:")
                            LOGGER.info(stderr)

                        # LOGGER.info("Stdout:")
                        # LOGGER.info(stdout)

                        filenames=stdout.decode().strip('\n').split("\n")
                        LOGGER.info("SUBLIME_URL_HANDLER_FZF_ROOT='%s' - filenames[%d]=%s"%(root, len(filenames), str(filenames)))

                        if len(filenames)==1:
                            url=url.replace(filename, filenames[0])
                            LOGGER.info("fzf single match !! url=%s"%(url))
                            break
                        else:
                            exact_filenames=[f for f in  filenames if f.endswith("/" + filename)]
                            if exact_filenames:
                                LOGGER.info("reduce to exact matches : filenames[%d]=%s"%(len(exact_filenames), str(exact_filenames)))
                                url=url.replace(filename, exact_filenames[0])
                            else:
                                url=url.replace(filename, filenames[0])

                            LOGGER.info("fzf multiple matches - take first match !! url=%s"%(url))
                            break

                    else:
                        LOGGER.info("SUBLIME_URL_HANDLER_FZF_ROOT='%s' is unset or directory does not exists"%(str(root)))

            filename=url.split(":")[0]
            LOGGER.info("url = %s"%url)
            LOGGER.info("filename = %s"%filename)

            if not os.path.exists(filename):
                LOGGER.info("filename (%s) does not exists - opening logfile instead..."%filename)
                url = SublimeURLHandler.sublime_wrapper_log_filename
            else:
                if filename.endswith(".pdf"):
                    print("SublimeURLHandler PDF")
                    os.system("pkill evince")
                    match = re.match(r'^.*\.pdf:(\w+)$', url)
                    page = " -i " + match.group(1) if match else ""
                    os.system("evince " + filename + page + " &")
                    url = None # -> return will be redo in finally
                    return None

            with open(SublimeURLHandler.sublime_wrapper_filename, 'w') as f:
                f.write(url)

            url = SublimeURLHandler.sublime_wrapper_filename

        except Exception as e:
            LOGGER.info(traceback.format_exc())

            url = SublimeURLHandler.sublime_wrapper_log_filename

        finally:
            LOGGER.info("Return url=%s"%url)

            os.system("subl " + filename + page + " &")


            #
            # return the filename to open
            #
            return None
            # return url

class PythonExceptionURLHandler(SublimeURLHandler):

    handler_name = 'python_exception_url_st3'
    match = 'File "([' + pathchars + ']+\.py)", line ([0-9]+)'

    def callback(self, url):
        LOGGER.info("PythonExceptionURLHandler (url=%s)"%url)

        m = re.search( PythonExceptionURLHandler.match , url)

        if m:
            # url = m.group(1) + ":" + m.group(2)
            print(m.groups())
            print(m.group(1))
            print(m.group(2))
            return self.smart_open(filename=m.group(1), line=m.group(2))

        else:
            LOGGER.info("PythonExceptionURLHandler - regex failed on " + url + " - fallback to AnyFileURLHandler")

        return SublimeURLHandler.callback(self, url)

class GolangURLHandler(SublimeURLHandler):

    handler_name = 'golang_exception_url_st3'
    match = '([^: \r\n\t\f\[\]]*\.go):([0-9]+)'

    # def callback(self, url):

    #     m = re.search( GolangURLHandler.match , url)

    #     if m:
    #         url = m.group(1) + ":" + m.group(2)
    #     else:
    #         LOGGER.info("GolangURLHandler - regex failed on " + url + " - fallback to AnyFileURLHandler")

    #     return SublimeURLHandler.callback(self, url)

class GitDiffURLHandler(SublimeURLHandler):
    handler_name = 'git_diff_url_st3'
    match = '([-]{3} a/|[+]{3} b/)([^: \r\n\t\f]+)\n@@ -[0-9]+,[0-9]+ [+]([0-9]+),[0-9]+ @@'
    def callback(self, url):
        LOGGER.info("GitDiffURLHandler (url=%s)"%url)

        match = re.match(GitDiffURLHandler.match, url)
        # LOGGER.info("groups: ", match.groups())
        # for i in range(4):
        #     LOGGER.info(i,match.group(i))

        url = f"{match.group(2)}:{match.group(3)}"
        LOGGER.info("url %s"%url)

        return SublimeURLHandler.callback(self, url)

         # vte_terminal = Terminal.get_vte()
         #    col, row = vte_terminal.get_cursor_position()
         #    content = vte_terminal.get_text_range(0, 0, row, col, lambda *a: True)
         #    fd.write(content.strip() + "\n")

class TpURLHandler(plugin.URLHandler):
    handler_name = 'tp_jira_url'
    match = '[tp|TP]-\d{4}'
    def callback(self, url):
        print("TP URL HANDLER")
        return "https://myateme.atlassian.net/browse/" + url.upper()

# class GccFileURLHandler(SublimeURLHandler):
#     handler_name = 'gcc_file_url_st3'
#     match = '[^: \r\n\t\f\[\]()]+(:[0-9]+){1,2}'

#     def callback(self, url):
        # LOGGER.info("GccFileURLHandler (url=%s)"%url)
#         return SublimeURLHandler.callback(self, url)

class PytestURLHandler(SublimeURLHandler):
    handler_name = 'pytest_file_url_st3'

    # units/TitanAudioEncoder/tests/test_audio_encoder.py::TestAudioEncoder::test_transcoding[dolbye-to-ec3] self.real_path(path)='/home/fdutrech/pytest-output/TitanAudioEncoder/dolbye-to-ec3.txt'

    # match = 'units/Titan.*/tests/.*\.py::Test.*::.*(\[.*\])?'
    # match = 'units/Titan.*/tests/.*\.py::Test.*::.*'
    match = 'units/Titan.*'

    # units/TitanMosaic/tests/test_mosaic.py::TestMosaic::test_statistics[mosaic-nominal-1-0] PASSED


    def callback(self, url):
        LOGGER.info(f"PytestFileURLHandler ({url=})")

        return SublimeURLHandler.callback(self, url)

class AnyFileURLHandler(SublimeURLHandler):
    handler_name = 'any_file_url_st3'
    match = '([^: \[\]\r\n\t\f()=\'"]+)((?::)([0-9]+))?((?::)([0-9]+))?'


    def callback(self, url):
        LOGGER.info("AnyFileURLHandler (url=%s)"%url)

        match = re.match(self.match, url)
        if match:
            return self.smart_open(filename=match.group(1), line=match.group(3), column=match.group(5))
        else:
            LOGGER.warning("NO MATCH !")

        return SublimeURLHandler.callback(self, url)


if __name__ == '__main__':

    plugins = [ globals()[plugin] for plugin in AVAILABLE ]

    tests = []
    
    #
    # GCC TESTS
    #
    tests += ['/work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:5:10: error: missing terminating " character [-Werror]']
    tests += ['/work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:164:15']
    tests += ['                   /work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:164:15']
    tests += ['                   /work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:164']
    tests += ['                   /usr/include/c++/4.8.2/ostream:612:0']
    tests += ['                   :612']

    #
    # ANY FILE TESTS
    #
    tests += ['data/config/gets/nauchets/config.json']
    tests += ['[data/config/gets/nauchets/config.json] bad key format : config[']
    tests += ['./blue-vpn.py --blabla']
    tests += ['./blue-vpn.py --blabla']
    tests += ['[MISSING]   "data/evaluations/alpehuez/marmottes1/marmottes1_20190302_1445.json" : { "include_tracks" : [9] }']
    tests += ['- blueski_args : --snapshot="\'/bluecime/publicRW/dataset/blueksi_snapshot.json\'"']
    tests += ['- smart_eval_tool_args : --eval-dir="/bluecime/publicRW/sivao/dataset/blueski_snaphot_update.json"']


    #
    # PYTHON TESTS
    #
    tests += ['        # File "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/log.py", line 390']
    tests += ['  File "/work-crypt/fdutrech/workspace/bluecime/blue-vpn/package/bluevpn/docker.py", line 427, in remove']
    tests += ['  File "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/rules/evaluation.py", line 20, in <module>']
    tests += ['  File "./vte_test.py", line 125, in on_buttonpress ']
  

    import gtk, vte, termcolor

    class Terminal(gtk.Window):

        def __init__(self, plugins, tests):
            gtk.Window.__init__(self)
            self.set_default_size(1600, 300)

            self.vte = vte.Terminal()
            self.vte.fork_command()

            self.button = gtk.Button("Fill tests")
            self.button.connect("clicked", self.InputToTerm)

            box = gtk.VBox()
            box.pack_start(self.button, False, True, 0)
            box.pack_start(self.vte, False, True, 2)
            self.add(box)

            self.vte.connect('button-press-event', self.on_buttonpress)

            self.plugins = {}
            self.matches = {}
            for plugin in plugins:
                self.matches[plugin.handler_name] = self.vte.match_add(plugin.match)
                self.plugins[plugin.handler_name] = plugin

            self.tests = tests


        def InputToTerm(self, clicker):

            self.run_cmd("clear")

            for test in self.tests:
                self.run_cmd("# %s"%test)


        def run_cmd(self, cmd):
            self.vte.feed_child(cmd + "\n", len(cmd)+1)

        def get_cwd(self):
            return os.getcwd()

        def on_buttonpress(self, widget, event):

            if event.button == 1:
        
                x = int(event.x / self.vte.get_char_width())
                y = int(event.y / self.vte.get_char_height())

                txt = "# "
                # txt = "# [line %d - col %d] "%(y,x)

                match = self.vte.match_check(x,y)

                if match:

                    txt += termcolor.colored('SUCCESS ', 'green')

                    for handler_name, match_id in self.matches.items():
                        if match[1] == match_id:
                            txt += termcolor.colored(handler_name, attrs=["bold"])

                            txt += " "
                            txt += termcolor.colored(match[0], 'cyan')
                            txt += "\n"
                            txt += termcolor.colored("URL : ", attrs=["bold"])
                            txt += str(self.plugins[handler_name]().callback(match[0]))

                else:
                    txt += termcolor.colored('FAILURE', 'red')

                LOGGER.info(txt)

    win = Terminal(plugins, tests)
    win.connect("delete-event", gtk.main_quit)
    win.show_all()
    gtk.main()
