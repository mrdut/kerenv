# PKI

```bash
sudo apt-get install pcscd libpcsclite1 pcsc-tools 
```

```bash
>> pcsc_scan
Using reader plug'n play mechanism
Scanning present readers...
0: Alcor Micro AU9560 00 00
 
Fri Oct  1 11:09:59 2021
 Reader 0: Alcor Micro AU9560 00 00
  Event number: 1
  Card state: Card inserted, 
  ATR: 3B D2 18 00 81 31 FE 58 C9 02 17

ATR: 3B D2 18 00 81 31 FE 58 C9 02 17
+ TS = 3B --> Direct Convention
+ T0 = D2, Y(1): 1101, K: 2 (historical bytes)
  TA(1) = 18 --> Fi=372, Di=12, 31 cycles/ETU
    129032 bits/s at 4 MHz, fMax for Fi = 5 MHz => 161290 bits/s
  TC(1) = 00 --> Extra guard time: 0
  TD(1) = 81 --> Y(i+1) = 1000, Protocol T = 1 
-----
  TD(2) = 31 --> Y(i+1) = 0011, Protocol T = 1 
-----
  TA(3) = FE --> IFSC: 254
  TB(3) = 58 --> Block Waiting Integer: 5 - Character Waiting Integer: 8
+ Historical bytes: C9 02
  Category indicator byte: C9 (proprietary format)
+ TCK = 17 (correct checksum)

Possibly identified card (using /usr/share/pcsc/smartcard_list.txt):
3B D2 18 00 81 31 FE 58 C9 02 17
    WVDP (West Valley Demonstration Project?) (PKI)
 \   
```



# Links

 - https://wiki.archlinux.org/title/Smartcards
 - https://doc.ubuntu-fr.org/smartcards