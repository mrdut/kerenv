# Jenkins

### connect koios while running on jenkins
```
(laptop) >> sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/ssh
(laptop) >> ssh -L 443:0.0.0.0:443 naboo25
(chrome) >> https://0.0.0.0
```

### Troubleshooting

```bash
# --------------------------------------------------------------------
# ~/.ssh/config
Host=root_yoda
    User=root
    ProxyCommand ssh -q nwadmin nc yoda 22
# --------------------------------------------------------------------
>> ssh root_yoda "docker exec jenkins-mivision rm -rv PATH" 
```


```bash
>> docker run -t -d -u 1100:1100 -u root:root --network=host \
-e http_proxy=193.56.47.20:8080 -e HTTP_PROXY=193.56.47.20:8080 \
-e https_proxy=193.56.47.20:8080  -e HTTPS_PROXY=193.56.47.20:8080 \
-e no_proxy=172.16.118.137,127.0.0.1 -e NO_PROXY=172.16.118.137,127.0.0.1 \
-v /var/jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /home/dutrechf/Workspace/:/git \
172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master

>> docker ps
CONTAINER ID        IMAGE                                                                                    COMMAND             CREATED             STATUS              PORTS               NAMES
db780cc5147a        172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master   "/bin/bash"         5 seconds ago       Up 4 seconds                            gracious_lehmann

>> docker exec -it gracious_lehmann /bin/bash
(root) ...
```

https://stackoverflow.com/questions/49674004/docker-repository-server-gave-http-response-to-https-client




```bash
# jenkins + DEBUG
(home) ssh naboo0
(naboo0) ssh root@yoda
(yoda) docker exec -it jenkins-mivision /bin/bash
(jenkins-mivision) docker ps
(jenkins-mivision) docker exec -it IMAGE_ID /bin/bash
>> stty columns 160 rows 30
```


### Jenkins agent

```bash
(naboo25) docker pull 172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-jenkins-slave:master
(naboo25) docker rm -f koios-jenkins-agent_koios-slave_1
(naboo25) cd /koios/git/koios-jenkins-agent
# (naboo25) less readme.md
(naboo25) make up koios-slave
(naboo25) ctrl+z

```
    - https://bitbucket.sdmc.ao-srv.com/projects/BREBD/repos/koios-ansible-playbooks/browse/roles/koios-jenkins-agent



### Replay groovy

1. Click replay

Replace 
```
library 'koios' false
```

By
```
library identifier: 'koios@master', retriever: modernSCM(
  [$class: 'GitSCMSource',
   remote: 'https://bitbucketbdsfr.fsc.atos-services.net/scm/brebd/koios-jenkins-shared-libraries.git',
   credentialsId: 'jenkins-bdsrd-id'])
```

2. Re-click replay

You have access to all groovy files

### List queue

```bash
#! bash
JENKINS_URL_CREDENTIAL="http://mivision-user:mivision-user@172.16.118.137:94"
JENKINS_URL="http://mivision-user:mivision-user@172.16.118.137:94"

# LIST QUEUE
curl --silent ${JENKINS_URL_CREDENTIAL}/queue/api/json | jq

# EXECUTE GROOVY
COOKIE_PATH="/tmp/cookie_jenkins_crumb.txt"
CRUMB=`curl  -c ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/crumbIssuer/api/xml?xpath=concat\(//crumbRequestField,%22:%22,//crumb\) --silent`
GROOVY_GET_QUEUE_LENGTH="script=println Hudson.instance.queue.items.length"

curl -b ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/scriptText -H "${CRUMB}" -d "${GROOVY_GET_QUEUE_LENGTH}"
```

### Purge queue

```bash
#! bash

JENKINS_URL="http://mivision-user:mivision-user@172.16.118.137:94"

# EXECUTE GROOVY
COOKIE_PATH="/tmp/cookie_jenkins_crumb.txt"
CRUMB=`curl  -c ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/crumbIssuer/api/xml?xpath=concat\(//crumbRequestField,%22:%22,//crumb\) --silent`
GROOVY_GET_QUEUE_LENGTH="script=println Hudson.instance.queue.items.length"
GROOVY_CANCEL_QUEUE="script=Jenkins.instance.queue.clear()"

curl -b ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/scriptText -H "${CRUMB}" -d "${GROOVY_GET_QUEUE_LENGTH}"
curl -b ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/scriptText -H "${CRUMB}" -d "${GROOVY_CANCEL_QUEUE}"
curl -b ${COOKIE_PATH} --user mivision-user:mivision-user ${JENKINS_URL}/scriptText -H "${CRUMB}" -d "${GROOVY_GET_QUEUE_LENGTH}"
```

## Ressources

https://www.jenkins.io/doc/book/pipeline/shared-libraries/