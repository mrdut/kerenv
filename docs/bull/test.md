# 

### Troubleshooting

```bash
# --------------------------------------------------------------------
# ~/.ssh/config
Host=root_yoda
    User=root
    ProxyCommand ssh -q nwadmin nc yoda 22
# --------------------------------------------------------------------
>> ssh root_yoda "docker exec jenkins-mivision rm -rv PATH" 
```


```bash
>> docker run -t -d -u 1100:1100 -u root:root --network=host \
-e http_proxy=193.56.47.20:8080 -e HTTP_PROXY=193.56.47.20:8080 \
-e https_proxy=193.56.47.20:8080  -e HTTPS_PROXY=193.56.47.20:8080 \
-e no_proxy=172.16.118.137,127.0.0.1 -e NO_PROXY=172.16.118.137,127.0.0.1 \
-v /var/jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /home/dutrechf/Workspace/:/git \
172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master

>> docker ps
CONTAINER ID        IMAGE                                                                                    COMMAND             CREATED             STATUS              PORTS               NAMES
db780cc5147a        172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master   "/bin/bash"         5 seconds ago       Up 4 seconds                            gracious_lehmann

>> docker exec -it gracious_lehmann /bin/bash
(root) ...
```

https://stackoverflow.com/questions/49674004/docker-repository-server-gave-http-response-to-https-client




```bash
# jenkins + DEBUG
(home) ssh naboo0
(naboo0) ssh root@yoda
(yoda) docker exec -it jenkins-mivision /bin/bash
(jenkins-mivision) docker ps
(jenkins-mivision) docker exec -it IMAGE_ID /bin/bash
>> stty columns 160 rows 30
```