# Developpment


## Koios


### WKM

```
>> docker exec -it -e PGPASSWORD=postgres dutrechf_working-memory_1  psql -h wkm -U postgres -d koioswm

koioswm=# select count (*) from attribute;
 count 
-------
     0
(1 row)

koioswm=# select count (*) from apparition;

# thumbnail ?
koioswm=# select a.sum as without, b.sum as with, a.sum::float *100/(a.sum::float +b.sum::float) from 
(select count(*) sum from apparition where id not in (select apparitionid from thumbnail)) as a , 
(select count(*) sum from apparition where id in (select apparitionid from thumbnail)) as b;
```


## Python


```
# ~/.pypirc
[distutils]
index-servers=local

[local]
repository: http://172.16.118.137:8081/artifactory/api/pypi/mivision-pypi-local
username: mivision-user
password: mivision-user
```

```
# ~/.pip/pip.conf
[global]
index-url = http://mivision-user:mivision-user@172.16.118.137:8081/artifactory/api/pypi/mivision-pypi-repo/simple
trusted-host = 172.16.118.137
```

```
error: invalid command 'bdist_wheel'

pip3 install wheel
python3 setup.py bdist_wheel
```


```bash
run in component builder 
>> docker pull 172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master
>> docker run -t -d \
            -u 1100:1100 \
            -u root:root --network=host \
            -e http_proxy=http://193.56.47.20:8080 -e HTTP_PROXY=http://193.56.47.20:8080 \
            -e https_proxy=http://193.56.47.20:8080  -e HTTPS_PROXY=http://193.56.47.20:8080 \
             -e no_proxy=172.16.118.137,127.0.0.1 -e NO_PROXY=172.16.118.137,127.0.0.1 \
             -v /var/jenkins_home:/var/jenkins_home \
             -v /var/run/docker.sock:/var/run/docker.sock \
             -v /home/dutrechf/Workspace/:/git \
             -w /git \
             --name component_builder \
             172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master
#>> docker ps
#CONTAINER ID        IMAGE                                                                                    COMMAND             CREATED             STATUS              PORTS               NAMES
#9d13be049d3a        172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/cpu:master   "/bin/bash"         8 seconds ago       Up 7 seconds                            frosty_haslett
#>> docker exec -it CONTAINER_ID bash
>> docker exec -it component_builder bash
```


### microservice dev cookbook

```bash
(naboo36) >> docker images | grep sensor_monitor
(naboo36) >> docker run -it -w /usr/local/lib/python3.6/dist-packages/tracker bash
...
do some edition
...
(naboo36) >> docker commit CONTAINER_ID 172.16.118.137:8081/mivision-docker-repo/xbd-vision/sensor-monitor/turing:BREBD-4340_api

(naboo36) >> KOIOS_HARDWARE=turing BRANCH=BREBD-4340_api UPDATE_KOIOS=false make test-e2e_merge_fast
or
(naboo36) >> docker push 172.16.118.137:8081/mivision-docker-repo/xbd-vision/sensor-monitor/turing:BREBD-4340_api



# scripts/docker/microservice.dockerfile
RUN mkdir -p ~/.pip && printf "[global]\nindex-url = http://mivision-user:mivision-user@172.16.118.137:8081/artifactory/api/pypi/mivision-pypi-repo/simple\ntrusted-host = 172.16.118.137\n" > ~/.pip/pip.conf

make build && make build-docker-images

docker run -it 172.16.118.137:8081/mivision-docker-repo/xbd-vision/tracklet-attributes-generator/generic:BREBD-4340_api cat /usr/local/lib/python3.6/dist-packages/koioscommon/interface/ism/ism_client.py

```



### test custom branch


1. Change `requirements.txt`

```
# from
common=1.3.0

# to
-e git+https://jenkins-bdsrd:OTkxNjQzMzg3NzI2OiWwO7whXIIIZr2UJkHSNyk3%2FQn9@bitbucket.sdmc.ao-srv.com/scm/brebd/koios-common.git@BRANCHNAME#egg=koios-common
```

2. Change common .gitmodule

```
# from
[submodule "koios-shared-build-scripts"]
    path = koios-shared-build-scripts
    url = https://bitbucket.sdmc.ao-srv.com/scm/brebd/koios-shared-build-scripts.git

# to
[submodule "koios-shared-build-scripts"]
    path = koios-shared-build-scripts
    url = https://jenkins-bdsrd:OTkxNjQzMzg3NzI2OiWwO7whXIIIZr2UJkHSNyk3%2FQn9@bitbucket.sdmc.ao-srv.com/scm/brebd/koios-shared-build-scripts.git

```

### debug stuck process

```bash
https://stackoverflow.com/questions/132058/showing-the-stack-trace-from-a-running-python-application
 >> which pytest

#!/home/mrdut/workspace/bull/dev/koios-input-stream-manager/.venv/bin/python3

# -*- coding: utf-8 -*-
import re
import sys

from pytest import main

import code, traceback, signal

def debug(sig, frame):
    """Interrupt running process, and provide a python prompt for
    interactive debugging."""
    d={'_frame':frame}         # Allow access to frame object.
    d.update(frame.f_globals)  # Unless shadowed by global
    d.update(frame.f_locals)

    i = code.InteractiveConsole(d)
    message  = "Signal received : entering python shell.\nTraceback:\n"
    message += ''.join(traceback.format_stack(frame))
    i.interact(message)

def listen():
    signal.signal(signal.SIGUSR1, debug)  # Register handler


if __name__ == '__main__':
    listen()
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
```

## Go

