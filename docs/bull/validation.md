# Validation


## Snippets

```
function validation_ssh() {
    NABOO=${1:-naboo25}
    WORKDIR=/koios/data/koios-validation
    echo "open SSH connection to $NABOO -> $WORKDIR"
    sshpass -p 'koios' ssh -t koios@${NABOO} "cd $WORKDIR && exec \$SHELL"
} 
alias vssh="validation_ssh"
```

```
docker run -it --rm \
        -v /koios/data/koios-validation/artifacts:/koios/artifacts \
        -v /koios/data/koios-validation:/koios/validation \
        -w /koios/validation \
        -e VALIDATION_ARTIFACTS_DIR=/koios/artifacts \
         naboo25/validation /bin/bash
```



```
No space left on device

>> df -h
>> docker volumes ls
>> docker volume ls --format="{{.Name}}" | grep -v koios | xargs docker volume rm

```