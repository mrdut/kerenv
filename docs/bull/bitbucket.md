# Bitbucket

### API

 - https://docs.atlassian.com/bitbucket-server/rest/6.6.3/bitbucket-rest.html

### List repo

```bash
ROOT=/home/dutrechf/Workspace
LIMIT=100
START=0
PROJECT_KEY=BREBD
while true; do
    REQ="https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos?start=${START}&limit=${LIMIT}"
    # echo $REQ
    REP=$(curl --silent $REQ)
    START=$(echo $REP | jq -r '.nextPageStart')
    for SLUG in $(echo "$REP" | jq -r '.values[].slug'); do
        if [[ "$SLUG" =~ ^koios-.* ]]; then 
            [ -d ${ROOT}/${SLUG} ] && COLOR_SLUG="\033[32m" || COLOR_SLUG="\033[31m"
            echo -e "$COLOR_SLUG$SLUG\033[0m"
        # else
        #     echo -e "\033[31m${SLUG}\033[0m"
        fi
    done
    [ "$(echo $REP | jq -r '.isLastPage')" == "true" ] && break
done
```

### List users

```bash
# https://confluence.atlassian.com/bitbucketserverkb/how-to-report-on-permissions-by-using-rest-api-endpoints-858696596.html
curl --silent https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/BREBD/permissions/users?limit=100 | jq | grep name -i
# ALSAYASNEH, Maha                       a806173
# ARNAISE, NICOLAS                       a677483
# BOUKAMEL-DONNOU, CECILE                s227695
# Metge, Baptiste                        a760714
# PELLETIER, Benoit                      a454985
# TATO, GENC                             a779360
```

### Create PR

```bash

GIT_BRANCH=BREBD-4340_api

curl \
    https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/BREBD/repos/${REPO}/pull-requests \
    --request POST \
    --header 'Content-Type: application/json' \
    --data "{
        \"title\": \"Clean changelog\",
        \"description\": \"Clean changelog\",
        \"state\": \"OPEN\",
        \"open\": true,
        \"closed\": false,
        \"fromRef\": {
            \"id\": \"refs/heads/$GIT_BRANCH\"
        },
        \"toRef\": {
            \"id\": \"refs/heads/master\"
        },
        \"reviewers\": [
            { \"user\": { \"name\" : \"a806173\" } },
            { \"user\": { \"name\" : \"a677483\" } },
            { \"user\": { \"name\" : \"s227695\" } },
            { \"user\": { \"name\" : \"a760714\" } },
            { \"user\": { \"name\" : \"a779360\" } }
        ],
        \"close_source_branch\": true
      }"


```

## Check PR

```bash
PROJECT_KEY=BREBD
REPOSITORY_SLUG=koios-input-stream-manager
REQ=https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests
# echo $REQ
REP=$(curl --silent $REQ)
# REP=$(cat /tmp/pr.json)

# echo $REP | tr '\r\n' ' ' | jq .isLastPage

IFS=$'\n'
for PR in $(echo "$REP" | jq -c '.values[]'); do
    # echo $row | tr '\r\n' '$' | jq

    pullRequestId=$(echo "$PR" | jq -r '.id')
    fromSlug=$(echo "$PR" | jq -r '.fromRef.repository.slug')
    fromRef=$(echo "$PR" | jq -r '.fromRef.displayId')
    toRef=$(echo "$PR" | jq -r '.toRef.displayId')
    outcome=$(echo "$PR" | jq -r '.properties.mergeResult.outcome')
    [ "$outcome" == "CLEAN" ] && OUTCOME_STATUS="\033[34m" || OUTCOME_STATUS="\033[31m"

    echo -e "# Pull-Request : $fromSlug"
    echo -e "## $fromRef -> $toRef"
    echo -e "## Link : $(echo "$PR" | jq -r '.links.self[0].href')"
    echo -e "## Author : $(echo "$PR" | jq -r '.author.user.displayName') ($(echo "$PR" | jq -r '.author.user.name'))"
    echo -e "## Outcome : ${OUTCOME_STATUS}${outcome}\033[0m"
    echo -e "## Reviewer(s) : "

    APPROVED=false
    for reviewer in $(echo "$PR" | jq -c '.reviewers[]'); do
        name=$(echo $reviewer | jq -r '.user.name' )
        displayName=$(echo $reviewer | jq -r '.user.displayName' )
        status=$(echo $reviewer | jq -r '.status' )
        [ "$status" == "APPROVED" ] && { APPROVED=true; COLOR_STATUS="\033[32m"; }  || COLOR_STATUS="\033[33m"
        echo -e " - $displayName ($name) : ${COLOR_STATUS}$status\033[0m"
    done
    echo "APPROVED $APPROVED"
    # break


    # if MERGE && author.name == $ATLASSIAN_ID && Outcome==CLEAN && 1 

    echo " "

    # -----------
    # GET /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/merge
    # -----------
    # {
    #     "canMerge": false,
    #     "conflicted": true,
    #     "outcome": "CONFLICTED",
    #     "vetoes": [
    #         {
    #             "summaryMessage": "You may not merge after 6pm on a Friday.",
    #             "detailedMessage": "It is likely that your Blood Alcohol Content (BAC) exceeds the threshold for making sensible decisions regarding pull requests. Please try again on Monday."
    #         }
    #     ]
    # }
    # -----------
    REQ=https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests/${pullRequestId}/merge
    echo $REQ
    curl --silent $REQ | jq



    # https://docs.atlassian.com/bitbucket-server/rest/6.6.3/bitbucket-rest.html#idp277
    # -----------
    # POST /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/merge
    # -----------
    # curl -H "Content-Type:application/json" -H "Accept:application/json" --user admin:admin -X POST http://<Your_Stash_URL>//rest/api/1.0/projects/<PROJECT_ID>/repos/<REPO_ID>/pull-requests/<PR_ID>/merge?version=0
    REQ="https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests/${pullRequestId}/merge?version=0"
    echo $REQ
    REP=$(curl --silent --request POST $REQ -H "X-Atlassian-Token: no-check" )
    echo $REP | jq

done

```