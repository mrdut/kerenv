# Network

## Overview

![atos network](img/atos_network.png)

 - nwadmin : 129.183.22.27
 - bitbucket : 161.89.117.136
 - yoda : 172.16.118.137
 - edge : naboo0:22121
 - jenkins : 172.16.118.137:94

```
sudo iptables -t nat -A OUTPUT -p tcp --dport 94 -d 172.16.118.137 -j DNAT --to-destination 127.0.0.1:1234
ssh -fNT -L 1234:172.16.118.137:94 dutrechf@nwadmin.frec.bull.fr

# Login/pwd : mivision-user / mivision-user
```

  - artifactory : 172.16.118.137:8081

```
sudo iptables -t nat -A OUTPUT -p tcp --dport 8081 -d 172.16.118.137 -j DNAT --to-destination 127.0.0.1:1235
ssh -fNT -L 1235:172.16.118.137:8081 dutrechf@nwadmin.frec.bull.fr

      ssh -L 18082:localhost:18082 dutrechf@nwadmin.frec.bull.fr
      ssh -L 18082:localhost:8081 root@yoda
      # oneliner
      ssh -L 18082:localhost:18082 dutrechf@nwadmin.frec.bull.fr -t ssh -L 18082:localhost:8081 root@yoda

# mpd -> root
# ensuite sur ton browser tu vas sur http://localhost:18082/artifactory/webapp/

```

```
alias vpn-bull-start="usr-vpnc bull && (notify-send 'VPN bull' 'VPN started' --icon=network-transmit-receive; ) || notify-send 'VPN bull' 'VPN already started' --icon=dialog-warning"
alias vpn-bull-stop="usr-vpnc-disconnect && notify-send 'VPN bull' 'VPN stopped' --icon=network-offline"


YODA_URL=172.16.118.137

function yoda_connect() {
    if ps -ax | grep ssh | grep $YODA_URL | grep -q $2; then
        echo "Connected to $1"
    else
        echo "Not connected to $1"
        ssh -fNT -L $3:$YODA_URL:$2 dutrechf@nwadmin.frec.bull.fr
        if sudo iptables -t nat -C OUTPUT -p tcp --dport $2 -d $YODA_URL -j DNAT --to-destination 127.0.0.1:$3; then
            echo "iptables already set"
        else
            sudo iptables -t nat -A OUTPUT -p tcp --dport $2 -d $YODA_URL -j DNAT --to-destination 127.0.0.1:$3
        fi
    fi
}

alias jenkins_connect='
    yoda_connect Jenkins 94 1234
'

alias artifactory_connect='
    yoda_connect Artifactory 8081 1235
'
```
## VPN

if official vpn  is not working, backup to:
1 - enable port 22 wifi router
2 - get public IP on router
3 - run ssh server laptop
4 - make service : 
 31750 ?        Ss     0:00 /usr/bin/ssh -i /home/dutrechf/.ssh/id_rsa -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -NTR 22022:localhost:22 dutrechf@77.153.225.244

```
ssh -D 12345 edge
```

## iptables

```bash
IPTABLE_RULE="OUTPUT -p tcp --dport 94 -d 172.16.118.137 -j DNAT --to-destination 127.0.0.1:1234"

# check exists
sudo iptables -t nat -C $IPTABLE_RULE

# append
sudo iptables -t nat -A $IPTABLE_RULE

# delete
sudo iptables -t nat -D $IPTABLE_RULE

# list rules
sudo iptables --table nat --list
sudo iptables -t nat -L

# persistent iptable rules
sudo apt-get install iptables-persistent
sudo iptables-save > /etc/iptables/rules.v4
```



## Config

### Ssh


```bash
# ~/.ssh/config
Host=naboo
 Hostname=172.16.118.134
 User=dutrechf
 Compression=yes
 ForwardX11=yes

Host=edge
 User=dutrechf
 Compression=yes
 ForwardX11=yes
 ProxyCommand ssh -q nwadmin nc naboo0 22121

Host=root_yoda
 User=root
 Compression=yes
 ProxyCommand ssh -q nwadmin nc yoda 22

Host=nwadmin nwadmin.frec.bull.fr
 Hostname=129.183.22.27
 User=dutrechf
 Compression=yes
 ForwardX11=yes

Host naboo0
  User dutrechf
  ProxyCommand ssh -q nwadmin nc naboo0 22

Host=naboo?? naboo? !naboo0
 User=dutrechf
 ForwardX11 yes
 ProxyCommand ssh -q naboo0 nc %h 22

Host=bitbucket.sdmc.ao-srv.com
 Hostname=161.89.117.136
 Port=7999

Host *
 ServerAliveInterval 60
```

```bash
# Ssh tunnel:
ssh -L port-local:HOSTNAME:port-distant nomutilisateur@nomhôte
```
    - `-L` : Local port forwarding
    - `-R` : Remote port forwarding
    - `-N` : Do not execute a remote command.  This is useful for just forwarding ports.
    - `-f` : Requests ssh to go to background just before command execution.  This is useful if ssh is going to ask for passwords or passphrases, but the user wants it in the background.  This implies -n.
             The recommended way to start X11 programs at a remote site is with something like ssh -f host xterm.
             If the ExitOnForwardFailure configuration option is set to “yes”, then a client started with -f will wait for all remote port forwards to be successfully established before placing itself in
             the background.
    - `-l` : Specifies the user to log in as on the remote machine.
    - `-g` : Allows remote hosts to connect to local forwarded ports.
    - `-T` : Disable pseudo-tty allocation.

 - http://www.linux-france.org/prj/edu/archinet/systeme/ch13s04.html
 - https://gist.github.com/scy/6781836


```bash
# check port that are in used
>> netstat -lntp
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:8000          0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:22022         0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      -                   
tcp6       0      0 ::1:22022               :::*                    LISTEN      -                   
tcp6       0      0 :::1716                 :::*                    LISTEN      1420/kdeconnectd    
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 ::1:631                 :::*                    LISTEN      -               
```

### Go

```bash
# pour compiler en local il faut indiquer à go l'artifactory donc rajouter dans son ~/.profile par exemple
export GOPROXY="http://172.16.118.137:8081/artifactory/mivision-go-repo,direct"
export GOPRIVATE="bitbucket.sdmc.ao-srv.com"
export GONOSUMDB="*"
```

## Proxy

### Linux

```bash
export http_proxy=http://193.56.47.20:8080
export https_proxy=http://193.56.47.20:8080
export no_proxy=172.16.118.137
export HTTP_PROXY=http://193.56.47.20:8080
export HTTPS_PROXY=http://193.56.47.20:8080
export NO_PROXY=172.16.118.137
```

### Apt

```bash
>> cat /etc/apt/apt.conf.d/proxy.conf
Acquire {
  HTTP::proxy "http://193.56.47.20:8080";
  HTTPS::proxy "http://193.56.47.20:8080";
}
```

### Windows

193.56.47.8:8080 (should be 193.56.47.20 ?)
for the following adresses : localhost;0.0.0.0;127.0.0.1;172.16.118.137;172.16.118.134

### Docker

```bash
>> cat /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://193.56.47.20:8080/" "HTTPS_PROXY=http://193.56.47.20:8080/" "NO_PROXY=172.16.118.137"

>> sudo systemctl daemon-reload
>> sudo systemctl stop docker
>> sudo systemctl start docker
```

### RDP

https://github.com/GoogleCloudPlatform/iap-desktop/issues/216#issuecomment-662870469


```bash
>> gcloud compute start-iap-tunnel cvlab-windows-fdutrech-ssd 3389 --project=cvlab-20200319 --zone=europe-west4-c --local-host-port=localhost:13389
Testing if tunnel connection works.
Listening on port [13389].   # <--- we are wating for this log
>> remmina # localhost:13389

```


## Links

https://www.quennec.fr/trucs-astuces/syst%C3%A8mes/gnulinux/utilisation/git-authentification-https-automatique
https://erik.torgesta.com/2013/12/creating-a-persistent-ssh-tunnel-in-ubuntu/
https://superuser.com/questions/107679/forward-ssh-traffic-through-a-middle-machine



