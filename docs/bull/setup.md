# Setup

## User

```bash
# add docker group

```

## Shell

```
>> echo $SHELL
>> chsh -l
>> su -c "yum install zsh"
>> chsh -s /bin/zsh
>> echo $SHELL
```


1. `mkdir ~/workspace`
1. `clone kerenv`

### git setup

1. `~/.gitconfig`
    ```
    [url "git@bitbucket.sdmc.ao-srv.com:"]
        insteadOf = https://bitbucket.sdmc.ao-srv.com/scm/
    ```
2. 




RUN mkdir -p ~/.pip && printf "[global]\nindex-url = http://mivision-user:mivision-user@172.16.118.137:8081/artifactory/api/pypi/mivision-pypi-repo/simple\ntrusted-host = 172.16.118.137\n" > ~/.pip/pip.conf



```
export ARTIFACTORY_URL=http://172.16.118.137:8081
export ARTIFACTORY_USER=mivision-user
export ARTIFACTORY_PWD=mivision-user

export GCP_REGISTRY_URL=https://europe-west6-docker.pkg.dev
export GCP_PROJECT_ID=cvlab-20200319

# some useful aliases
alias gco="git checkout"; alias gci="git commit"; alias gst="git status"; alias gadd="git add"; alias gdiff="git diff"; alias gb="git branch"; alias gba="git branch --all";

# prevent multiple credential
git config --global credential.helper "cache --timeout=36000"

# /etc/fstab
cvlab_datasets /usr/local/share/koios_videos gcsfuse ro,implicit_dirs,allow_other,key_file=/root/cvlab-video-intelligence-dev-a90e48b37b7e.json,x-systemd.requires=network-online.target,user
```

  echo "curl --user $ARTIFACTORY_USER:$ARTIFACTORY_PWD $ARTIFACTORY_URL:mivision-generic-local/koios/data/map/old_nice.osm.pbf"
