# Awk

Pattern scanning and processing language

## Format

    - `-F <SEP>` : field separator (default is ` `)

    - `-FS <SEP>` : field separator (default is ` `)


    - `$0` : current line
    - `$<1...N>` : field #N
    - `""` : blank line
    - `"blabla"` : any string

    - `BEGIN` { ... } : before all lines are parsed
    - `END` { ... } : after all lines are parsed
    - `x=0` or `x="0"` : variables (all variables are stored as strings and can bu updated so)
    - `%`, `*=`, `^`, `\=`... : variables operators (almost any c++ operator)

## Command line

```
>> cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
...

>> awk ‑F":" '{ print "username: " $1 "\t\tuid:" $3 }' /etc/passwd
username: root      uid:0
username: bin       uid:1
username: daemon        uid:2
username: adm       uid:3
...
```

## Scripts

```
>> awk ‑f <SCRIPTNAME> myfile.in
>> awk ‑f '<SCRIPT CONTENT>' myfile.in

>> cat <SCRIPTNAME>
# I'm a comment
BEGIN { x=0 } 
/^$/  { x=x+1 } 
END   { print "I found " x " blank lines. :)" }

```

###  Regex

`/<regex>/ { print }`

### Filtering

```
$1 <CMP> "fred" { print $3 }
( $1 == "foo" ) && ( $2 == "bar" ) { print }
```

 - CMP : `==`, `<`, `>`, `<=`, `>=`, `!=`, `~` (matches) and `!~` (not matches)

### if/else

```
{
    if ($1 == "hey")
    {
        print $3
    }
    else if($2 == "ho")
    {
        print $4
    }
    else
    {
        print $4
    }
}
```

## Links

 - [ibm tutorial](https://developer.ibm.com/technologies/linux/tutorials/l-awk1/)