# Bash

### Display

To reset `tty`, just use `>> reset`

On python, if print is broken on subprocess : `print(text.replace("\n", "\r\n")`



### Set

`set -e` : break on first error

`set -x` or `set -o xtrace` expands variables and prints a little + sign before the line.

`set -v` or `set -o verbose` does not expand the variables before printing.

### Redirection

- `1` : _stdout_
- `2` : _stderr_
- `&` : _both_
  - `&> FILENAME`
  - `&>> FILENAME`
  - !! `> FILENAME 2>&1` does not seems to works every times ?? 

- `2>/dev/null` redirect `stderr` to null
- `2>&1` redirect `sterr` to `stdout`
- `&> filename` redirect both to file

- `$(< FILE)` expand to file content


* To replaced the first occurrence of a string (regular expression actually), use ${string/regexp/replacement}:

* redirect to same file : sponge (pat install moreutils)

* remove trailing spaces

https://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
```
echo "   lol  " | xargs
```

## Color

```
VERBOSE=1 unbuffer make 2>&1 | grep -v make
```

## Command line

Use content from textfile : `cmd @textfile.args`

````bash
```
# textfile.txt
--arg1=hey
--arg2=ho
```
````

## colors

https://misc.flogisoft.com/bash/tip_colors_and_formatting


## String manipulation

* remove suffix/preffix

```
foo=${string#$prefix}
foo=${foo%$suffix}
```

* replace in variable

```
# replace one
${parameter/FROM/TO}

# replace all
${parameter//FROM/TO}
```

### Return code

 - `$?` : last command return code (`if [ $? -ne 0 ]; then ...; fi`)
 - `$$` : script PID
 - `$!` : last command PID
 - `$0` : scriptname
 - `$#` : parameters count
 - `$*` : all parameters as one argument
 - `$@` : all parameters
 - `$<N>` : nth argument
 - `${@:N}` : all arguments starting at N

```
# https://tldp.org/LDP/abs/html/internalvariables.html#PIPESTATUSREF
In case of a pipe, in bash:
  bogus_command | tee file
$? -> 0
echo ${PIPESTATUS[@]} -> 127 0
```

### interact with files/process/...

 - `expect` : interact with command
 - wait 30 s that a line appears in logfile : `(timeout 30 tail -f $LOG_FILE & ) | grep -q "Tunnel is up and running"`


## test command exists

```
command -v foo >/dev/null 2>&1 || { echo >&2 "I require foo but it's not installed.  Aborting."; exit 1; }

>> command -v heyho
>> echo $?
1

>> command -v ls
alias ls='ls --color=tty'
>> echo $?
0
```


### fast delete directory

```
mkdir empty
rsync -r --delete empty/ dir_to_remove/
rmdir empty
rmdir dir_to_remove
```
** don't forget the '/' after directories **

### copy to clipboard

`echo test | xclip -selection c`

### replace in file

rm line matching pattern "python" within each xml file

```sed --in-place -n '/python/!p' *.xml```

of

`replace <FROM> <TO> -- <FILENAMES>`

### [sort files by size recursively](https://unix.stackexchange.com/questions/88065/sorting-files-according-to-size-recursively)
`du -ah <dir> | grep -v "/$" | sort -rh`

```
>> yum install ncdu
>> ncdu
    8.5 GiB [##########] /deep-blue-lfs
    5.4 GiB [######    ] /tilt
    4.1 GiB [####      ] /deep-blue
    3.9 GiB [####      ] /devops
    1.7 GiB [#         ] /extra-eyes
    1.3 GiB [#         ] /cabins
    1.2 GiB [#         ] /tools_remote
  895.9 MiB [#         ] /smart-supervisor
  818.3 MiB [          ] /sivao
  693.4 MiB [          ] /general
  468.7 MiB [          ] /hardware
  340.7 MiB [          ] /rifyle++

```

### [parameter substitution](https://www.gnu.org/software/bash/manual/bash.html#Shell-Parameter-Expansion) :

 - `${parameter:-word}` :  If parameter is unset or null, the expansion of word is substituted. Otherwise, the value of parameter is substituted.

 - `${parameter:=word}` : If parameter is unset or null, the expansion of word is assigned to parameter. The value of parameter is then substituted. Positional parameters and special parameters may not be assigned to in this way.

 - `${parameter:?word}` : If parameter is null or unset, the expansion of word (or a message to that effect if word is not present) is written to the standard error and the shell, if it is not interactive, exits. Otherwise, the value of parameter is substituted.

 - `${parameter:+word}` : If parameter is null or unset, nothing is substituted, otherwise the expansion of word is substituted.

...

### LUT (associative array)

```bash
declare -A MAP
MAP["koios-back-end-api"]="bea"
MAP["koios-common"]="com"
MAP["koios-common-go"]="com-go"
...

# snippet 1
if [ ${MAP[$key]+_} ]; then # test key in MAP 

# snippet 2
for i in "${!MAP[@]}"
do
   echo "key  : $i"
   echo "value: ${MAP[$i]}"
done
```

### Change string case

https://stackoverflow.com/questions/2264428/how-to-convert-a-string-to-lower-case-in-bash
