ceph osd df tree
ceph osd lspools
rados bench -p objectstore.rgw.buckets.data 10 write -t 96
kubectl rollout restart deployment -n rook-ceph

ceph config set osd osd_op_num_threads_per_shard 32
ceph config dump


ceph config set osd 8
ceph config set osd osd_max_op_threads 128
ceph config set osd osd_op_queue 4096


https://10.96.0.102/grafana/d/iN68NRINk/node-exporter-use-method-cluster?orgId=1&refresh=30s