# Terminal

### Use arrow for history search

The readline commands that you are looking for are the `history-search-*` commands:

> **history-search-forward**
> Search forward through the history for the string of characters between the start of the current line and the current cursor position (the point). This is a non-incremental search.
> **history-search-backward**
> Search backward through the history for the string of characters between the start of the current line and the point. This is a non-incremental search.

Binding these in your `.inputrc`, like so:

```bash
"\e[A": history-search-backward            # arrow up
"\e[B": history-search-forward             # arrow down
```

https://unix.stackexchange.com/questions/53814/configure-up-arrow-to-browse-through-commands-with-same-initial-characters-rathe

```bash
# ~/.bashrc

if [[ $- == *i* ]] # (the if statement checks for interactive mode)
then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
fi
```



https://stackoverflow.com/questions/1030182/how-do-i-change-bash-history-completion-to-complete-whats-already-on-the-line

### clear terminal

```bash
>> clear
>> printf '\e[3J'
```


### Line wrapping

 - disable line wrap : 'tput rmam' or 'setterm -linewrap off'
 - enable line wrap : 'tput smam' or 'setterm -linewrap on'
 - http://askubuntu.com/questions/73443/how-to-stop-the-terminal-from-wrapping-lines


### Terminal size

```bash
# GET
>> stty -a
speed 38400 baud; rows 33; columns 237; line = 0;
intr = ^C; quit = ^\; erase = ^?; kill = ^U; eof = ^D; eol = <undef>; eol2 = <undef>; swtch = <undef>; start = ^Q; stop = ^S; susp = ^Z; rprnt = ^R; werase = ^W; lnext = ^V; discard = ^O; min = 1; time = 0;
-parenb -parodd -cmspar cs8 -hupcl -cstopb cread -clocal -crtscts
-ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr icrnl ixon -ixoff -iuclc -ixany -imaxbel iutf8
opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
isig icanon iexten echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke -flusho -extproc


# SET (in docker...)
>> stty columns 237


# docker exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -ti container bash
>> echo $COLUMNS

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
>> shopt -s checkwinsize


>> apt update && apt install xterm
>> sudo resize

```


### Write to user

1. ssh to host
1. who is logged in ?
`who -T` (-T is to see write permission)
1. what terminal am i using ?
`tty`
1. enable messages : `mesg y` / disable messages : `mesg n`
1. write : `echo "heyho" | write <USERNAME> <TTY>`

