# Sshfs

https://doc.ubuntu-fr.org/sshfs

## Mount
## Mount

```bash 
mkdir mount_dir
sshfs [user@]host:[dir] mount_dir [options]

# exemple
sshfs fdutrech@annecy:/work-crypt/fdutrech/workspace/bluecime/extra-eyes ssh-deep-blue

sshfs edge:/home/dutrechf mount_dir -o idmap=user,allow_other
```

## Umount

```
fusermount -u mount_dir

# force umount
sudo umount -l mount_dir

```