# Virtualization

## Virtual Box

Full Screen : install guest addition


Host key : right-ctrl

Fullscreen : host + f

### Install

```
apt install --reinstall linux-headers-$(uname -r) virtualbox-dkms dkms
```

https://stackoverflow.com/questions/60350358/how-do-i-resolve-the-character-device-dev-vboxdrv-does-not-exist-error-in-ubu

- Disable secure boot : https://www.itadminguide.com/disable-secure-boot-in-ubuntu/

```bash
sudo mokutil --sb-state
sudo mokutil --disable-validation
# set password to '12345678' as you will be prompted to enter password character by index (1-based indexing)
# be careful on QWERTY / AZERTY...
sudo shutdown -r
```



