# Docker


### Install

```
sudo dnf install docker
```

Out of the box, the docker command can only be run with admin privileges. Because of security issues, you won’t want to be working with Docker either from the root user or with the help of sudo. To get around this, you need to add your user to the docker group. This is done with the command:

```
sudo usermod -a -G dockerroot $USER
```

Once you’ve taken care of that, log out and back in, and you should be good to go. That is, unless your platform is Fedora. When adding a user to the docker group to this distribution, you’ll find the group doesn’t exist. What do you do? You create it first. Here are the commands to take care of this:

```
sudo groupadd docker && sudo gpasswd -a ${USER} docker && sudo systemctl restart docker
newgrp docker  # login to newly created group
```

Log out and log back in. You should be ready to use Docker.

Once installed, you will want to enable the Docker daemon at boot. To do this, issue the following two commands:

```
sudo systemctl start docker
sudo systemctl enable docker
```

Should you need to stop or restart the Docker daemon, the commands are:


```
sudo systemctl stop docker
sudo systemctl restart docker
```

Docker is now ready to deploy containers.

### Usage

 - can user use docker : `docker --version`
 - list all images : `docker image ls`
 - build new conastiner : ` cd <PATH>/Dockerfile; docker build -t <name> .`
 - run by tag : `docker run  -i -t fedora_vision /bin/bash `
 - re-tag : `docker tag SHA1 <name>:<TAG>`
 - rm image/duplicate : `docker rmi <name>`

 - setup HOST env :
    1. copy setup script from HOST to container : `docker cp scripts/jenkins/jenkins_setup.sh  eaa8e2b89a83:/jenkins_setup.sh`
    1. setup env : `HOST >> source jenkins_setup.sh` (do not execute setup.sh, otherwise all environment variable will be unset at script exit)


 - list all runnning dockers

```
>> docker ps
CONTAINER ID        IMAGE                                            COMMAND          ...
fa81be10d975        gitlab.asygn.com:5678/...jenkins_fedora_vision   "/bin/bash"      ...
```
 - kill runnning docker `>> docker kill fa81be10d975`



 - Download gitlab image, + run with correct rights :

    1. `docker login gitlab.asygn.com:5678` (use gitlab identifiants)
    2. `docker pull gitlab.asygn.com:5678/bluecime/general/jenkins_fedora_vision`
    3. `docker image ls`
    4. `docker run --rm=true -it --privileged -v /bluecime/videos/:/bluecime/videos/:ro -v /bluecime/publicRW/:/bluecime/publicRW/:rw gitlab.asygn.com:5678/bluecime/general/jenkins_fedora_vision /bin/bash`
    5. `ctrl+d` (exit)

### free space

docker images -q --filter dangling=true  | xargs -r docker rmi 



### Docker user

How to configure a user within docker to have same uid/gid than host user, and having `sudo` permission (without password)

```dockerfile
ARG USER_ID
ARG GROUP_ID
ARG USER_NAME=docker-user

RUN if [ ${USER_ID:-0} -ne 0 ] && [ ${GROUP_ID:-0} -ne 0 ]; then \
    groupadd -g ${GROUP_ID} ${USER_NAME} &&\
    useradd -l -u ${USER_ID} -g ${USER_NAME} ${USER_NAME} &&\
    install -d -m 0755 -o ${USER_NAME} -g ${USER_NAME} /home/${USER_NAME} &&\
    chown --changes --silent --no-dereference --recursive \
          --from=33:33 ${USER_ID}:${GROUP_ID} \
        /home/${USER_NAME} \
    && echo "${USER_NAME}  ALL=(ALL) NOPASSWD:ALL" >>  /etc/sudoers \
    && echo "${USER_NAME}:docker" | chpasswd \
;fi
```

```
# build
docker build
	--build-arg USER_ID=$(shell id -u) \
	--build-arg GROUP_ID=$(shell id -g) \
	--tag $(DOCKER_IMAGE_TAG) \
	.

# run
#-v ~/workspace/common/bashrc.sh:/home/dutrechf/.bashrc
#-v /home/dutrechf/.gitconfig:/home/dutrechf/.gitconfig
#-v /home/dutrechf/.ssh:/home/dutrechf/.ssh
docker run -it --rm $(DOCKER_IMAGE_TAG) --name $(DOCKER_NAME) bash

# exec
docker exec -it $(DOCKER_NAME) bash
```



### Links

 - [cheatsheet](https://www.docker.com/sites/default/files/Docker_CheatSheet_08.09.2016_0.pdf)
 - [dockerfile-examples](https://docs.docker.com/engine/reference/builder/#dockerfile-examples)

 - [gitlab regitery (all images)](https://gitlab.asygn.com/bluecime/general/container_registry)
 - [ee-eyes snippets](https://gitlab.asygn.com/bluecime/cime-extraeyes/snippets/4)

### Snippets


```
# get docker ID
# >> docker ps

# copy setup script from HOST to container
# >> docker cp scripts/jenkins/jenkins_setup.sh  <DOCKER_ID>:/jenkins_setup.sh

# setup env
# NB : do not execute setup.sh, otherwise all environment variable will be unset at script exit
# >> source jenkins_setup.sh


export gitbranch=fd/auto_annot
export break_on_diff=false
export cmake_args=
export blueski_args=--replay=write_0_1
export smart_eval_tool_args="--filter-place=zore_20180123 --pre-annot"
export tune_config=false

git clone git@gitlab.asygn.com:bluecime/cime-extraeyes.git
cd cime-extraeyes
git checkout ${gitbranch}
git submodule update --init
```



