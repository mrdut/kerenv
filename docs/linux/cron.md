# Cron

### Usage

 - list cron tab : `crontab -l`
 - edit cron tab : `crontab -e`

### Links

 - https://www.ostechnix.com/a-beginners-guide-to-cron-jobs/


The format for cronjobs is:

```
 +---------------- minute (0 - 59)
 |  +------------- hour (0 - 23)
 |  |  +---------- day of month (1 - 31)
 |  |  |  +------- month (1 - 12)
 |  |  |  |  +---- day of week (0 - 6) (Sunday=0 or 7)
 |  |  |  |  |
 *  *  *  *  *  command to be executed
 ```


```
>> VISUAL=vim crontab -e
>> crontab -l
```

### Notify-send

 - https://wiki.archlinux.org/title/Desktop_notifications#Usage_in_programming


```
5 10 * * 0-5 DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1001/bus  /usr/bin/notify-send 'Point quotidien !' "<a href='https://teams.microsoft.com/_\#/conversations/19:meeting_NDAyOGRkODEtYzY3Ni00NjI4LWExZWEtN2Q0MTgyNzg2OGY3@thread.v2\?ctx\=chat'>Teams link</a>" --icon=call-start
```