# Rsync

### Basic example

```
# syncing folder src into dest:
rsync -avz ./src /dest

# syncing the content of src into dest:
rsync -avz ./src/ /dest

# compare directories (no change)
rsync -rvnc --delete ${SOURCE}/ ${DEST}

# fast delete directory - don't forget the / after the directory names
mkdir empty 
rsync -r --delete empty/ ${SOURCE}/  
rmdir empty ${SOURCE}
```

### Transfer options
 - `-z`, ``--compress``
 - `-n`, ``--dry-run``
 -     ``--partial``   # allows resuming of aborted syncs

### Display options

 - `-q`, `--quiet`
 - `-v`, `--verbose`
 - `-h`, `--human-readable`
 -     `--progress`
 - `-P`                     # same as `--partial` + `--progress`

### Skipping options

`-u`, `--update`     # skip files newer on dest
`-c`, `--checksum`   # skip based on checksum, not mod-time & size

### Backup options

`-b`, `--backup`           # backup with suffix
    `--suffix`=SUFFIX    # default ~ without `--backup-dir`
    `--backup-dir`=DIR

### Include options
 - `--exclude`=PATTERN
 - `--include`=PATTERN
 - `--exclude-from`=FILE
 - `--include-from`=FILE
 - `--files-from`=FILE    # read list of filenames from FILE

### Archive options
 - `-a`, `--archive`    # archive (-rlptgoD)
 - `-r`, `--recursive`
 - `-l`, `--links`      # copy symlinks as links
 - `-p`, `--perms`      # preserve permissions
 - `-t`, `--times`      # preserve times
 - `-g`, -`-g`roup      # preserve group
 - `-o`, -`-o`wner      # preserve owner
 - `-D`               # `--devices` `--specials`
 - `--delete`         # Delete extra files

