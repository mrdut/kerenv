# Kubernetes

``` bash
>>  kubectl cluster-info
Kubernetes master is running at https://35.195.54.90
GLBCDefaultBackend is running at https://35.195.54.90/api/v1/namespaces/kube-system/services/default-http-backend:http/proxy
Heapster is running at https://35.195.54.90/api/v1/namespaces/kube-system/services/heapster/proxy
KubeDNS is running at https://35.195.54.90/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://35.195.54.90/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

# NODES
>> kubectl get nodes
NAME                                         STATUS   ROLES    AGE   VERSION
gke-bull-k8s-01-default-pool-601ae43c-1wqh   Ready    <none>   42m   v1.15.11-gke.12
gke-bull-k8s-01-default-pool-601ae43c-dkw8   Ready    <none>   42m   v1.15.11-gke.12
gke-bull-k8s-01-default-pool-601ae43c-vrlt   Ready    <none>   42m   v1.15.11-gke.12

>> kubectl get namespaces
NAME              STATUS   AGE
default           Active   43m
kube-node-lease   Active   43m
kube-public       Active   43m
kube-system       Active   43m

# PODES
>> kubectl get -n kube-system pod
NAME                                                        READY   STATUS    RESTARTS   AGE
event-exporter-v0.3.0-5cd6ccb7f7-chjfx                      2/2     Running   0          44m
fluentd-gcp-scaler-6855f55bcc-m9v9p                         1/1     Running   0          44m
fluentd-gcp-v3.1.1-84j8p                                    2/2     Running   0          44m
fluentd-gcp-v3.1.1-kgj2t                                    2/2     Running   0          44m
fluentd-gcp-v3.1.1-zmrdd                                    2/2     Running   0          44m
heapster-gke-77779df49c-fr6xl                               3/3     Running   0          43m
kube-dns-5c446b66bd-cpwjq                                   4/4     Running   0          44m
kube-dns-5c446b66bd-jgzwl                                   4/4     Running   0          44m
kube-dns-autoscaler-6b7f784798-ld6vw                        1/1     Running   0          44m
kube-proxy-gke-bull-k8s-01-default-pool-601ae43c-1wqh       1/1     Running   0          44m
kube-proxy-gke-bull-k8s-01-default-pool-601ae43c-dkw8       1/1     Running   0          44m
kube-proxy-gke-bull-k8s-01-default-pool-601ae43c-vrlt       1/1     Running   0          44m
l7-default-backend-84c9fcfbb-hzkmd                          1/1     Running   0          44m
metrics-server-v0.3.3-7599dd85cd-jzvxw                      2/2     Running   0          44m
prometheus-to-sd-4qrzr                                      2/2     Running   0          44m
prometheus-to-sd-5qc6f                                      2/2     Running   0          44m
prometheus-to-sd-v448x                                      2/2     Running   0          44m
stackdriver-metadata-agent-cluster-level-677786786f-vzmm8   2/2     Running   0          43m
```


Connect to Master Server API

```bash
>> kubectl proxy &
>> curl http://localhost:8001/api/
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "35.195.54.90:443"
    }
  ]
```

``` bash
>> kubectl explain pod
KIND:     Pod
VERSION:  v1
DESCRIPTION:
     Pod is a collection of containers that can run on a host. This resource is
     created by clients and scheduled onto hosts.
FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/api-conventions.md#resources
   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds
...

#further explaintion
>> kubectl explain pod.apiVersion
...

>> kubectl explain pod.status.podIP
...

```

## Nodes

```bash
>> kubectl get nodes
>> kubectl label node NODENAME disk=ssd # addlabel
```

## Namespace

namescape = cluster virtuel

```
# {
#     "kind": "Namespace",
#     "apiVersion": "v1",
#     "metadata": {
#         "name": "formation"
#     }
# }
>> kubectl create -f file.json
>> kubectl apply -f file.json # same than create, but update already created ressource - can be use all the tiems instead of create
>> kubectl get namespace
>> kubectl config view | grep namespace # get current namespace

>> kubectl config set-context $(kubectl config current-context) --namespace=kube-system
```


## Pods

``` bash
>> kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          99s

# get pod config
>> kubectl get pod nginx -o yaml/json




# connect to pod
>> kubectl exec -it PODNAME /bin/bash
# or
>> kubectl exec -it PODNAME -- COMMAND

# delete pod
>> kubectl delete pod <NAME>
>> kubectl delete po -l app=frontend # delete by label

# labels
>> kubectl label pod nginx1 rel=beta
>> kubectl get po --show-labels
>> kubectl get po -L app,rel # show labels rel and app as columns

>> kubectl get po -l rel # list pods with label rel
>> kubectl get po -l rel=stable # list pods with label rel with value = stable
>> kubectl get po -l '!rel'  # list pods with NO label rel

# annotation
>> kubectl annotate pod PODNAME LONG_ANNOTATION_STRING
>> kubectl describe pod PODNAME
...
Annotations: LONG_ANNOTATION_STRING
...
```

```
# list running containers on pod
>> docker -r /var/run/docker.sock ps

# connect to container
>> docker -r /var/run/docker.sock exec -it [ID_CONTAINER] /bin/bash


>> cat /etc/hosts


# in this case new instance of containers are started
>> docker -r /var/run/docker.sock stop [ID_CONTAINER]
>> docker -r /var/run/docker.sock rm [ID_CONTAINER]

kubectl
```

## Replica set

```
>> kubectl create -f set.json
replicaset.apps/fortune created

```
replicaset.apps/fortune created


## Services

```bash
>> kubectl get service
>> kubectl get svc
>> kubectl describe svc

curl SERVICE_NAME.SERVICE_NAMESPACE.CLUSTER_DOMAIN:PORT
```