# Grep



### usefull flags

 * `-B x` : display x lines before
 * `-A x` : display x lines after

 * `-r` : recursive (to search within directories)
 * `-I` : ignore binary files
 * `-i` : case insensitive
 * `-n` : display line number
 * `-l` : display only filename
 * `-h`: no filename
 * `-q` : return only returncode (no display)

### exclude

`grep <pattern> -rn --exclude-dir 'exclude1' --exclude-dir 'exclude2'`
`grep <pattern> -rn --exclude \*.yaml`

### no filename

`grep "PATTERN" DIRECTORY -rh`

or

`grep "PATTERN" DIRECTORY -r --no-filename`

### [highlight search](https://stackoverflow.com/questions/981601/colorized-grep-viewing-the-entire-file-with-highlighted-matches)

-> will highligh pattern or end-of-line character

```
COMMAND | grep --color -E '<PATTERN>|$'
COMMAND | egrep --color '<PATTERN>|$'
COMMAND | grep --color '<PATTERN>\|$'
```


capturing group regex
https://stackoverflow.com/questions/18892670/can-not-extract-the-capture-group-with-neither-sed-nor-grep
  version=`cat $git_toplevel/go.mod | grep --line-regexp -oP '^\s*[^ ]*koios-common-go.git \K([a-z0-9.]*)$'`
