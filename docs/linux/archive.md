# Archive

## Tar

### Extensions

 - archive.tar.**gz** : `z`
 - archive.tar.**bz2** : `j`

### Usage

    - list : `tar -vtf archive.tar`
    - extract single file : `tar -xf archive.tar <FILE>`



## Zip