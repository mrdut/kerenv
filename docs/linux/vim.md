# Vim

[Your problem with Vim is that you don't grok vi.](https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim)

## Modes

![vim modes](img/vim_modes_small.png)

- `command mode` : type commands
- `insert mode` : type text
- `visual mode` : select text
    - `v` : character-wise visual mode
    - `V` : line-wise visual mode
    - `Ctrl-v` : block-wise visual mode
    - depuis `insert mode` : `ctrl+o + XXX`


## Commands (`:` or `/`)

### Basic:

 - `w` : write changes
 - `q` : exit vim
 - `q!` : exit vim (discard changes)
 - `u` : Undo last edit
 - `e PATH` : open file browser
    - `-` : browse up one dir
    - `i` : change the browser style
    - `I` : hide the header
    - `D` : delete a file (confirmation required)
    - `R` : rename a file
    - `s` : change the sorting (name, time, size, exten)
    - `r` : revert the sorting

### Actions

[Think _operators_, _text objects_, and _motions_](https://www.youtube.com/watch?v=E-ZbrtoSuzw&t=233s)

 - Operators:
> - `c` : change
> - `d` : delete
> - `y` : yank into register ("arrachement")
> - `v` : select
> - `u` : lowercase
> - `U` : Uppercase
> - `~` : swap case
> - `=` : indent
> - `<` : shift left (dedent)
> - `>` : shift rigth (indent)
> - `=` : shift left

 - Text objects

> - Modifiers:
>>  - `#` : number
>>  - `i` : inside
>>  - `a` : around
>
> - Nouns:
>> - `w` : word
>> - `s` / `)` : sentence
>> - `s` / `)` : sentence
>> - `t` : tag (html)
>> - `b` : block
>> - `p` : paragraph

 - Motions (https://vimdoc.sourceforge.net/htmldoc/motion.html)

> - `%` : go to first matching apren / bracket
> - `[count]+` : down to first non-blank char of line
> - `[count]$` : end of line
> - `[count]f/F{char}` : next occurence of {char}
> - `[count]t/T{char}` : to before next occurence of {char}
> - `[count]h/j/k/l` : left/down/up/right
> - `[count]m` : beginning of next method
> - `[count]w/W` : go a word/WORd to the right
> - `[count]b/B` : go a word/WORd to the left
> - `[count]e/E` : go to end of word/WORd right
> - `[count](` : sentences backward
> - `[count])` : sentences forward
> - `[count]{` : paragraphs backward
> - `[count]}` : paragraphs forward

 - Usefull commands

 [count][operator][text object/motions]



## Snippets and troubleshouting

    - [Accessing the system clipboard](https://vim.fandom.com/wiki/Accessing_the_system_clipboard)
            - `yum install vim-X11`
            - _.zshrc_ : `if [[ -x "$(command -v vimx)" ]]; then alias vim='vimx'; fi`

    - no format when pasting
    - `:set paste`
    - `:set nopaste`

    - [vim-inserting-comments-while-copy-pasting](https://stackoverflow.com/questions/7736006/vim-inserting-comments-while-copy-pasting) -> `: set paste` + `Ctrl+Shift+V` or `"+p`
        - `"` : indicates next character is a register (`help registers`)
        - `*` : system select buffer
        - `+` : system clipboard
        - `p` : paste

    - delete all lines after cursor : `dG`
    - repeat last colon command : '@:'

### Multiple line editing:

1. Move the cursor to first line
1. Enter visual block mode (`Ctrl+v`)
1. Select lines (`j` or `down arrow`).
1. Press `I` (insert mode) + edit (it will only update the screen in the first line)
1. Press `Esc` (changes applied to all lines)

Or

1. Press `I` (insert mode) + edit
1. Press `j` (next line) + `.` (redo last command)

## Search

 - `/ <BLABLA>` + `n` (next) or `N` (previous)

 - `#` : previous occurence of current word
 - `*` : next occurence of current word
 - `*` : next occurence of current word

- `gd` : local definition of current word
- `:hls!` : toggle search highlighting

## [Replace](https://vim.fandom.com/wiki/Search_and_replace)

 - `s/<OLD>/<NEW>` replace 1 occurence in CURRENT line
 - `s/<OLD>/<NEW>/g` replace all occurences in CURRENT line
 - `%s/<OLD>/<NEW>` replace 1 occurence in ALL line
 - `%s/<OLD>/<NEW>/g` replace all occurences in ALL line
 - `%s/<OLD>/<NEW>/gc` + confirmation
 - [ranges](https://vim.fandom.com/wiki/Ranges)

Range |   Description | Example
-----|----------|---------------
21 | line 21 | :21s/old/new/g
1  | first line | :1s/old/new/g
$  | last line  | :$s/old/new/g
.  | current line  |  :.w single.txt
%  | all lines (same as 1,$) | :%s/old/new/g
21,25 |  lines 21 to 25 inclusive  |  :21,25s/old/new/g
21,$  |  lines 21 to end | :21,$s/old/new/g
.,$ | current line to end | :.,$s/old/new/g
.+1,$ |  line after current line to end | :.+1,$s/old/new/g
.,.+5 |  six lines (current to current+5 inclusive) | :.,.+5s/old/new/g
.,.5  |  same (.5 is interpreted as .+5) | :.,.5s/old/new/g



## Navigate

### Window

 - `Ctrl-w + v` : split window vertically
 - `Ctrl-w + s` : split window horizontally
 - `Ctrl-w + w` : switch window
 - `Ctrl-w + q` : close current window


### Cursor


![vim move keys](img/vim_move_key.png)
![vim move keys](img/vim_line_motions.png)

 - `: <LINE>` : Goto line number

 - `w` : Move the cursor to the starting of the next _word_
 - `W` : Move the cursor to the starting of the next _WORD_
 - `b` : Move the cursor to the starting of the previous _word_ (Mnemonic - **back**)
 - `B` : Move the cursor to the starting of the previous _WORD_
 - `e` : Move the cursor to the end of the current _word_ (Mnemonic - **end**)
 - `E` : Move the cursor to the end of the current _WORD_
 - `H` : Move the cursor to the First line of the current visible screen (Mnemonic - **High**)
 - `M` : Move the cursor to the Middle line of the current visible screen (Mnemonic - **Middle**)
 - `L` : Move the cursor to the Last line of the current visible screen (Mnemonic - **Low**)

 - _word_ : sequence of letters, digits and underscores, or a sequence of other non-blank characters, separated with white space (spaces, tabs, )
 - _WORD_ : sequence of non-blank characters, separated with white space

## Configuration

 - `: syntax on`
 - `: set background=dark`
 - `: set number`
 - `: set showcmd`
 - `: set mouse=a`
 - `: set encoding=utf-8`
 - `: set shiftwidth=4`
 - `: set smartindent`
 - `: set cursorline`
 - `: set paste` or `set pastetoggle=<F10>`

 ```~/.vimrc
"https://stackoverflow.com/questions/234564/tab-key-4-spaces-and-auto-indent-after-curly-braces-in-vim
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
 ```

# reset to vim-defaults
if &compatible          # only if not set before:
  set nocompatible      # use vim-defaults instead of vi-defaults (easier, more user friendly)
endif


 - `: set background=dark`     :  enable for dark terminals
 - `: set nowrap`              :  dont wrap lines
 - `: set scrolloff=2`         :  2 lines above/below cursor when scrolling
 - `: set number`              :  show line numbers
 - `: set showmatch`           :  show matching bracket (briefly jump)
 - `: set showmode`            :  show mode in status bar (insert/replace/...)
 - `: set showcmd`             :  show typed command in status bar
 - `: set ruler`               :  show cursor position in status bar
 - `: set title`               :  show file in titlebar
 - `: set wildmenu`            :  completion with menu
 - `: set wildignore=*.o,*.obj,*.bak,*.exe,*.py[co],*.swp,*~,*.pyc,.svn
 - `: set laststatus=2`         : use 2 lines for the status bar
 - `: set matchtime=2`          : show matching bracket for 0.2 seconds
 - `: set matchpairs+=<:>`      : specially for html

# editor settings
 - `set esckeys`             :  map missed escape sequences (enables keypad keys)
 - `set ignorecase`          :  case insensitive searching
 - `set smartcase`           :  but become case sensitive if you type uppercase characters
 - `set smartindent`         :  smart auto indenting
 - `set smarttab`            :  smart tab handling for indenting
 - `set magic`               :  change the way backslashes are used in search patterns
 - `set bs=indent,eol,start` :  Allow backspacing over everything in insert mode

