# Pdf

convert jpeg -> pdf 

merge pdf

```
pdfunite DUTRECH_Florent_CV.pdf DUTRECH_Florent_lettre_Alma.pdf DUTRECH_Florent_Alma.pdf
```

extract part of pdf

mix pdf ([replace page within pdf](https://superuser.com/questions/611227/how-to-replace-a-single-page-in-a-pdf-using-another-pdf-in-linux))
```
pdftk A=Contrat\ Signé\ RH.pdf B=20200207083714_001.pdf cat A1-4 B1 A6 output Contrat_FDUTRECH.pdf 
```

edit pdf 
```
xournal <pdf>
```

edit pdf 
```
master pdf editor
https://code-industry.net/get-master-pdf-editor-for-ubuntu/?download
sudo dpkg -i package_file.deb
```

