# Boot


## Time

```
➜  ~ sudo systemd-analyze blame
Startup finished in 3.076s (kernel) + 7.719s (userspace) = 10.795s
graphical.target reached after 7.711s in userspace
```

```
➜  ~ sudo systemd-analyze blame
           6.245s NetworkManager-wait-online.service
           860ms dev-sda2.device
           366ms systemd-journal-flush.service
           264ms upower.service
           213ms udisks2.service
           200ms systemd-logind.service
           193ms snapd.service
           172ms keyboard-setup.service
           171ms networkd-dispatcher.service
           162ms NetworkManager.service
           131ms systemd-rfkill.service
           122ms ModemManager.service
           122ms systemd-journald.service
           114ms accounts-daemon.service
           114ms systemd-fsck@dev-disk-by\x2duuid-
```

https://itsfoss.com/speed-up-ubuntu-1310/

https://askubuntu.com/questions/1018576/what-does-networkmanager-wait-online-service-do
Some code runs off the network
In some multi-user environments part of the boot-up process can come from the network. For this case systemd defaults to waiting for the network to come on-line before certain steps are taken.

Majority of Desktop Users
Unlike some multi-user environments most Ubuntu desktop users have the Operating System and drivers on their hard disks, SSDs or Live Boot USBs.

```
sudo systemctl disable/enable NetworkManager-wait-online.service
```


speed up boot time :

https://wiki.archlinux.org/index.php/Improving_performance/Boot_process
https://www.techrepublic.com/blog/10-things/10-ways-to-make-linux-boot-faster/
https://www.tecmint.com/systemd-analyze-monitor-linux-bootup-performance/