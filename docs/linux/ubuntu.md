# Ubuntu


## Packages

```
sudo apt-get install curl vim terminator tree ncdu ranger vlc chrome spotify git zsh mkdocs htop
```

[oh my zsh](https://ohmyz.sh/)

scanner
https://support.hp.com/fr-fr/drivers/selfservice/closure/samsung-xpress-sl-m2070-laser-multifunction-printer-series/16450377?ssfFlag=true&sku=

➜  uld sudo sane-find-scanner


  # sane-find-scanner will now attempt to detect your scanner. If the
  # result is different from what you expected, first make sure your
  # scanner is powered up and properly connected to your computer.

  # No SCSI scanners found. If you expected something different, make sure that
  # you have loaded a kernel SCSI driver for your SCSI adapter.

found USB scanner (vendor=0x04e8, product=0x3469 [M2070 Series]) at libusb:002:002
  # Your USB scanner was (probably) detected. It may or may not be supported by
  # SANE. Try scanimage -L and read the backend's manpage.

  # Not checking for parallel port scanners.

  # Most Scanners connected to the parallel port or other proprietary ports
  # can't be detected by this program.

Afin de pouvoir installer le pilote en cliquant sur son nom dans la page de la documentation, installer au préalable apturl :
sudo apt-get install apturl
https://doc.ubuntu-fr.org/liste_imprimantes_samsung#laser_monochrome_multifunction_mfp-_scx-_sf-_m_sl-_xpress_proxpress

### Reduce grub timeout

```
>> sudo vim /etc/default/grubsudo ufw status
GRUB_TIMEOUT=1 # 1 second
>> sudo update-grub
```

### display grub lçg message

```
>> sudo vim /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="" # remove quiet and splash
>> sudo update-grub
```

### no login at startup

https://doc.ubuntu-fr.org/lightdm
+
```
subl /etc/lightdm/lightdm.conf
autologin-user-timeout=1 # was 0
```




### speedup

See Kerenv > Linux > Boot

## Troobleshooting

### System Program Problem Detected


```
>>  ~ ls -l /var/crash 
total 106272
-rw-r----- 1 mrdut whoopsie 52618537 mars   9 21:05 _opt_google_chrome_chrome.1000.crash
-rw-r----- 1 mrdut whoopsie  1448298 mars   9 16:50 _usr_bin_xfce4-power-manager.1000.crash
-rw-r----- 1 root  whoopsie   370719 mars   9 16:29 _usr_lib_xorg_Xorg.0.crash
-rw-r----- 1 mrdut whoopsie 54375508 mars   9 21:04 _usr_share_spotify_spotify.1000.crash

>> rm /var/crash/*

# disable the Apport (debug tool) and permanently get rid of the pop-ups.
>> vim /etc/default/apport 
enable = 0
```



### Google Chrome Asks Password to Unlock Login Keyring

https://tipsonubuntu.com/2017/12/20/google-chrome-asks-password-unlock-login-keyring/

### Network manager icon open twice

https://bugs.launchpad.net/ubuntu/+source/xfce4-panel/+bug/1685502

```
xfce4-panel --restart
```

### No network

```
================================================================================================================================================================

mrdut@dolmen:~$ ifconfig -a
enp2s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 60:a4:4c:56:be:86  txqueuelen 1000  (Ethernet)
        RX packets 897  bytes 177900 (177.9 KB)
        RX errors 0  dropped 220  overruns 0  frame 0
        TX packets 240  bytes 40256 (40.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 2603  bytes 186294 (186.2 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2603  bytes 186294 (186.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

================================================================================================================================================================

mrdut@dolmen:~$ cat /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback

## MRDUT UPDATE ##

# mrdut
auto enp2s0
iface enp2s0 inet dhcp


================================================================================================================================================================

mrdut@dolmen:~$ sudo lshw -c network -sanitize
[sudo] password for mrdut: 
  *-network                 
       description: Ethernet interface
       product: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
       vendor: Realtek Semiconductor Co., Ltd.
       physical id: 0
       bus info: pci@0000:02:00.0
       logical name: enp2s0
       version: 09
       serial: [REMOVED]
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=r8169 duplex=full firmware=rtl8168f-1_0.0.5 06/18/12 latency=0 link=yes multicast=yes port=MII speed=1Gbit/s
       resources: irq:38 ioport:e000(size=256) memory:f8004000-f8004fff memory:f8000000-f8003fff

================================================================================================================================================================

mrdut@dolmen:~$ sudo ifconfig enp2s0 up

================================================================================================================================================================

mrdut@dolmen:~$ lspci -nnk | grep -i -A2 net
02:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller [10ec:8168] (rev 09)
	Subsystem: ASUSTeK Computer Inc. P8 series motherboard [1043:8505]
	Kernel driver in use: r8169

================================================================================================================================================================

mrdut@dolmen:~$ iwconfig
lo        no wireless extensions.

enp2s0    no wireless extensions.

================================================================================================================================================================

mrdut@dolmen:~$ lsmod | grep r8169
r8169                  81920  0

================================================================================================================================================================

mrdut@dolmen:~$ nmcli d
DEVICE  TYPE      STATE      CONNECTION 
enp2s0  ethernet  unmanaged  --         
lo      loopback  unmanaged  --         

================================================================================================================================================================

mrdut@dolmen:~$ cat /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf_orig 
[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:wwan

## MRDUT UPDATE ##

[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:wwan,except:type:ethernet

## ALTERNATIVE unmanaged-devices=none 


================================================================================================================================================================

# check cable ethernet is plugged
grep "" /sys/class/net/enp2s0/*

```