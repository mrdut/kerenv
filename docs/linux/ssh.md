# Ssh

## Trick and tips

 - `ssh IP "COMMAND"` : run custom command instead of opening shell



## Troubleshooting

### Permissions 

```
Permissions 0755 for '~/.ssh/id_rsa' are too open.
```

```
>> chmod 600 ~/.ssh/id_rsa
```

### Get rid of SSH message

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:M+kV0YIK6aH4VYoVvUYvEgea4QEqRvUxvwk7m/giGMo.
Please contact your system administrator.
Add correct host key in /nfs/home/fdutrech/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /nfs/home/fdutrech/.ssh/known_hosts:287
Password authentication is disabled to avoid man-in-the-middle attacks.
Keyboard-interactive authentication is disabled to avoid man-in-the-middle attacks.
```

 - solution 1

```
ssh-keygen -R sivao2018_201_2868
ssh-keygen -t rsa -C "f.dutrech@ateme.com"
```

 - solution 2

```
# remove line
# <LINE> = Offending ECDSA key in /nfs/home/fdutrech/.ssh/known_hosts:287
sed -i <LINE>d /nfs/home/fdutrech/.ssh/known_hosts
```

### no more password

```
>> ssh fdutrech@annecy
fdutrech@annecy's password: 
```

1. Create public and private keys using ssh-key-gen on local-host

```
>> ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/bluecime/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/bluecime/.ssh/id_rsa.
Your public key has been saved in /home/bluecime/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:jsENL9E3GK9hk1myV2umSLc3bgd0VjeZXRKKDGSDh+c bluecime@deepblue.bluecime
The key's randomart image is:
+---[RSA 2048]----+
|       +B . . ooO|
|      ooo& o o =+|
|      o+@ O * o  |
|     . BEO B o   |
|      + S o +    |
|       =   o o   |
|      . .   o .  |
|           . .   |
|                 |
+----[SHA256]-----+
```

2. Copy the public key to remote-host using ssh-copy-id : `ssh-copy-id -i ~/.ssh/id_rsa.pub <USER>@<REMOTE>`

```
#ssh-copy-id appends the keys to the remote-host’s .ssh/authorized_key
-
```

3. Connect with no password

```
>> ssh fdutrech@annecy
```


### add new Host

```
# ~/.ssh/config
Host deepblue
    User bluecime
    Hostname 172.16.128.37
```
