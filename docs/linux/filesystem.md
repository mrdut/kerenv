# Filesystem

## Filesystem usage

```
df -h
```


## Directory usage
 
```
du -hs
ncdu
```

## Partition table

```
>> cat /etc/fstab
```

## Usb drive

### Troubleshouting

```
(root) >> gparted

# select partition
# menu > umount
# menu > check
# menu > apply
```

## Ntfs

You may need to install 

```
yum install ntfs-3g ntfsprogs -y
```

