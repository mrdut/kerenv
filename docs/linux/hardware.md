# Hardware

```
➜  kerenv git:(master) inxi -Fxz
System:    Host: dolmen Kernel: 5.3.0-42-generic x86_64 bits: 64 gcc: 7.4.0 Desktop: Xfce 4.12.3 (Gtk 2.24.31)
           Distro: Ubuntu 18.04.4 LTS
Machine:   Device: desktop Mobo: ASUSTeK model: M5A97 R2.0 v: Rev 1.xx serial: N/A
           UEFI: American Megatrends v: 1302 date: 11/14/2012
CPU:       Quad core AMD FX-4100 (-MCP-) arch: Bulldozer rev.2 cache: 8192 KB
           flags: (lm nx sse sse2 sse3 sse4_1 sse4_2 sse4a ssse3 svm) bmips: 28898
           clock speeds: max: 3600 MHz 1: 1702 MHz 2: 1794 MHz 3: 1605 MHz 4: 1650 MHz
Graphics:  Card: NVIDIA GK208B [GeForce GT 710] bus-ID: 01:00.0
           Display Server: x11 (X.Org 1.20.5 ) drivers: modesetting (unloaded: fbdev,vesa)
           Resolution: 1360x768@60.02hz
           OpenGL: renderer: NV106 version: 4.3 Mesa 19.2.8 Direct Render: Yes
Audio:     Card-1 NVIDIA GK208 HDMI/DP Audio Controller driver: snd_hda_intel bus-ID: 01:00.1
           Card-2 Advanced Micro Devices [AMD/ATI] SBx00 Azalia (Intel HDA)
           driver: snd_hda_intel bus-ID: 00:14.2
           Sound: Advanced Linux Sound Architecture v: k5.3.0-42-generic
Network:   Card-1: Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
           driver: r8169 port: d000 bus-ID: 02:00.0
           IF: enp2s0 state: down mac: <filter>
           Card-2: Broadcom and subsidiaries BCM4352 802.11ac Wireless Network Adapter driver: wl bus-ID: 05:00.0
           IF: wlp5s0 state: up mac: <filter>
Drives:    HDD Total Size: 2388.5GB (11.9% used)
           ID-1: /dev/sda model: M4 size: 64.0GB
           ID-2: /dev/sdb model: WDC_WD3200KS size: 320.1GB
           ID-3: /dev/sdc model: CT500MX500SSD1 size: 500.1GB
           ID-4: /dev/sdd model: SAMSUNG_HD154UI size: 1500.3GB
           ID-5: USB /dev/sde model: USB_Flash_Disk size: 4.0GB
Partition: ID-1: / size: 30G used: 8.4G (31%) fs: ext4 dev: /dev/sdc2
           ID-2: /home size: 428G used: 258G (64%) fs: ext4 dev: /dev/sdc3
RAID:      No RAID devices: /proc/mdstat, md_mod kernel module present
Sensors:   System Temperatures: cpu: 9.5C mobo: N/A gpu: 50.0
           Fan Speeds (in rpm): cpu: 0
Info:      Processes: 209 Uptime: 13 min Memory: 1021.8/7864.0MB Init: systemd runlevel: 5 Gcc sys: 7.5.0
           Client: Shell (zsh 5.4.2) inxi: 2.3.56 
```

