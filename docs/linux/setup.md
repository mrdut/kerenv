# Setup new machine


## User

```bash

# Create user
root > useradd -m <USER> # create also home directory
root > passwd <USER>

# Delete user
root > userdel <USER>

# Sudoer (you can also edit /etc/sudoers: %wheel  ALL=(ALL)       NOPASSWD: ALL`)

root > usermod -a -G wheel <USER>

```

```bash
# Docker
usermod -a -G docker $USER
```

## Ethernet share

```bash
# Connect the 2 laptops with ethernet cable, and configure on both:

sudo apt install openssh-server

Edit Connection
> Ethernet/Device : select ethernet interface
> IPv4 Settings/ Address=192.168.1.2 (or 3), Netmask=24, Gateway=NULL

ssh fdutrech@192.168.1.2 !
```

## Kerenv

```bash
git clone git@bitbucket.org:mrdut/kerenv.git

cd ../kerenv
cd dev/ateme/vpn
./compile.sh
/usr/local/sbin/ateme-vpn kg
# pip install --user yubikey-manager
#  sudo apt-add-repository ppa:yubico/stable
sudo apt install xclip expect openconnect yubikey-manager
```

## Gnome

```bash
>> sudo apt install gnome-shell-extension-manager
>> Browse : "focus" -> Install NoAnnoyance v2

# https://github.com/tak0kada/gnome-shell-extension-stealmyfocus
# https://github.com/v-dimitrov/gnome-shell-extension-stealmyfocus
    @mkdir -p ~/.local/share/gnome-shell/extensions && mv ../gnome-shell-extension-stealmyfocus ~/.local/share/gnome-shell/extensions/steal-my-focus@kagesenshi.org

mkdir -p ~/.local/share/gnome-shell/extensions
cd ~/.local/share/gnome-shell/extensions
git clone https://github.com/v-dimitrov/gnome-shell-extension-stealmyfocus.git
reboot

```
## Packages

```bash
`
sudo dnf update
sudo dnf install epel-release`

sudo apt install vim ncdu tree htop curl yq wget screen ranger net-tools rsync 
sudo apt terminator gnome-disk-utility meld cifs-utils lnav
sudo apt install git gitk gitg git-lfs make cmake gdb valgrind

# lnav: hotkeys: i / p /


# clang
wget https://apt.llvm.org/llvm.sh
chmod u+x llvm.sh
sudo ./llvm.sh 18
clang --version
pip3 install clang-format==18.1.2 # see requirement in Atebuild/clanf-format/requirements.txt
sudo apt-get install clangd-18

# chrome
sudo apt install curl software-properties-common apt-transport-https ca-certificates -y
curl -fSsL https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | sudo tee /usr/share/keyrings/google-chrome.gpg > /dev/null
echo deb [arch=amd64 signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt update
sudo apt install google-chrome-stable

# albert
wget -O albert.key https://download.opensuse.org/repositories/home:manuelschneid3r/Debian_12/Release.key\n
mkdir -p ls /etc/apt/keyrings
gpg --dearmor < albert.key | sudo tee /etc/apt/keyrings/albert.gpg > /dev/null\n
echo "deb [signed-by=/etc/apt/keyrings/albert.gpg] https://download.opensuse.org/repositories/home:/manuelschneid3r/Debian_12/ /" | sudo tee /etc/apt/sources.list.d/albert.list\n
sudo apt update
sudo apt install albert
albert
```


## Zsh

```bash
sudo dnf install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
cat /etc/shells
dnf provides chsh
sudo dnf install util-linux-user
sudo chsh -s $(which zsh) $USER

# Edit ~/.zshrc
ZSH_THEME="agnoster"

export ATEME_WORKSPACE=${HOME}/Workspace
export ATEMELIB=${ATEME_WORKSPACE}/AtemeLib

alias gadd="git add";
alias gdiff="git diff";
alias gci="git commit";
alias gpu='git push --set-upstream origin $(git branch --show-current)'

alias ,tp="cd $ATEME_WORKSPACE/TitanProcessing"
alias ,ng="cd $ATEME_WORKSPACE/nea-genesis"

```

## SSH

```bash

# LAPTOP : ~/.ssh/config
Host myhost
    Hostname 10.96.0.23
    User fdutrech

# Authorized laptop
ssh-copy-id -i ~/.ssh/id_rsa.pub myhost

# Share filesytem
sudo mkdir /mnt/myhost
sudo chown $USER /mnt/myhost
sshfs myhost:/home/fdutrech /mnt/myhost


# Copy Pkgmg keys
cp ~/.ssh/id_rsa_svn* /mnt/titan_perf/.ssh -v
```

## Python

```bash

# UBUNTU
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt-get install python3.11\n
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 2
sudo update-alternatives --config python3
sudo apt install python3-pip
python3 -m pip install --upgrade pip





sudo dnf list 'python3.??'
sudo dnf install python3.11 python3.11-devel

ls -ls /usr/bin/python*
sudo ln -s /usr/bin/python3.11 /usr/bin/python3
sudo ln -s /usr/bin/python3.11 /usr/bin/python

sudo dnf install python3.11-pip
python3 -m pip install --upgrade pip
pip install --extra-index-url https://nexus-rennes.ateme.net/repository/pypi-dev-hosted/simple pkgmg
```

## Gitlab

```bash

git config --global user.name "Florent Dutrech"
git config --global user.email "f.dutrech@ateme.com"
git config --global pull.rebase true
git config --global fetch.prune true
git config --global core.editor "vim -c 'startinsert'"

git config --global pager.log false # OR git config --global core.pager cat

git config --global -e

#register key : https://gitlab.ateme.net/-/user_settings/ssh_keys
cat ~/.ssh/id_rsa.pub

git clone git@gitlab.ateme.net:sw/TitanProcessing.git
pkgmg install pkg.xml --update --arch=x64-buster-clang

git clone git@gitlab.ateme.net:sw/micro-services/nea-live/nea-live.git
git lfs install

git clone git@gitlab.ateme.net:/sw/DashTsInput.git
git clone git@gitlab.ateme.net:/sw/genesis-container.git
git clone git@gitlab.ateme.net:/sw/genesis-request.git
git clone git@gitlab.ateme.net:/sw/genesis-utils.git
git clone git@gitlab.ateme.net:/sw/genesis-s3.git

```

## Docker

```bash
# https://docs.docker.com/engine/install/centos/
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin\n
sudo systemctl enable --now docker

# https://docs.docker.com/engine/install/debian/

sudo usermod -a -G docker $USER
newgrp docker

docker run hello-world
```

## K8s

```bash

# kubectl
# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-kubectl-binary-with-curl-on-linux
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl*

# helm : https://helm.sh/docs/intro/install/#from-apt-debianubuntu

# kubeconfig
cp ~/.kube/config /mnt/titan_perf/.kube -v

# k9s
# https://github.com/derailed/k9s
curl -sS https://webinstall.dev/k9s | bash
```

## Sublime / Mutagen

```bash
# packages
 - FavoriteFiles
 - Dockerfile Syntax

# https://www.sublimetext.com/docs/linux_repositories.html
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text



.config/sublime-text/Packages
├── Default
│   └── Default (Linux).sublime-keymap
├── jsonify -> /home/fdutrech/Workspace/kerenv/sublime/jsonify
├── smart_open -> /home/fdutrech/Workspace/kerenv/sublime/smart_open
└── User
    ├── ANSIescape
    │   └── ansi.sublime-color-scheme
    ├── ANSI.sublime-settings
    ├── copy_filename_line.py -> /home/fdutrech/Workspace/kerenv/sublime/copy_filename_line.py
    ├── Default (Linux).sublime-keymap
    ├── example_commande.py
    ├── favorite_files_list.json
    ├── Package Control.sublime-settings
    ├── Package Control.user-ca-bundle
    └── Preferences.sublime-settings

Eror could not load default linux keymap : simply save as Preference > Key bindings left page as the default !
sublime key bindings
[
    { "keys": ["ctrl+k", "ctrl+m"], "command": "reveal_and_focus_side_bar"},
    { "keys": ["ctrl+right"], "command": "hide_side_bar_or_move" },

    { "keys": ["ctrl+k", "ctrl+j"], "command": "jsonify" },
    { "keys": ["ctrl+k", "ctrl+y"], "command": "yamlify" },

    { "keys": ["ctrl+k", "ctrl+a"], "command": "favorite_files_add"},
    { "keys": ["ctrl+k", "ctrl+o"], "command": "favorite_files_open"},
    { "keys": ["ctrl+k", "ctrl+r"], "command": "favorite_files_remove"},
    { "keys": ["ctrl+k", "ctrl+p"], "command": "copy_path"},
    { "keys": ["ctrl+k", "ctrl+l"], "command": "copy_filename_line"},
    { "keys": ["ctrl+k", "ctrl+n"], "command": "open_line"},

    { "keys": ["ctrl+k", "ctrl+f"], "command": "example"},

    { "keys": ["ctrl+shift+o"], "command": "smart_open" }
]

# https://mutagen.io/documentation/introduction/installation
# https://github.com/mutagen-io/mutagen
mutagen sync create /mnt/titan_perf titan-perf:/home/fdutrech
mutagen sync list

```

## Nea Genesis

```bash
python -m venv venv
source venv/bin/activate
cp $HOME/.config/genesis/env/ceph.sh /mnt/titan_perf/.config/genesis/env/ -v

# edit /etc/hosts
127.0.0.1 mongodb
127.0.0.1 minio
127.17.0.1 host.docker.internal
10.96.0.102     objectstore.dev.genesis.ateme.net
# 10.96.0.102     objectstore.genesis.ateme.net
```

## Perf

```bash
sudo dnf install perf
sudo sysctl -w kernel.perf_event_paranoid=-1

# enable kernel.kallsyms
cat /proc/sys/kernel/kptr_restrict
# 0: All users can read /proc/kallsyms.
# 1: Only root can read /proc/kallsyms.
# 2: Kernel symbols are restricted (even for root).
echo 0 | sudo tee /proc/sys/kernel/kptr_restrict


git clone https://github.com/brendangregg/FlameGraph.git
PATH=$PATH:${HOME}/Workspace/ThirdParty/FlameGraph
sudo dnf install perl-open

#
# Tuned
#
https://gitlab.ateme.net/sw/architecture/performance-tuning
sudo install.sh
sudo tuned-adm profile ateme-performance

#
# BPF
#
uname -a # check kernel > 4.1
grep bpf /proc/kallsyms
sudo dnf install bpftool bcc

git clone https://github.com/iovisor/bcc.git
https://github.com/iovisor/bcc/blob/master/INSTALL.md#install-and-compile-bcc-1
https://github.com/iovisor/bcc/blob/master/INSTALL.md#centos-85---source

```


## Netstat

```bash
# https://learn.netdata.cloud/docs/netdata-agent/installation/linux/
curl https://get.netdata.cloud/kickstart.sh > /tmp/netdata-kickstart.sh && sh /tmp/netdata-kickstart.sh


```

## Printers

```bash

# https://atemehq-my.sharepoint.com/:w:/g/personal/z_ye_ateme_com/EU_O5aMorsJAk4mEsHdIkn4BNTDTNPO0RfsbKoQ183VeSw?e=fgB6JC

>> sudo vim /etc/cups/cups-browsed.conf et ajouter :
BrowseRemoteProtocols cups
BrowseAllow cups01.anevia.com
BrowsePoll cups01.anevia.com:63

>> sudo service cups-browsed restart

# Issue : conflict with Docker bridge (cups server and docker uses 172.27.X.X)
>> ip route
***
172.27.0.0/16 dev br-4273cb004f2a proto kernel scope link src 172.27.0.1 linkdown
***
>> sudo ip route del 172.27.0.0/16 dev br-4273cb004f2a

# https://myateme.atlassian.net/wiki/spaces/IT/pages/4994105362/Network+issues+with+Docker
# Docker use temporary bridges which may cause IP overlap with Ateme network, to prevent using Ateme networks you can reconfigure docker like this with a network not used by Ateme :
# Edit the following file : cat /etc/docker/daemon.json and add the following lines :
# {
#   "bip":"192.168.63.1/24",
#   "default-address-pools": [
#     { "base": "192.168.64.0/20", "size": 24 }
#   ]
# }


"bip":"192.168.63.1/24",
"default-address-pools": [
    { "base": "192.168.64.0/20", "size": 24 }
]
```
