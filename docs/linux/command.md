# Commands

Here are soime useful commands

https://itsfoss.com/legacy-linux-commands-alternatives/

## Bat 

Enhanced `cat` command :

 - Syntax highlighting
 - Git integration
 - Show non-printable characters
 - ...

```
wget https://github.com/sharkdp/bat/releases/download/v0.19.0/bat_0.19.0_amd64.deb
dpkg -i bat_0.19.0_amd64.deb

bat INSTALL.md
bat INSTALL.md --color=always | less -R
git diff --name-only --diff-filter=d | xargs bat --diff
```

#### Links

- https://github.com/sharkdp/bat
- https://github.com/eth-p/bat-extras

> - batgrep : Quickly search through and highlight files using ripgrep. Requirements: ripgrep
> - batman : Read system manual pages (man) using bat as the manual page formatter.
> - batpipe : A less (and soon bat) preprocessor for viewing more types of files in the terminal.
> - batwatch : Watch for changes in one or more files, and print them with bat. Requirements: entr (optional)
> - batdiff : Diff a file against the current git index, or display the diff between two files. Requirements: bat, delta (optional)
> - prettybat : Pretty-print source code and highlight it with `bat`. Requirements: (see doc/prettybat.md)

## Exa 

Alternative to `ls`:

- more color friendly
- git file status support

https://github.com/ogham/exa

## fd

Alternative to `find` :

- Fast
- Intuitive
- ...

```bash
wget https://github.com/sharkdp/fd/releases/download/v8.3.1/fd_8.3.1_amd64.deb
sudo dpkg -i fd_8.3.1_amd64.deb 
fd CMakeLists.txt

```



## HTTPie

https://github.com/httpie/httpie

Make easier API dev life (instead of `wget` and `curl`)



## htop

https://carlchenet.com/htop-explique-partie-7-utilisation-de-la-memoire/
Usage de la mémoire – VIRT/RES/SHR/MEM

VIRT : La quantité totale de mémoire virtuelle utilisé par une tâche. Cela inclut tout le code, les données et les bibliothèques partagées plus les pages qui ont été mises dans la swap (ndt : sur disque) et les pages qui ont été mises sur disque mais pas utilisées.

**RES** : La taille de la mémoire physique non-mise en swap qu’une tâche utilise.

SHR : La quantité de mémoire partagée utilisée par une tâche.
Cela représente simplement la mémoire qui pourrait potentiellement être partagée par d’autres processus.

MEM : La part de la mémoire physique disponible couramment utilisée par une tâche.
Il s’agit de RES divisé par la totalité de la RAM que vous avez.
Si RES est de 400 megaoctets et que vous avez 8 gigaoctets de RAM, MEM% sera 400/8192*100 = 4.8% .
