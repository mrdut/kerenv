# Sound

 - `alsamixer`
 - `pavucontrol`
 - `xfce4-mixer`
 - `audacity`
 - `mhwaveedit`
 - `gnome-alsamixer`
 - `hdajackretask`   (package alsa-tools-gui)

```
➜  ~ acpi_listen
jack/lineout LINEOUT unplug
jack/lineout LINEOUT plug
jack/lineout LINEOUT unplug
jack/microphone MICROPHONE plug
jack/microphone MICROPHONE unplug
jack/lineout LINEOUT plug
jack/microphone MICROPHONE p
```

```
➜  kerenv git:(master) aplay -l
**** List of PLAYBACK Hardware Devices ****
card 0: SB [HDA ATI SB], device 0: ALC887-VD Analog [ALC887-VD Analog]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 0: SB [HDA ATI SB], device 1: ALC887-VD Digital [ALC887-VD Digital]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 3: HDMI 0 [HDMI 0]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 1: NVidia [HDA NVidia], device 7: HDMI 1 [HDMI 1]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
```


https://superuser.com/questions/1312970/headset-microphone-not-detected-by-pulse-und-alsa
https://www.ldlc.com/fiche/PB00147565.html
https://www.ldlc.com/fiche/PB00219359.html
https://www.ldlc.com/fiche/PB00184171.html

 ![jack pin](img/jacks.jpeg)