# Image

## Resize


```bash
# get image info
$ file 20220308_092449.jpg
20220308_092449.jpg: JPEG image data, Exif standard: [TIFF image data, big-endian, direntries=11, yresolution=146, xresolution=154, model=SM-G973F, height=3444, manufacturer=samsung, software=G973FXXSEFUL1, orientation=upper-left, datetime=2022:03:08 09:24:49, width=2732], baseline, precision 8, 2732x3444, components 3

# inplace
$ convert image.jpg -resize 600x400\> image.jpg

# new file
$ convert image.jpg -resize 600x400 image_resized.jpg

# batch resize
$ mkdir -p resized/ && ls *.jpg | xargs -L1 -I IMG convert IMG -resize 1800 resized/IMG

# set only width
$ convert a.jpg -resize 500x b.jpg

# scale
convert -resize 50% myfigure.png myfigure.jpg

# rotate
convert input.jpg -rotate 90 out.jpg

```

## Info

```bash
mediainfo img.png
identiy -verbose img.png
```

## White border

https://www.imagemagick.org/Usage/crop/#border

```bash
magick rose: -border 10%x10%  border_percent.jpg
convert -bordercolor white -border 20 input.jpg output.jpg
```


## Photo


 - Qualité : 300 dpi (dot per inch)
 - 1 inch = 2.54 cm
 - photo `10x15 cm` -> `4x6 inch` -> `1181×1772 pixels` -> `1200x1800 pixels`
 - [online pixel calculator](https://www.pixelcalculator.com/index.php?round=&FORM=1&DP=1&FA=&lang=fr&pix1=1181&pix2=1772&dpi2=300&sub2=+calculer+#a2)


Photo format in inches | Size in millimeters (mm) |   Size in pixels (300 dpi) | Aspect ratio (landscape)
-----------------------|--------------------------|----------------------------|--------------------------   
3,5x5                   |       89x127              |            1050x1500     |  10:7 (1.43)
4x6                     |       102x152             |           1200x1800      |  3:2 (1.5)
4,5x6                   |       114х152             |           1350x1800      |  4:3 (1.33)
5x7                     |       127х178             |           1500x2100      |  7:5 (1.4)
6x8                     |       152х203             |           1800x2400      |  4:3 (1.33)
8x10                    |       203х254             |           2400x3000      |  5:4 (1.25)
8x12 (≈A4)              |       203х305             |           2400x3600      |  3:2 (1.5)
10x15                   |       254x381             |           3000x4500      |  3:2 (1.5)
11x14                   |       279x356             |           3300x4200      |  14:11 (1.27)
12x16                   |       305х406             |           3600x4800      |  4:3 (1.33)
16x20                   |       406x508             |           4800x6000      |  5:4 (1.25)
20x24                   |       508x610             |           6000x7200      |  6:5 (1.2)

![Dimension tirage photo Bigger10](img/bigger10.jpg)

## Quality

https://www.photoweb.fr/espaces/magazine/resolution-photo/


![Photo](img/image_photos.png)
![Photo](img/image_posters.png)
![Photo](img/image_deco.png)

