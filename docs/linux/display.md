

cmd line : xrandr
GUI : lxrandr, arandr

xrandr --output DP-1 --mode 1920x1080 --right-of HDMI-1 

### current resolution

```
>> xrandr
```

### new resolution (no display connected -> x2goclient )

```
>>> xrandr --fb 1920x1080    (default is 1024x768)
```

### new resolution 

 - https://unix.stackexchange.com/questions/47749/cannot-add-new-mode-in-xrandr-for-external-monitor

```
>> gtf 1920 1080 60

  # 1920x1080 @ 60.00 Hz (GTF) hsync: 67.08 kHz; pclk: 172.80 MHz
  Modeline "1920x1080_60.00"  172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync

>> xrandr --newmode "1920x1080_60.00"  172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync
>> xrandr --addmode VGA_1 "1920x1080_60.00"
>> xrandr --output VGA_1 --mode "1920x1080_60.00"

```

## Troubleshooting

check xrandr --listmonitors - that you have another display there


```bash
➜  ~ xrandr --listmonitors
Monitors: 2
 0: +eDP-1 1920/344x1080/194+0+0  eDP-1
 1: +HDMI-1 1024/271x768/203+0+0  HDMI-1

➜  ~ 
➜  ~ xrandr --listmonitors
Monitors: 1
 0: +eDP-1 1920/344x1080/194+0+0  eDP-1


➜  ~ gtf 1680 1050 60

  # 1680x1050 @ 60.00 Hz (GTF) hsync: 65.22 kHz; pclk: 147.14 MHz
  Modeline "1680x1050_60.00"  147.14  1680 1784 1968 2256  1050 1051 1054 1087  -HSync +Vsync

➜  ~  xrandr --newmode "1680x1050_60.00"  147.14  1680 1784 1968 2256  1050 1051 1054 1087  -HSync +Vsync
➜  ~ xrandr --addmode HDMI-1 "1680x1050_60.00"

xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --output HDMI-1 --mode 1920x1200_60.00 --pos 1920x0

```


check which display manager you use
```bash
sudo systemctl status display-manager
```

https://askubuntu.com/questions/1234235/no-hdmi-output-neither-sound-nor-video-after-upgrading-to-20-04
➜  ~ uname -r
5.4.0-42-generic
