### copy file to clipboard

xclip -cli sel < FILE

### SOUND
 * `pavucontrol`
 * `alsamixer`

https://askubuntu.com/questions/457978/no-sound-from-chrome-sound-works-fine-in-firefox-etc-13-10

=> pavucontrol


write to user
1. ssh to host
1. who is logged in ?
`who -T` (-T is to see write permission)
1. what terminal am i using ?
`tty`
1. enable messages : `mesg y` / disable messages : `mesg n`
1. write : `echo "heyho" | write <USERNAME> <TTY>`




### DNF

whicch package does  this file belong to :
https://unix.stackexchange.com/questions/4705/which-fedora-package-does-a-specific-file-belong-to

 * list package content `sudo dnf repoquery -l <package>`
 * [disable repo (eg chrome repo...) ](https://docs.fedoraproject.org/en-US/Fedora/23/html/System_Administrators_Guide/sec-Managing_DNF_Repositories.html
)
```
dnf config-manager --set-enabled repository…
dnf config-manager --set-disabled repository…
```

* [convert .deb -> .rpm](https://www.systutorials.com/qa/212/how-to-install-deb-packages-on-fedora) :
```
alien -r package.deb
sudo yum localinstall package.rpm
```


### SSH


 * no more password

>1. generate key  (create id_rsa file) : `cd ~/.ssh & ssh-keygen`
>2. for each host : `ssh-copy-id USERNAME@IP + enter password`
that's it :)

* run command over ssh : `ssh USERNAME@IP "<CMD>"`

### swap

```
su -c "swapoff -a"
su -c "swapon -a"
```

### keyboard

* [change layout](http://www.tux-planet.fr/basculer-un-clavier-en-azerty-ou-en-qwerty-sous-linux/):
 - `setxkbmap fr`
 - `setxkbmap gb`
 - `setxkbmap gb -variant intl`
(if there is no server X, use `loadkeys fr` instead)

* list all layouts : `man -P cat xkeyboard-config`
* find layout variants : `localectl list-x11-keymap-variants uk`

[System_Locale_and_Keyboard_Configuration](https://docs.fedoraproject.org/f26/system-administrators-guide/basic-system-configuration/System_Locale_and_Keyboard_Configuration.html)
* list system settings : `localectl` or `/etc/locale.conf`
* list system keymaps : `localectl list-keymaps`
* change system keymaps : `localectl list-keymaps`


### Sound

 * `alsamixer`
 * `gnome-control-center sound`

### Window Manager

* restart : `restart xfce4 window manager : xfwm4 --replace`
* [How to determine which window manager is running](https://askubuntu.com/questions/72549/how-to-determine-which-window-manager-is-running) : ` wmctrl -m`
* [How to switch windows manager on-the-fly?](https://askubuntu.com/questions/159505/how-to-switch-windows-manager-on-the-fly)
```
xfwm --replace   # xfce, I think
metacity --replace   # default wm in Gnome 2
icewm --replace    # old-school wm, my favorite back in the 1990s before modern wms
```
