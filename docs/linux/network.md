# Network


## monitor

bmon
iftop

## Wifi

```
>> nmcli connection show
NAME                UUID                                  TYPE      DEVICE 
SFR-4998_5GHz       0eede857-52e5-4e4e-aa15-51bc8c245210  wifi      wlp5s0 
SFR-4998            f74590f4-21e6-40be-89b5-0ff501d8b1d1  wifi      --     
Wired connection 1  97022f9c-e7e1-3a87-a840-db91758cfc9d  ethernet  --     
```

```
>> nmcli dev wifi
IN-USE  SSID           MODE   CHAN  RATE        SIGNAL  BARS  SECURITY  
*       SFR-4998_5GHz  Infra  36    405 Mbit/s  45      ▂▄__  WPA1 WPA2 
```

https://www.cyberciti.biz/tips/linux-find-out-wireless-network-speed-signal-strength.html



## DNS

```bash
$ sudo echo "192.168.10.26 ipsotek_dns_server" >> /etc/hosts

$ ipsotek-vpn-start

$ nslookup vih0100080.headquarters.ipsotek.com
Server: 127.0.0.53
Address: 127.0.0.53#53

** server can't find vih0100080.headquarters.ipsotek.com: NXDOMAIN

$ nslookup vih0100080.headquarters.ipsotek.com ipsotek_dns_server
Server: ipsotek_dns_server
Address: 192.168.10.26#53

Name: vih0100080.headquarters.ipsotek.com
Address: 192.168.14.254

$ sudo vim /etc/resolv.conf
nameserver ipsotek_dns_server # first entry

# not necessary ? $ sudo systemctl restart network-manager

$ nslookup vih0100080.headquarters.ipsotek.com
Server: 192.168.10.26
Address: 192.168.10.26#53

Name: vih0100080.headquarters.ipsotek.com
Address: 192.168.14.254
```





### No network

```
================================================================================================================================================================

mrdut@dolmen:~$ ifconfig -a
enp2s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 60:a4:4c:56:be:86  txqueuelen 1000  (Ethernet)
        RX packets 897  bytes 177900 (177.9 KB)
        RX errors 0  dropped 220  overruns 0  frame 0
        TX packets 240  bytes 40256 (40.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 2603  bytes 186294 (186.2 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2603  bytes 186294 (186.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

================================================================================================================================================================

mrdut@dolmen:~$ cat /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback

## MRDUT UPDATE ##

# mrdut
auto enp2s0
iface enp2s0 inet dhcp


================================================================================================================================================================

mrdut@dolmen:~$ sudo lshw -c network -sanitize
[sudo] password for mrdut: 
  *-network                 
       description: Ethernet interface
       product: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
       vendor: Realtek Semiconductor Co., Ltd.
       physical id: 0
       bus info: pci@0000:02:00.0
       logical name: enp2s0
       version: 09
       serial: [REMOVED]
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=r8169 duplex=full firmware=rtl8168f-1_0.0.5 06/18/12 latency=0 link=yes multicast=yes port=MII speed=1Gbit/s
       resources: irq:38 ioport:e000(size=256) memory:f8004000-f8004fff memory:f8000000-f8003fff

================================================================================================================================================================

mrdut@dolmen:~$ sudo ifconfig enp2s0 up

================================================================================================================================================================

mrdut@dolmen:~$ lspci -nnk | grep -i -A2 net
02:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller [10ec:8168] (rev 09)
	Subsystem: ASUSTeK Computer Inc. P8 series motherboard [1043:8505]
	Kernel driver in use: r8169

================================================================================================================================================================

mrdut@dolmen:~$ iwconfig
lo        no wireless extensions.

enp2s0    no wireless extensions.

================================================================================================================================================================

mrdut@dolmen:~$ lsmod | grep r8169
r8169                  81920  0

================================================================================================================================================================

mrdut@dolmen:~$ nmcli d
DEVICE  TYPE      STATE      CONNECTION 
enp2s0  ethernet  unmanaged  --         
lo      loopback  unmanaged  --         

================================================================================================================================================================

mrdut@dolmen:~$ cat /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf_orig 
[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:wwan

## MRDUT UPDATE ##

[keyfile]
unmanaged-devices=*,except:type:wifi,except:type:wwan,except:type:ethernet

## ALTERNATIVE unmanaged-devices=none 


================================================================================================================================================================

# check cable ethernet is plugged
grep "" /sys/class/net/enp2s0/*

```