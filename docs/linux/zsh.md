### links


## Install

```bash
>> sudo apt-get install zsh
>> chsh -l
/bin/sh
/bin/bash
/usr/bin/sh
/usr/bin/bash
/bin/tcsh
/bin/csh
/usr/bin/tmux
/bin/zsh
>> chsh -s /bin/zsh

# https://github.com/ohmyzsh/ohmyzsh
>> sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```


[]()
[official zsh howto](https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org)
[official zsh man](http://webcache.googleusercontent.com/search?q=cache:4k9xMSV0CScJ:zsh.sourceforge.net/Doc/Release/Completion-System.html+&cd=1&hl=fr&ct=clnk&gl=fr)
[short (but good) tutorial](https://arcanis.me/en/2014/07/17/writting-own-completions-p1)
[another tutorial](http://www.linux-mag.com/id/1106/)
[many option format exemple](https://is.muni.cz/www/xsiska2/2014/08/05/generating-completing-functions.html)
[many exemples](https://github.com/zsh-users/zsh-completions/tree/master/src)

clear
https://unix.stackexchange.com/questions/280622/zsh-fails-at-path-completition-when-command-is-vim

## Complete

https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
https://zsh.sourceforge.io/Doc/Release/Completion-System.html#Completion-System

https://askql.wordpress.com/2011/01/11/zsh-writing-own-completion/
http://bewatermyfriend.org/p/2012/003/
https://tar-jx.bz/notes/zsh-completion.html
https://github.com/zsh-users/zsh/blob/master/Etc/completion-style-guide

### history


https://sanctum.geek.nz/arabesque/better-bash-history/
https://github.com/aaronharnly/dotfiles-public/blob/master/.bash-functions.sh#L263
https://til.hashrocket.com/posts/upc2hof2jf-reloading-shell-history-in-zsh