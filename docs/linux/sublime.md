# Sublime Text

## Usefull links

 - [sublime_text_regex_cheat_sheet](https://jdhao.github.io/2019/02/28/sublime_text_regex_cheat_sheet/)

## Installation

[Official installation guide](https://www.sublimetext.com/docs/3/linux_repositories.html)

## Settings

```json
{
    "close_windows_when_empty": false,
    "color_scheme": "Packages/User/SublimeLinter/Monokai (SL).tmTheme",
    "detect_indentation": false,
    "draw_white_space": "selection",
    "ensure_newline_at_eof_on_save": false,
    "font_size": 10,
    "highlight_line": true,
    "highlight_modified_tabs": true,
    "ignored_packages":
    [
        "Vintage"
    ],
    "indent_to_bracket": true,
    "index_files": true,
    "open_files_in_new_window": false,
    "rulers":
    [
        120
    ],
    "save_on_focus_lost": true,
    "tab_completion": false,
    "tab_size": 4,
    "translate_tabs_to_spaces": true,
    "trim_trailing_white_space_on_save": false,
    "use_gcc_sublime": false,
    "use_sublime_blueski": true
}
```

## Plugins

 - Theme : Monokai
 - CMake
 - FileBrowser
 - GitGutter
 - Mardown extended : extended syntax color
 - Move By Paragraph
 - PlainTask : https://github.com/aziz/PlainTasks
 - sublime LSP : https://lsp.sublimetext.io/guides/cplusplus/
   - CCLS : https://snapcraft.io/install/ccls/ubuntu#install


## Regex

 - multiline regex : start regex by `(?s)`
 - non-matching character : `[^abc]`

## Snippets

[detect non ascii character : `[^\x00-\x7F]`](https://stackoverflow.com/questions/13976022/is-there-a-way-to-highlight-all-the-special-accent-characters-in-sublime-text-or)

[chromium : Using Sublime Text as your IDE](https://www.chromium.org/developers/sublime-text)


```
sublime.log_commands(False)
```
