# MIME TYPE

## Howto


 - How to find the MIME type of a file

```
>>xdg-mime query filetype /work1/fdutrech/heyho.st3
text/plain
```

 - Howto identify the current default application for given MIME type

```
>> xdg-mime query default text/plain
kwrite.desktop
```

 - Howto list existing application

```
ls -l /usr/share/applications/
...
-rw-r--r--  1 root root   532 Apr  6 02:58 sublime_text.desktop
...
ls -l ~/.local/share/applications
...
-rw-r----- 1 fdutrech asygn 170 Mar 24  2017 userapp-st3-wrapper-U9GJXY.desktop
...
```

```
>> gnomevfs-info /tmp/sublime_wrapper_file.st3
Name              : sublime_wrapper_file.st3
Type              : Regular
MIME type         : application/x-st3
Size              : 31
Blocks            : 8
I/O block size    : 4096
Local             : YES
SUID              : NO
SGID              : NO
Sticky            : NO
Permissions       : 600644
Link count        : 1
UID               : 1002
GID               : 1007
Access time       : Tue Dec  3 21:33:02 2019
Modification time : Tue Dec  3 21:21:26 2019
Change time       : Tue Dec  3 21:21:26 2019
Device #          : 64772
Inode #           : 21
Readable          : YES
Writable          : YES
Executable        : NO
```



## Create new MIME type

https://docs.oracle.com/cd/E26502_01/html/E28056/glvax.html#scrolltoc
https://linuxcommando.blogspot.com/2014/03/how-to-specify-default-applications-for.html


```
>> gnomevfs-info /tmp/sublime_wrapper_file.st3
...
MIME type         : text/plain  <--- default MIME type
...
```

1. create new MIME type


```
>> cat ~/.local/share/mime/packages/x-st3.xml

# NEW VERSION
<?xml version="1.0" encoding="utf-8"?>
<mime-info xmlns='http://www.freedesktop.org/standards/shared-mime-info'>
<mime-type xmlns="http://www.freedesktop.org/standards/shared-mime-info" type="application/x-st3">
  <!--Created automatically by update-mime-database. DO NOT EDIT!-->
  <comment>foo file</comment>
  <glob-deleteall/>
  <glob pattern="*.st3"/>
</mime-type>
</mime-info>

# OLD
<?xml version="1.0" encoding="utf-8"?>
<mime-type xmlns="http://www.freedesktop.org/standards/shared-mime-info" type="application/x-st3">
  <!--Created automatically by update-mime-database. DO NOT EDIT!-->
  <comment>foo file</comment>
  <glob-deleteall/>
  <glob pattern="*.st3"/>
</mime-type>
```

1. Update mime database

```
update-mime-database ~/.local/share/mime/
```

1. [Create new desktop application](https://developer.gnome.org/integration-guide/stable/desktop-files.html.en)

```
>> cat ~/.local/share/applications/sublime_wrapper.desktop
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
NoDisplay=true
Exec=/work1/fdutrech/st3-wrapper %f
Name=st3-wrapper
Comment=Custom definition for st3-wrapper
```


1. set the default application

```
>> xdg-mime default sublime_wrapper.desktop application/x-st3
```


check for changes

```
>> gnomevfs-info /tmp/sublime_wrapper_file.st3
...
MIME type         : application/x-st3
...
```

default open it !

```
gio open /tmp/sublime_wrapper_file.st3
```


## Troubleshouting
pt-get install libfile-mimeinfo-perl
yum install perl-File-MimeInfo
https://unix.stackexchange.com/questions/9856/why-does-xdg-mime-query-filetype-fail-to-find-a-new-added-file-type?rq=1
https://unix.stackexchange.com/questions/283601/xdg-resolves-filename-as-text-plain





----------------------------------------------------------------------
>>  find ~/ -name "*.desktop"                                                                                                           ...
/nfs/home/fdutrech/.local/share/applications/chrome-ejjicmeblgpmajnghnpcppodonldlgfn-Default.desktop
/nfs/home/fdutrech/.local/share/applications/chrome-blpcfgokakmgnkcojhhkbfbldkacnbeo-Default.desktop
/nfs/home/fdutrech/.local/share/applications/chrome-lneaknkopdijkpnocmklfnjbeapigfbh-Default.desktop
/nfs/home/fdutrech/.local/share/xfce4/helpers/google-chrome.desktop
/nfs/home/fdutrech/Downloads/sublime_text_3_3126/sublime_text.desktop

>> ln -s /nfs/home/fdutrech/Downloads/sublime_text_3_3126/sublime_text.desktop /nfs/home/fdutrech/.local/share/applications -v

>> xdg-mime query default text/x-c++src
emacsclient.desktop

>> grep src /usr/share/applications/mimeinfo.cache
text/x-c++src=emacsclient.desktop;emacs.desktop;
text/x-csrc=emacsclient.desktop;emacs.desktop;

>> ln -s SUBLIME /opt/sublime_text
>> ln -s SUBLIME/sublime.desktop /usr/share/applications

in thunar, right-click, open with, set default



>> su -c  "desktop-file-install ./st3.desktop"
Add Comment
Florent (fdutrech)

Write a comment…
Save
ActivityHide Details
Florent (fdutrech)
Florent moved this card from ExtraEyes to Linux
Apr 12, 2017 at 11:15 AM
Florent (fdutrech)
Florent
>>  mimetype query /tmp/heyho.st3
query:
/tmp/heyho.st3: application/x-st3
>>  xdg-mime query default application/x-st3
userapp-st3-wrapper-0P7RXY.desktop
>> cat ~/.local/share/applications/userapp-st3-wrapper-0P7RXY.desktop
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
NoDisplay=true
Exec=/work1/fdutrech/st3-wrapper %f
Name=st3-wrapper
Comment=Custom definition for st3-wrapper
>> cat /work1/fdutrech/st3-wrapper
#! /usr/bin/bash

if [ $# == 1 ]; then
    /opt/sublime_text/sublime_text $(cat $1)
fi
Mar 24, 2017 at 4:26 PM (edited) - Edit - Delete -
Florent (fdutrech)
Florent
create script to parse new file format *.st3:
eg:

>> cat /tmp/heyho.st3
filename:line
in thunar, rightclick, open custom command







https://stackoverflow.com/questions/2060284/how-to-use-the-xdg-mime-command

/nfs/home/fdutrech/.local/share/applications/userapp-st3-wrapper-0P7RXY.desktop
 1 [Desktop Entry]
  2 Encoding=UTF-8
  3 Version=1.0
  4 Type=Application
  5 NoDisplay=true
  6 Exec=/work1/fdutrech/st3-wrapper %f
  7 Name=st3-wrapper
  8 Comment=Custom definition for st3-wrapper


https://askubuntu.com/questions/714196/how-to-set-the-default-program-to-open-a-certain-file-type-in-a-certain-folder


>> cat ~/.local/share/mime/packages/x-st3.xml                                                                                                                                                 (12:18)
<?xml version="1.0" encoding="utf-8"?>
<mime-type xmlns="http://www.freedesktop.org/standards/shared-mime-info" type="application/x-st3">
  <!--Created automatically by update-mime-database. DO NOT EDIT!-->
  <comment>foo file</comment>
  <glob-deleteall/>
  <glob pattern="*.st3"/>
</mime-type>
