# Terminator

https://launchpad.net/terminator/+download
https://terminator-gtk3.readthedocs.io/en/latest/

### Install 

Recommanded way :

```
>> yum install terminator
>> terminator --version
0.98
```

Install last version
```
>> yum remove terminator
>> wget https://launchpad.net/terminator/gtk3/1.91/+download/terminator-1.91.tar.gz
>> tar -xzf terminator-1.91.tar.gz
>> cd terminator-1.91
>> ./setup.py build
>> ./setup.py install --record=install-files.txt

# uninstall
>> ./setup.py uninstall --manifest=install-files.txt
```
