# Audacity

## Generate signal

 - Tracks > Add New > Mono Track
 - Generate > Tone: Sine, Amplitude 0.316227 (volume standard), Frequency (!! LFE freq basse sinon coupée)

## Duplicate track

 - Edit > Preferences: Import/Export > Usae advanced mixing options
 - File > Export > Export as WAV

## Spectrum

 - Analyse > Plot Spectrum
 - Track: Audio Track > Spectogram