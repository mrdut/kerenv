
# Packages

## Usefull :
 - package content : see `dnf repoquery`
 - which package provide file : `dpkg -S (--search) /bin/ls`


## Apt

```bash
# find which package provide a file (https://www.cyberciti.biz/faq/equivalent-of-rpm-qf-command/)
#  - not installed on my Debian/Ubuntu system -> https://packages.ubuntu.com
#  - installed :
dpkg-query -S '/bin/ls'

# list installed packages
$ apt list --installed
Listing...
accountsservice/focal-updates,focal-security,now 0.6.55-0ubuntu12~20.04.5 amd64 [installed,automatic]
acl/focal,now 2.2.53-6 amd64 [installed,automatic]
adduser/focal,now 3.118ubuntu2 all [installed,automatic]
adwaita-icon-theme/focal-updates,now 3.36.1-2ubuntu0.20.04.2 all [installed,automatic]
alsa-base/focal,now 1.0.25+dfsg-0ubuntu5 all [installed]
...

# list available version (==madison)
$ apt list -a PACKAGE
Listing... Done
cuda/unknown 11.6.1-1 amd64
cuda/unknown 11.6.0-1 amd64
cuda/unknown 11.5.2-1 amd64
cuda/unknown 11.5.1-1 amd64
...
$ sudo apt install --dry-run cuda=11.5.1-1

# list packagae depency
$ apt rdepends PACKAGE

# reverse list packagae depency
$ apt depend (--recurse) PACKAGE

# from which repo
$ apt policy PACKAGE
cuda:
  Installed: (none)
  Candidate: 11.6.1-1
  Version table:
     11.6.1-1 600
        600 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64  Packages
     11.6.0-1 600
        600 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64  Packages
     11.5.2-1 600
        600 https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64  Packages
...

# fake run
$ apt install PACKAGE --dry-run
```



### Trouble shouting

Remove package with unmet dependency : `sudo apt-get checksudo apt purge -f <PACKAGE>`



sudo dpkg --configure -a
 sudo dpkg -P PACKAGE (if hold by dependency, remove depency first)



#### Unmet Dependencies

Unmet Dependency means that the package you are trying to install is looking for “dependencies” that it cannot find in the current version. The simple way to fix this error is to update the package database, clean out the package cache, and download-and-reinstall the newer version.

```
sudo apt-get autocleansudo apt-get -f installsudo dpkg --configure -asudo apt-get -f install
```

If the output is: 0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded. That means the solution failed and you will have to run the following commands to fix the error.



#### Method 1

```
sudo apt-get updatesudo apt-get -u dist-upgradesudo apt-get -o Debug::pkgProblemResolver=yes dist-upgradesudo apt-get remove --dry-run package-name
```

#### Method 2

```bash
sudo apt-get updatesudo apt-get -u dist-upgradesudo apt-get clean package-namesudo apt-get install --reinstall package-name
```

## Dpkg

-c : list content







## DNF

Basic | -
:----------- |:-------------
install |   `dnf install <package> (--assume-yes)`
remove |    `dnf remove <package>`
search |    `dnf search <package>`
info |  `dnf info <package>`
list |  `dnf list installed`

Repo | -
:----------- |:-------------
list |  `dnf repolist all`
disable |  `dnf --enablerepo=<repo> install <package>`
enable |  `dnf --disablerepo=<repo> install <package>`

Advanced | -
:----------- |:-------------
history | `dnf history`
download rpm | `dnf download <package>`


debuginfo | -
:----------- |:-------------
 update | `dnf --enablerepo=updates-debuginfo update`
 install |   `sudo dnf debuginfo-install <packet>`
 list | `sudo dnf list --installed | grep debuginfo`

## RPM

Basic | -
:----------- |:-------------
install | `rpm -ivh <package>.rpm`
install with dep | `dnf install <package>.rpm`
list | `rpm -qa`
details |  `rpm -qi <package>.rpm`
remove |  `rpm -e <package>.rpm`
content |  `dnf repoquery -l packagename` or `rpm -qlp <package>.rpm`
list installed packages by size  | `rpm -qa --queryformat '%10{size} - %-25{name} \t %{version}\n' | sort -n`

### Links

* [dnf](https://www.rootusers.com/25-useful-dnf-command-examples-for-package-management-in-linux/)
* [rpm](http://dnf.readthedocs.io/en/latest/command_ref.html)


## Yum


### Repo

```bash
# add repo
yum-config-manager --add-repo  URL.repo

# list repo
yum repolist [enabled (default)/disabled/al/]

# enable/disable repo
yum-config-manager --enable(disable) REPO_NAME
```

### Package



```bash

# install package
yum install PACKAGE

# remove package
yum remove PACKAGE

# update packages
yum check-update
yum update PACKAGE
yum update

# search package
yum search NAME1 NAME2...

# list package
yum list [installed/available/all] PATTERN

# list package version
yum --showduplicates list PACKAGE

# package info
yum info PACKAGE

# package content
repoquery --list package_name
# See the files installed by a yum package named bash:
repoquery -l '*bash*'


# which package owns a file
yum provides "*bin/named"

# which package provides a file
yum whatprovides <FILE>

```

### Link

[redhat yum](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/ch-yum)