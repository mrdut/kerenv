# Systemctl

systemctl is-enabled SERVICE-NAME

    Check if a service is enabled or disabled.
systemctl status SERVICE-NAME

    Check if a service is running, stopped, enabled, or masked, and display the most recent log entries.
systemctl enable SERVICE-NAME

    Enable a service, without starting it. It will start automatically at the next system restart, or it can be started manually, or as a dependency of another service.
systemctl enable --now SERVICE-NAME

    Enable a service and start it immediately.
systemctl disable SERVICE-NAME

    Disable a service. If it is running, it will continue to run until it is stopped manually. It will not start at the next system restart, but can be started manually, or as a dependency of another service.
systemctl disable --now SERVICE-NAME

    Disable a service and stop it immediately.
systemctl re-enable SERVICE-NAME

    Stop and restart a service, and restore its default start behavior.
systemctl mask SERVICE-NAME

    Mask the service so that it cannot be started by any means. It must be stopped manually, or it will continue to run, possibly in an inconsistent state. The mask command does not take the --now option.
systemctl unmask SERVICE-NAME

    Unmask the service. It will start after a system restart, or start it manually. The unmask command does not take the --now option.