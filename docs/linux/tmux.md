# Tmux

## Snippets

tmux capture-pane -peS -1000000 > file.out

## Conf

~/.tmux.conf

```
# enable mouse scrolling
set -g mode-mouse on
```



https://www.rushiagr.com/blog/2016/06/16/everything-you-need-to-know-about-tmux-copy-pasting-ubuntu/