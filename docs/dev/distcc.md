http://0pointer.de/blog/projects/avahi-distcc.html

# Installation

### Master

> install packages

```
sudo dnf install --assumeyes distcc distcc-server
```

> set hosts

```
echo "localhost 172.16.128.59/24 172.16.128.66/24 " >> ~/.distcc/hosts
```

Set the addresses either in :
 - environment variable `DISTCC_HOSTS`
 - configuration file `$DISTCC_HOSTS`
 - configuration file `$DISTCC_DIR/hosts`
 - configuration file  `~/.distcc/hosts`
 - configuration file  `/etc/distcc/hosts`

### Hosts

> install packages

```
sshpass -p <ROOT_PWD> ssh root@<HOST_IP> dnf install --assumeyes distcc distcc-server
```

> open port

```
(root) >> firewall-cmd --add-port=3632/tcp --permanent
(root) >> firewall-cmd --reload
(root) >> systemctl restart firewalld
```

> launch daemon

```
(root) >> distccd  --allow <IP>/<NTHREADS> --daemon --port 3632
```

 - `--daemon` Listen on a socket.
 - `--port` listen on given port number (default  is 3632)
 - `--allow` : restrict access - What address range should distccd accept connections from. The example above allows all connections from addresses starting with 192.168. The second one also allows connections from the specific address 127.0.0.1 which we want too.

 - `--no-detach` Do not detach from the shell you started it in. This enables you to end the daemon with ctrl-c etc. Alternatively, use killall -9 distccd to kill all daemons.
 - `--log-stderr` Output a log to the console.
 - `--verbose` Use this initially to debug issues until you are satisfied it is working.
 - `--log-level LEVEL`  [critical,error,warning, notice, info, debug]
 - `--log-file FILE` (leaving it blank will log to syslog)

over vpn : add tun0 address as well as wlan0

### Usage

* get local IP address:
get ip address in CIDR format
$ watch distccmon-tex
# get ip address in CIDR format
$ ip addr show

* manually : before
* ccache :  `export CCACHE_PREFIX="distcc"`

export DISTCC_FALLBACK=0/1
export DISTCC_VERBOSE=0/1
export DISTCC_BACKOFF=0/1
export DISTCC_DEBUG=0/1 -> TODO

# Usage

### what are my hosts ?

> list current hosts

```
distcc --show-hosts
```

# what is going on ? (from Master)

> watch host activity

```
watch distccmon-text
```

> add verbosity

```
export DISTCC_VERBOSITY=1
```

> disable fallback (do not run on localhost on fail)

```
export DISTCC_FALLBACK=0
```


### Troubleshouting

* no route to host : port is open ? (test it : `nc -zv <IP> <PORT>`)
* connection refused : distcc started on host ?

#### backoff

```
# disable backoff period
export DISTCC_BACKOFF_PERIOD=0

# set backoff period (in seconds)
export DISTCC_BACKOFF_PERIOD=1

# backoff directory
`~/distcc` or `$DISTCC_DIR/` if set
```

# Links

* [github official](https://github.com/distcc/distcc/blob/master)
* [official website](https://cdn.rawgit.com/distcc/distcc/9a09372bd3f420cdd7021e52eda14fa536a3c10e/doc/web/index.html)
* [man page](https://cdn.rawgit.com/distcc/distcc/master/doc/web/man/distcc_1.html)
* [tutorial](https://pspdfkit.com/blog/2017/crazy-fast-builds-using-distcc/)
* [archi linux page](https://wiki.archlinux.org/index.php/Distcc)
