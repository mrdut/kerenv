# Golang

## Install

```bash
#https://golang.org/dl/
wget https://golang.org/dl/go1.15.2.linux-amd64.tar.gz

#https://golang.org/doc/install
tar -C /usr/local -xzf go1.14.3.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
go version
```

### Advantages

 - **fast** compiled language
 - built-in test support
 - built-in installer support
 - concurrency support

### Disadvantages

 - image processing
 - data science


 - [Youtube : The Go Language: What Makes it Different? - Jay McGavren](https://www.youtube.com/watch?v=FEFXjRoac_U&t=1563s)

## Perf

https://github.com/dgryski/go-perfbook/blob/master/performance.md



## Snippets

```go
log.Info().Msgf("ismClient req=%+v", req)
```

