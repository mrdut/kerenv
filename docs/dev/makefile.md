# Makefile

https://makefiletutorial.com/

filename, basename, ...
https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html

Makefile:15: *** missing separator.  Stop.

Makefile command must start with a TAB (and not 4 spaces...)

https://forum.sublimetext.com/t/tabs-in-makefiles-only/1443/2
https://stackoverflow.com/questions/16931770/makefile4-missing-separator-stop

check is starts with TAB :
cat -e -t -v  modules/jsoncpp/Makefile

## Troubleshooting

```
Makefile:17: *** missing separator.  Stop.
```
 - Makfile needs tabls, not 4 spaces
 - `cat -e -t -v Makefile` -> `^I` == tab
 - see https://stackoverflow.com/questions/16931770/makefile4-missing-separator-stop