# Style

### 


[juce](https://juce.com/discover/stories/coding-standards)
[QT](https://wiki.qt.io/API_Design_Principles)


[clang format](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)
```
find src/ -iname "*.hpp" -o -iname "*.cpp" | xargs clang-format -i -style=src/.clang-format
```

### Best practices 

 - use `unique_ptr` when possible over `shared_ptr`
 - pass object by referece or pointers (if optional), not `unique_ptr` or `shared_ptr`, nobody cares how you manage your object memory...
 - [use `std::unordered_map` when possible over `std::map`](https://www.geeksforgeeks.org/map-vs-unordered_map-c/)
 - "Many minor coding issue leads to major performance drop"
 - `override`, `final`, ... not usefull for compiler but for readability
 - Interface classe : Interface suffix
 - **every comment is a failure to express yourself in code**
 - https://youtu.be/JMrfqFwm_wA
 - https://wiki.qt.io/D-Pointer
 - [From STUPID to SOLID Code!](https://williamdurand.fr/2013/07/30/from-stupid-to-solid-code/)


 - re-evaluate rules periodically :)