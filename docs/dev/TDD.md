# Test Driven Development

 - Écrire un test,
 - Vérifier qu’il échoue,
 - Écrire le code suffisant pour que le test passe,
 - Vérifier que le test passe,
 - Optimiser le code et vérifier qu’il n’y ait pas de régression.