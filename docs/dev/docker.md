# Docker

```
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
$ newgrp docker 
```

```bash
# list running dockers
docker ps
CONTAINER ID        IMAGE                                                                                           COMMAND                  CREATED             STATUS              PORTS                                                  NAMES
6fa9b3f350dd        172.16.118.137:8081/mivision-docker-repo/xbd-vision/human-model/generic:BREBD-3493-hpar_l1      "/bin/sh -c 'echo Wa…"   44 seconds ago      Up 42 seconds       0.0.0.0:33237->47190/tcp                               dutrechf_human-model_1
b808b20cf359        172.16.118.137:8081/mivision-docker-repo/xbd-vision/inference-reid-attributes/turing:master     "/bin/sh -c 'echo 'W…"   46 seconds ago      Up 44 seconds       0.0.0.0:33236->6896/tcp                                dutrechf_inference-reid-attributes-0_1
...

# show docker container logs
docker logs dutrechf_human-model_1

```



clean dockers :

```
docker ps | grep 'weeks ago' | awk '{print $1}' | xargs --no-run-if-empty docker kill
docker ps -aq -f status=exited | xargs docker rm
docker images -q --filter dangling=true  | xargs -r docker rmi
```

run centos 7 vision docker :

```
docker login gitlab.asygn.com:5678
docker pull gitlab.asygn.com:5678/bluecime/devops/dockerimages/ci_centos_vision
docker run --rm=true -it --privileged -v /bluecime/videos/:/bluecime/videos/:ro -v /bluecime/publicRW/:/bluecime/publicRW/:rw gitlab.asygn.com:5678/bluecime/devops/dockerimages/ci_centos_vision /bin/bash
```

## Inspect

```bash
docker inspect DOCKER_IMAGE
docker history DOCKER_IMAGE
dive DOCKER_IMAGE
```

## Tags

```bash
# create tag
docker tag DOCKER_IMAGE NEW_DOCKER_IMAGE

# remove tag
docker rm DOCKER_IMAGE:TAG
```

## Free space

```
 >> docker sytem prune
 >> docker volume prune
```



### Troubleshooting

```
>> docker ps -a
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json?all=1: dial unix /var/run/docker.sock: connect: permission denied

(root) >> usermod -a -G docker dutrechf
reload session
```

```bash
# build docker file with proxy
>> docker build . --build-arg=HTTP_PROXY=...

```