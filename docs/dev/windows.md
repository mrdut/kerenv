# Windows

https://docs.microsoft.com/en-us/windows/win32/winprog/windows-data-types?redirectedfrom=MSDN
VOID -> void

winapi
WINAPI is a macro that evaluates to __stdcall, a Microsoft-specific keyword that specifies a calling convention where the callee cleans the stack. 
The function's caller and callee need to agree on a calling convention to avoid corrupting the stack.


LPSTR
https://softwareengineering.stackexchange.com/questions/194764/what-is-lpctstr
LPSTR = char*
LPCSTR = const char*
LPWSTR = wchar_t*
LPCWSTR = const wchar_t*
LPTSTR = char* or wchar_t* depending on _UNICODE
LPCTSTR = const char* or const wchar_t* depending on _UNICODE




https://docs.microsoft.com/en-us/windows/win32/winprog/windows-data-types?redirectedfrom=MSDN
https://docs.microsoft.com/en-us/windows/win32/api/winsvc/ns-winsvc-service_status
https://docs.microsoft.com/en-us/windows/win32/api/