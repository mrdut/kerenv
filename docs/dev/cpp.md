# Cpp tricks and tips

### Books

The linux programming interface
Standard C++ IOStreams and Locales: Advanced Programmer's Guide and Reference
C++ Templates: The Complete Guide (2nd Edition) 2nd Edition

### Style

https://google.github.io/styleguide/cppguide.html




### Best practices 

 - use `unique_ptr` when possible over `shared_ptr`
 - pass object by referece or pointers (if optional), not `unique_ptr` or `shared_ptr`, nobody cares how you manage your object memory...
 - [use `std::unordered_map` when possible over `std::map`](https://www.geeksforgeeks.org/map-vs-unordered_map-c/)
 - "Many minor coding issue leads to major performance drop"
 - `override`, `final`, ... not usefull for compiler but for readability
 - Interface classe : Interface suffix
 - **every comment is a failure to express yourself in code**
 - https://youtu.be/JMrfqFwm_wA
 - https://wiki.qt.io/D-Pointer
 - [From STUPID to SOLID Code!](https://williamdurand.fr/2013/07/30/from-stupid-to-solid-code/)


 - re-evaluate rules periodically :)

### Tools



### Boost

 - you need a lifetime to understand boost
 - you will result in a program that takes ages to compile, and works you don't know how
 - boost was very usefull before c++11 - not anymore
 - if you need matches, boost provides you flamethrower, which works as well with gas, fuel, wood or nuclear power.

 - need grubbs test stuff ? include the whole boost math package !!


### Casting

 - _c-style_ casting : `(TYPE) expr` or `TYPE(expr)`

It can cast prtaically any type to any other type. Improper usage can lead to disatrous results. 
Moreover, it is hard for both compiler and user to know what is the purpose of the cast.
_c++ style_ casting is more explicit.

 - `static_cast` : conversion that can be done at compile-time
 - `const_cast`
 - `dynamic_cast`
 - `reinterpret_cast`

### Template



##### how to fully specialize nested class ?

    ---------
    | WRONG |
    ---------

        * code :

            template<typename A> struct Hey
            {
                template<typename B> struct Ho;     <- the nested class we want to fully specialize
            };

            template<typename A>            <= (1) enclosing class templates are not explicitly specialized
            template<>                      <= (2) explicit specialization
            struct Hey<A>::Ho<int> {};

        * gcc output :

            error: invalid explicit specialization before ‘>’ token
                 template<>
                          ^
            error: enclosing class templates are not explicitly specialized
            error: template parameters not deducible in partial specialization:
                 struct Hey<A>::Ho<int> {};
                                ^
    --------
    | GOOD |
    --------

        * code :

            template<typename A> struct Hey
            {
                template<typename B, bool Dummy=true> struct Ho;  <- add Dummy template parameter
            };

            template<typename A>
            template<bool Dummy>                      <= no more explicit specialization, but partial specialization
            struct Hey<A>::Ho<int, Dummy> {};

##### how to specialize nested class (that was already specialized) for a specific parent class ?

    ---------
    | WRONG |
    ---------

        * code :

            template<typename A>
            struct Hey
            {
                template<typename B, bool Dummy=true> struct Ho;
            };

            // specialization 1
            template<typename A>   // generic Hey
            template<bool Dummy>   // specialize Ho for `int`
            struct Hey<A>::Ho<int, Dummy> { static inline void hein() { std::cout << "Hey<generic>::Ho<int>\n"; } };

            // specialization 2
            template<>             // specialize Hey for `int`
            template<bool Dummy>   // specialize Ho for `int`
            struct Hey<float>::Ho<int, Dummy> { static inline void hein() { std::cout << "Hey<float>::Ho<int>\n"; } };

            int main()
            {
                Hey<void>::Ho<int>::hein();    // we expect specialization 1
                Hey<float>::Ho<int>::hein();   // we expect specialization 2
            }

        * output:

            Hey<generic>::Ho<int>
            Hey<generic>::Ho<int>   <- WTF ??


    --------
    | GOOD |
    --------

        * code :

            template<typename A> struct Hey
            {
                template<typename B, typename C=A> struct Ho;
            };

            template<typename A>
            template<typename C>
            struct Hey<A>::Ho<int, C> { static inline void hein() { std::cout << "Hey<generic>::Ho<int>\n"; } };

            template<>
            template<>
            struct Hey<float>::Ho<int> { static inline void hein() { std::cout << "Hey<float>::Ho<int>\n"; } };

            int main()
            {
                Hey<void>::Ho<int>::hein();    // we expect specialization 1
                Hey<float>::Ho<int>::hein();   // we expect specialization 2
            }

        * output:

            Hey<generic>::Ho<int>
            Hey<float>::Ho<int>
