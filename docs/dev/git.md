# Git

https://www.atlassian.com/git/tutorials
http://marklodato.github.io/visual-git-guide/index-en.html



### speed up

- `git status -uno` (do not discover untracked file, iseful for oh-my-zsh prompt...)
- [`git gc`](https://git-scm.com/docs/git-gc) + `git clean` (`git config --global gc.auto 0`)
 - link [stackoverflow](https://stackoverflow.com/questions/4994772/ways-to-improve-git-status-performance)

## Log

```
# show all commits history for given file
git --no-pager  log --format=format:%H   -- <FILENAME>
```

## Show

```
git show <ID=commit, HEAD~4, ...>:<FILENAME>
```

## History 

```
git log --pretty=oneline --graph --decorate --all
git log --pretty=oneline --graph --decorate -n 20
```

## Diff

### Usage

* difference between edited files and current HEAD

```
git diff
```

* difference between edited files and current HEAD for given FILE

```
git diff -- FILENAME
```

* difference between edited files and BRANCH

```
git diff master
```

### Options

- **--no-pager** : echo to terminal (between git and git-action keyword) - see config for default
- **--name-only** : echo only filename
- **--stat** : ??? (to be used with --name-only)
- **--unified=0** or **-U0** : does not show context line (only diff lines)

# Ignore

 - local : `REPO/.gitignore`
 - user local : `REPO/.gitignore`REPO/.git/info/exclude
 - global : `~/.config/git/ignore`

# Clean

### Remove not tracked files

```
git clean -n # dry run : SEE wha files will be removed 
git clean -f # REMOVE files
```

# LFS

    - [install](https://help.github.com/en/articles/installing-git-large-file-storage)
    - track given type as LFS : `git lfs track "*.<TYPE>"`
    - list LFS files : `git lfs ls-files`
    - by defaut, on clone we download (possibly) not necessary large files - to avoid this : : `GIT_LFS_SKIP_SMUDGE=1 git clone ...`

Migration Tool : https://rtyley.github.io/bfg-repo-cleaner/

# Patch

### Create patch

```
git --no-pager diff master --patch > PATCH_FILE
git --no-pager diff SHA1-1 SHA1-2  --patch > PATCH_FILE
```

### Apply patch

```
git apply PATCH_FILE
```

### Revert patch

```
git apply -R  PATCH_FILE
```


# Config

https://www.jvt.me/posts/2019/03/20/git-rewrite-url-https-ssh/

```
# Use SSH instead of HTTPS for all repo ~/.gitconfig: 
[url "git@bitbucket.sdmc.ao-srv.com:"]
        insteadOf = https://bitbucket.sdmc.ao-srv.com/scm/


git config --global fetch.prune true
git config --global pull.rebase true

git config --global core.askPass
git config --local http.sslverify false
git config --global credential.helper "cache --timeout=36000"
# or store permanently : git config --global credential.helper "store"
# some useful alias
alias gco="git checkout"; alias gci="git commit"; alias gst="git status"; alias gadd="git add"; alias gdiff="git diff"; alias gb="git branch"; alias gba="git branch --all";

# push only current branch
#fatal: The current branch BREBD-5048_koios_install has no upstream branch.
#To push the current branch and set the remote as upstream, use
#
#    git push --set-upstream origin BREBD-5048_koios_install
# DEPRECATED ? git config --global push.default simple
git config --global push.default current



# add color to git diff, ...
git config --global color.ui auto

# set vim as default editor
git config --global core.editor "vim -c 'startinsert'"

OR

# set subl as default editor (mind the --wait : it allows to type text in sublime and will wait for save/close event)
git config --global core.editor "'subl' --wait"

# do not use pager by default
git config --global core.pager cat
# but use pager for git diff
git config --global pager.diff "less -x1,5"
# same with clear screen on diff exit
git config --global pager.diff "less -+X -+F"

# ~/.vimrc
" hit <mapleader> to (quit edit mode) + save and exit "
let mapleader = "`"
inoremap <leader><leader> <esc>:xa<cr>
nnoremap <leader><leader> :xa<cr>
set number


```

# Snippets


## ignore file change

```
git update-index --assume-unchanged units/TitanDvbSubtitlesEncoder/build/TitanDvbSubtitlesEncoder-linux/CMakeLists.txt
git update-index --no-assume-unchanged units/TitanDvbSubtitlesEncoder/build/TitanDvbSubtitlesEncoder-linux/CMakeLists.txt
```

## get directory authors

```
➜  TitanProcessing git:(tp-4964) git shortlog -n -s -- units/TitanOttManager/
    49  daloe
    14  Julien Lapebie
    12  Étienne Dupuis
     5  Aurélien Nephtali
     5  Valentin Besnard
     4  Kevin Salmon
     4  Nicolas Ollivier
     3  Theo Vitry
```

## Tag

```bash
git tag                             # list tags
git tag v1.2.2                      # create new tag
git tag -d v1.2.2                   # delete local tag
git push --delete origin v1.2.2     # delete origin tag
git push origin v1.2.2              # push single tag
git push --tags                     # push all tags                  
```

### print current branch (no `*`)

`git rev-parse --abbrev-ref HEAD`

### print current SHA1

`git rev-parse HEAD`

## Deleting remote branches in Git

`$ git push origin --delete feature/login`

## pritn branches authors and date

```
git for-each-ref --format='%(color:cyan)%(authordate:format:%m/%d/%Y %I:%M %p)    %(align:25,left)%(color:yellow)%(authorname)%(end) %(color:reset)%(refname:strip=3)' --sort=authordate refs/remotes
01/17/2019 02:54     ARNAISE, NICOLAS       debug_img
...
02/04/2019 03:41     ARNAISE, NICOLAS       BREBD-2406_set_jpg_quality
04/29/2019 04:51     youri.chabert@atos.net bib-number-dev
04/29/2020 10:05     DAVIN, YANN            master
```

## print stat about current line editing by authors

```
git ls-files | while read f; do git blame --line-porcelain $f | grep '^author '; done | sort -f | uniq -ic | sort -n -k 1
```

## force replace local branch by origin branch

```
git reset --hard origin/BRANCHNAME
```

## Tricks

 - http://gitready.com/intermediate/2009/02/18/temporarily-ignoring-files.html

## Troubleshooting

```bash
# insufficient permission for adding an object to repository database .git/objects
>> cd .git/objects
>> sudo chown -R USERNAME:GROUPNAME *  # eg chown -R dutrechf:dutrechf *
```

### Submodule

 - https://www.atlassian.com/git/tutorials/git-submodule

```bash
# get current submodule SHA1
>> git submodule status

# get current submodule SHA1 recorded in parent project
>> git submodule status --cached

# force submodule to last master
git submodule update --init --remote --force

# remove submodule
# 1. remove section from .gitmodule
# 2. remove section from .git/config
# 3. git rm --cached path_to_submodule
# 4. rm -rf .git/modules/path_to_submodule
# 5. rm -rf path_to_submodule
```

## Remote

 - https://linuxize.com/post/how-to-change-git-remote-url/

```
>> git remote -v
>> git remote set-url origin https://bitbucketbdsfr.fsc.atos-services.net/scm/brebd/koios-docker-component-builder.git
```

### Subtree

 - https://hpc.uni.lu/blog/2014/understanding-git-subtree/
 - https://blog.developer.atlassian.com/the-power-of-git-subtree/

### Repo

Repo complements Git by simplifying work across multiple repositories.

 - https://source.android.com/setup/develop/repo

### Gerrit

Code review tool

 - https://gerrit-review.googlesource.com/Documentation/

### C++

access git commit from code :

http://xit0.org/2013/04/cmake-use-git-branch-and-commit-details-in-project/


### Rebase from 1 first

`git rebase -i --root`
