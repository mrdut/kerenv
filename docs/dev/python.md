# Python

staticmethod vs classmethod

## Threads, process, GIL

https://www.cloudcity.io/blog/2019/02/27/things-i-wish-they-told-me-about-multiprocessing-in-python/
http://www.dabeaz.com/python/UnderstandingGIL.pdf
https://stackoverflow.com/questions/44983304/how-to-achive-true-parallelism-with-thread-in-python
https://stackoverflow.com/questions/3044580/multiprocessing-vs-threading-python/3046201#3046201
## Virtual env

```
# install
>> pip3 install virtualenv

# check install
>> which virtualenv

# create
>> python3 -m venv PATH/TO/VENV

# activate
>> source PATH/TO/VENV/bin/activate

# setup
>> pip3 install -r requirements.txt

# do some work
...
(heyho) >>
...

# deactivate
>> deactivate
```



## get function source code

https://stackoverflow.com/questions/1562759/can-python-print-a-function-definition
```
>>> import re
>>> import inspect
>>> print inspect.getsource(re.compile)
def compile(pattern, flags=0):
    "Compile a regular expression pattern, returning a pattern object."
    return _compile(pattern, flags)
```

## eval commmand line code
python2.7 -c 'print(u"foo" + "bar")'

## copy, shallow copy, deepcopy...
https://stackoverflow.com/questions/3975376/understanding-dict-copy-shallow-or-deep/3975388

## debgu

 - http://code.activestate.com/recipes/576515/

what are the expected arg for a function ?
import inspect
print(  inspect.getargspec(JsonCppValue.overwrite) )

what is module filename ?
import sys
print( sys.__file__ )

[what are class methods/members ?](https://stackoverflow.com/questions/2675028/list-attributes-of-an-object)

```
>>> dir(a)
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'multi', 'str']
>>> vars(a)
{'multi': 4, 'str': '2'}
>>> a.__dict__
{'multi': 4, 'str': '2'}
```

## Python 3

http://pympler.readthedocs.io/en/latest/tutorials/muppy_tutorial.html

## Pip3

```
yum install python3-pip

python3 -m pip install pyperclip --user
```

## Debug

```
gdb python[2/3]
(gdb) run script.py
```

## Profile

```
>> python -m cProfile -s tottime {script.py args...}
```

[visualize](https://thirld.com/blog/2014/11/30/visualizing-the-results-of-profiling-python-code/)
```
>> python -m cProfile -o prof.out mycode.py
>> pip install pyprof2calltree --user
>> pyprof2calltree -i prof.out -k    (python /nfs/home/fdutrech/.local/lib/python2.7/site-packages/pyprof2calltree.py -i prof.out -k)

OR

>> pip install pyinstrument
>> python -m pyinstrument mycode.py

```



### Misc

pythonx.y vs pythonx.ym
see https://www.python.org/dev/peps/pep-3149/
--with-pydebug (flag: d)
--with-pymalloc (flag: m)
--with-wide-unicode (flag: u)

### Extension

https://stackoverflow.com/questions/35774011/segment-fault-when-creating-pylist-new-in-python-c-extention

PyObject* test(const char* filename)
{
    PyGILState_STATE gstate = PyGILState_Ensure();

    // The bulk of your code stays the same and goes here ...

    PyGILState_Release(gstate);

    return result;
}

## Devel

### Installation

Install default version :

```
yum install python36-devel
```

Install custom version :

```
>> yum search python3 | grep devel
>> repoquery --list python36-devel
>> repoquery --list python33-python-devel
```

Use custom version :

```
>> export LD_LIBRARY_PATH=/opt/rh/python33/root/usr/lib64/ (for python3.3 libs)
>> export PATH=/opt/rh/python33/root/bin:$PATH (for python3.3-config)
```


## Cython

```
# input_stream_manager/file.pyx
def say_hello_to(name):
    print("Hello %s!" % name)

# setup.py
from distutils.extension import Extension
from Cython.Build import cythonize
    setup(
        ...
        ext_modules=cythonize(Extension("mymodule", ["input_stream_manager/file.pyx"])),
        ...
        )

# 
from mymodule import say_hello_to

```
