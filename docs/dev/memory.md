# Memory


## Tools

valgrind --massif + massif-visualizer
valgrind --memcheck
heaptrack + heaptrack_gui_



## Generality

```
L1 cache reference ......................... 0.5 ns
Branch mispredict ............................ 5 ns
L2 cache reference ........................... 7 ns
Mutex lock/unlock ........................... 25 ns
Main memory reference ...................... 100 ns             
Compress 1K bytes with Zippy ............. 3,000 ns  =   3 µs
Send 2K bytes over 1 Gbps network ....... 20,000 ns  =  20 µs
SSD random read ........................ 150,000 ns  = 150 µs
Read 1 MB sequentially from memory ..... 250,000 ns  = 250 µs
Round trip within same datacenter ...... 500,000 ns  = 0.5 ms
Read 1 MB sequentially from SSD* ..... 1,000,000 ns  =   1 ms
Disk seek ........................... 10,000,000 ns  =  10 ms
Read 1 MB sequentially from disk .... 20,000,000 ns  =  20 ms
Send packet CA->Netherlands->CA .... 150,000,000 ns  = 150 ms
```
 ![memory latency visualization](img/memory_latency.png)

[Latency Numbers Every Programmer Should Know](https://gist.github.com/jboner/2841832)

 - CPU fetches memory via cacheline (64 bytes)
 - every times it reads or write a single byte, it does it for the full cacheline

```
>> getconf -a | grep CACHE                                                                                                                               [Bluecime::extra-eyes] (16:13)
LEVEL1_ICACHE_SIZE                 32768
LEVEL1_ICACHE_ASSOC                8
LEVEL1_ICACHE_LINESIZE             64
LEVEL1_DCACHE_SIZE                 32768
LEVEL1_DCACHE_ASSOC                8
LEVEL1_DCACHE_LINESIZE             64
LEVEL2_CACHE_SIZE                  262144
LEVEL2_CACHE_ASSOC                 4
LEVEL2_CACHE_LINESIZE              64
LEVEL3_CACHE_SIZE                  6291456
LEVEL3_CACHE_ASSOC                 12
LEVEL3_CACHE_LINESIZE              64
LEVEL4_CACHE_SIZE                  0
LEVEL4_CACHE_ASSOC                 0
LEVEL4_CACHE_LINESIZE              0
```

[fighting latency](https://events.static.linuxfound.org/sites/events/files/slides/ELCE%20-%20fighting%20latency.pdf)

[what every programer should know about memory (online)](https://lwn.net/Articles/250967/)


 - cache coherency : when a write operation occured on a cachleline, it is flagged as modified to ensure coherency
 - cache associativity
    - *fully associative cache* : a cacheline can go anywhere in cache memory
    - *direct mapped cache* : a cacheline goes to **single** predefined cache address (eg cachline 0xabc0001 goes to cache adresse 0x01, ...) -> many cache miss since every cacheline would go to same cache adress
    - *2-way set associative cache* : a cacheline can go in **any** predefined cache address set (eg 128kb L2 cache, 8 way associativity -> 8 sets, 16kb each)

 - False sharing - 2 or more threads access data stored on same cacheline and one or more operation performed is a write

 ![false sharing](img/false_sharing.png)

 use padding to avoid fakse sharing

Some examples to look for variables sharing same cacheline
```
// globals 
int s_x;  // accessed by thread A
int s_y;  // accessed by thread B

// dynamic allocated data
auto x = new X();  // accessed by thread A
auto y = new Y();  // accessed by thread B

// class/struct fields
class C
{
    int x;  // accessed by thread A
    int y;  // accessed by thread B
};

// array
int arr[100];
x[17]++;  // accessed by thread A
x[18]++;  // accessed by thread B

```


    [Episode 5.9 - Elimination of False Cache Line Sharing](https://www.youtube.com/watch?v=h58X-PaEGng&t=245s)


https://www.youtube.com/watch?v=WDIkqP4JbkE
https://www.aristeia.com/TalkNotes/codedive-CPUCachesHandouts.pdf

(Microsoft perforamnce engineer) "...most of out performance problems came down to stamping out false sharing in numerous place"


## Memory leak

[CppCon 2019: Milian Wolff “How to Write a Heap Memory Profiler”](https://www.youtube.com/watch?v=YB0QoWI-g8E)
    - is it data-bounded or by computation-bounded ?

https://github.com/milianw/how-to-write-a-memory-profiler


[CppCon 2015: Milian Wolff "Heaptrack: A Heap Memory Profiler for Linux"](https://www.youtube.com/watch?v=myDWLPBiHn0)


Monitor memory usage

https://techtalk.intersec.com/2013/07/memory-part-1-memory-types/

tools :
`gnome-system-monitor`
`ps_mem (pip install ps_mem)`

minitor from C++
```
COUT( BASH_RED << "[" << getpid() << "] before factoryReset() " << BASH_RESET )
system( cv::format("ps o pmem,rss,vsz %d",getpid()).c_str());
```

## Data structure alignement

    word size: 4 bytes (32 bit) or 8 bytes (64 bit)
    `alignof(int)`


 - [wikipedia](https://en.wikipedia.org/wiki/Data_structure_alignment)

 - `addr` is _n-byte aligned_ when _n_ is a power of two  and _a_ is multiple of _n_ bytes
 - `addr` is _n-byte aligned_ when it as _log2(n)_ leading zeros in binary
 - _b-bit aligned_ == _b/8-byte aligned_
 - `addr` is _aligned_ when datum being accessed is _n-bytes_ long and is _n-bytes aligned_

![aligned memory](img/memory_align01.jpg)
![misaligned memory](img/memory_align02.jpg)

Memory address | 8 bit / 1 byte | 16 bit / 2 bytes | 32 bit / 4 bytes | 64 bit / 8 bytes |
-------------- |----------------|------------------|------------------|-------------------
0x0000_0000 |Aligned | Aligned | Aligned | Aligned
0x0000_0001 |Aligned | Non Aligned | Non Aligned | Non Aligned
0x0000_0002 |Aligned | Aligned | Non Aligned | Non Aligned
0x0000_0003 |Aligned | Non Aligned | Non Aligned | Non Aligned
0x0000_0004 |Aligned | Aligned | Aligned | Non Aligned
0x0000_0005 |Aligned | Non Aligned | Non Aligned | Non Aligned
0x0000_0006 |Aligned | Aligned | Non Aligned | Non Aligned
0x0000_0007 |Aligned | Non Aligned | Non Aligned | Non Aligned
0x0000_0008 |Aligned | Aligned | Aligned | Aligned

## Snippet

 - Allocate memory aligned to cache lines

```
# exemple to allocate double array
double *alloc64f(size_t n)
{
    double* p;

    if(posix_memalign((void**)&p, 64, n*sizeof(double)) == 0)
    {
        return p;
    }
    else
    {
        return NULL;
    }
}
```

 - Manually align address

```
// allocate 15 byte larger array because in worst case, the data can be misaligned upto 15 bytes.
float* array = (float*)malloc(SIZE*sizeof(float)+15);

// find the aligned position and use this pointer to read or write data into array
float* alignedArray = (float*)(((unsigned long)array + 15) & (~0x0F));
...

// dellocate memory original "array", NOT alignedArray
free(array);
array = alignedArray = 0;
```

### Padding 

Data alignment for each type

Data Type | Alignment (bytes)
--------|----------- 
char    | 1
short   | 2
int     | 4 
float   | 4
double  | 4 or 8 (if compiled with `-malign-double`)

Reorder struct member to fill gaps and avoid padding 

```
struct Bad
{
    char Data1;             // 1 byte
    // char Padding[1];     -> added at compile time to ensure following short is 2-byte aligned
    short Data2;            // 2 bytes
    int Data3;              // 4 bytes
    char Data4;             // 1 byte
    // char Padding2[3];    -> added at compile time to make total size of the struct 12 bytes
}; // 12 bytes

struct Good
{
    char Data1;  // 1 byte
    char Data4;  // 1 byte
    short Data2; // 2 bytes
    int Data3;   // 4 bytes
}; // 8 bytes
```

## Resources

 - http://www.songho.ca/misc/alignment/dataalign.html
 - http://igoro.com/archive/gallery-of-processor-cache-effects/
 - [what every programmer should know about memory](https://people.freebsd.org/~lstewart/articles/cpumemory.pdf)





https://lwn.net/Articles/28345/
VmSize : Quantité de mémoire totale utilisée. En fait, c'est plutot la taille de la mémoire virtuelle utilisée.
VmRSS : (Virtual Memory Resident Stack Size). Taille de la mémoire physique utilisée.

