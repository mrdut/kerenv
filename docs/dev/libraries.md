# Librariries

## Search path order

 1. RPATH - a list of directories which is linked into the executable, supported on most UNIX systems. It is ignored if RUNPATH is present.

 1. `LD_LIBRARY_PATH` environment variable

    - This method should be avoided (it overrides lib search for all binaries, not only the one you are interested in)

 1. Any `rpath` encoded in the binary

    - Better than `LD_LIBRARY_PATH` since it only affects one binary
    - To add a path to binary, either set `LD_RUN_PATH` env variable at compile time or add `-R <path>` to command line (overwrites `LD_RUN_PATH`) or `-Wl,-rpath=<PATH>`
    - read binary : `readelf -d <binary> | grep RPATH` (you can do the same for any library listed with `ldd`)

<!-- /etc/ld.so.conf - configuration file for ld.so which lists additional library directories -->

 1. System search path (_/lib_, then _/usr/lib_, then any path in _/etc/ld.so.conf_)
    
    - To add new path (eg _/usr/local/lib_), add to _/etc/ld.so.conf_ and run [`ldconfig`](https://linux.die.net/man/8/ldconfig)

```bash

# display linker search path
# https://stackoverflow.com/questions/9922949/how-to-print-the-ldlinker-search-path
ld --verbose | grep SEARCH_DIR 
ldconfig -v 2>/dev/null | grep -v ^$'\t'

# https://en.wikipedia.org/wiki/Rpath
```

##

```
# NB : libTitanBackbone-x64-buster-clang.so in LD_LIBRARY_PATH
./bin/TitanProcessingMain: error while loading shared libraries: libTitanBackbone-x64-buster-clang.so: cannot open shared object file: No such file or directory

# Fix:
───────┬─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────       │ File: /etc/ld.so.conf.d/fdutrech.conf
───────┼─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
   1   │
   2   │ /home/fdutrech/Workspace/dev/TitanProcessing/bin
   3   │ /home/fdutrech/Workspace/dev/AtemeLib/x64-buster-clang/bin/

>> sudo ldconfig

# explanation :

```

https://unix.stackexchange.com/questions/226076/ldd-shows-library-exists-and-is-linked-program-doesnt-find-it
https://unix.stackexchange.com/questions/76490/no-such-file-or-directory-on-an-executable-yet-file-exists-and-ldd-reports-al

## Snippet

use custom lib (eg _Debug_ opencv) instead of system one [source - 13:00](https://www.youtube.com/watch?v=YB0QoWI-g8E) 

```
LD_PRELOAD=$(readlink -f path/to/lib) <BINARY>
```

Specifying Dynamic Libraries at Build Time :  gcc ... -rpath -  `>> dump -Lv a.out | grep RPATH`

from [stackoverflow dynamic-dependency-lib-tls-search-path](https://stackoverflow.com/questions/23565423/dynamic-dependency-lib-tls-search-path)
```
The GLIBC loader takes RPATH from your executable, and appends PLATFORM strings (tls/x86_64:tls:x86_64 here) to it to construct the full search path.
```

## Debug

 - architecture : `objdump -f $CONAN_PATH/lib/libavcodec.so | grep ^architecture`
 -  `file libavformat.so.58.45.100 `
   `libavformat.so.58.45.100: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=33cf543335efa8762f26c8b2703c213306f4effa, strippe`
 - `ldd`  :print shared object dependencies (see next LD_DEBUG section) 
 - `lddtree` : same as `ldd`, but tree display
 - `lsof` : show which libraries are being used for one particular process.

```bash
>> pidof <PROCESS NAME>
6920 6919

>> lsof -p 6919|grep mem
<PROCESS>   6919 root  mem    REG               0,64    65960     43 /lib64/libnss_files-2.12.so
<PROCESS>   6919 root  mem    REG               0,64    19536     36 /lib64/libdl-2.12.so
<PROCESS>   6919 root  mem    REG               0,64    10312   1875 /lib64/libfreebl3.so
<PROCESS>   6919 root  mem    REG               0,64  1923352     38 /lib64/libc-2.12.so
<PROCESS>   6919 root  mem    REG               0,64    88600   1034 /lib64/libz.so.1.2.3
<PROCESS>   6919 root  mem    REG               0,64  1967392   1927 /usr/lib64/libcrypto.so.1.0.1e
<PROCESS>   6919 root  mem    REG               0,64   183080   1898 /lib64/libpcre.so.0.0.1
<PROCESS>   6919 root  mem    REG               0,64    40400   1217 /lib64/libcrypt-2.12.so
<PROCESS>   6919 root  mem    REG               0,64   142688     77 /lib64/libpthread-2.12.so
<PROCESS>   6919 root  mem    REG               0,64   154664     31 /lib64/ld-2.12.so
```

 - `readelf -d` will list the direct dependencies as _NEEDED_ sections. (`readelf -d <BIN/LIB> | grep 'NEEDED'`)
 - `readelf -d` will list the direct `rpath` as _RPATH_ sections. (`readelf -d <BIN/LIB> | grep 'RPATH'`)

```
>> patchelf
syntax: patchelfhttps://en.wikipedia.org/wiki/Rpa
  [--set-interpreter FILENAME]
  [--page-size SIZE]
  [--print-interpreter]
  [--print-soname]      Prints 'DT_SONAME' entry of .dynamic section. Raises an error if DT_SONAME doesn't exist
  [--set-soname SONAME]     Sets 'DT_SONAME' entry to SONAME.
  [--set-rpath RPATH]
  [--remove-rpath]
  [--shrink-rpath]
  [--print-rpath]
  [--force-rpath]
  [--add-needed LIBRARY]
  [--remove-needed LIBRARY]
  [--replace-needed LIBRARY NEW_LIBRARY]
  [--print-needed]
  [--no-default-lib]
  [--debug]
  [--version]
  FILENAME
```

# `LD_DEBUG` environment variable

```
 LD_DEBUG=help  build/bin/blueski

Valid options for the LD_DEBUG environment variable are:

  libs        display library search paths
  reloc       display relocation processing
  files       display progress for input file
  symbols     display symbol table processing
  bindings    display information about symbol binding
  versions    display version dependencies
  scopes      display scope information
  all         all previous options combined
  statistics  display relocation statistics
  unused      determined unused DSOs
  help        display this help message and exit

To direct the debugging output into a file instead of standard output
a filename can be specified using the LD_DEBUG_OUTPUT environment variable.

```


```
$ LD_DEBUG=libs ldd ./build/bin/blue-ski  

...
     27996:     find library=libpthread.so.0 [0]; searching
     27996:      search path=/work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib:/usr/local/lib64/tls/x86_64:/usr/local/lib64/tls:/usr/local/lib64/x86_64:/usr/local/lib64:/usr/local/lib/tls/x86_64     2
7995:          trying file=/lib64/libc.so.6
:/usr/local/lib/tls:/usr/local/lib/x86_64:/usr/local/lib                (RPATH from file ./build/bin/blue-ski)
     27996:       trying file=/work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib/libpthread.so.0
     27995:     
     27996:       trying file=/usr/local/lib64/tls/x86_64/libpthread.so.0
     27996:       trying file=/usr/local/lib64/tls/libpthread.so.0
     27996:       trying file=/usr/local/lib64/x86_64/libpthread.so.0
     27996:       trying file=/usr/local/lib64/libpthread.so.0
     27996:       trying file=/usr/local/lib/tls/x86_64/libpthread.so.0
     27996:       trying file=/usr/local/lib/tls/libpthread.so.0
     27996:       trying file=/usr/local/lib/x86_64/libpthread.so.0
     27996:       trying file=/usr/local/lib/libpthread.so.0
     27996:      search path=/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls/x86_64:/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls:/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/x86_64:/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib:/opt/rh/devtoolset-7/root/usr/lib64/tls/x86_64:/opt/rh/devtoolset-7/root/usr/lib64/tls:/opt/rh/devtoolset-7/root/usr/lib64/x86_64:/opt/rh/devtoolset-7/root/usr/lib64:/opt/rh/devtoolset-7/root/usr/lib/tls/x86_64:/opt/rh/devtoolset-7/root/usr/lib/tls:/opt/rh/devtoolset-7/root/usr/lib/x86_64:/opt/rh/devtoolset-7/root/usr/lib:/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls/x86_64:/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls:/opt/rh/devtoolset-7/root/usr/lib64/dyninst/x86_64:/opt/rh/devtoolset-7/root/usr/lib64/dyninst:/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls/x86_64:/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls:/opt/rh/devtoolset-7/root/usr/lib/dyninst/x86_64:/opt/rh/devtoolset-7/root/usr/lib/dyninst:./usr/lib/tls/x86_64:./usr/lib/tls:./usr/lib/x86_64:./usr/lib:./tls/x86_64:./tls:./x86_64:.              (LD_LIBRARY_PATH)
     27996:       trying file=/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls/x86_64/libpthread.so.0
     27996:       trying file=/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls/libpthread.so.0
     27996:       trying file=/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/x86_64/libpthread.so.0
     27996:       trying file=/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/tls/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/tls/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/tls/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/tls/libpthread.so.0
     27995:     
     27995:     calling init: /lib64/libc.so.6
     27995:     
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/dyninst/x86_64/libpthread.so.0
     27995:     
     27995:     initialize program: cat
     27995:     
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib64/dyninst/libpthread.so.0
     27995:     
     27995:     transferring control: cat
     27995:     
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/dyninst/x86_64/libpthread.so.0
     27996:       trying file=/opt/rh/devtoolset-7/root/usr/lib/dyninst/libpthread.so.0
     27996:       trying file=./usr/lib/tls/x86_64/libpthread.so.0
     27996:       trying file=./usr/lib/tls/libpthread.so.0
     27996:       trying file=./usr/lib/x86_64/libpthread.so.0
     27996:       trying file=./usr/lib/libpthread.so.0
     27996:       trying file=./tls/x86_64/libpthread.so.0
     27996:       trying file=./tls/libpthread.so.0
     27996:       trying file=./x86_64/libpthread.so.0
     27996:       trying file=./libpthread.so.0
     27996:      search cache=/etc/ld.so.cache
     27996:       trying file=/lib64/libpthread.so.0
     27996:     

```


## strace

```
strace -e trace=open  build/bin/blue-ski --no-display --progress 2>&1 

...
#
# RPATH  
#
# $ readelf -d build/bin/blue-ski | grep 'RPATH'
# 0x000000000000000f (RPATH)              Library rpath: [/work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib:/usr/local/lib64:/usr/local/lib]
#
open("/work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)

open("/usr/local/lib64/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib64/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib64/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/usr/local/lib/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/work-crypt/fdutrech/third_party/OpenCV_3.4.2_RelWithDebInfo/lib/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/dyninst/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/dyninst/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib64/dyninst/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/dyninst/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/dyninst/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/opt/rh/devtoolset-7/root/usr/lib/dyninst/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./usr/lib/tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./usr/lib/tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./usr/lib/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./usr/lib/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./tls/x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./tls/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./x86_64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("./libpthread.so.0", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
open("/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
open("/lib64/libpthread.so.0", O_RDONLY|O_CLOEXEC) = 3
...


```
# Links

 - http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html
 - http://tldp.org/HOWTO/Program-Library-HOWTO/shared-libraries.html
 - https://unix.stackexchange.com/questions/120015/how-to-find-out-the-dynamic-libraries-executables-loads-when-run
 - https://unix.stackexchange.com/questions/22926/where-do-executables-look-for-shared-objects-at-runtime
 - https://enchildfone.wordpress.com/2010/03/23/a-description-of-rpath-origin-ld_library_path-and-portable-linux-binaries/i Maha