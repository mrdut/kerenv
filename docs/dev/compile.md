
https://developers.redhat.com/blog/2018/03/21/compiler-and-linker-flags-gcc/

# librrary path

-> you can includ elibrary path during compilation :

LDFLAGS => -Wl,-rpath,"../../path"

combined with pkg_config:  -Wl,-rpath,`PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig/  pkg-config --libs-only-L opencv | sed -e "s/-L//"`

#pkg config :

list search path:


https://stackoverflow.com/questions/29540368/are-there-techniques-to-greatly-improve-c-building-time-for-3d-applications
precompiled
https://lists.samba.org/archive/ccache/2010q3/000583.html

https://cmake.org/pipermail/cmake/2013-March/054099.html

1) first you can have a broad look at the overall target dependencies
cd <builddir>
cmake --graphiz=yourproject.dot .
then render/display the dot file with graphviz dot tool.

https://musescore.org/en/node/182331


### print include header tree

       -H  Print the name of each header file used, in addition to other normal activities.  Each name is indented to show how deep in the #include stack it is.  Precompiled header files are also printed,
           even if they are found to be invalid; an invalid precompiled header file is printed with ...x and a valid one with ...! .

```
[  1%] Building CXX object src/CMakeFiles/bluevision.dir/descriptor/histogram/integral_image.cpp.o
cd /work-crypt/fdutrech/workspace/bluecime/bluevision/build/None_Linux-4.4.14-200.fc22.x86_64/src && ccache /usr/bin/c++   -DUSE_PRECOMPILED -Dbluevision_EXPORTS -fPIC -I/usr/local/include/opencv -I/usr/local/include -I/work-crypt/fdutrech/workspace/bluecime/bluevision/src -I/work-crypt/fdutrech/workspace/bluecime/bluevision/build/None_Linux-4.4.14-200.fc22.x86_64/src    -std=c++11 -fPIC -H -Werror -Wall -march=corei7-avx -mavx -msse4.1 -msse4.2 -fabi-version=6 -mavx2 -Winvalid-pch -include precompiled_header.hpp -o CMakeFiles/bluevision.dir/descriptor/histogram/integral_image.cpp.o -c /work-crypt/fdutrech/workspace/bluecime/bluevision/src/descriptor/histogram/integral_image.cpp
! ./precompiled_header.hpp.gch
. /work-crypt/fdutrech/workspace/bluecime/bluevision/src/descriptor/histogram/integral_image.hpp
.. /work-crypt/fdutrech/workspace/bluecime/bluevision/src/descriptor/histogram/histogram.hpp
... /work-crypt/fdutrech/workspace/bluecime/bluevision/src/core/math.hpp
.... /work-crypt/fdutrech/workspace/bluecime/bluevision/src/core/enum.hpp
..... /usr/include/c++/5.3.1/string
..... /usr/include/c++/5.3.1/climits
...... /usr/lib/gcc/x86_64-redhat-linux/5.3.1/include/limits.h
.... /work-crypt/fdutrech/workspace/bluecime/bluevision/src/core/error.hpp
..... /work-crypt/fdutrech/workspace/bluecime/bluevision/src/core/print.hpp
...... /usr/include/c++/5.3.1/bitset
....... /usr/include/c++/5.3.1/bits/functexcept.h
....... /usr/include/c++/5.3.1/iosfwd
....... /usr/include/c++/5.3.1/bits/cxxabi_forced.h
....... /usr/include/c++/5.3.1/bits/functional_hash.h
...... /usr/include/c++/5.3.1/iomanip
....... /usr/include/c++/5.3.1/x86_64-redhat-linux/bits/c++config.h
....... /usr/include/c++/5.3.1/bits/ios_base.h
....... /usr/include/c++/5.3.1/locale
........ /usr/include/c++/5.3.1/bits/localefwd.h
........ /usr/include/c++/5.3.1/bits/locale_classes.h
........ /usr/include/c++/5.3.1/bits/locale_facets.h
........ /usr/include/c++/5.3.1/bits/locale_facets_nonio.h
......... /usr/include/c++/5.3.1/ctime
.......... /usr/include/time.h
......... /usr/include/c++/5.3.1/x86_64-redhat-linux/bits/time_members.h
......... /usr/include/c++/5.3.1/x86_64-redhat-linux/bits/messages_members.h
.......... /usr/include/libintl.h
........... /usr/include/features.h
......... /usr/include/c++/5.3.1/bits/codecvt.h
......... /usr/include/c++/5.3.1/bits/locale_facets_nonio.tcc
........ /usr/include/c++/5.3.1/bits/locale_conv.h
......... /usr/include/c++/5.3.1/streambuf
......... /usr/include/c++/5.3.1/bits/stringfwd.h
......... /usr/include/c++/5.3.1/bits/allocator.h
......... /usr/include/c++/5.3.1/bits/codecvt.h
......... /usr/include/c++/5.3.1/bits/unique_ptr.h
...
```


https://lefticus.gitbooks.io/cpp-best-practices/content/08-Considering_Performance.html
Nov 29, 2017 at 10:27 AM - Edit - Add Link as Attachment - Delete -
Florent (fdutrech)
Florent
https://crascit.com/2016/04/09/using-ccache-with-cmake/
http://www.bitsnbites.eu/faster-c-builds/
http://itscompiling.eu/2017/03/19/speed-up-c-compilation-distributed-work/

https://stackoverflow.com/questions/29540368/are-there-techniques-to-greatly-improve-c-building-time-for-3d-applications

# list(APPEND BLUECIME_COMPILE_OPTIONS -Q) # (!!very verbose) Makes the compiler print out each function name as it is compiled, and print some statistics about each pass when it finishes.
# list(APPEND BLUECIME_COMPILE_OPTIONS -H) # Print the name of each header file used... see man gcc
# list(APPEND BLUECIME_COMPILE_OPTIONS -ftime-report) # Makes the compiler print some statistics about the time consumed by each pass when it finishes.


### list absolute paths of include files:

```
gcc -M showtime.c
```

If you don't want the system includes (i.e. #include <something.h>) then use:

```
gcc -MM showtime.c
```

### see how macro are expanded within a file :

```
g++ -E </path/to/filename.cpp> <-I...> -std=c++11  | grep <PATTERN> | clang-format
```

### list symbol within library :

```
nm -gC yourLib.so
```

from man nm:

       "T"
       "t" The symbol is in the text (code) section.

       "U" The symbol is undefined.