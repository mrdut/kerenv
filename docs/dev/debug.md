# GDB

## Plugin

https://github.com/cyrus-and/gdb-dashboard

```bash
git clone https://github.com/cyrus-and/gdb-dashboard.git
ln -s gdb-dashboard/.gbd_init ~/.gbd_init
(gdb) help dashboard
...
(gdb) dashboard registers # show/hide registers tab
```



##  Compilation

 * `-g` produces debugging information in the OS¹s native format (stabs, COFF, XCOFF, or DWARF 2).
 * `-ggdb` produces debugging information specifically intended for gdb.

## Usage

http://www.linuxembedded.fr/2018/11/survol-des-outils-de-debug/
[Give me 15 minutes & I'll change your view of GDB"](https://www.youtube.com/watch?v=PorfLSr3DDI) -> learn to deal with ??? stack

### invoke

    gdb <PROG>
    gdb --args <PROG> <PROG_ARGS>
    '(gdb) show args'

### `break` or `br`
* `info breakpoints`
* break at function : `br <FUNC>`
* break at address : `br *0x7ffff13add39`
* delete breakpoint : [`delete [breakpoints] [range...]`](ftp://ftp.gnu.org/old-gnu/Manuals/gdb/html_node/gdb_31.html)
* conditional breakpoints : `break WHERE if COND` (eg : (gdb) break context_switch if next == init_task)
* command to run everytime a breakpont is hit : 
(gdb) b <function>
Breakpoint 1 at 0xffffffff8111a441: file mm/mmap.c, line 940.
(gdb) command 1 <- breakpont number
Type commands for when breakpoint 1 is hit, one per line.
End with a line saying just "end".
>print addr
>print len
>print prot
>continue
>end
(gdb)

### show variable

 * `disp VARNAME`
 * `next` `continue`

### `backtrace` or `bt`

 * print full backtrace : `bt`
 * print first n backtraces : `bt <n>`
 * print last n backtraces : `bt <-n>`
 * list all :  `bt full`

### `info`

 * search for function : info functions <regex>


### `frame` or `f`

 * which frame am I ? : `frame`
 * select frame n : `frame n`
 * move up/down n frames : `up/down <n=1>`
 * list frame arguments : `info args`
 * list frame variables : `info locals`

### `list` or `l`

 * print 10 lines around : `l`
 * print 10 lines around line X : `l X`
  Two arguments with comma between specify starting and ending lines to list.
  Lines can be specified in these ways:
    LINENUM, to list around that line in current file,
    FILE:LINENUM, to list around that line in that file,
    FUNCTION, to list around beginning of that function,
    FILE:FUNCTION, to distinguish among like-named static functions.
    *ADDRESS, to list around the line containing that address.



## Thread debugging

 - "pause" program exeution : `CTRL+C`
 - "restart" : `continue`
 - `set print thread-events`

```gcc
info threads            # list all threads
thread <THREAD ID>      # 'move' to thread
bt                      # print thread backtrace
```

break on thread creation:

(gdb) info func pthread_create
All functions matching regular expression "pthread_create":

Non-debugging symbols:
0x080485e0  pthread_create
0x080485e0  pthread_create@plt
0x00786590  __pthread_create_2_1
0x00786590  pthread_create@@GLIBC_2.1
0x00786ee0  __pthread_create_2_0
0x00786ee0  pthread_create@GLIBC_2.0


(gdb) b __pthread_create_2_1

or 

`catch syscall clone`


https://sourceware.org/gdb/onlinedocs/gdb/Threads.html
## Python

[official API](https://sourceware.org/gdb/onlinedocs/gdb/Python-API.html)
[tutorial](https://www.wzdftpd.net/blog/python-scripts-in-gdb.html)

 - load module : `(gdb) source ../kerenv/gdb/mymodule.py`

[auto load](https://sourceware.org/gdb/current/onlinedocs/gdb/Python-Auto_002dloading.html)

 - `set auto-load python-scripts [on|off]`
 - `show auto-load python-scripts`
 - `info auto-load python-scripts [regexp]`

```
>> gdb python
(gdb) run <SCRIPT> <ARGS>
```

## QT

gdb ./soffice.bin
(gdb) catch throw
(gdb) run

## debuginfo

update : `dnf --enablerepo=updates-debuginfo update`
install : `sudo dnf debuginfo-install <packet>`
list : `sudo dnf list --installed | grep debuginfo`

```bash
objcopy --only-keep-debug $BINARY_PATH $DEBUG_INFO_FILE
objcopy --strip-debug $BINARY_PATH
objcopy --add-gnu-debuglink=$DEBUG_INFO_FILE $BINARY_PATH
```

https://stackoverflow.com/questions/866721/how-to-generate-gcc-debug-symbol-outside-the-build-target
https://sourceware.org/gdb/onlinedocs/gdb/Separate-Debug-Files.html
https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Developer_Guide/intro.debuginfo.html
http://cmake.3232098.n2.nabble.com/Save-stripped-debugging-information-td6819195.html

## coredumps

generate coredump

```bash
kill -s SIGSEGV <PID>
```

or

```c++
int *hey = new int;
delete hey; 
delete hey; 
```

configure :
 - `ulimit -c unlimited`         (ENABLE / SIZE)
 - /proc/sys/kernel/core_pattern (WHERE, default=|/usr/lib/systemd/systemd-coredump %P %u %g %s %t %c %e)

commands:
https://jvns.ca/blog/2018/04/28/debugging-a-segfault-on-linux/

Example 1. List all the core dumps of a program named <PROG>

```bash
>> coredumpctl list <PROG> (--reverse)
```

Example 2. Invoke gdb on the last core dump

```bash
>> coredumpctl gdb
```

Example 3. Show information about a process that dumped core, matching by its PID 6654

```bash
>> coredumpctl info 6654
```

Example 4. Extract the last core dump of /usr/bin/bar to a file named bar.coredump

```bash
>> coredumpctl -o bar.coredump dump /usr/bin/bar
```


options:
 - `--reverse`  print newer first
 - `-1`         single match




### Links

 * [HUNTING THE CORE (cookbook)](https://www.fromdual.com/hunting-the-core)
 * [“NO SYMBOL TABLE INFO AVAILABLE.”](https://tech4research.wordpress.com/2015/08/11/gdb-no-symbol-table-info-available-but-nm-and-objdump-show-those/)
 * [Beej's Quick Guide to GDB](http://beej.us/guide/bggdb/)
 * [8 gdb tricks you should know] https://blogs.oracle.com/linux/8-gdb-tricks-you-should-know-v2

### test debug info :

```
>> file ./build/bin/<BINARY>
./build/bin/<BINARY>: ELF 64-bit LSB executable, x86-64, version 1 (GNU/Linux), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=98e5ea2a409399b40e6dddb2aa0e67cb16d36495, with debug_info, not stripped
```

```
objdump --debugging <BINARY>
if A LOT of symbols, compiled with -g
```

```
readelf --debug-dump=decodedline <BINARY>
```

```
nm -a <BINARTY> | grep " N "
```


# Examining binary files in Linux

> * determine file type : `file file.bin`
> * print the strings of printable characters in files : `strings file.bin`
> * list symbols from object files : `nm file.bin`
> * print shared object dependencies : `ldd file.bin`
> * Demangle C++ and Java symbols : `echo "<mangled_symbol_name>" | c++filt`

> * To see a trace of what libraries it calls / files open dynamically: `dtrace file.bin`
> * To do a hexdump of the file: `od -tx1 file.bin`
> * To disassemble a compiled binary: `readelf -b file.bin -m i8086`
> * To disassemble an binary object file: `objdump -DaflSx -b file.bin -m i8086`

http://blog.vinceliu.com/2009/06/examining-binary-files-in-linux.html


# Pimp gdb

install colout (pip install colout)
    - bug https://github.com/nojhan/colout/issues/88 : pip3 install --user git+git://github.com/nojhan/colout.git@master
https://github.com/nojhan/colout/blob/master/colout/example.gdbinit

OR 

https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging