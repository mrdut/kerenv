# Thread

## Wait condition

### Qt

https://doc.qt.io/qt-5/qwaitcondition.html
https://doc.qt.io/qt-5/qtcore-threads-waitconditions-example.html#



https://resources.qt.io/resources-by-content-type-videos-demos-tutorials/qtws17-multithreading-with-qt-giuseppe-dangelo-kdab

From a non-main thread:
 - FORBIDDEN : Perform any GUI operation , including, but not limited to : using any QWidget/Qt Quick / QPIxmap API
 - FORBIDDEN : Call Q(Core|Gui)Application::exec()
 - OK : using QImage, QPainter
 - Maybe OK : check a runtime QOpenGLContext::supportThreadedOpenGl()


### Pthread

 [glibc src on code.woboq](https://code.woboq.org/userspace/glibc/nptl/)

https://www.cs.cmu.edu/afs/cs/academic/class/15492-f07/www/pthreads.html#SCHEDULING


### Barrier

```c++
pthread_barrier_t   barrier; // the barrier synchronization object

void* func(void *unused)
{
    sleep (40); // do the computation
    pthread_barrier_wait (&barrier); // after this point, all three threads have completed.
}

main () // ignore arguments
{
    // create a barrier object with a count of 3 (number of time we expect to call pthread_barrier_wait)
    pthread_barrier_init (&barrier, NULL, 3);

    // start up two threads, thread1 and thread2
    pthread_create (NULL, NULL, func, NULL);
    pthread_create (NULL, NULL, func, NULL);

    // at this point, thread1 and thread2 are running

    // now wait for completion
    pthread_barrier_wait (&barrier);

    // after this point, the barrier have been reached 3 times

    pthread_barrier_destroy(&barrier);
}
```

    - Calling `pthread_barrier_wait()` causes each thread to block until the call has been made by number of threads specified in the pthread_barrier_init() call.
    - After the required number of threads have called `pthread_barrier_wait()`, all of the threads unblock, and  the barrier is reset to the state it had after the call to
           `pthread_barrier_init()`. In other words, the barrier can be once again used by the threads as a synchronization point.
    - On success, `pthread_barrier_wait()` returns the special value `PTHREAD_BARRIER_SERIAL_THREAD` in exactly one of the waiting threads, and `0` in all of the other threads. 
    This permits the program to ensure that some action is performed exactly once each time a barrier is passed.
 
 - http://man7.org/tlpi/code/online/dist/threads/pthread_barrier_demo.c.html
 
### Condition variable

Used to avoid this kind of code : `while(!queue.empty()) { sleep(1); }`


```c++
// WORKER
pthread_mutex_lock(&mutex);
while(condition==false)               // prevent spurious wakeup
{
    pthread_cond_wait(&cond, &mutex); // mutex is unlock while waiting
                                      // mutex is re-locked after wait
}
pthread_mutex_unlock(&mutex);n

// PRODUCER
pthread_mutex_lock(&mutex);           // prevent lost wakeup
condition = true;
pthread_cond_signal(&cond);
pthread_mutex_unlock(&mutex);
```


[Caveats](https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables)

 - **Lost wakeup** [see](https://stackoverflow.com/questions/4544234/calling-pthread-cond-signal-without-locking-mutex)

    The phenomenon of the lost wakeup is that the sender sends its notification before the receiver gets to its wait state. The consequence is that the notification is lost. The C++ standard describes condition variables as a simultaneous synchronisation mechanism: "The condition_variable class is a synchronisation primitive that can be used to block a thread, or multiple threads at the same time, ...". So the notification gets lost, and the receiver is waiting and waiting and ... .

```c++
// possible interleaving of instructions leading to lost wakeup 
Process A                             Process B

pthread_mutex_lock(&mutex);
while (condition == FALSE)
                                        // NB condition is not protected by mutex
                                      condition = TRUE;
                                      pthread_cond_signal(&cond);

pthread_cond_wait(&cond, &mutex);
```

 - **Spurious wakeup**

    It may happen that the receiver wakes up, although no notification happened. At a minimum POSIX Threads and the Windows API can be victims of these phenomena.


### [semaphore](https://linux.die.net/man/7/sem_overview)

    - **Init** : `sem_open` function creates a new named semaphore or opens an existing named semaphore
    - **P** (*Proberen*, *test*) : `sem_wait`
    - **V** (*Verhogen*, *inc*) : `sem_post`

https://stackoverflow.com/questions/368322/differences-between-system-v-and-posix-semaphores

### pipes

```
#include <unistd.h>
int pipe(int fd[2]);
```

 - read and writes are atomic 
 - can be used for producer/consummer sync

http://deptinfo.unice.fr/~baude/SYSL3/FBchap3-2s.pdf

### monitor threads

```
watch -n 0.1 ./monitor_process_threads.sh PROGNAME
```

monitor_process_threads.sh
```bash
#! /usr/bin/bash

PID=`pidof $1`

if [[ $? == 0 ]]; then
   ps -T -p $PID
   ps -T -p $PID | wc -l
else
    echo "No $1 process"
fi
```











```c++
#include <pthread.h>

static void* thread_run(void* vdata)
{
    auto data = reinterpret_cast<SharedData*>(vdata);

    while( continue_loop )         // infinite loop 
    {
        //
        // wait for job (only is no job yet)
        //
        data->wait_mutex.lock();
        if( data.jobs.empty() )
        {
            data->job_wait_condition.wait(&data->wait_mutex);
        }
        // wait_mutex is locked at this point - we don't release it here since we need to relock it to safely take job

        while( data->continue_loop && not data->jobs.empty() )
        {
            // take job
            auto job = data->jobs.front();
            data->jobs.pop_front();

            data->wait_mutex.unlock();

            // 
            // MUTEX FREE ZONE
            // 
            job->process();
            delete job;
        }

    }

    pthread_cond_signal(exit_condition);

    pthread_exit(NULL);
}

struct SharedData
{
    std::atomic<bool> continue_loop;
    pthread_mutex_t mutex;
    pthread_cond_t job_wait_condition;
    pthread_cond_t exit_condition;

    std::list<Job*> jobs;
};

int main()
{

    //
    // create shared data
    //
    SharedData data;
    data->continue_loop=true;
    data->mutex = PTHREAD_MUTEX_INITIALIZER;
    data->job_wait_condition = PTHREAD_COND_INITIALIZER;
    data->finished_condition = PTHREAD_COND_INITIALIZER;

    //
    // create threads
    //
    std::vector<pthread_t*> threads;
    for(int i=0 ; i<2 ; i++)
    {
        threads.push_back( new pthread_t );
        pthread_create(threads.back(), NULL, thread_run, reinterpret_cast<void*>(data));
        pthread_setname_np(threads.back(), "thread_%d" + i );
    }

    //
    // add jobs
    //
    for(int i=0 ; i<5 ; ++i)
    {
        sleep(5);

        // add job
        {
            Lock lock(&data->mutex);
            data->jobs.push_back( new Job(...) );
        }

        pthread_cond_signal(&data->job_wait_condition);    // signal one
        pthread_cond_broadcast(&data->job_wait_condition); // signal all
    }

    //
    // terminate threads
    //
    {
        Lock lock(&data->mutex);
        data->continue_loop = false;
        pthread_cond_broadcast(&data->job_wait_condition);
    }
    for(auto tdid : threads )
    {
        pthread_join(*tid, NULL);
    }

}
```
