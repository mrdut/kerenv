# Deep Learning


https://stanford.edu/~shervine/teaching/cs-230/cheatsheet-convolutional-neural-networks

## Architecture

![](https://cdn-images-1.medium.com/max/1200/1*vkQ0hXDaQv57sALXAJquxA.jpeg)


### Convolution + ReLU

#### Convolution

![](https://jhui.github.io/assets/cnn/cnn3d.png)

The objective of the Convolution Operation is to extract the high-level features such as edges, from the input image. 
The convolutional layer creates a bunch of feature maps.


#### Batch normalization & ReLU

After applying filters on the input, we apply a batch normalization followed by a ReLU for non-linearity.
The batch normalization renormalizes data to make learning faster with the Gradient descent.


![](https://miro.medium.com/max/700/1*DfMRHwxY1gyyDmrIAd-gjQ.png)

`relu(x) = max(0.0, x)`

[link](https://medium.com/tinymind/a-practical-guide-to-relu-b83ca804f1f7)

### Pooling 

the Pooling layer is responsible for reducing the spatial size of the Convolved Feature : 
    - To decrease the computational power required to process the data through dimensionality reduction
    - It is useful for extracting dominant features which are rotational and positional invariant, thus maintaining the process of effectively training of the model.

Max Pooling (much better, noise suppression) vs  Average Pooling


### Flatten

### Fully connected

Cheap way of learning non-linear combinations of the high-level features as represented by the output of the convolutional layer. The Fully-Connected layer is learning a possibly non-linear function in that space.

### Softmax


### Links

[a-comprehensive-guide-to-convolutional-neural-networks](https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53)
https://towardsdatascience.com/applied-deep-learning-part-4-convolutional-neural-networks-584bc134c1e2
[a deeper guide to conv networks](http://cs231n.github.io/convolutional-networks/)


## Visualization

### Feature maps

![](https://cdn-images-1.medium.com/max/800/1*A86wUjL-Z0SWDDI3slKqtg@2x.png)
(only the first 8 features map of each layers are displayed)

  - see how the input is transformed passing through the convolution layers
  - How to visualize the feature maps is actually pretty simple. We pass an input image through the CNN and record the intermediate activations
  - first layers acts as low-level features detectors (eg edge detectors)
  - last layers acts as high-level features detectors (eg "eye", "nose", ...)

### Convnet filters
![](https://cdn-images-1.medium.com/max/800/1*67eDFN3-TzDZFF0ndzGKwQ@2x.png)
![](https://cdn-images-1.medium.com/max/800/1*yo61wKzuTNreu7lRgLChyw@2x.png)
![](https://cdn-images-1.medium.com/max/800/1*WOuaw0J7TJvdbsJRK3Zsuw@2x.png)
![](https://cdn-images-1.medium.com/max/800/1*WOuaw0J7TJvdbsJRK3Zsuw@2x.png)
![](https://cdn-images-1.medium.com/max/800/1*w41F9cu7vnvts1e06VoK0A@2x.png)

 - we won’t actually visualize the filters themselves, but instead we will display the patterns each filter maximally responds to. 

### Class output

![](https://cdn-images-1.medium.com/max/800/1*xaGx06kgYDd7gyezz6-9rw@2x.png)
 visualize at the final softmax layer
  Given a particular category, like hammer or lamp, we will ask the CNN to generate an image that maximally represents the category. Basically the CNN will draw us an image of what it thinks a hammer looks like.

### Class Activation Mappings (CAM) 

#### CAM

![](http://snappishproductions.com/images/cam_3.png)
![](https://camo.githubusercontent.com/fb9a2d0813e5d530f49fa074c378cf83959346f7/687474703a2f2f636e6e6c6f63616c697a6174696f6e2e637361696c2e6d69742e6564752f6672616d65776f726b2e6a7067)

Overlay a heatmap over the original image to show us where our model thought most strongly that this cat was indeed a cat.

CAM works for modified image classification CNN (not containing any fully connected layer)

[good tutorial (display++)](http://snappishproductions.com/blog/2018/01/03/class-activation-mapping-in-pytorch.html)
[original CAM publication](http://cnnlocalization.csail.mit.edu/)
[original CAM github](https://github.com/metalbubble/CAM)



#### Grad-CAM

![](http://gradcam.cloudcv.org/static/images/network.png)

[pdf](https://arxiv.org/pdf/1610.02391.pdf)

Gradient weighted CAM : generalization of CAM for any CNN-based architecture

### Links

https://towardsdatascience.com/applied-deep-learning-part-4-convolutional-neural-networks-584bc134c1e2
https://github.com/utkuozbulak/pytorch-cnn-visualizations#gradient-visualization