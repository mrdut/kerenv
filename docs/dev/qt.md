# QT

### Good practices

 - **never** open `QDialog` or `QWidget` outside of master (eg GUI) thread
 - ensure`_lock` methods only calls `_nolock` methods
 - choose coherent naming:
     - slot : `on...`
     - signal : `emit ...Signal()`
     - dialog : `class ...Dialog()`
     - widget : `class ...Widget()`



### Troubleshooting 

 - `qt5ct: using qt5ct plugin` : `export QT_LOGGING_RULES="qt5ct.debug=false"`