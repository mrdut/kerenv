# Logging

## Keywords / Concepts : 

 - https://kubernetes.io/docs/concepts/cluster-administration/logging/

 - Cluster-level logging : store log speratly in case container/node/pod crashes
 - log rotation

## Framework


### Fluentd

 <img src="img/fluentd.png" style="zoom: 50%;" />


 - Small version : Fluent-bit 

```
# input plugins (tail, http, ...)
<source>

#output plugins (to file, to another fluentd, to elastic search, ...)
<match>
```

### Logstash

 Part of EFK
 Smmal version : Elastic Beats

 - [Fluentd vs Logstash](https://platform9.com/blog/kubernetes-logging-comparing-fluentd-vs-logstash/)

### [Openshift](https://cloud.redhat.com/blog/introduction-to-the-openshift-4-logging-stack)

 https://docs.openshift.com/container-platform/4.3/logging/cluster-logging.html

 ![](img/openshift-logging-topology.png)

