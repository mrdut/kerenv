# SSE

### Memory

How memory alignement impacts load ?

Short answer :

```
load_aligned( aligned_memory ) ~= load_unaligned( aligned_memory ) > load_unaligned( unaligned_memory )
```

[Long answer](https://stackoverflow.com/questions/20259694/is-the-sse-unaligned-load-intrinsic-any-slower-than-the-aligned-load-intrinsic-o)

### [SSE support](https://en.wikipedia.org/wiki/CPUID)

```
static bool HaveSSE2()
{
// ?? return false;
    asm mov EAX,1 ;
    asm cpuid ;
    asm test EDX, 4000000h ;test whether bit 26 is set
    asm jnz yes ;yes
    return false;
yes:
    return true;
}
```



http://www.songho.ca/misc/sse/sse.html