# Benchmark


https://github.com/intel/lmbench/tree/master/src

[CppCon 2016: Timur Doumler “Want fast C++? Know your hardware!"](https://www.youtube.com/watch?v=BP6NxVxDQIs)

To Optimize BIOS by removing every factor that could cause indeterminism, turn off
    - All power optimization
    - Intel Hyper-Threading technology
    - Frequency scaling
    - Turbo mode functionalities 





## Intel benchmark

https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/ia-32-ia-64-benchmark-code-execution-paper.pdf

- `RDTSCP` : Read Time-Stamp Counter and Processor ID IA assembly instruction
- `RTDSC` : Read Time-Stamp Counter and Processor ID IA assembly instruction


1. Copy appendix to intel_bench.c ([+ `#include<linux/slab.h`>]())
1. create Makefile

```
obj-m += intel_bench.o

all:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
    make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```

1. `(root) insmod intel_bench.ko`
1. `(root) rmmod intel_bench.ko`

## Mindmaze

use _profile*_ file

## Google


exemple of use :
https://github.com/delfrrr/delaunator-cpp/blob/master/bench/run.cpp
