# Cpp Optimization

### Gcc

CPU architecture 

```
cat /sys/devices/cpu/caps/pmu_name
```

https://gcc.gnu.org/onlinedocs/gcc/Invoking-GCC.html#Invoking-GCC
https://wiki.gentoo.org/wiki/GCC_optimization/fr

### Inline 


`inline` keyword is only a **hint** to the compiler, it may decide or not to effectively inline the function
For _GCC_, you can force inlining with the following syntax :  `inline void foo (const char) __attribute__((always_inline));`

### arrays of structure (AoS) vs structure of arrays (SoA) 

```
// AoS
typedef struct{
float x,y,z;
int a,b,c;
. . .
} Vertex;
Vertex Vertices[NumOfVertices];


// SoA
typedef struct{
float x[NumOfVertices];
float y[NumOfVertices];
float z[NumOfVertices];
int a[NumOfVertices];
int b[NumOfVertices];
int c[NumOfVertices];
} VerticesList;
VerticesList Vertices;


```

Data is organized to enable more efficient vertical SIMD computation
Simpler/less address generation than AoS
Fewer streams, which reduces page misses
Use of fewer prefetches, due to fewer streams
Efficient cache-line packing of data elements that are used concurrently


### Ressources

http://igoro.com/archive/gallery-of-processor-cache-effects/



 - [[slides] ](https://github.com/CppCon/CppCon2014/blob/master/Presentations/Data-Oriented%20Design%20and%20C%2B%2B/Data-Oriented%20Design%20and%20C%2B%2B%20-%20Mike%20Acton%20-%20CppCon%202014.pptx)[CppCon 2014: Mike Acton "Data-Oriented Design and C++"](https://www.youtube.com/watch?v=rX0ItVEVjHc)
 - [[slides] ](https://github.com/CppCon/CppCon2014/blob/master/Presentations/Efficiency%20with%20Algorithms%2C%20Performance%20with%20Data%20Structures/Efficiency%20with%20Algorithms%2C%20Performance%20with%20Data%20Structures%20-%20Chandler%20Carruth%20-%20CppCon%202014.pdf)[CppCon 2014: Chandler Carruth "Efficiency with Algorithms, Performance with Data Structures"](https://www.youtube.com/watch?v=fHNmRkzxHWs)
     - _"efficiency through algorithm"_ (do the minimum amount of work to accomplish a given task)
     - _"performance through Data structures"_ (how quickly a program does its work)
      -_"nothing in the code is slow, but kinda everything is not fast. [...] It comes from systemic minor efficiency problems"
 - [CppCon 2015: Chandler Carruth "Tuning C++: Benchmarks, and CPUs, and Compilers! Oh My!"](https://www.youtube.com/watch?v=nXaxk27zwlk&t=719s) 
     - _"benchmark the code that matters"_
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2016/garbage_in_garbage_out.html#1)[CppCon 2016: Chandler Carruth “Garbage In, Garbage Out: Arguing about Undefined Behavior..."](https://www.youtube.com/watch?v=yG1OZ69H_-o&t=1662s)
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2016/hybrid_data_structures.html#1)[CppCon 2016: Chandler Carruth “High Performance Code 201: Hybrid Data Structures"](https://www.youtube.com/watch?v=vElZc6zSIXM&t=508s)
     - _"use hybrid data structures to optimize allocations and cache locality"_
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2017/going_nowhere_faster.html#1)[CppCon 2017: Chandler Carruth “Going Nowhere Faster”](https://www.youtube.com/watch?v=2EWejmkKlxs&t=3174s) 
 - [CppCon 2018: Chandler Carruth “Spectre: Secrets, Side-Channels, Sandboxes, and Security”](https://www.youtube.com/watch?v=_f7O3IfIR2k&t=7s)
 - [CppCon 2019: Chandler Carruth “There Are No Zero-cost Abstractions”](https://www.youtube.com/watch?v=rHIkrotSwcc&t=537s)
 - [CppCon 2019: Chandler Carruth, Titus Winters “What is C++”](https://www.youtube.com/watch?v=LJh5QCV4wDg&t=1147s)

 - [Understanding Compiler Optimization - Chandler Carruth - Opening Keynote Meeting C++ 2015](https://www.youtube.com/watch?v=FnGCDLhaxKU&t=5046s)
 - [On "simple" Optimizations - Chandler Carruth - Secret Lightning Talks - Meeting C++ 2016](https://www.youtube.com/watch?v=s4wnuiCwTGU)
