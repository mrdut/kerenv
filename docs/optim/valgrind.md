# Valgrind


## Memcheck
## Cachegrind

## Callgrind


 - _Ir_: I cache reads (instructions executed)
 - _I1mr_: I1 cache read misses (instruction wasn't in I1 cache but was in L2)
 - _I2mr_: L2 cache instruction read misses (instruction wasn't in I1 or L2 cache, had to be fetched from memory)
 - _Dr_: D cache reads (memory reads)
 - _D1mr_: D1 cache read misses (data location not in D1 cache, but in L2)
 - _D2mr_: L2 cache data read misses (location not in D1 or L2)
 - _Dw_: D cache writes (memory writes)
 - _D1mw_: D1 cache write misses (location not in D1 cache, but in L2)
 - _D2mw_: L2 cache data write misses (location not in D1 or L2)

    [stamdford callgrind guide](https://web.stanford.edu/class/archive/cs/cs107/cs107.1174/guide_callgrind.html)

[cycle estimation]|(https://stackoverflow.com/questions/38311201/kcachegrind-cycle-estimation)

```
CEst = Ir + 10 L1m + 100 LLm
```

## Tricks and tips

L2 misses are much more expensive than L1 misses, so pay attention to passages with high D2mr or D2mw counts.

### [Profile only part of code](http://zariko.taba.free.fr/c++/callgrind_profile_only_a_part.html)

To be used with `--instr-atstart=no` valgrind option.

```
#include <valgrind/callgrind.h>

int main(int argc, char * argv[]) {
  longInitialisation();
  CALLGRIND_START_INSTRUMENTATION;
  int a = function(argc);
  CALLGRIND_STOP_INSTRUMENTATION;
  CALLGRIND_DUMP_STATS;
  cout << a << endl;
  return 0;
}
```

```bash
time valgrind --tool=memcheck  --leak-check=full --suppressions=/nfs/home/fdutrech/glib2.supp ./build/bin/smartw
```

## [Helgrind](http://valgrind.org/docs/manual/hg-manual.html)


```
{
    NAME # First line: its name. This merely gives a handy name to the suppression, by which it is referred to in the summary of used suppressions printed out when a program finishes. It's not important what the name is; any identifying string will do.

    TOOL[Memcheck,Helgrind] # Second line: name of the tool(s) that the suppression is for (if more than one, comma-separated), and the name of the suppression itself, separated by a colon (n.b.: no spaces are allowed), eg:

    tool_name1,tool_name2:suppression_name
    Recall that Valgrind is a modular system, in which different instrumentation tools can observe your program whilst it is running. Since different tools detect different kinds of errors, it is necessary to say which tool(s) the suppression is meaningful to.

    Tools will complain, at startup, if a tool does not understand any suppression directed to it. Tools ignore suppressions which are not directed to them. As a result, it is quite practical to put suppressions for all tools into the same suppression file.

    Next line: a small number of suppression types have extra information after the second line (eg. the Param suppression for Memcheck)

    Remaining lines: This is the calling context for the error -- the chain of function calls that led to it. There can be up to 24 of these lines.

    Locations may be names of either shared objects, functions, or source lines. They begin with obj:, fun:, or src: respectively. Function, object, and file names to match against may use the wildcard characters * and ?. Source lines are specified using the form filename[:lineNumber].

    Important note: C++ function names must be mangled. If you are writing suppressions by hand, use the --demangle=no option to get the mangled names in your error messages. An example of a mangled C++ name is _ZN9QListView4showEv. This is the form that the GNU C++ compiler uses internally, and the form that must be used in suppression files. The equivalent demangled name, QListView::show(), is what you see at the C++ source code level.

    A location line may also be simply "..." (three dots). This is a frame-level wildcard, which matches zero or more frames. Frame level wildcards are useful because they make it easy to ignore varying numbers of uninteresting frames in between frames of interest. That is often important when writing suppressions which are intended to be robust against variations in the amount of function inlining done by compilers.

    Finally, the entire suppression must be between curly braces. Each brace must be the first character on its own line.

}
```

    - --suppressions

## Suppression files




inscrease stack size : `--num-callers=100`

## Config

**glib2.supp**
```

# gobject
{
    constructor
    Memcheck:Leak
    ...
    fun:gobject_init_ctor
}

# glib

# GPrivate
{
    g_private_get
    Memcheck:Leak
    fun:malloc
    ...
    fun:g_private_*
}

# GThread
{
    g_thread_self
    Memcheck:Leak
    ...
    fun:g_slice_*
    fun:g_thread_self
}

{
    g_thread_new
    Memcheck:Leak
    ...
    fun:g_mutex_*
    ...
    fun:g_thread_*new
}

# GVariant
{
    g_variant_type_info_get
    Memcheck:Leak
    ...
    fun:g_rec_mutex_*
    ...
    fun:g_variant_type_info_get
}

# GMainContext
{
    g_main_context_default
    Memcheck:Leak
    ...
    fun:g_main_context_default
}

{
    g_main_context_new
    Memcheck:Leak
    ...
    fun:g_once_init_*
    fun:g_main_context_new
}

{
    g_main_context_new
    Memcheck:Leak
    ...
    fun:g_mutex_*
    fun:g_main_context_new
}

{
    g_main_context_dispatch
    Memcheck:Leak
    ...
    fun:g_slice_*
    ...
    fun:g_main_context_dispatch
}

# GQuark
{
    g_quake_from_
    Memcheck:Leak
    ...
    fun:g_quark_from_*
}

# Memory Slice
{
    g_slice_
    Memcheck:Leak
    ...
    fun:thread_memory_from_self
    fun:g_slice_*
}

# Character Set
{
    gconvert
    Memcheck:Leak
    ...
    fun:g_get_filename_charsets
}
```

```
{
    NAME # First line: its name. This merely gives a handy name to the suppression, by which it is referred to in the summary of used suppressions printed out when a program finishes. It's not important what the name is; any identifying string will do.

    TOOL[Memcheck,Helgrind] # Second line: name of the tool(s) that the suppression is for (if more than one, comma-separated), and the name of the suppression itself, separated by a colon (n.b.: no spaces are allowed), eg:

    tool_name1,tool_name2:suppression_name
    Recall that Valgrind is a modular system, in which different instrumentation tools can observe your program whilst it is running. Since different tools detect different kinds of errors, it is necessary to say which tool(s) the suppression is meaningful to.

    Tools will complain, at startup, if a tool does not understand any suppression directed to it. Tools ignore suppressions which are not directed to them. As a result, it is quite practical to put suppressions for all tools into the same suppression file.

    Next line: a small number of suppression types have extra information after the second line (eg. the Param suppression for Memcheck)

    Remaining lines: This is the calling context for the error -- the chain of function calls that led to it. There can be up to 24 of these lines.

    Locations may be names of either shared objects, functions, or source lines. They begin with obj:, fun:, or src: respectively. Function, object, and file names to match against may use the wildcard characters * and ?. Source lines are specified using the form filename[:lineNumber].

    Important note: C++ function names must be mangled. If you are writing suppressions by hand, use the --demangle=no option to get the mangled names in your error messages. An example of a mangled C++ name is _ZN9QListView4showEv. This is the form that the GNU C++ compiler uses internally, and the form that must be used in suppression files. The equivalent demangled name, QListView::show(), is what you see at the C++ source code level.

    A location line may also be simply "..." (three dots). This is a frame-level wildcard, which matches zero or more frames. Frame level wildcards are useful because they make it easy to ignore varying numbers of uninteresting frames in between frames of interest. That is often important when writing suppressions which are intended to be robust against variations in the amount of function inlining done by compilers.

    Finally, the entire suppression must be between curly braces. Each brace must be the first character on its own line.

}
```

```

{
   TF_RACE_2
   Helgrind:Race
   ...
   fun:execute_native_thread_routine
   fun:mythread_wrapper
   fun:start_thread
   fun:clone
}

{
   TF_MISC_2
   Helgrind:Misc
   ...
   fun:execute_native_thread_routine
   fun:mythread_wrapper
   fun:start_thread
   fun:clone
}

{
   TF_RACE_4
   Helgrind:Race
   ...
   fun:_ZN10tensorflow*
}

{
   TF_MISC_4
   Helgrind:Misc
   ...
   fun:_ZN10tensorflow*
}

{
   <insert_a_suppression_name_here>
   Helgrind:Race
   ...
   obj:/usr/local/lib/libavcodec.so.56.13.100
   ...
}

{
   <insert_a_suppression_name_here>
   Helgrind:Misc
   ...
   obj:/usr/local/lib/libavcodec.so.56.13.100
   ...
}

{
   <insert_a_suppression_name_here>
   Helgrind:Misc
   ...
   obj:/usr/local/lib/libswscale.so.3.1.101
   ...
}
{
   <insert_a_suppression_name_here>
   Helgrind:Race
   ...
   obj:/usr/local/lib/libswscale.so.3.1.101
   ...
}
```