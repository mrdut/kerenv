# Big O 

![](img/big-o.png)


The entire point of Big-O notation is to be able to compare how efficiently one algorithm solves big problems compared to another.
Some algorithms are good at problems when they’re small, but fail at scale, e.g., with very long lists of things to sort or search.
Big-O notation is the way to tell how good a given algorithm is at solving very large problems.

 - `O(1)` : _constant complexity_

> No matter what you provide as input to the algorithm, it’ll still run in the same amount of time.

>  - 1 item, 1 second
>  - 10 items, 1 second
>  - 100 items, 1 second
>  - Example: Determining if a binary number is even or odd.

```
void printFirstElementOfArray(int arr[])
{
    printf("First element of array = %d",arr[0]);
}
```


 - `O(log n)` : _logarithmic complexity_

> The calculation time barely increases as you exponentially increase the input numbers.

>  - 1 item, 1 second
>  - 10 items, 2 seconds
>  - 100 items, 3 seconds
>  - Example: Binary search.

It’s helpful to think of logarithms as exponents in reverse.

 - `O(n)` : _linear complexity_

> The calculation time increases at the same pace as the input.

>  - 1 item, 1 second
>  - 10 items, 10 seconds
>  - 100 items, 100 seconds
>  - Example: Unsorted list search.


 - `O(n2)` : _quadratic complexity_

> The calculation time increases at the pace of n2.

>  - 1 item, 1 second
>  - 10 items, 100 seconds
>  - 100 items, 10,000 seconds
>  - Example: Bubble sort.

Things raised to the second power are called quadratic because it comes from the Latin for “making square”, i.e., to multiply times itself.

 - `O(n!)` : _factorial complexity_

> The calculation time increases at the pace of n!, which means if n is 5, it’s 5x4x3x2x1, or 120. This isn’t so bad at low values of n, but it quickly becomes impossible.

>  - N=1, 1 option
>  - N=10, 3,628,800 options
>  - N=100, 9.332621544×10157 options
>  - Example: Traveling salesman.

N! is a bad place in the algorithm world. It means it’s basically unsolvable.

## Exemples

```

// assumed to be O(1)
void f();

// O(n)
for(int i=0 ; i<n ; ++i)
{
    f();
}

// O(2n) = O(n)
for(int i=0 ; i<n ; ++i)
{
    f(); 
    f(); 
}

// O(n2)
for(int i=0 ; i<n ; ++i)
    for(int j=0 ; j<n ; ++j)
        f();

// O(n2)
for(int i=0 ; i<n ; ++i)
    for(int j=0 ; j<i ; ++j)
        f();

// O(n3)
for(int i=0 ; i<n ; ++i)
    for(int j=0 ; j<n ; ++j)
        for(int k=0 ; k<n ; ++k)
            f();

// O(mnp)
for(int i=0 ; i<m ; ++i)
    for(int j=0 ; j<n ; ++j)
        for(int k=0 ; k<p ; ++k)
            f();

```


## Links

 - [tutorial (src for this page)](https://danielmiessler.com/study/big-o-notation/)