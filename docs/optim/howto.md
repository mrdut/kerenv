# Optimization

![Linux Performance observability tool](img/all_perf_tools.jpg)


### 60 seconds guideline

[Netflix - Linux Performance Analysis in 60,000 Milliseconds](https://netflixtechblog.com/linux-performance-analysis-in-60-000-milliseconds-accc10403c55)

1. `uptime` : load averages

```
$ uptime 
23:51:26 up 21:31, 1 user, load average: 30.02, 26.43, 19.02
```

    Check how evolves average system load (last 1 minute, 5 minutes, 15 minutes)
    It is recommanded that the average load is not greater than #thread (eg `cat proc cpuinfo -> ncores`), otherwise system is overloaded.

2. `dmesg -T | tail` : kernel errors

    Check if there is no error logged.

3. `vmstat 1` : virtual memory overall stats by time (here 1 second)

    ```
    procs ---------memory---------- ---swap-- -----io---- -system-- ------cpu-----
     r  b swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
    34  0    0 200889792  73708 591828    0    0     0     5    6   10 96  1  3  0  0
    32  0    0 200889920  73708 591860    0    0     0   592 13284 4282 98  1  1  0  0
    32  0    0 200890112  73708 591860    0    0     0     0 9501 2154 99  1  0  0  0
    ```

4. `mpstat -P ALL 1` : CPU balance, multi-processor stat per CPU 

Look for unbalanced workloads, hot CPU

5. `pidstat 1` : process usage

```
$ pidstat -t <PID>
...

$ pidstat -d <PID>
...
```

6. `iotop` or `iostat -xz 1` : disk I/O
7. `free -m` : main memory usage
8. `sar -n DEV 1` : network I/O
9. `sar -n TCP,ETCP 1` : TCP stats [Linux Performance observability tool  -SAR](http://www.brendangregg.com/Perf/linux_observability_sar.png)
10. `top` `htop` : check overview


[linux-performance-observability-tools](https://medium.com/@chrishantha/linux-performance-observability-tools-19ae2328f87f)
http://www.brendangregg.com/linuxperf.html

Going further:


1. `perf` sampling tool

Locate hotspot

(1. `gprof` tracing tool)
1. `valgrind` : Emulation on virtual CPU
1. [grafana](https://grafana.com/grafana/) : analytics platform for all your metrics

1. [compiler explorer](https://godbolt.org/)
1. [quick-bench : online google benchmark + compiler explorer + perf] ](http://quick-bench.com/G7B2w0xPUWgOVvuzI7unES6cU4w)
1. `google benchmark`
1. strace -> syscall tracing : `strace -tttT -p PID` (massive overhead, eg slow target over x100), ptrace based



### [**USE** method](http://www.brendangregg.com/usemethod.html)

For every ressource, check:

1. Utilization
1. Saturations
1. Erros

Ressource is one of :
 - CPUs: sockets, cores, hardware threads (virtual CPUs)
 - Memory: capacity
 - Network interfaces
 - Storage devices: I/O, capacity
 - Controllers: storage, network cards
 - Interconnects: CPUs, memory, I/O

### General

 - profile binary performance : `perf` -> `gprof` -> `valgrind`
 - profile processes : `htop`
 - profile disk usage (I/O) : `iotop` or `iostat -x 2`
 - profile network : `iftop -i eno1` (to see interface name, eg `eno1` -> `ip a`)


basic : vmstat, iostat, mpstat, ps, top, ...
medium : strace, netstat, tcpdump, netstat, nicstat, pidstat, sar, swapon, lsof, ...
advanced: ss, slaptop, perf_events, ...

### Ressources

http://igoro.com/archive/gallery-of-processor-cache-effects/

    [netflix tech blog - performance](https://netflixtechblog.com/tagged/performance)
    [redhat performance blog](https://developers.redhat.com/blog/category/performance/)

 - [[slides] ](https://github.com/CppCon/CppCon2014/blob/master/Presentations/Data-Oriented%20Design%20and%20C%2B%2B/Data-Oriented%20Design%20and%20C%2B%2B%20-%20Mike%20Acton%20-%20CppCon%202014.pptx)[CppCon 2014: Mike Acton "Data-Oriented Design and C++"](https://www.youtube.com/watch?v=rX0ItVEVjHc)
 - [[slides] ](https://github.com/CppCon/CppCon2014/blob/master/Presentations/Efficiency%20with%20Algorithms%2C%20Performance%20with%20Data%20Structures/Efficiency%20with%20Algorithms%2C%20Performance%20with%20Data%20Structures%20-%20Chandler%20Carruth%20-%20CppCon%202014.pdf)[CppCon 2014: Chandler Carruth "Efficiency with Algorithms, Performance with Data Structures"](https://www.youtube.com/watch?v=fHNmRkzxHWs)
     - _"efficiency through algorithm"_ (do the minimum amount of work to accomplish a given task)
     - _"performance through Data structures"_ (how quickly a program does its work)
      -_"nothing in the code is slow, but kinda everything is not fast. [...] It comes from systemic minor efficiency problems"
 - [CppCon 2015: Chandler Carruth "Tuning C++: Benchmarks, and CPUs, and Compilers! Oh My!"](https://www.youtube.com/watch?v=nXaxk27zwlk&t=719s) 
     - _"benchmark the code that matters"_
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2016/garbage_in_garbage_out.html#1)[CppCon 2016: Chandler Carruth “Garbage In, Garbage Out: Arguing about Undefined Behavior..."](https://www.youtube.com/watch?v=yG1OZ69H_-o&t=1662s)
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2016/hybrid_data_structures.html#1)[CppCon 2016: Chandler Carruth “High Performance Code 201: Hybrid Data Structures"](https://www.youtube.com/watch?v=vElZc6zSIXM&t=508s)
     - _"use hybrid data structures to optimize allocations and cache locality"_
 - [[slides] ](https://chandlerc.github.io/talks/cppcon2017/going_nowhere_faster.html#1)[CppCon 2017: Chandler Carruth “Going Nowhere Faster”](https://www.youtube.com/watch?v=2EWejmkKlxs&t=3174s) 
 - [CppCon 2018: Chandler Carruth “Spectre: Secrets, Side-Channels, Sandboxes, and Security”](https://www.youtube.com/watch?v=_f7O3IfIR2k&t=7s)
 - [CppCon 2019: Chandler Carruth “There Are No Zero-cost Abstractions”](https://www.youtube.com/watch?v=rHIkrotSwcc&t=537s)
 - [CppCon 2019: Chandler Carruth, Titus Winters “What is C++”](https://www.youtube.com/watch?v=LJh5QCV4wDg&t=1147s)

 - [Understanding Compiler Optimization - Chandler Carruth - Opening Keynote Meeting C++ 2015](https://www.youtube.com/watch?v=FnGCDLhaxKU&t=5046s)
 - [On "simple" Optimizations - Chandler Carruth - Secret Lightning Talks - Meeting C++ 2016](https://www.youtube.com/watch?v=s4wnuiCwTGU)
