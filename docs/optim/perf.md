# Perf

Performance analysis tools for Linux

```
man perf-stat
man perf-top
man perf-record
man perf-report
man perf-list
```

By default, perf uses the ‘cycles’ event, with sampling frequency = 4 kHz.

### Usage

 - `perf stat` : obtain event counts
 - `perf record` : record events for later reporting
 - `perf report` : break down events by process, function, etc.
 - `perf annotate` : annotate assembly or source code with event counts
 - `perf top` : see live event count
 - `perf bench` : run different kernel microbenchmarks
 - -g : call graph
 - --no-children : do not cumulate ...

```
# record (compile with -fno-omit-pointer)
perf record -F 99 -g ./build/bin/blue-ski
perf record -F 99 --call-graph dwarf -- build/bin/blue-ski --no-display --progress                                                                    [Bluecime::extra-eyes] (15:24)

```

# compile with -fno-omit-frame-pointer flag, otherwise lots of information of interest is discarded at compilation... + almost not decrease performance at all OR compile cmake None https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html

```
# play (either caller/callee)
perf report -g 'graph,0.5,caller' --no-children
```


```
perf record -g -F 99  build/bin/blue-ski --no-display --progress

export PATH=$PATH:/work-crypt/fdutrech/third_party/FlameGraph
perf script | stackcollapse-perf.pl | flamegraph.pl > perf.svg


```

## Hotspots

https://github.com/KDAB/hotspot#on-fedora

```
git clone 

cd 

mkdir build && cd build
cmake ..
make hotspot-perfparser -j5
make hotspot -j5
./bin/hotspot
```

## Gotchas

 - Stack trace and symbols often don't work
    Can be a significant project to fix them. It worth it !
     `-fno-omit-frame-pointer`

## Links

man perf-report
    all man

[perf Examples](http://www.brendangregg.com/perf.html)
[CppCon 2015: Chandler Carruth "Tuning C++: Benchmarks, and CPUs, and Compilers! Oh My!"](https://www.youtube.com/watch?v=nXaxk27zwlk&t=719s) 
[IBM tutorial - Analyzing performance with perf annotate](https://developer.ibm.com/technologies/linux/tutorials/l-analyzing-performance-perf-annotate-trs)

[Linux perf for Qt developers - Milian Wolff](https://www.kdab.com/linux-perf-for-qt-developers/)

[perf at Netflix](https://www.slideshare.net/brendangregg/kernel-recipes-2017-using-linux-perf-at-netflix)

https://muehe.org/posts/profiling-only-parts-of-your-code-with-perf/
https://github.com/KDAB/hotspot#on-fedora
perf visualizer