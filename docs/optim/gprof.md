# GProf

Display call graph profile data

## Usage

 1. Enable profiling during compilation : `-pg` flag
 2. Run code `./prog` , it will generate a `gmon.out` file
 3. Explore profile : `gprof ./prog gmon.out`

```
# Flat profile:

Each sample counts as 0.01 seconds.
%   cumulative   self              self     total
time   seconds   seconds    calls  us/call  us/call  name
37.50      0.15     0.15    48000     3.12     3.12  Life::neighbor_count(int, int)
17.50      0.22     0.07                             _IO_do_write
10.00      0.26     0.04                             __overflow
7.50      0.29     0.03                             _IO_file_overflow
7.50      0.32     0.03                             _IO_putc
5.00      0.34     0.02       12  1666.67 14166.67  Life::update(void)
5.00      0.36     0.02                             stdiobuf::overflow(int)
5.00      0.38     0.02                             stdiobuf::sys_write(char const *, int)
2.50      0.39     0.01                             ostream::operator<<(char)
2.50      0.40     0.01                             internal_mcount
0.00      0.40     0.00       12     0.00     0.00  Life::print(void)
0.00      0.40     0.00       12     0.00     0.00  to_continue(void)
0.00      0.40     0.00        1     0.00     0.00  Life::initialize(void)
0.00      0.40     0.00        1     0.00     0.00  instructions(void)
0.00      0.40     0.00        1     0.00 170000.00  main
```

```
# Call graph

granularity: each sample hit covers 4 byte(s) for 2.50% of 0.40 seconds

index % time    self  children    called     name
                0.02    0.15      12/12          main [2]
[1]     42.5    0.02    0.15      12         Life::update(void) [1]
                0.15    0.00   48000/48000       Life::neighbor_count(int, int) [4]
-----------------------------------------------
                0.00    0.17       1/1           _start [3]
[2]     42.5    0.00    0.17       1         main [2]
                0.02    0.15      12/12          Life::update(void) [1]
                0.00    0.00      12/12          Life::print(void) [13]
                0.00    0.00      12/12          to_continue(void) [14]
                0.00    0.00       1/1           instructions(void) [16]
                0.00    0.00       1/1           Life::initialize(void) [15]
-----------------------------------------------
                                                 
[3]     42.5    0.00    0.17                 _start [3]
                0.00    0.17       1/1           main [2]
-----------------------------------------------
                0.15    0.00   48000/48000       Life::update(void) [1]
[4]     37.5    0.15    0.00   48000         Life::neighbor_count(int, int) [4]
-----------------------------------------------
```

## Links

    
 - [quick start guide](https://web.eecs.umich.edu/~sugih/pointers/gprof_quick.html)
 - [tutorial](https://www.thegeekstuff.com/2012/08/gprof-tutorial/)