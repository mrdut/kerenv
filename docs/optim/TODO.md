# TODO

## TO SORT

https://colfaxresearch.com/how-tools/

 - books
https://www.sciencedirect.com/book/9780128007266/power-and-performance
https://www.amazon.com/Software-Performance-Scalability-Quantitative-Approach/dp/0470462531/ref=pd_sbs_14_t_0/141-0478337-3050253?_encoding=UTF8&pd_rd_i=0470462531&pd_rd_r=35deb1b9-d3a2-48b6-ad7a-58457ceb3b9f&pd_rd_w=OZrM9&pd_rd_wg=FxLZd&pf_rd_p=5cfcfe89-300f-47d2-b1ad-a4e27203a02a&pf_rd_r=7ZCHVXD355AZBR2AQ4R0&psc=1&refRID=7ZCHVXD355AZBR2AQ4R0
https://github.com/gg-daddy/ebooks/blob/master/Systems.Performance.Enterprise.and.the.Cloud.2013.10.pdf

 - blog
https://lemire.me/blog/2018/07/31/getting-4-bytes-or-a-full-cache-line-same-speed-or-not/
https://speakerdeck.com/lemire/parsing-json-really-quickly-lessons-learned?slide=63
https://stackoverflow.com/questions/4365980/how-to-use-profile-guided-optimizations-in-g
https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/ms973852(v=msdn.10)?redirectedfrom=MSDN
https://www.eetimes.com/optimizing-for-instruction-caches-part-1/#
https://www.aristeia.com/TalkNotes/codedive-CPUCachesHandouts.pdf
https://github.com/fenbf/AwesomePerfCpp/blob/master/README.md
https://software.intel.com/en-us/modern-code/overview

 - code
https://github.com/vtjnash/atlas-3.10.0/blob/5b4899fc806c367552c052d6d2cf3febb1e8fea0/tune/sysinfo/ATL_cputime.c
https://github.com/Const-me/MemoryAccessCosts/blob/master/MemoryAccessCosts.cpp
https://github.com/lemire/Code-used-on-Daniel-Lemire-s-blog/blob/master/2018/07/31/cachelinefun.c
https://en.wikipedia.org/wiki/Big_O_notation

- talks
https://www.youtube.com/watch?v=WDIkqP4JbkE
https://www.youtube.com/watch?v=h58X-PaEGng
https://www.youtube.com/user/vadikus0/videos?view=0&sort=dd&shelf_id=2
[computed goto](https://www.youtube.com/watch?v=9cPU1NdsgDQ&list=PLFjq8z-aGyQ77zA5OkzNETLca4ad864Da&index=16&t=0s)

### General

 - profile binary performance : `perf` -> `gprof` -> `valgrind`
 - profile processes : `htop`
 - profile disk usage (I/O) : `iotop` or `iostat -x 2`
 - profile network : `iftop -i eno1` (to see interface name, eg `eno1` -> `ip a`)

 - use grafana : http://bangkok:3000/login
    - Home > Node explorer full > choose node
        - load is in > Filesystem details


