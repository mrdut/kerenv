from recommonmark.parser import CommonMarkParser

# exemple file : https://github.com/pydanny/markdown-guide/blob/master/docs/conf.py

source_parsers = {
    '.md': CommonMarkParser,
}


# General information about the project.
project = u'KerEnv'
copyright = u'2012, Daniel Greenfeld'

# The suffix of source filenames.
source_suffix = ['.rst', '.md']

# The master toctree document.
master_doc = 'index'
