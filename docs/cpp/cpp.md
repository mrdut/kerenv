# Cpp tricks and tips

### Books

The linux programming interface
Standard C++ IOStreams and Locales: Advanced Programmer's Guide and Reference
C++ Templates: The Complete Guide (2nd Edition) 2nd Edition

### Style

https://google.github.io/styleguide/cppguide.html




### Best practices 

 - use `unique_ptr` when possible over `shared_ptr`
 - pass object by referece or pointers (if optional), not `unique_ptr` or `shared_ptr`, nobody cares how you manage your object memory...
 - [use `std::unordered_map` when possible over `std::map`](https://www.geeksforgeeks.org/map-vs-unordered_map-c/)
 - "Many minor coding issue leads to major performance drop"
 - `override`, `final`, ... not usefull for compiler but for readability
 - Interface classe : Interface suffix
 - **every comment is a failure to express yourself in code**
 - https://youtu.be/JMrfqFwm_wA
 - https://wiki.qt.io/D-Pointer
 - [From STUPID to SOLID Code!](https://williamdurand.fr/2013/07/30/from-stupid-to-solid-code/)


 - re-evaluate rules periodically :)

### Comments

```
-- hpp --

// WHAT : do something
void method();
-- cpp --

// HOW : implements <paper>
void method() {}
```