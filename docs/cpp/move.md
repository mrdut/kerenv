# Move

```
#! /usr/bin/bash
COUNTER=1

for test in \
    "Object obj(\"hey\");" \
    "Object obj = Object(\"hey\");" \
    "Object obj( Object::fromStatic() );" \
    "Object obj=Object::fromStatic();" \
    "Object obj(\"hey\"); Object obj2(obj);" \
    "Object obj(\"hey\"); Object obj2=obj;" \
    "Object obj(\"hey\"); Object obj2(\"ho\"); obj2=obj;" \
    "Object obj(\"hey\"); obj=Object::fromStatic();" \
        ; do

    echo "# Test $COUNTER"
    echo "\`$test\`"

    # echo ""
    # echo "VALGRIND"
    # g++ main.cpp -o MoveIt -DTEST="$test" -std=c++11 && valgrind --tool=memcheck  ./MoveIt

    for flag in " " "-DNO_MOVE" "-fno-elide-constructors " "-DNO_MOVE  -fno-elide-constructors "; do
        echo ""
        echo " - g++ flags : -std=c++11 **$flag**"
        echo ""
        echo "\`\`\`"
        g++ main.cpp -o MoveIt -DTEST="$test" $flag -std=c++11 && ./MoveIt
        echo "\`\`\`"
    done

    COUNTER=$[$COUNTER +1]
done
```

```
// compile and run : g++ /tmp/hey.cpp -o MoveIt -std=c++11  -fno-elide-constructors  && ./MoveIt

#include <iostream>
#include <assert.h>
#include <string.h>


// #define NO_MOVE

#define BOLD(string) string
// #define BOLD(string) "\e[1m" string "\e[0m"

// http://www.cplusplus.com/doc/tutorial/classes2/
class Object
{
  public:
    static int ncopy;
    static int nalloc;
    static int nclear;

    // constructor
    Object(const char* name)
        : m_name(nullptr)
    {
        std::cout << "Object() - " BOLD("constructor") << "[this=" << this << "]" << '\n';
        alloc(name);
        copy(m_name, name);
    }

    // destructor
    ~Object()
    {
        std::cout << "~Object() - " BOLD("destructor") << *this << '\n';
        clear();
    }

    // copy constructor
    Object(const Object & obj)
        : m_name(nullptr)
    {
        std::cout << "Object(const Object & obj) - " BOLD("copy constructor") << *this << '\n';
        alloc(obj.m_name);
        copy(m_name, obj.m_name);
    }


    // copy assignment
    Object & operator=(const Object & obj)
    {
        std::cout << "operator=(const Object & obj) - " BOLD("copy assignment")  << *this << '\n';
        clear();
        alloc(obj.m_name);
        copy(m_name, obj.m_name);
        return *this;
    }

    #ifndef NO_MOVE
        // move constructor - disable g++ Return Value Optimization ( -fno-elide-constructors ), otherwise never call...
        Object(Object && obj)
            : m_name(obj.m_name)
        {
            std::cout << "Object(Object && obj) - " BOLD("move constructor") << *this << '\n';
            obj.m_name=nullptr;
        }

        // move assignment
        Object & operator= (Object&& obj)
        {
            std::cout << "operator=(Object && obj) - " BOLD("move assignment")  << *this << '\n';

            clear();

            m_name = obj.m_name;
            obj.m_name=nullptr;

            return *this;
        }
    #endif

    static Object fromStatic()
    {
        Object obj("fromStatic::");
        return obj;
    }

    friend std::ostream & operator<<(std::ostream & os, const Object & obj)
    {
        return os <<  "[name=\"" << (obj.m_name==nullptr ? "<nullptr>" : obj.m_name) << "\" this=" << &obj << "]";
    }

  private:

    void copy(char* dst, const char* src)
    {
        ncopy++;
        strcpy(dst, src);
        std::cout << "\t└── copy(\"" << src << "\"=>" << (void*)dst << ")\n";
    }

    void clear()
    {
        if(m_name!=nullptr)
        {
            nclear++;
            std::cout << "\t└── clear (" << strlen(m_name) << " bytes allocated " << (void*) m_name << ")\n";
            delete[] m_name;
            m_name=nullptr;
        }
        else
        {
            std::cout << "\t└── clear (no memory allocated)\n";
        }
    }

    void alloc(const char* name)
    {
        assert(m_name==nullptr);
        nalloc++;

        const size_t n = strlen(name)+1;
        m_name=new char[ n ];

        std::cout << "\t└── alloc("<< n << " bytes " << (void*)m_name << ")\n";

    }


    char* m_name;
};

int Object::ncopy = 0;
int Object::nalloc = 0;
int Object::nclear = 0;

int main(int argc, const char** argv)
{

    { TEST }

    std::cout << "Object::ncopy = " << Object::ncopy << '\n';
    std::cout << "Object::nalloc = " << Object::nalloc << '\n';
    std::cout << "Object::nclear = " << Object::nclear << '\n';
}

```



# Test 1 : Constructor
`Object obj("hey");`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffe099522b0]
    └── alloc(4 bytes 0x2246c20)
    └── copy("hey"=>0x2246c20)
~Object() - destructor[name="hey" this=0x7ffe099522b0]
    └── clear (3 bytes allocated 0x2246c20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

# Test 2 : Move constructor
`Object obj = Object("hey");`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffc8e66a7a0]
    └── alloc(4 bytes 0x22b5c20)
    └── copy("hey"=>0x22b5c20)
~Object() - destructor[name="hey" this=0x7ffc8e66a7a0]
    └── clear (3 bytes allocated 0x22b5c20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffec56eba90]
    └── alloc(4 bytes 0xf3bc20)
    └── copy("hey"=>0xf3bc20)
Object(Object && obj) - move constructor[name="hey" this=0x7ffec56eba80]
~Object() - destructor[name="<nullptr>" this=0x7ffec56eba90]
    └── clear (no memory allocated)
~Object() - destructor[name="hey" this=0x7ffec56eba80]
    └── clear (3 bytes allocated 0xf3bc20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7fff34475550]
    └── alloc(4 bytes 0x1db2c20)
    └── copy("hey"=>0x1db2c20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7fff34475540]
    └── alloc(4 bytes 0x1db2c40)
    └── copy("hey"=>0x1db2c40)
~Object() - destructor[name="hey" this=0x7fff34475550]
    └── clear (3 bytes allocated 0x1db2c20)
~Object() - destructor[name="hey" this=0x7fff34475540]
    └── clear (3 bytes allocated 0x1db2c40)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```
# Test 3
`Object obj( Object::fromStatic() );`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffdc1d52310]
    └── alloc(13 bytes 0xa1ec20)
    └── copy("fromStatic::"=>0xa1ec20)
~Object() - destructor[name="fromStatic::" this=0x7ffdc1d52310]
    └── clear (12 bytes allocated 0xa1ec20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffee3320d50]
    └── alloc(13 bytes 0x133cc20)
    └── copy("fromStatic::"=>0x133cc20)
Object(Object && obj) - move constructor[name="fromStatic::" this=0x7ffee3320da0]
~Object() - destructor[name="<nullptr>" this=0x7ffee3320d50]
    └── clear (no memory allocated)
Object(Object && obj) - move constructor[name="fromStatic::" this=0x7ffee3320d90]
~Object() - destructor[name="<nullptr>" this=0x7ffee3320da0]
    └── clear (no memory allocated)
~Object() - destructor[name="fromStatic::" this=0x7ffee3320d90]
    └── clear (12 bytes allocated 0x133cc20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7fff1f921bb0]
    └── alloc(13 bytes 0x9fdc20)
    └── copy("fromStatic::"=>0x9fdc20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7fff1f921c00]
    └── alloc(13 bytes 0x9fdc40)
    └── copy("fromStatic::"=>0x9fdc40)
~Object() - destructor[name="fromStatic::" this=0x7fff1f921bb0]
    └── clear (12 bytes allocated 0x9fdc20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7fff1f921bf0]
    └── alloc(13 bytes 0x9fdc20)
    └── copy("fromStatic::"=>0x9fdc20)
~Object() - destructor[name="fromStatic::" this=0x7fff1f921c00]
    └── clear (12 bytes allocated 0x9fdc40)
~Object() - destructor[name="fromStatic::" this=0x7fff1f921bf0]
    └── clear (12 bytes allocated 0x9fdc20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```
# Test 4
`Object obj=Object::fromStatic();`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffe40716400]
    └── alloc(13 bytes 0x24a4c20)
    └── copy("fromStatic::"=>0x24a4c20)
~Object() - destructor[name="fromStatic::" this=0x7ffe40716400]
    └── clear (12 bytes allocated 0x24a4c20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffdfb8693d0]
    └── alloc(13 bytes 0xdf9c20)
    └── copy("fromStatic::"=>0xdf9c20)
Object(Object && obj) - move constructor[name="fromStatic::" this=0x7ffdfb869420]
~Object() - destructor[name="<nullptr>" this=0x7ffdfb8693d0]
    └── clear (no memory allocated)
Object(Object && obj) - move constructor[name="fromStatic::" this=0x7ffdfb869410]
~Object() - destructor[name="<nullptr>" this=0x7ffdfb869420]
    └── clear (no memory allocated)
~Object() - destructor[name="fromStatic::" this=0x7ffdfb869410]
    └── clear (12 bytes allocated 0xdf9c20)
Object::ncopy = 1
Object::nalloc = 1
Object::nclear = 1
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7ffe535a25b0]
    └── alloc(13 bytes 0x1efac20)
    └── copy("fromStatic::"=>0x1efac20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffe535a2600]
    └── alloc(13 bytes 0x1efac40)
    └── copy("fromStatic::"=>0x1efac40)
~Object() - destructor[name="fromStatic::" this=0x7ffe535a25b0]
    └── clear (12 bytes allocated 0x1efac20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffe535a25f0]
    └── alloc(13 bytes 0x1efac20)
    └── copy("fromStatic::"=>0x1efac20)
~Object() - destructor[name="fromStatic::" this=0x7ffe535a2600]
    └── clear (12 bytes allocated 0x1efac40)
~Object() - destructor[name="fromStatic::" this=0x7ffe535a25f0]
    └── clear (12 bytes allocated 0x1efac20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```
# Test 5
`Object obj("hey"); Object obj2(obj);`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffd72608d90]
    └── alloc(4 bytes 0x1de4c20)
    └── copy("hey"=>0x1de4c20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffd72608d80]
    └── alloc(4 bytes 0x1de4c40)
    └── copy("hey"=>0x1de4c40)
~Object() - destructor[name="hey" this=0x7ffd72608d80]
    └── clear (3 bytes allocated 0x1de4c40)
~Object() - destructor[name="hey" this=0x7ffd72608d90]
    └── clear (3 bytes allocated 0x1de4c20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffc10511600]
    └── alloc(4 bytes 0xbb2c20)
    └── copy("hey"=>0xbb2c20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffc105115f0]
    └── alloc(4 bytes 0xbb2c40)
    └── copy("hey"=>0xbb2c40)
~Object() - destructor[name="hey" this=0x7ffc105115f0]
    └── clear (3 bytes allocated 0xbb2c40)
~Object() - destructor[name="hey" this=0x7ffc10511600]
    └── clear (3 bytes allocated 0xbb2c20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7ffef1628ab0]
    └── alloc(4 bytes 0x1daec20)
    └── copy("hey"=>0x1daec20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffef1628aa0]
    └── alloc(4 bytes 0x1daec40)
    └── copy("hey"=>0x1daec40)
~Object() - destructor[name="hey" this=0x7ffef1628aa0]
    └── clear (3 bytes allocated 0x1daec40)
~Object() - destructor[name="hey" this=0x7ffef1628ab0]
    └── clear (3 bytes allocated 0x1daec20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```
# Test 6
`Object obj("hey"); Object obj2=obj;`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7fff01b43ef0]
    └── alloc(4 bytes 0x1bb5c20)
    └── copy("hey"=>0x1bb5c20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7fff01b43ee0]
    └── alloc(4 bytes 0x1bb5c40)
    └── copy("hey"=>0x1bb5c40)
~Object() - destructor[name="hey" this=0x7fff01b43ee0]
    └── clear (3 bytes allocated 0x1bb5c40)
~Object() - destructor[name="hey" this=0x7fff01b43ef0]
    └── clear (3 bytes allocated 0x1bb5c20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffc8db722d0]
    └── alloc(4 bytes 0x177ec20)
    └── copy("hey"=>0x177ec20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffc8db722c0]
    └── alloc(4 bytes 0x177ec40)
    └── copy("hey"=>0x177ec40)
~Object() - destructor[name="hey" this=0x7ffc8db722c0]
    └── clear (3 bytes allocated 0x177ec40)
~Object() - destructor[name="hey" this=0x7ffc8db722d0]
    └── clear (3 bytes allocated 0x177ec20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7ffeabcbb820]
    └── alloc(4 bytes 0x18e1c20)
    └── copy("hey"=>0x18e1c20)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffeabcbb810]
    └── alloc(4 bytes 0x18e1c40)
    └── copy("hey"=>0x18e1c40)
~Object() - destructor[name="hey" this=0x7ffeabcbb810]
    └── clear (3 bytes allocated 0x18e1c40)
~Object() - destructor[name="hey" this=0x7ffeabcbb820]
    └── clear (3 bytes allocated 0x18e1c20)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```
# Test 7
`Object obj("hey"); Object obj2("ho"); obj2=obj;`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffc53a81ff0]
    └── alloc(4 bytes 0x20edc20)
    └── copy("hey"=>0x20edc20)
Object() - constructor[this=0x7ffc53a81fe0]
    └── alloc(3 bytes 0x20edc40)
    └── copy("ho"=>0x20edc40)
operator=(const Object & obj) - copy assignment[name="ho" this=0x7ffc53a81fe0]
    └── clear (2 bytes allocated 0x20edc40)
    └── alloc(4 bytes 0x20edc40)
    └── copy("hey"=>0x20edc40)
~Object() - destructor[name="hey" this=0x7ffc53a81fe0]
    └── clear (3 bytes allocated 0x20edc40)
~Object() - destructor[name="hey" this=0x7ffc53a81ff0]
    └── clear (3 bytes allocated 0x20edc20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```

 - g++ flags : -std=c++11 **-DNO_MOVE**

```
Object() - constructor[this=0x7ffc91410be0]
    └── alloc(4 bytes 0x111bc20)
    └── copy("hey"=>0x111bc20)
Object() - constructor[this=0x7ffc91410bd0]
    └── alloc(3 bytes 0x111bc40)
    └── copy("ho"=>0x111bc40)
operator=(const Object & obj) - copy assignment[name="ho" this=0x7ffc91410bd0]
    └── clear (2 bytes allocated 0x111bc40)
    └── alloc(4 bytes 0x111bc40)
    └── copy("hey"=>0x111bc40)
~Object() - destructor[name="hey" this=0x7ffc91410bd0]
    └── clear (3 bytes allocated 0x111bc40)
~Object() - destructor[name="hey" this=0x7ffc91410be0]
    └── clear (3 bytes allocated 0x111bc20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7ffea8b1db50]
    └── alloc(4 bytes 0x12b3c20)
    └── copy("hey"=>0x12b3c20)
Object() - constructor[this=0x7ffea8b1db40]
    └── alloc(3 bytes 0x12b3c40)
    └── copy("ho"=>0x12b3c40)
operator=(const Object & obj) - copy assignment[name="ho" this=0x7ffea8b1db40]
    └── clear (2 bytes allocated 0x12b3c40)
    └── alloc(4 bytes 0x12b3c40)
    └── copy("hey"=>0x12b3c40)
~Object() - destructor[name="hey" this=0x7ffea8b1db40]
    └── clear (3 bytes allocated 0x12b3c40)
~Object() - destructor[name="hey" this=0x7ffea8b1db50]
    └── clear (3 bytes allocated 0x12b3c20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7ffd80722c40]
    └── alloc(4 bytes 0x17a2c20)
    └── copy("hey"=>0x17a2c20)
Object() - constructor[this=0x7ffd80722c30]
    └── alloc(3 bytes 0x17a2c40)
    └── copy("ho"=>0x17a2c40)
operator=(const Object & obj) - copy assignment[name="ho" this=0x7ffd80722c30]
    └── clear (2 bytes allocated 0x17a2c40)
    └── alloc(4 bytes 0x17a2c40)
    └── copy("hey"=>0x17a2c40)
~Object() - destructor[name="hey" this=0x7ffd80722c30]
    └── clear (3 bytes allocated 0x17a2c40)
~Object() - destructor[name="hey" this=0x7ffd80722c40]
    └── clear (3 bytes allocated 0x17a2c20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```
# Test 8
`Object obj("hey"); obj=Object::fromStatic();`

 - g++ flags : -std=c++11 ** **

```
Object() - constructor[this=0x7ffd954a5800]
    └── alloc(4 bytes 0x1cc4c20)
    └── copy("hey"=>0x1cc4c20)
Object() - constructor[this=0x7ffd954a5810]
    └── alloc(13 bytes 0x1cc4c40)
    └── copy("fromStatic::"=>0x1cc4c40)
operator=(Object && obj) - move assignment[name="hey" this=0x7ffd954a5800]
    └── clear (3 bytes allocated 0x1cc4c20)
~Object() - destructor[name="<nullptr>" this=0x7ffd954a5810]
    └── clear (no memory allocated)
~Object() - destructor[name="fromStatic::" this=0x7ffd954a5800]
    └── clear (12 bytes allocated 0x1cc4c40)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-DNO_MOVE**

```
Object() - constructor[this=0x7ffc61642610]
    └── alloc(4 bytes 0xec4c20)
    └── copy("hey"=>0xec4c20)
Object() - constructor[this=0x7ffc61642620]
    └── alloc(13 bytes 0xec4c40)
    └── copy("fromStatic::"=>0xec4c40)
operator=(const Object & obj) - copy assignment[name="hey" this=0x7ffc61642610]
    └── clear (3 bytes allocated 0xec4c20)
    └── alloc(13 bytes 0xec4c20)
    └── copy("fromStatic::"=>0xec4c20)
~Object() - destructor[name="fromStatic::" this=0x7ffc61642620]
    └── clear (12 bytes allocated 0xec4c40)
~Object() - destructor[name="fromStatic::" this=0x7ffc61642610]
    └── clear (12 bytes allocated 0xec4c20)
Object::ncopy = 3
Object::nalloc = 3
Object::nclear = 3
```

 - g++ flags : -std=c++11 **-fno-elide-constructors **

```
Object() - constructor[this=0x7fff88ae10c0]
    └── alloc(4 bytes 0x1a08c20)
    └── copy("hey"=>0x1a08c20)
Object() - constructor[this=0x7fff88ae1080]
    └── alloc(13 bytes 0x1a08c40)
    └── copy("fromStatic::"=>0x1a08c40)
Object(Object && obj) - move constructor[name="fromStatic::" this=0x7fff88ae10d0]
~Object() - destructor[name="<nullptr>" this=0x7fff88ae1080]
    └── clear (no memory allocated)
operator=(Object && obj) - move assignment[name="hey" this=0x7fff88ae10c0]
    └── clear (3 bytes allocated 0x1a08c20)
~Object() - destructor[name="<nullptr>" this=0x7fff88ae10d0]
    └── clear (no memory allocated)
~Object() - destructor[name="fromStatic::" this=0x7fff88ae10c0]
    └── clear (12 bytes allocated 0x1a08c40)
Object::ncopy = 2
Object::nalloc = 2
Object::nclear = 2
```

 - g++ flags : -std=c++11 **-DNO_MOVE  -fno-elide-constructors **

```
Object() - constructor[this=0x7ffc109fdcb0]
    └── alloc(4 bytes 0xbe5c20)
    └── copy("hey"=>0xbe5c20)
Object() - constructor[this=0x7ffc109fdc70]
    └── alloc(13 bytes 0xbe5c40)
    └── copy("fromStatic::"=>0xbe5c40)
Object(const Object & obj) - copy constructor[name="<nullptr>" this=0x7ffc109fdcc0]
    └── alloc(13 bytes 0xbe5c60)
    └── copy("fromStatic::"=>0xbe5c60)
~Object() - destructor[name="fromStatic::" this=0x7ffc109fdc70]
    └── clear (12 bytes allocated 0xbe5c40)
operator=(const Object & obj) - copy assignment[name="hey" this=0x7ffc109fdcb0]
    └── clear (3 bytes allocated 0xbe5c20)
    └── alloc(13 bytes 0xbe5c20)
    └── copy("fromStatic::"=>0xbe5c20)
~Object() - destructor[name="fromStatic::" this=0x7ffc109fdcc0]
    └── clear (12 bytes allocated 0xbe5c60)
~Object() - destructor[name="fromStatic::" this=0x7ffc109fdcb0]
    └── clear (12 bytes allocated 0xbe5c20)
Object::ncopy = 4
Object::nalloc = 4
Object::nclear = 4
```
