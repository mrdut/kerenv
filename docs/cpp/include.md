# Include

## Name mangling

```c++
#ifdef _cplusplus
	extern C {
#endif
...
#ifdef _cplusplus
    }
#endif
```

```bash
# C++ -> name mangling (support function overloading)
>> objdump -t a.out
0000000000698b30 g     F .text	00000000000000e9              _ZN3vps9analytics2ai21NMSLayerPluginCreator12
00000000006c0640 g     F .text	0000000000000017              _ZN3vps9analytics12segmentation12SegmentationD1Ev

# C
>> objdump -t a.out
000000000035c7a0 g     F .text	00000000000000e5              gst_value_list_append_and_take_value
000000000049d660 g     F .text	0000000000000015              BIO_meth_set_read

# => if binary is C++ and lib C, linker cannot find symbols without `extern "C"`
```

