# Casting

 - _c-style_ casting : `(TYPE) expr` or `TYPE(expr)`

It can cast prtaically any type to any other type. Improper usage can lead to disatrous results. 
Moreover, it is hard for both compiler and user to know what is the purpose of the cast.
_c++ style_ casting is more explicit.

 - `static_cast` : conversion that can be done at compile-time
 - `const_cast`
 - `dynamic_cast`
 - `reinterpret_cast`


 - [stackoverflow - regular-cast-vs-static-cast-vs-dynamic-cast](https://stackoverflow.com/questions/28002/regular-cast-vs-static-cast-vs-dynamic-cast)
 - [stackoverflow -  how-expensive-are-dynamic-casts-in-c](https://stackoverflow.com/questions/5694265/how-expensive-are-dynamic-casts-in-c)
 - [cpp-dynamic-cast-performance/](https://tinodidriksen.com/2010/04/cpp-dynamic-cast-performance/)
 - [http://www.nerdblog.com/2006/12/how-slow-is-dynamiccast.html](http://www.nerdblog.com/2006/12/how-slow-is-dynamiccast.html)
 - http://www.stroustrup.com/fast_dynamic_casting.pdf
 - [dynamic_cast_From_Scratch](https://trello-attachments.s3.amazonaws.com/56cd7e0dd54f89eafbd1af24/5a9ff0d5f092cd37c9aec6f3/fe354d8c4b4c2308a6ac47f7bd85aa68/dynamic_cast_From_Scratch_-_Arthur_O_Dwyer_-_CppCon_2017.pdf)