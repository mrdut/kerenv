# Format

### 

### precision

```
std::scientific
std::fixed
std::setprecision( std::numeric_limits< double >::max_digits10)
```

## Alignment

```
std::cout << std::setfill('.') << std::setw(10) << std::left << "TEXT" << std::endl;
TEXT......

std::cout << std::setfill('.') << std::setw(10) << std::right << "TEXT" << std::endl;
......TEXT
```

## Base


### Hexa

```
std::cout << std::hex               // print in hexa format
          << std::showbase          // show '0x' in formt of number
          << std::setw(12)          // how long should be number
          << std::setfill('0')      // if number is less than required width, pad with '0'
          << std::internal          // pad between '0x' and number
          << 0xabc123               // the value to print
          << std::dec;              // go back to decimal
```

### Binary

```
#include <bitset>

int i = 0b10101;    

int i=4568413;
std::cout << std::bitset< sizeof(int)*8 >(i) << std::endl;

```


## 

std::ostream& bold_on(std::ostream& os)
{
    return os << "\e[1m";
}

std::ostream& bold_off(std::ostream& os)
{
    return os << "\e[0m";
}

int main()
{
std::cout << bold_on << "bold" << bold_off << " non-bold" << std::endl;
}


### Links

 - [boost format parsing src](https://www.boost.org/doc/libs/1_53_0/boost/format/parsing.hpp)