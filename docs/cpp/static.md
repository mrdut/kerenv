# Static

All non-function-scope static variables are constructed before main(), while there is only one active thread