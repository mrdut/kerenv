
# Template

[TRELLO](https://trello.com/c/Se0JfQYH/36-template)

full specialization can be moved to .cpp
partial cannot

template             | Type deduction ?    | Full specialization?  | Partial specialization?
---------------------|---------------------|-----------------------|-------------------------
 Function (c++98)    |        Yes          |    Yes                | No 
 Class  (c++98)      |        No           |    Yes                | Yes 
 Alias  (c++11)      |        No           |    No                 | No
 Variable (c++14)    |        No           |    Yes                | Yes

you cannot specialize a single class method, but the whole class...


 - [stackoverflow - nested-class-template-specialization](https://stackoverflow.com/questions/33099306/nested-class-template-specialization)
 - [stackoverflow - partial-template-specialization-outside-class-definition](https://stackoverflow.com/questions/12875187/partial-template-specialization-outside-class-definition)



## how to fully specialize nested class ?

**WRONG**

```
template<typename A> struct Hey
{
    template<typename B> struct Ho;     <- the nested class we want to fully specialize
};

template<typename A>            <= (1) enclosing class templates are not explicitly specialized
template<>                      <= (2) explicit specialization
struct Hey<A>::Ho<int> {};
```

```
error: invalid explicit specialization before ‘>’ token
     template<>
              ^
error: enclosing class templates are not explicitly specialized
error: template parameters not deducible in partial specialization:
     struct Hey<A>::Ho<int> {};
                    ^
```

**GOOD**

```
template<typename A> struct Hey
{
    template<typename B, bool Dummy=true> struct Ho;  <- add Dummy template parameter
};

template<typename A>
template<bool Dummy>                      <= no more explicit specialization, but partial specialization
struct Hey<A>::Ho<int, Dummy> {};
```

## how to specialize nested class (that was already specialized) for a specific parent class ?

**WRONG**

```
template<typename A>
struct Hey
{
    template<typename B, bool Dummy=true> struct Ho;
};

// specialization 1
template<typename A>   // generic Hey
template<bool Dummy>   // specialize Ho for `int`
struct Hey<A>::Ho<int, Dummy> { static inline void hein() { std::cout << "Hey<generic>::Ho<int>\n"; } };

// specialization 2
template<>             // specialize Hey for `int`
template<bool Dummy>   // specialize Ho for `int`
struct Hey<float>::Ho<int, Dummy> { static inline void hein() { std::cout << "Hey<float>::Ho<int>\n"; } };

int main()
{
    Hey<void>::Ho<int>::hein();    // we expect specialization 1
    Hey<float>::Ho<int>::hein();   // we expect specialization 2
}
```

```
Hey<generic>::Ho<int>
Hey<generic>::Ho<int>   <- WTF ??
```

**GOOD**

```
template<typename A> struct Hey
{
    template<typename B, typename C=A> struct Ho;
};

template<typename A>
template<typename C>
struct Hey<A>::Ho<int, C> { static inline void hein() { std::cout << "Hey<generic>::Ho<int>\n"; } };

template<>
template<>
struct Hey<float>::Ho<int> { static inline void hein() { std::cout << "Hey<float>::Ho<int>\n"; } };

int main()
{
    Hey<void>::Ho<int>::hein();    // we expect specialization 1
    Hey<float>::Ho<int>::hein();   // we expect specialization 2
}
```

```
Hey<generic>::Ho<int>
Hey<float>::Ho<int>
```