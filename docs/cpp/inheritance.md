# Inheritance

## Dependency graph

https://stackoverflow.com/questions/8509243/class-hierarchy-dependency-diagram-generator-for-c-in-linux

### Doxygen


```
cd lib/extra-eyes-core

// generate default Doxyfile config file
(optional) doxygen -g

// edit DoxyFile
HAVE_DOT = YES
RECURSIVE = YES
EXTRACT_ALL = YES
GENERATE_LATEX = NO
GENERATE_HTML = NO
PROJECT_NAME = Extra-Eyes-Core
OUTPUT_DIRECTORY = /tmp/eec-doc/

# generate doc
doxygen Doxyfile

# open doc
xdg-open /tmp/eec-doc/html/index.html
```

### cinclude2dot.pl

** .../cime-extraeyes/heyho.sh **

```
#! /usr/bin/bash

cd bin/blue-ski

if [ ! -f cinclude2dot.pl ]; then
    wget https://trello-attachments.s3.amazonaws.com/56cd7e0dd54f89eafbd1af24/589847b2f941978c267b93eb/8897d5b3fb4bda1558e63bc239c2a9c8/cinclude2dot.pl;
fi

ls -l cinclude2dot.pl

chmod +x cinclude2dot.pl
./cinclude2dot.pl --include=$(pwd)/../../lib/bluevision/src/,$(pwd)/../../lib/extra-eyes-core/,$(pwd)/../../lib/ --quotetypes=quote > /tmp/graph.dot
neato -Tps /tmp/graph.dot > /tmp/graph.ps
ls -l /tmp/graph.ps
evince /tmp/graph.ps

rm cinclude2dot.pl -f
```

## Memory layout

 - [memory-layout-for-multiple-and-virtual.html](http://cplusplus-development.blogspot.fr/2013/09/memory-layout-for-multiple-and-virtual.html)
