# LOCK

does not works over NFS

```
int fd_lock = open(filename.c_str(), O_CREAT | O_WRONLY);
if ( fd_lock==-1 ) { throw Exception(...); }
flock( fd_lock, LOCK_EX ); // blocking call, consider LOCK_EX | LOCK_NB
flock(fd_lock, LOCK_UN);
close(fd_lock);
```

# FCNTL

works on NFS bu not on multiple different machine...
!! all locks are releases when **any** file descriptor is closed...
!! need a **write** file descriptor (w, r+, a...) to get exclusive lock, see manpage...

```
struct flock fl;
fl.l_type   = F_WRLCK;
fl.l_whence = SEEK_SET;
fl.l_start  = 0;
fl.l_len    = 0;
fl.l_pid    = getpid();

int fd_lock = open(share_jobs_filename.c_str(), O_WRONLY, (mode_t) 0777);
if ( fd_lock==-1 ) { throw Exception(...); }
fcntl(fd_lock, F_SETLKW, &fl);

fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
fcntl(fd_lock, F_SETLK, &fl); /* set the region to unlocked */
close(fd_lock);
```

# DIRECTORY

works on NFS with multiple machines

```
try:
    # mkdir is an atomic operation
    os.makedirs(lockfile)

    # do locked stuff...

    # "release lock"
    os.rmdir(lockfile)

except OSError:
    # an OSError is raised on mkdir fail ('file exists')
    pass
```

# LINKS
 - [Linux file lock discussion](https://gavv.github.io/blog/file-locks/)
 - [Linux file lock discussion](http://0pointer.de/blog/projects/locking.html)
 - http://www.informit.com/articles/article.aspx?p=23618&seqNum=4
