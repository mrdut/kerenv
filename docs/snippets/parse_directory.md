void _readdir(const std::string & dir)
{
    struct dirent *entry;
    DIR *dirp;
    dirp = opendir(dir.c_str());
    while ((entry = readdir(dirp)))
    {
        printf("%s\n", entry->d_name);

        if(entry->d_type==DT_DIR)
        {
            if( entry->d_name[0] != '.' ) // do not parse '.', '..' and hidden directories
            {
                _readdir(dir + "/" + entry->d_name);
            }
        }

    }
    closedir(dirp);
}


void _scandir(const std::string & dir)
{
   struct dirent **namelist;
   int n;

   n = scandir(dir.c_str(), &namelist, NULL, alphasort);
   if (n < 0)
       perror("scandir");
   else {
       while (n--)
       {
            printf("%s\n", namelist[n]->d_name);
            if(namelist[n]->d_type==DT_DIR)
            {
                if( namelist[n]->d_name[0] != '.' ) // do not parse '.', '..' and hidden directories
                {
                    _scandir(dir + "/" + namelist[n]->d_name);
                }
            }
           free(namelist[n]);
       }
       free(namelist);
   }
}
