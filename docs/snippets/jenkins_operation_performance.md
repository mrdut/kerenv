# Operation performance

### Build argument

```
# filter by station (mind the '/' at the end of the skilift name)
--filter-place="(fort/|plandurepos|prolays/)"

```

### Docker

How to run jenkins operation performance ?


1. get docker `ci_centos_vision`

```bash
docker login gitlab.asygn.com:5678 (use gitlab identifiants)
docker pull gitlab.asygn.com:5678/bluecime/devops/dockerimages/ci_centos_vision
docker image ls
docker run --rm=true -it --privileged --network host -v /bluecime/videos/:/bluecime/videos/:ro -v /bluecime/publicRW/:/bluecime/publicRW/:rw gitlab.asygn.com:5678/bluecime/devops/dockerimages/ci_centos_vision /bin/bash

DOCKER SESSION

ctrl+d` (exit)
```

2. get extra-eyes

```bash
git clone git@gitlab.asygn.com:bluecime/cime-extraeyes.git
cd cime-extraeyes/
git branch --all
git checkout <GIT-BRANCH>
git submodule update --init
```

3. run operation performance

```bash
smart_eval_tool_args="--filter-smart=5" ./scripts/jenkins/operations_performances.sh
```

or custom script

```bash
#! /usr/bin/bash

export gitbranch=`git rev-parse --abbrev-ref HEAD`
export use_master_build=false
export cmake_args=""
export blueski_args=""
export smart_eval_tool_args="--eval-dir="data/config/all_jobs.json" --filter-place=rmottes1_20190113_"

./scripts/jenkins/operations_performances.sh
```

4. setup developement environment

```
sudo dnf install vim -> BROKEN
alias gco="git checkout"; alias gci="git commit"; alias gst="git status"; alias gadd="git add"; alias gdiff="git diff"; alias gb="git branch"; alias gba="git branch --all";
dnf remove vim-minimal; dnf --assumeyes install vim; printf "set number\n" > ~/.vimrc; git config --global core.editor "vim";
```



launch vnc server

from CONTAINER

```
yum install -y terminator
yum install -y lesstif-mwm
# yum install -y openbox

vncpasswd
<ENTER PWD>
<CONFIRM PWD>

export DISPLAY=:1
vncserver -name :1 &> /dev/null
{ mwm & } &> /dev/null
#{ openbox & } &> /dev/null

terminator
```

from HOST
```
vncviewer --shared --maximize  127.0.0.1:5901
```




