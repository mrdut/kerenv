# Gitlab

### No more space on server : 

 * [check disk usage](http://bangkok:3000/d/1mcL1c-mz/node-exporter-full?orgId=1&var-job=nodes&var-node=172.16.130.6&var-port=9100) admin/blue


```
# must be done by someone from admin group (nico, christophe, leo)
ssh kobe
sudo -i
su backuppc
ssh root@gitlab

df -h

cd /ftp/gitlab_backups

df -h
```
Puis effacer les sauvegardes buggées. Le script /root/scripts/cleanBeforeBackup.sh permet aussi de faire le ménage 
dans les sauvegardes + les images docker
