# Bull.md

## Env

https://www.quennec.fr/trucs-astuces/syst%C3%A8mes/gnulinux/utilisation/git-authentification-https-automatique
https://erik.torgesta.com/2013/12/creating-a-persistent-ssh-tunnel-in-ubuntu/


Windows proxy
193.56.47.8:8080 (should be 193.56.47.20 ?)
for the following adresses : localhost;0.0.0.0;127.0.0.1;172.16.118.137;172.16.118.134

Linux proxy

 - nwadmin : 129.183.22.27
 - bitbucket : 161.89.117.136
 - artifactory : 172.16.118.137:8081

```bash
export http_proxy=http://193.56.47.20:8080
export https_proxy=http://193.56.47.20:8080
export no_proxy=172.16.118.137
export HTTP_PROXY=http://193.56.47.20:8080
export HTTPS_PROXY=http://193.56.47.20:8080
export NO_PROXY=172.16.118.137
```


https://superuser.com/questions/107679/forward-ssh-traffic-through-a-middle-machine

## Troobleshooting

``` python
# error: invalid command 'bdist_wheel'
>> pip3 install wheel
>> python3 setup.py bdist_wheel

```



```
# pour compiler en local il faut indiquer à go l'artifactory donc rajouter dans son ~/.profile par exemple
export GOPROXY="http://172.16.118.137:8081/artifactory/mivision-go-repo,direct"
export GOPRIVATE="bitbucket.sdmc.ao-srv.com"
export GONOSUMDB="*"
```




```
# binary
wget http://172.16.118.137:8081/artifactory/mivision-generic-local/koios/bin/koios && chmod +x koios

OR
curl --user mivision-user:mivision-user http://172.16.118.137:94/view/%20Koios%20/job/executable-go-koios/job/BREBD-3493-hpar_l1/lastSuccessfulBuild/artifact/koios > koios

# mise à jour du binaire
./koios -update

./koios -install -install-folder=<koios installation folder>
(./koios -install -install-video -install-src -install-folder <INSTALL_DIR>)

#
# OR from sources (no need to install)
#
git clone https://bitbucket.sdmc.ao-srv.com/scm/brebd/koios.git
cd koios

# pour récup la dernière version des images master
./koios -branch master -pull

# lancer le tout
./koios -branch MA_BRANCH -video-folder=<ou est la vidéo par défaut> -d up

# tu peux faire un docker ps pour vérifier si besoin que ton image est bien utilisée
si tu utilises pas le git pour koios faut aussi penser faire après koios -update
koios -install -install-folder=<koios installation folder>
```



```
# get koios binary
(naboo30) >> cd /home/fdutrech/koios-root
(naboo30) >> curl --user mivision-user:mivision-user http://172.16.118.137:94/view/%20Koios%20/job/executable-go-koios/job/BREBD-3493-hpar_l1/<2481 OR lastSuccessfulBuild>/artifact/koios > koios
(naboo30) >> chmod +x koios

# setup env
(naboo30) >> ./koios --install --install-folder .
(naboo30) >> ./koios -branch master -pull
(naboo30) >> ./koios -branch BREBD-3493-hpar_l1 -pull

(naboo30) >> alias kup-dev="./koios -branch=BREBD-3493-hpar_l1 -video-folder=/koios/data/videos/ -folder /home/fdutrech/git/koios-dev/fdutrech/naboo-2-cam/ --dev -d up"
(naboo30) >> alias kup="./koios -branch=BREBD-3493-hpar_l1 -video-folder=/koios/data/videos/ -folder /home/fdutrech/git/koios-dev/fdutrech/naboo-2-cam/ -d up"
(naboo30) >> alias kdown="./koios -branch=BREBD-3493-hpar_l1 -video-folder=/koios/data/videos/ -folder /home/fdutrech/git/koios-dev/fdutrech/naboo-2-cam/ -d down"
(naboo30) >> alias ks="./koios -branch=BREBD-3493-hpar_l1 -video-folder=/koios/data/videos/ -folder /home/fdutrech/git/koios-dev/fdutrech/naboo-2-cam/"

# local branch
(naboo30) cd /home/fdutrech/git/koios-human-model
...do some work...
(naboo30) cd /home/fdutrech/git/koios-master/bootstrap/builder
(naboo30) COMPONENT_BUILDER_IMAGE_VERSION=master KOIOS_HARDWARE=generic  docker-compose run builder
(builder) export export HTTP_PROXY=http://129.183.4.13:8080 HTTPS_PROXY=http://129.183.4.13:8080 NO_PROXY=172.16.118.137,172.16.118.134,naboo5,naboo0,129.183.101.5 http_proxy=http://129.183.4.13:8080 https_proxy=http://129.183.4.13:8080 no_proxy=172.16.118.137,172.16.118.134,naboo5,naboo0,129.183.101.5
(builder) cd /koios/git/koios-human-model
(builder) make dependencies
(builder) make build

OR
(naboo30) cd /home/fdutrech/git/koios-human-model
(naboo30) source ../../pyenv/bin/activate
(naboo30) make dependencies
(naboo30) make dependencies-test
(naboo30) make build && KOIOS_HARDWARE=generic make build-docker-images



# which version
docker -it exec IMAGE cat build-metadata
or koios -version


 docker exec -it -e PGPASSWORD=postgres dutrechf_working-memory_1  psql -h wkm -U postgres -d koioswm
 SELECT * FROM detection;
 SELECT COUNT (*) FROM detection;


```

## Component builder

```
docker run --name=fdutrech-koios-dev --network=host -it -v /var/run/docker.sock:/var/run/docker.sock -v /home/dutrechf/git/:/git/ -v ~/:/root 172.16.118.137:8081/mivision-docker-repo/xbd-vision/koios-component-builder/turing:master bash
```

## WKM

```bash
(naboo30) >> docker save -o wkm.img  172.16.118.137:8081/mivision-docker-repo/xbd-vision/working-memory/generic:master
>> rsync naboo30:wkm.img . -avz --progress
>> docker load < wkm.img
>> docker tag IMAGE_ID koios_image_wkm
>> docker run -e PGPASSWORD=postgres -v /home/dutrechf/data/wkm:/wkm -p 5432 --name=koios_wkm -d koios_image_wkm
>> docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' koios_wkm
>> docker logs koios_wkm
>> export PGPASSWORD=postgres
>> psql -h 172.17.0.2 -U postgres -d koioswm
>> docker rm -f koios_wkm

>> docker images
>> docker rmi IMAGE_ID

>> docker network ls
>> docker network rm NETWORK_ID
>> docker network create --driver=bridge --subnet=192.100.0.0/16 koios-network
```

## Naboo

![atos network](img/atos_network.png)

```
(local) >> ssh dutrechf@nwadmin.frec.bull.fr
(nwadmin) >> ssh dutrechf@naboo0
(naboo0) >> ssh dutrechf@naboo36
(naboo36) >> ...

(naboo36) >> cd /tmp/
(naboo36) >> export https_proxy=http://129.183.4.13:8080
(naboo36) >> env | grep proxy
http_proxy=http://129.183.4.13:8080
ftp_proxy=http://193.56.47.20:8080/
https_proxy=http://193.56.47.20:8080/
no_proxy=172.16.118.137,172.16.118.134,naboo5,naboo0,129.183.101.5
(naboo36) >> git clone https://bitbucket.sdmc.ao-srv.com/scm/brebd/koios-input-stream-manager.git
(naboo36) >> cd koios-input-stream-manager/
(naboo36) >> git checkout <BRANCH>
(naboo36) >> python3 -m venv /tmp/fdutrech_venv
(naboo36) >> source /tmp/fdutrech_venv/bin/activate

(naboo36) >> unset ftp_proxy
(naboo36) >> export http_proxy=http://129.183.4.13:8080
(naboo36) >> export https_proxy=$http_proxy
(naboo36) >> env | grep proxy
http_proxy=http://129.183.4.13:8080
https_proxy=http://129.183.4.13:8080
no_proxy=172.16.118.137,172.16.118.134,naboo5,naboo0,129.183.101.5

make dependencies
make build && make build-docker-images
```

## [Jenkins](http://localhost:1234/)

Login/pwd : mivision-user / mivision-user

```
# Redirect to local port 1234
ssh -L 1234:172.16.118.137:94 -N dutrechf@nwadmin.frec.bull.fr
```

```
(naboo20) >> docker exec -it koiosjenkinsagent_koios-slave_1 /bin/bash
(docker) >> rm -rf /tmp/jenkins-agent/workspace/stream-manager_BREBD-3711_strict
        Ctrl^D
    Ctrl^D
```

```
# @cecile
# c'est la procédure que j'utilise lorsque j'ai un pb de droit sur le .git dans le build jenkins
# ca supprime l'environnement de build
ssh nwadmin
ssh root@yoda
(mdp root)
docker exec -it jenkins-mivision bash
rm -r /var/jenkins_home/workspace/s-human-model_BREBD-3493-hpar_l1*
```

```bash
# No space left on device
# from machine where there is no space left (eg naboo25)
>> df -h
>> docker system prune
```


```
curl -u mivision-user:mivision-user http://localhost:1234/job/_e2e-fast-tests/21/consoleText | less
```

## Artifactory

Login/pwd : mivision-user / mivision-user

```
>> ssh -L 18081:localhost:18081 dutrechf@nwadmin.frec.bull.fr
>> ssh -L 18081:localhost:8081 root@yoda
# mpd -> root
# ensuite sur ton browser tu vas sur http://localhost:18081/artifactory/webapp/
```

## Mivision

depuis un naboo

``` bash
# http://129.183.101.5:8090/mivision-release/
ssh mivision@129.183.101.5
pwd=pwd4vision
```

