
Javascript element documentation : 
https://developer.mozilla.org/fr/docs/Web/API/Element

JAvascript class override
https://javascript.info/class-inheritance#overriding-constructor

form guideline : 
http://files.smashingmagazine.com/wallpapers/images/form-crib-sheet/full_preview.png?_ga=2.1170865.1017595775.1542032514-602051344.1542032514
https://www.smashingmagazine.com/2017/06/designing-efficient-web-forms/
https://www.smashingmagazine.com/2011/06/useful-ideas-and-guidelines-for-good-web-form-design/

jquery builder :
http://projects.jga.me/jquery-builder/

CSS + javascript :
https://codepen.io/greenlightstyleguide/pen/VLrqYb

icon :
https://www.flaticon.com/packs/weather-173

button:
https://cssgradientbutton.com/


SVG lib :
http://dmitrybaranovskiy.github.io/raphael/
http://fabricjs.com/

Cursor:
https://www.w3schools.com/cssref/pr_class_cursor.asp

Disable Chrome cache for current page: 
    ... | More Tools | Developer Tools | Network | Disable cache (while DevTools is open)
    OR F12 | Network | Disable cache (while DevTools is open)