# [Sketchup](https://app.sketchup.com/app#)

[online help](https://help.sketchup.com/fr/sketchup/creating-3d-model)



![](img/shortcut.jpg)

## View

 - Rotate : hold mouse middle button (Orbit)
 - Translate :  shift+middle button (h) (Panoramique)
 - Zoom : mouse wheel

## Draw

### Axis

 Lock tool to axis : select tool, click appropriate arrow key, the tool will draw along selected axis

![](img/image2.png)



### Rectangle

press, release, (enter dimension), press

change origin : Ctrl


## Object

### Resize

![](img/resize.gif)

 - select edge
 - click scale icon ![](img/scale_icon.png)
 - enter measure + **unit**, eg 50mm (50 alone would scale x50 current dimensions)

### Inference Points (eg reference points)

![](img/image1.png)


### [Select (spacebar)](https://help.sketchup.com/ko/article/3000085)


 - Double-click a face : The face and all its bounding edges
 - Double-click an edge : The edge and its connected face
 - Triple-click an entity : All the connected entities. For example, if you triple-click a face in a cube, the entire cube is selected. (3)

### Component

Select an object (triple-click), then right click, make component
Now, it will not be merged with other object -> **do component as soon as possible**

If you edit a component, all copy will be edited too

### Move￼

- Select object to move
- Click on inference point
- (opt) Press ctrl : move a copy
- (opt) Press arrow: constrain on axis
- (opt) Type distance to move
- Validate

### Multiple Duplicate

- Select component

- Press control, select "Faire pivoter" (Q)

- Type angle for first copy

- Type "xN" for number of rotation

