#! /usr/bin/env bash

BACKUP=/tmp/backup

mkdir -vp $BACKUP

cp -rv ~/.ssh $BACKUP
cp -rv ~/.kube $BACKUP
cp -rv ~/.zshrc $BACKUP
cp -rv ~/.zsh_history $BACKUP
cp -rv ~/.Pictures/Icons $BACKUP
cp -rv /etc/hosts $BACKUP
cp -rv /etc/hosts $BACKUP

cp $ATEME_WORKSPACE/kerenv $BACKUP -rv

for repo in $ATEME_WORKSPACE/*; do
    cd $repo
    git config --get remote.origin.url >> $BACKUP/Workspace.txt
    cd - 2>&1 > /dev/null
done

for repo in $ATEME_WORKSPACE/ThirdParty*; do
    cd $repo
    git config --get remote.origin.url >> $BACKUP/WorkspaceThirdParty.txt
    cd - 2>&1 > /dev/null
done

