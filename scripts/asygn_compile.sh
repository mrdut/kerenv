#! /usr/bin/bash

TARGET=blue-ski
CMAKE_BUILD_TYPE=RelWithDebInfoAndAssert

# git push

if [[ -z "${FDUTRECH_PWD}" ]]; then echo "env FDUTRECH_PWD is undefined, please run export FDUTRECH_PWD=..."; exit -1; fi

sshpass -p$FDUTRECH_PWD ssh fdutrech@annecy "\
cd /work-crypt/fdutrech/workspace/bluecime/extra-eyes; \
ls;\
git status\
"

# git pull \
# ./blue-master.py --target=$TARGET  --cmake-build-type=$CMAKE_BUILD_TYPE --nthreads=fast --distcc --gcc-parse --cmake=auto   

# vncserver -name :1 && export DISPLAY=:1