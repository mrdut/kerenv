DIRNAME=$(dirname $0)
BASENAME=$(basename $0)

ATEME_LOG_LEVEL=DEBUG
# ATEME_LOG_LEVEL=INFO
source $DIRNAME/log.sh

JIRA_URL="https://myateme.atlassian.net"
# ATLASSIAN_ID=f.dutrech@ateme.com
# JIRA_TOKEN=NwYFNNYKtZSnylrCxG8IABB0


#
# Where everything is stored
# - a symbolic link to this directory is made in every git repo for convenience
# - this link can be cleaned by "git clean -dfx", the original is safe
#
JIRA_ROOT=$HOME/Workspace/mrdut/jira

#
# Create new JIRA env :
#  - new git branch
#  - new JIRA_ROOT directory
#  - set 'current'
#  - download artifacts (TODO)
#
# Arg : JIRA_ID
#
function jira_new() {
   log_debug "jira_new()"

   JIRA_ID=$1

   # basics checks to ensure this command will fail without modifying anything
   ls -l $JIRA_ROOT/${JIRA_ID} 2>/dev/null&& { log_error "$JIRA_ROOT/${JIRA_ID} already exists - exit"; return 1; }
   mkdir -v $JIRA_ROOT/${JIRA_ID}
   mkdir -vp $JIRA_ROOT/${JIRA_ID}/.jira

   RUN=$JIRA_ROOT/${JIRA_ID}/run.sh
   echo -e '#! bash\n\nsource /home/fdutrech/Workspace/mrdut/tprc.sh\n\n#./bin/TitanProcessingMain-debug $(tp_units jira/current/pipeline.yaml)\n' > $RUN
   chmod +x $RUN

   jira_init $JIRA_ID
   jira_switch $JIRA_ID
}

#
# returns the JIRA_ID of the current
# ex : 1234
#
# Arg : None
#
function jira_current() {
   if [ -L $JIRA_ROOT/current ]; then
      JIRA_ID=$(basename $(readlink $JIRA_ROOT/current))
      echo ${JIRA_ID}
      return
   fi
   return 1
}

#
# Move 'current' to done/
#
# Arg : None
#
function jira_done() {
   log_debug "jira_done()"

   if [ -L $JIRA_ROOT/current ]; then
      JIRA_CURRENT=$(jira_current)
      GIT_BRANCH=tp-$JIRA_CURRENT
      git checkout master || { log_error "Could not git checkout 'master' - exit"; return 1; }

      GIT_REAL_BRANCH=$(git branch | grep ${GIT_BRANCH} | xargs) # in case the branch name is 'tp-1234_blabla'
      if [ ! -z "${GIT_REAL_BRANCH}" ]; then
         git branch -d ${GIT_REAL_BRANCH}
      fi

      JIRA_LINK=$(readlink $JIRA_ROOT/current)
      echo " -- Moving $JIRA_LINK to done..."
      mkdir -vp $JIRA_ROOT/done
      mv -v $JIRA_LINK $JIRA_ROOT/done
      rm -v $JIRA_ROOT/current
   else
      echo " -- current : None"
   fi
}

function _nchar() {
   printf "${1}%.0s" {1..$2}
}


URL_TABULATE="\r\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"

#
# Dump single JIRA_ID to terminal
#
# Arg : JIRA_ID
#
function _jira_dump() {

   JIRA_ID=$1
   JIRA_ID=${JIRA_ID#tp-}
   JIRA_PATH=$JIRA_ROOT/$JIRA_ID

   if [ ! -d $JIRA_PATH ]; then
      JIRA_PATH=$JIRA_ROOT/done/$JIRA_ID
      if [ ! -d $JIRA_PATH ]; then
         echo "Error : Unknown directory JIRA_ID=$JIRA_ID"
         return 1
      fi
   fi

   if [ ! -f $JIRA_PATH/.jira/issue_type.txt ]; then
      cat $JIRA_PATH/.jira/issue.json | jq .fields.issuetype.name | sed 's/"//g' > $JIRA_PATH/.jira/issue_type.txt
   fi
   JIRA_TYPE=$(cat $JIRA_PATH/.jira/issue_type.txt)
   case ${JIRA_TYPE} in
      "Bug") printf "${COLOR_RED}";;
      "Story") printf "${COLOR_GREEN}";;
      *) printf "${COLOR_RED}" ;;
   esac
   printf "${JIRA_TYPE} "

   printf "${COLOR_BLUE}TP-$JIRA_ID"

   if [ ! -f $JIRA_PATH/.jira/issue_summary.txt ]; then
      cat $JIRA_PATH/.jira/issue.json | jq .fields.summary | sed 's/"//g' > $JIRA_PATH/.jira/issue_summary.txt
   fi
   JIRA_SUMMARY=$(cat $JIRA_PATH/.jira/issue_summary.txt)
   printf "${COLOR_CYAN} $JIRA_SUMMARY"

   printf "${URL_TABULATE}${COLOR_DARK_GRAY}https://myateme.atlassian.net/browse/TP-${JIRA_ID}"

   if [ ! -f $JIRA_PATH/.jira/issue_reporter.txt ]; then
      cat $JIRA_PATH/.jira/issue.json | jq .fields.reporter.displayName | sed 's/"//g' > $JIRA_PATH/.jira/issue_reporter.txt
   fi
   JIRA_REPORTER=$(cat $JIRA_PATH/.jira/issue_reporter.txt)
   printf " ($JIRA_REPORTER)"

   # if [ ! -f $JIRA_PATH/.jira/issue_status.txt ]; then
   #    cat $JIRA_PATH/.jira/issue.json | jq .fields.status.name | sed 's/"//g' > $JIRA_PATH/.jira/issue_status.txt
   # fi
   # JIRA_STATUS=$(cat $JIRA_PATH/.jira/issue_status.txt)
   # case ${JIRA_STATUS} in
   #    "Fermée") printf " ${COLOR_GREEN}";;
   #    "Rejected") printf " ${COLOR_YELLOW}";;
   #    "En cours") printf " ${COLOR_BLUE}";;
   #    "Résolu") printf " ${COLOR_CYAN}";;
   #    "Ouvert") printf " ${COLOR_CYAN}";;
   #    "Selected for Development") printf " ${COLOR_CYAN}";;
   #    *) printf " \e[44m${COLOR_WHITE}" ;;
   # esac
   # printf "${JIRA_STATUS}"


   printf "${COLOR_RESET}"
}

function _jira_git_dump() {
   JIRA_ID=$1
   JIRA_ID=${JIRA_ID#tp-}

   # git rev-parse --show-toplevel &> /dev/null
   GIT_DIR=$ATEMEWORKSPACE/$2

   GIT_ORIGIN=$(git -C ${GIT_DIR} config --get remote.origin.url 2> /dev/null)
   if [ $? -eq 0 ]; then
      GIT_ORIGIN=${GIT_ORIGIN#*:}
      GIT_BRANCH=$(git -C ${GIT_DIR} branch | grep ${JIRA_ID})
      if [ $? -eq 0 ]; then
         printf "      └── "

         #
         # GIT BRANCH
         #
         GIT_BRANCH=$(echo "$GIT_BRANCH" | sed 's/*//' | xargs) # trim and remove '* '
         printf "${COLOR_MAGENTA}$2 : ${GIT_BRANCH}"

         #
         # LEFT/RIGHT origin/master
         #
         lr=$(git -C ${GIT_DIR} rev-list --left-right --count origin/master...${GIT_BRANCH} | sed s'/\s/\//g')
         if [[ "${lr:0:1}" == "0" ]]; then
            printf " ${COLOR_GREEN}($lr)"
         else
            printf " ${COLOR_RED}($lr)"
         fi

         # printf "${COLOR_RESET}\n"


         if [ ! -d $JIRA_ROOT/$JIRA_ID/.jira/merge_requests ]; then
            mkdir -p $JIRA_ROOT/$JIRA_ID/.jira/merge_requests
            i=0
            cat $JIRA_ROOT/.jira/merge_requests.json  |  jq -c ".[] | select(.source_branch | contains(\"${GIT_BRANCH}\"))" | while read -r MR; do

               mkdir -p $JIRA_ROOT/$JIRA_ID/.jira/merge_requests/$i
               cd $JIRA_ROOT/$JIRA_ID/.jira/merge_requests/$i

               printf "%s" "$MR" | jq .web_url | sed 's/"//g' > url.txt
               printf "%s" "$MR" | jq .state | sed 's/"//g' > state.txt
               printf "%s" "$MR" | jq .labels > labels.txt
               printf "%s" "$MR" | jq ".reviewers[].username" | xargs > reviewers.txt
               printf "%s" "$MR" | jq ".assignees[].username" | xargs > assignees.txt
               printf "%s" "$MR" | jq ".merged_at" | sed 's/"//g' > merged_at.txt

               cd - > /dev/null
               i=$((i+1))
            done
         fi


         i=0
         while true; do
            DIR="$JIRA_ROOT/$JIRA_ID/.jira/merge_requests/$i"
            i=$((i+1))

            [ ! -d "${DIR}" ] && break
            cat ${DIR}/url.txt | grep -vq ${GIT_ORIGIN} && continue

            cd ${DIR}

            URL=$(cat url.txt)
            STATE=$(cat state.txt)
            LABELS=$(cat labels.txt)
            REVIEWERS=$(cat reviewers.txt)
            ASSIGNEES=$(cat assignees.txt)
            MERGED_AT=$(cat merged_at.txt)



            printf "${COLOR_RESET}"
            printf "\n          └── "
            printf "\e[38;5;144m${URL}"

            case ${STATE} in
               opened) printf " \e[44m${COLOR_WHITE}";;
               merged) printf " ${COLOR_GREEN}";;
               *) printf " ${COLOR_RED}" ;;
            esac
            printf "${STATE}${COLOR_RESET}"

            [ ! -z "${MERGED_AT}" ] && [[ "${MERGED_AT}" != "null" ]] &&  printf " ${COLOR_DARK_GRAY}$(date --date="$MERGED_AT" "+%d %B %Y %H:%M:%S")"

            echo "$LABELS" | grep -q 'Ready to review' && printf " \e[38;5;201mreview${COLOR_RESET}"
            echo "$LABELS" | grep -q 'Ready to merge' && printf " \e[38;5;34mmerge${COLOR_RESET}"

            printf "\e[38;5;242m"
            printf " ${ASSIGNEES}"
            printf " ${REVIEWERS}"

            [ -z "${ASSIGNEES}" ] && [ -z "${REVIWERS}" ] && printf " ${COLOR_RED}No reviewers"

            cd - > /dev/null
         done

         if [ ! -d "$JIRA_ROOT/$JIRA_ID/.jira/merge_requests/0" ]; then
               printf "${COLOR_DARK_GRAY}"
               printf "\n          └──"
               printf " no merge request"
         fi
         printf "${COLOR_RESET}\n"

      # else
      #    printf "      └── "
      #    printf "${COLOR_DARK_GRAY}$2: no git branch"
      #    printf "${COLOR_RESET}\n"
      fi

   else
      log_debug " -- not in git repo, skip link"
   fi

   # cd - > /dev/null
}


#
# Dump to terminal JIRA info :
# - current (title + url / None) (TODO title)
# - others
# - assigned (TODO)
# - done
#
# Arg : None
#
function jira_status() {
   echo "${COLOR_BOLD}JIRA status :${COLOR_RESET} ${COLOR_DARK_GRAY}${URL_TABULATE}Mes tickets : https://myateme.atlassian.net/jira/software/c/projects/TP/boards/38?quickFilter=124${COLOR_RESET}"

   JIRA_CURRENT=$(jira_current)
   echo "${COLOR_MAGENTA} * Current : ${COLOR_RESET}"
   if [ -L $JIRA_ROOT/current ]; then
      printf "  └── "
      cd ${JIRA_ROOT}
      ls current -1l --color=always | sed -e "s|${JIRA_ROOT}||g" | awk '{print $(NF-2),$(NF-1),$NF}'
      cd - > /dev/null
   else
      printf "  └── "
      echo "${COLOR_GRAY} None${COLOR_RESET} "
   fi

   ALL_TP=""
   for JIRA_PATH in $(ls $JIRA_ROOT -Idone -Icurrent | sed "s|^|${JIRA_ROOT}/|") $(ls -d $JIRA_ROOT/done/*); do  # | grep -v $(jira_current)
      if [ ! -f $JIRA_PATH/.jira/issue_status.txt ]; then
         cat $JIRA_PATH/.jira/issue.json | jq .fields.status.name | sed 's/"//g' > $JIRA_PATH/.jira/issue_status.txt
      fi
      ALL_TP="${ALL_TP}$(cat $JIRA_PATH/.jira/issue_status.txt) ${JIRA_PATH}\n"
   done

   for JIRA_STATUS in "Selected for Development" "Ouvert" "En cours" "Résolu" "Fermée" "Rejected" ; do

      # https://misc.flogisoft.com/bash/tip_colors_and_formatting
      printf "${COLOR_BOLD}"
      case ${JIRA_STATUS} in
         "Selected for Development") printf "\e[38;5;26m";;
         "Ouvert") printf "\e[38;5;26m";;
         "En cours") printf "\e[48;5;26m${COLOR_WHITE}";;
         "Résolu") printf "\e[38;5;2m";;
         "Fermée") printf "\e[38;5;2m";;
         "Rejected") printf "\e[38;5;214m";;
         *) printf " \e[44m" ;;
      esac
      printf "${JIRA_STATUS}:${COLOR_RESET}\n"

      echo $ALL_TP | grep "$JIRA_STATUS" | while read -r TP; do
         jira_id=$(echo $TP | awk  -F "/" '{print $NF}')

         if [[ "${jira_id}" == "${JIRA_CURRENT}" ]]; then
            WIDTH=$(tput cols)
            printf "\e[40m"
            _nchar " " $(($WIDTH-10))
            printf "\r"
         fi


         printf "  └── "
         echo $TP | grep -q done && printf "(done) "
         _jira_dump $jira_id
         printf "${COLOR_RESET}\n"

         if echo $TP | grep -vq done; then
            for GIT_DIR in $(ls $ATEMEWORKSPACE -Imrdut -IAteme -Ichroot); do
               _jira_git_dump $jira_id $GIT_DIR
            done
            printf "${COLOR_DARK_GRAY}"
            _nchar "─" $(($WIDTH-10))
            printf "${COLOR_RESET}"
            printf "\n"
         fi
      done
   done

   # echo "$ALL_TP" | sort

   return 0

   echo "${COLOR_MAGENTA} * Ongoing : ${COLOR_RESET}"
   for jira_id in $(ls $JIRA_ROOT/ -Idone -Icurrent); do  # | grep -v $(jira_current)
      if [[ "${jira_id}" == "${JIRA_CURRENT}" ]]; then
         WIDTH=$(tput cols)
         printf "\e[40m"
         _nchar " " $(($WIDTH-10))
         printf "\r"
      fi
      printf "  └── "
      _jira_dump $jira_id
      printf "\n"

      for GIT_DIR in $(ls $ATEMEWORKSPACE -Imrdut -IAteme -Ichroot); do
         _jira_git_dump $jira_id $GIT_DIR
      done
      printf "\n"

   done
   echo "${COLOR_MAGENTA} * Done :${COLOR_RESET}"
   for jira_id in $(ls $JIRA_ROOT/done); do
      printf "  └── "
      _jira_dump $jira_id
      printf "\n"
   done
}


function jira_git_checkout() {
   JIRA_ID=$1
   log_debug "jira_git_checkout() JIRA_ID=$JIRA_ID"

   GIT_BRANCH=tp-${JIRA_ID}

   GIT_ROOT=$(git rev-parse --show-toplevel 2> /dev/null)
   if [ $? -eq 0 ]; then
      GIT_REAL_BRANCH=$(git branch | grep ${GIT_BRANCH} | xargs) # in case the branch name is 'tp-1234_blabla'
      log_debug "GIT_BRANCH=$GIT_BRANCH"
      log_debug "GIT_ROOT=$GIT_ROOT"
      log_debug "GIT_REAL_BRANCH=$GIT_REAL_BRANCH"
      if [ "${PIPESTATUS[0]}" -eq 0 ]; then
         if echo $GIT_REAL_BRANCH | grep -v '*'; then
            git checkout ${GIT_REAL_BRANCH} || { echo "ERROR : Could not checkout '${GIT_REAL_BRANCH}' git branch - exit"; return 1; }
         else
            echo " -- already at branch $GIT_REAL_BRANCH"
         fi
      else
         git checkout -b ${GIT_BRANCH} || { echo "ERROR : Could not checkout '${GIT_BRANCH}' git branch - exit"; return 1; }
      fi
   else
      log_warning "jira_git_checkout() not within git repo"
   fi
}

#
# Setup 'current' to given JIRA_ID
#
# Arg : JIRA_ID
#
function jira_switch() {
   log_debug "jira_switch()"
   JIRA_ID=$1
   jira_git_checkout $JIRA_ID

   rm -vf $JIRA_ROOT/current
   ln -sv $JIRA_ROOT/${JIRA_ID} $JIRA_ROOT/current

   if [ ! -f $JIRA_ROOT/current/.issue.json ]; then
      curl -u "$ATLASSIAN_ID:$JIRA_TOKEN" -X GET -s --url "${JIRA_URL}/rest/api/2/issue/TP-${JIRA_ID}" > $JIRA_ROOT/current/.issue.json
   fi

   jira_tree
}

function jira_tree() {
   log_debug "jira_tree()"
   tree $JIRA_ROOT/current tree --filelimit=100
}

function jira_download() {
   echo "jira_download - TODO"

}

function jira_help() {
   echo "jira_help - TODO"
}

#
# Basic check to use JIRA commands
# - ensure JIRA_ROOT exists
# - if called within a git repo, setup the 'jira' link to JIRA_ROOT
#
function jira_init() {
   log_debug "jira_init()"
   GIT_ROOT=$(git rev-parse --show-toplevel 2> /dev/null)
   if [ $? -eq 0 ]; then


      JIRA_ID=$1

      [ -z "$JIRA_ID" ] && { log_warning "Empty JIRA_ID - return"; return 1; }

      GIT_BRANCH=tp-${JIRA_ID}

      log_debug " -- GIT_ROOT=${GIT_ROOT}"
      log_debug " -- JIRA_ID=${JIRA_ID}"

      if [ ! -d ${GIT_ROOT}/jira ]; then
         ln -s $JIRA_ROOT $GIT_ROOT
      fi
      GIT_REAL_BRANCH=$(git branch | grep ${JIRA_ID} | xargs)

      if [ ! -z "${GIT_REAL_BRANCH}" ]; then
         log_debug " -- GIT_REAL_BRANCH=${GIT_REAL_BRANCH}"
         git checkout ${GIT_REAL_BRANCH}
      else
         log_debug " -- Did not find any branch matching '$JIRA_ID' - create new one"
       # && { log_error "git branch $GIT_BRANCH already exists - exit"; return 1; }
         git checkout master || { log_error "Could not checkout 'master' git branch - exit"; return 1; }
         git pull
         git branch ${GIT_BRANCH}
         git checkout ${GIT_BRANCH}
      fi
   else
      log_debug " -- not in git repo, skip link"
   fi

   return 0
}

function jira_change_directory() {
   log_debug "jira_change_directory()"
   if [ -L $JIRA_ROOT/current ]; then
      JIRA_CURRENT=$(jira_current)
      GIT_ROOT=$(git rev-parse --show-toplevel 2> /dev/null)
      if [ $? -eq 0 ]; then
         cd $GIT_ROOT/jira/$JIRA_CURRENT
      else
         cd $JIRA_ROOT/$JIRA_CURRENT
      fi
   else
      log_warning "No JIRA current "
   fi
}

function jira_desc() {
   log_debug "jira_desc()"
   JIRA_ID=$1

   _jira_dump $JIRA_ID

   JIRA_PATH=$JIRA_ROOT/current
   if [ ! -f $JIRA_PATH/.jira/description.txt ]; then
      cat $JIRA_PATH/.jira/issue.json | jq .fields.description | sed 's/"//g' > $JIRA_PATH/.jira/description.txt
   fi
   JIRA_DESC=$(cat $JIRA_PATH/.jira/description.txt)

   printf "\nDescription :\n"
   printf "${JIRA_DESC}"

}

function jira_setup() {
   log_debug "jira_setup()"

   JIRA_ID=$1

   jira_tree

   if [ -f $JIRA_ROOT/current/run.sh ]; then
      batcat $JIRA_ROOT/current/run.sh
   fi

   if [ -f $JIRA_ROOT/current/play.sh ]; then
      batcat $JIRA_ROOT/current/play.sh
   fi

   ~/Workspace/mrdut/parse_connect.py $JIRA_ROOT/current/*.yaml
}

function jira_update() {
   log_debug "jira_update()"

   set -x
   mkdir -vp $JIRA_ROOT/.jira


   # printf "Fetching GITLAB merge_requests..."
   curl --silent --header "PRIVATE-TOKEN: $GITLAB_TOKEN" 'https://gitlab.ateme.net/api/v4/merge_requests' | jq --sort-keys > $JIRA_ROOT/.jira/merge_requests.json &
   # printf "\r"
   # _nchar ' ' 50
   # printf "\r"

   for TP in $(ls $JIRA_ROOT -Idone -Icurrent); do
      rm -rfv $JIRA_ROOT/${TP}/.jira
      mkdir -pv $JIRA_ROOT/${TP}/.jira
      curl -u "$ATLASSIAN_ID:$JIRA_TOKEN" -X GET -s --url "${JIRA_URL}/rest/api/2/issue/TP-${TP}" > $JIRA_ROOT/${TP}/.jira/issue.json &
   done

   for TP in $(ls $JIRA_ROOT/done); do
      rm -rfv $JIRA_ROOT/done/${TP}/.jira
      mkdir -pv $JIRA_ROOT/done/${TP}/.jira
      curl -u "$ATLASSIAN_ID:$JIRA_TOKEN" -X GET -s --url "${JIRA_URL}/rest/api/2/issue/TP-${TP}" > $JIRA_ROOT/done/${TP}/.jira/issue.json &
   done

   # for GIT_DIR in $(ls $ATEMEWORKSPACE -Imrdut -IAteme -Ichroot); do
   #    git -C $ATEMEWORKSPACE/$GIT_DIR fetch && git -C $ATEMEWORKSPACE/$GIT_DIR remote prune origin &
   # done


   wait
   set +x

   jira_status
}

function jira() {

   [ -d $JIRA_ROOT ] || { echo "JIRA_ROOT=$JIRA_ROOT does not exists !" ; return 1; }

   # set +e
   # set -x

   # log_debug "jira() -> jira_init()"
   # jira_init || return 1

   if [ "$#" -eq 0 ]; then

      JIRA_CURRENT=$(jira_current)
      if [ ! -z "${JIRA_CURRENT}" ]; then
         GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
         if [ "$?" -eq 0 ] && [[ ! "${GIT_BRANCH}" =~ "${JIRA_CURRENT}"* ]]; then
            log_warning " JIRA_CURRENT=$JIRA_CURRENT != GIT_BRANCH=$GIT_BRANCH - switch"
            jira_switch $JIRA_CURRENT
         fi
      fi

      jira_status
      return
   fi

   if [ "$#" -ne 1 ]; then
       echo "Error - wrong number of argument ($#)"
       return
   fi

   log_debug "jira() \$1=$1"

   case "$1" in

      "init")
         jira_init $(jira_current)
         return 0
         ;;
     "cd")
         jira_change_directory
         return 0
         ;;
     "co")
         jira_git_checkout $(jira_current)
         return 0
         ;;
     "update")
         jira_update
         return 0
         ;;
     "tree")
         jira_tree $(jira_current)
         return 0
         ;;
     "desc")
         jira_desc $(jira_current)
         return 0
         ;;
     "setup")
         jira_setup $(jira_current)
         return 0
         ;;
     "done")
         jira_done || return 1
         jira_status
         return 0
         ;;

     *) ;;
   esac

   JIRA_ID=$1
   JIRA_ID=${JIRA_ID#tp-} # ensure we are juste working with 4-digits input (eg 1234, not tp-1234...
   JIRA_ID=${JIRA_ID#TP-} # ... neither TP-1234)
   JIRA_CURRENT=$(jira_current)

   log_debug "jira() JIRA_ID=$JIRA_ID"
   log_debug "jira() JIRA_CURRENT=$JIRA_CURRENT"

   if [[ "$JIRA_ID" =~ [[:digit:]]{4} ]]; then

      if [[ ${JIRA_ID} == ${JIRA_CURRENT} ]]; then
         log_debug "jira() -> pass"
      elif [ -d $JIRA_ROOT/$JIRA_ID ]; then
         jira_switch $JIRA_ID
      elif [ -d $JIRA_ROOT/done/$JIRA_ID ]; then
         log_debug "jira() -> move from done"
         mv -v $JIRA_ROOT/done/$JIRA_ID $JIRA_ROOT/$JIRA_ID
         jira_switch $JIRA_ID
      else
         jira_new $JIRA_ID
         # jira_download
      fi
      jira_status

      return 0
   fi

   jira_help
   return 1
}


##########
# OK
##########

# https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issues/#api-group-issues
# https://docs.atlassian.com/DAC/rest/jira/6.1.html#d2e816
# auth : https://hevodata.com/learn/jira-api/

JIRA_URL="https://myateme.atlassian.net"
REQUEST="rest/api/2/dashboard?maxResults=400" # OK
# REQUEST="rest/api/3/dashboard?maxResults=30" # OK
# REQUEST="rest/api/3/dashboard/11005" # OK
# REQUEST="rest/api/3/project" #
# REQUEST="rest/api/2/status" #
# REQUEST="rest/api/2/user" #
# REQUEST="rest/api/2/version" #

# REQUEST="rest/api/2/board"
# REQUEST="rest/agile/1.0/board/38"
REQUEST="rest/api/2/issue/TP-5447"
# REQUEST="rest/api/2/issue/TP-5310"
# REQUEST="rest/api/3/issuetype"


ATLASSIAN_ID=f.dutrech@ateme.com
# ATLASSIAN_ID=f.dutrec
# JIRA_TOKEN=WOtsBRcpEgUPGeVE9XDlAEC1
JIRA_TOKEN=NwYFNNYKtZSnylrCxG8IABB0

AUTH=$(echo $ATLASSIAN_ID:$JIRA_TOKEN | base64)
# Zi5kdXRyZWNoQGF0ZW1lLmNvbTpXT3RzQlJjcEVnVVBHZVZFOVhEbEFFQzEK

# set -x

# # curl --silent -v ${JIRA_URL} --user $ATLASSIAN_ID:$JIRA_TOKEN

# curl -u "$ATLASSIAN_ID:$JIRA_TOKEN" \
#      -X GET \
#      --silent \
#      --url "${JIRA_URL}/${REQUEST}"

      # -H "Content-Type: application/json" \
     # --request POST \
     # --data "$DATA_JSON"

# curl --request GET \
#   --url "${JIRA_URL}/rest/api/3/issue" \
#   --header "Authorization: Basic $AUTH" \
#   --header "Accept: application/json" \
#   --header "Content-Type: application/json"

  # --data '{…}'
# curl --request POST
#   --url 'https://<your-jira-cloud-instance>/rest/api/3/issue'
#   --header 'Authorization: Basic YourEncodedStringHere'
#   --header 'Accept: application/json'
#   --header 'Content-Type: application/json'
#   --data '{…}'


# curl -X GET --silent --url https://fdutrech:WOtsBRcpEgUPGeVE9XDlAEC1@myateme.atlassian.net/rest/api/2/user

# curl -X GET -H "Content-Type: application/json" "${JIRA_URL}/jira/rest/api/2/issue/TP-5447 " --user "$ATLASSIAN_ID:$JIRA_TOKEN"



GITLAB_TOKEN=44zLZ8rn-1GGTjN1bSsY

# curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" 'https://gitlab.ateme.net/api/v4/user' | jq
#   =>user id=1146

# curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" 'https://gitlab.ateme.net/api/v4/users/1146/starred_projects' | jq
#   =>project id=92

# merge request :
   # https://gitlab.ateme.net/api/v4/projects/92/merge_requests/3485/discussions
   # "iid": 3485,
   # "project_id": 92,
