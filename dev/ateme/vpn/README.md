

## Run root app:

// src : https://doc.ubuntu-fr.org/vpnc
//
// gcc -Wall -o usvpnc usvpnc.c
// chown root:root usvpnc
// chmod u=rws,g=rx,o=rx usvpnc

// cp usvpnc /usr/local/sbin
// chmod u=rws,g=rx,o=rx /usr/local/sbin/usvpnc

## hide application

`xprop` + click app -> find  `WM_CLASS(STRING) = "name", "name"`

wmctrl -x -r name.name -b add,skip_taskbar

https://askubuntu.com/questions/646346/xfce-hiding-an-application-from-the-taskbar

or:

wmctrl -r :SELECT: -b add,skip_taskbar
...now you will only have to click on the window that you want to hide.

If you need to return this window to the taskbar, use
wmctrl -r :SELECT: -b remove,skip_taskbar