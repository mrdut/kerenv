#! bash -x

rm -fv ateme-vpn
gcc -Wall -o ateme-vpn usr-vpn.c
sudo chown root:root ateme-vpn
sudo mv -v ateme-vpn /usr/local/sbin
sudo chmod u=rws,g=rx,o=rx /usr/local/sbin/ateme-vpn