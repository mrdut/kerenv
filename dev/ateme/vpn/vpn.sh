#! /usr/bin/sh

# set -x

echo "hello"

if ping nexus-rennes.ateme.net -W 1 -c 1; then
    echo -e "\033[32mVPN is up\033[0m";
    exit 0
fi

# screen -D -R -S vpn


PWD2=$1

# if ! screen -list | grep -q "vpn"; then
#   screen -S vpn
# else
#   screen -r vpn
# fi


for i in {0..5}; do

    # YKMAN_LOG_LEVEL=debug
    YKMAN_LOG_LEVEL=info
    ykman --log-level=${YKMAN_LOG_LEVEL} oath accounts code secure.ateme |tee /dev/tty | tail -n 1 | awk -F' ' '{print $2}' | tr '\n' '@'| sed 's/@//' | xclip -selection clipboard

    YKMAN_SUCCESS=$?

    clipboard_content=`xclip -o -selection clipboard`
    echo "$clipboard_content"

    PWD2=$clipboard_content

    if [ -z $PWD2 ]; then
        echo -e "\033[33mykman failed ! retry... ($i - PWD2=${PWD2}) YKMAN_SUCCESS=${YKMAN_SUCCESS}\033[0m"
        sleep 1
        continue
    else
        echo -e "\033[32mykman success ! (OATH : ${PWD2}) YKMAN_SUCCESS=${YKMAN_SUCCESS}\033[0m"
        break
    fi

done

if [ -z $PWD2 ]; then
    echo -e "\033[33mykman failed ! exit\033[0m"
    sleep 3
    exit 1
fi

# sudo openconnect --user 'l.lemaire@ateme.com' --disable-ipv6 --protocol=nc https://secure.ateme.net/dana-na/auth/url_02OqgELVjXYCAnUE/welcome.cgi


# nohup
/usr/bin/expect -c "


spawn -ignore HUP openconnect --protocol=nc --disable-ipv6 https://secure.ateme.net/dana-na/auth/url_02OqgELVjXYCAnUE/welcome.cgi

# spawn -ignore HUP openconnect --protocol=nc --disable-ipv6 https://secure.ateme.net/OTP

# spawn -ignore HUP openconnect --protocol=nc --background --disable-ipv6 https://secure.ateme.net/OTP
# spawn openconnect --protocol=anyconnect  https://secure.ateme.net/OTP
# spawn openconnect --protocol=pulse  https://secure.ateme.net/OTP

set timeout 60

expect {
    \"username:\" {send \"f.dutrech@ateme.com\n\"}
}
expect {
    \"password:\" {send \"$(grep password /home/fdutrech/Workspace/kerateme/devops/mount_credential.txt | awk -F '=' '{print $2}')\n\"}
}

expect {
    \"password#2:\" {send \"${PWD2}\n\"}
}

interact
"
 # &

# disown
# less -F /tmp/nohup.out
