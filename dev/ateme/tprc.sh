
# https://myateme.atlassian.net/wiki/spaces/CC/overview#Deux-NAS-(partag%C3%A9s)-%3A
# sudo mount -t cifs -o rw,username=fdutrech,password=SvLIV9kPcZmJ,domain=ATEME //172.16.0.31/reference /ateme/nas/reference
# sudo mount -t cifs -o rw,username=fdutrech,password=SvLIV9kPcZmJ //172.16.0.31/temp /ateme/nas/temp

# OK + apwd : sudo mount -t cifs -o gid=1001,uid=1001,user=fdutrech,vers=3.0 //arty.ateme.net/shares /ateme/input

function tp_mount_cp_pytest_input() {
		# (set -x; sudo mount -t cifs -o ro,username=cp-ci-agent,password="0Orj}onye" //10.80.135.33/CoreProcessing/PytestInput /ateme/input)
		(set -x; sudo mount -t cifs -o rw,username=cp-ci-agent,password="0Orj}onye" //10.80.135.33/CoreProcessing/PytestInput /ateme/input)
}

function tp_mount_cp_pytest_input_integration() {
	(set -x; sudo mount -t cifs -o ro,username=cp-ci-agent,password="0Orj}onye" //10.80.135.33/CoreProcessing/PytestInputIntegration /ateme/input)
}

function tp_mount_rdnas2() {
	(set -x; sudo mount -t cifs -o ro,username=cp-ci-agent,password="0Orj}onye" //10.80.135.33/ /mnt/rd-nas-2)
}

# sudo mount -t cifs -o gid=1001,uid=1001,rw,user=adoussot //atmnas.ateme.net/Temp ~/support_win/mounts_temp/

function tp_mount_local() {
	(set -x; sudo bindfs /home/fdutrech/Videos /ateme/input)
}

function tp_umount() {
	(set -x; sudo umount /ateme/input)
}

TP_PIPELINE=${TP_PIPELINE:-anonymous}

TP_PORT=5560
TP_ADDR=http://localhost
TP_URL=${TP_ADDR}:${TP_PORT}

function tp_post() {
	yaml=${TP_YAML:-$1}
	pipeline=${2:-$TP_PIPELINE}
	echo "TP_YAML=$TP_YAML"
	[ -z "${yaml}" ] && { echo "Unset TP_YAML"; return 1; }
	[[ "${yaml}" != *.yaml ]] && { echo "Invalid TP_YAML=$yaml - must be yaml file"; return 1; }
	[ -z "${pipeline}" ] && { echo "Unset TP_PIPELINE"; return 1; }

	if [[ "$pipeline" == "--from-yaml" ]]; then
		pipeline=$(basename $yaml)
	fi
	echo "pipeline=$pipeline"
	echo "yaml=$yaml"
	(set -x; curl -s -H 'Content-Type: application/yaml' -H 'Expect:' -X POST -T ${yaml} ${TP_URL}/pipelines/${pipeline})
}

function tp_delete() {
	pipeline=${1:-$TP_PIPELINE}
	(set -x; curl -s -H 'Content-Type: application/yaml' -H 'Expect:' -X DELETE ${TP_URL}/pipelines/${pipeline})
}


function tp_delete_all() {
	for PIPELINE in $(tp_list | jq ".[]" | xargs); do
		tp_delete $PIPELINE
	done
}

function tp_stat() {
	# Usage : tp_stat (pipeline) JSON_PATH
	pipeline=$TP_PIPELINE

	if [ "$#" -eq 0 ]; then
		JSON_PATH=.
	elif [ "$#" -eq 1 ]; then
		JSON_PATH=$1
	elif [ "$#" -eq 2 ]; then
		pipeline=$1
		JSON_PATH=$2
	else
		echo "Invalid argument count ($#)"
		return 1
	fi
	(set -x; curl -s ${TP_URL}/statistics/${pipeline} | jq $JSON_PATH)
}

function tp_watch_stat() {
	JSON_PATH=${1:-.}
	(set -x; watch -n 0.1 "curl -s ${TP_URL}/statistics/${TP_PIPELINE} | jq $JSON_PATH")
}

function tp_list() {
	(set -x; curl -s ${TP_URL}/pipelines | jq .)
}

function tp_units() {
	printf $1
	echo " --units " $(cat $1  \
						| yq '.modules | .[] | keys[]' \
						| sort \
						| uniq \
						| sed 's/"//g' \
						| sed -E 's/(^|_)([a-z])/\U\2/g' \
						| sed -E 's/InterProcessPipeline(Input|Output)/InterProcessPipeline/g' \
						| sed -E 's/VideoEncoder.*/VideoEncoder/g' \
						| xargs \
						| sed 's/ /,/g')
}

function tp_mem() {
	JSON_PATH=${1:-.}
	(set -x; curl -s ${TP_URL}/memory | jq ${JSON_PATH})
}

function tp_watch_mem() {
	JSON_PATH=${1:-.}
	(set -x; watch -n 0.1 "curl -s ${TP_URL}/memory | jq $JSON_PATH")
}

function tp_kill() {
	(set +x; for PID in $(ps -aux | grep TitanProcessing | grep -v grep | awk '{print $2}'); do echo "Killing TitanProcessing pid=$PID"; kill -9 $PID; done; )
}

function yamlify() {
	JSON=$1
	[[ "${JSON}" != *.json ]] && { echo "Invalid input parameter $JSON - must be JSON file"; return 1; }
	YAML=${JSON%.json}.yaml
	echo "Yamlify : $JSON -> $YAML"
	cat $JSON | yq -y > $YAML
}

