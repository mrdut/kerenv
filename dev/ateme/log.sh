#! /bin/bash

export ATEME_LOG_LEVEL=${ATEME_LOG_LEVEL-INFO}
export ATEME_LOG_TITLE=${ATEME_LOG_TITLE-}

# bash ?
# declare -A ATEME_LOG_LEVEL_MAP
# ATEME_LOG_LEVEL_MAP["debug"]=0
# ATEME_LOG_LEVEL_MAP["info"]=10
# ATEME_LOG_LEVEL_MAP["warning"]=20
# ATEME_LOG_LEVEL_MAP["warn"]=30
# ATEME_LOG_LEVEL_MAP["error"]=40

declare -A ATEME_LOG_LEVEL_MAP
ATEME_LOG_LEVEL_MAP[debug]=0
ATEME_LOG_LEVEL_MAP[info]=10
ATEME_LOG_LEVEL_MAP[warning]=20
ATEME_LOG_LEVEL_MAP[warn]=30
ATEME_LOG_LEVEL_MAP[error]=40



COLOR_BOLD="\e[1m"
COLOR_RESET="\e[0m"
COLOR_RED="\e[31m"
COLOR_GREEN="\e[32m"
COLOR_YELLOW="\e[33m"
COLOR_BLUE="\e[34m"
COLOR_MAGENTA="\e[35m"
COLOR_CYAN="\e[36m"
COLOR_WHITE="\e[37m"
COLOR_LIGHT_GRAY="\e[37m"
COLOR_DARK_GRAY="\e[90m"


# https://misc.flogisoft.com/bash/tip_colors_and_formatting
declare -A ATEME_LOG_LEVEL_COLOR
# bash ?
# ATEME_LOG_LEVEL_COLOR["red"]=$COLOR_RED
# ATEME_LOG_LEVEL_COLOR["green"]=$COLOR_GREEN
# ATEME_LOG_LEVEL_COLOR["yellow"]=$COLOR_YELLOW
# ATEME_LOG_LEVEL_COLOR["blue"]=$COLOR_BLUE
# ATEME_LOG_LEVEL_COLOR["magenta"]=$COLOR_MAGENTA
# ATEME_LOG_LEVEL_COLOR["cyan"]=$COLOR_CYAN
ATEME_LOG_LEVEL_COLOR[red]=$COLOR_RED
ATEME_LOG_LEVEL_COLOR[green]=$COLOR_GREEN
ATEME_LOG_LEVEL_COLOR[yellow]=$COLOR_YELLOW
ATEME_LOG_LEVEL_COLOR[blue]=$COLOR_BLUE
ATEME_LOG_LEVEL_COLOR[magenta]=$COLOR_MAGENTA
ATEME_LOG_LEVEL_COLOR[cyan]=$COLOR_CYAN

function log_internal {
    

    user_log_level="${ATEME_LOG_LEVEL_MAP[${ATEME_LOG_LEVEL:l}]}" # ZSH lowercase : ${VAR:l}
    # user_log_level="${ATEME_LOG_LEVEL_MAP[${ATEME_LOG_LEVEL,,}]}" # BASH lowercase : ${VAR,,}
    log_level=${ATEME_LOG_LEVEL_MAP[$1]}
    log_color=${ATEME_LOG_LEVEL_COLOR[$2]}
    log_msg=$3
    # echo "ATEME_LOG_LEVEL=$ATEME_LOG_LEVEL"
    # echo "user_log_level=$user_log_level"
    # echo "log_level=$log_level"
    # echo "log_color=$2"
    # echo "log_msg=$log_msg"

    [ "$log_level" -ge "$user_log_level" ] && echo -e "[${log_color}$log_msg${COLOR_RESET}]${ATEME_LOG_TITLE}${@:4}" >&2 ;
}

function log_color {
    color=${ATEME_LOG_LEVEL_COLOR[$1]}
    echo -e "${color}${@:2}${COLOR_RESET}" >&2 ;
}


function log_failure  { log_internal "error" "red" "FAILURE" "$@"; }
function log_error  { log_internal "error" "red" "ERROR" "$@"; }
function log_debug  { log_internal "debug" "cyan" "DEBUG" "$@"; }
function log_ko     { log_internal "error" "red" "KO" "$@"; }
function log_ok     { log_internal "info" "green" "OK" "$@"; }
function log_success     { log_internal "info" "green" "SUCCESS" "$@"; }
function log_warning     { log_internal "warning" "yellow" "WARN" "$@"; }
function log_info     { log_internal "info" "blue" "INFO" "$@"; }
function log_cmd     { log_internal "info" "cyan" "CMD" "$@"; }
function log_returncode { if [ "$1" -eq "0" ]; then log_success "${@:2}"; else log_failure "${@:2}"; fi }

function log_cyan     { log_color "cyan" "$@"; }
