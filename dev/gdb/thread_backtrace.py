
import sys, re, termcolor

try:
    import gdb

except Exception as e:

    #
    # Fake gdb class to use outside of gdb debugger
    #
    print("!! fake gdb class !!")
    class gdb:
        
        COMMAND_STACK = None

        @staticmethod
        def string_to_argv(string):
            return string.split()

        class Command:
            def __init__(self, *args, **kwargs):
                pass


#
# Install : 

# ```
#   # in ~/gdbinit
#   source PATH_TO_KERENV/gdb/thread_backtrace.py
#   alias -a tbt=thread_backtrace
# ```
#
# Usage :
# 
# ```
# (gdb) thread_backtrace THREAD_ID
# (gdb) thread_backtrace  (all threads)
# ```
#


class ThreadBackTraceCommand(gdb.Command):

    # https://fy.blackhats.net.au/blog/html/2017/08/04/so_you_want_to_script_gdb_with_python.html

    # * 1    Thread 0x7ffff7f93c40 (LWP 11785) "blue-ski-Linux-" 0x00007ffff7181c32 in bluecime::matcher::sse_distance_transform (src_mat=..., dst=..., max_dist=10, value=255 '\377')
    thread_id_regex = re.compile(r'^\s*[*]?\s*(\d+).*"(.*)".*$')
    
    # #3  0x00007fffe380288d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:111
    small_backtrace_regex = re.compile(r"^(#\d)*\s*0x.* in (.*) \(.*$")


    def __init__(self):
        # This registers our class as "simple_command"
        super(ThreadBackTraceCommand, self).__init__("thread_backtrace", gdb.COMMAND_STACK)

    @staticmethod
    def smallBacktrace(bt):
        """
        Reduce backatrec for better lisibility

        From:
        #0  0x00007fffe32ef9f5 in pthread_cond_wait@@GLIBC_2.3.2 () at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
        #1  0x00007ffff708dfaa in bluecime::thread::Pool::work(void*) (arg=0xef9e10) at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/lib/bluevision/lib/bluevision/core/thread/pool.cpp:65
        #2  0x00007fffe32ebe65 in start_thread (arg=0x7fffcffff700) at pthread_create.c:307
        #3  0x00007fffe380288d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:111

        To:
        #0  pthread_cond_wait@@GLIBC_2.3.2 ()
        #1  bluecime::thread::Pool::work(void*)
        #2  start_thread
        #3  clone ()
        """

        # ret = []
        # for line in bt.split("\n"):
        #     m = ThreadBackTraceCommand.small_backtrace_regex.match(line)
        #     if m:
        #         ret.append("%s %s"%(m.groups()))
        #     else:
        #         print("!! small_backtrace_regex no match :", repr(line))

        # return "\n".join(ret)

        return "\n".join(["%s %s"%ThreadBackTraceCommand.small_backtrace_regex.match(line).groups() for line in bt.split("\n") if line])



    @staticmethod
    def parseAllThreadId(thread_info_str):
        """
        Parse threads id from gdb `info threads` command
    
        (gdb) info threads           
          Id   Target Id         Frame 
        * 1    Thread 0x7ffff7f93c40 (LWP 11785) "blue-ski-Linux-" 0x00007ffff7181c32 in bluecime::matcher::sse_distance_transform (src_mat=..., dst=..., max_dist=10, value=255 '\377')
            at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/lib/bluevision/lib/bluevision/matcher/chamfer/chamfer_distance_map.cpp:181
          3    Thread 0x7fffd7035700 (LWP 12265) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
            at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
          4    Thread 0x7fffd6834700 (LWP 12269) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
          ...
        """

        # tid = []
        # for line in thread_info_str.split("\n"):
        #     m = ThreadBackTraceCommand.thread_id_regex.match(line)
        #     if m:
        #         tid.append(m.group(1))
        #     else:
        #         print("!! thread_id_regex no match :", repr(line))
        # return tid

        return {m.group(1) : m.group(2) for m in [ThreadBackTraceCommand.thread_id_regex.match(line) for line in thread_info_str.split("\n")] if m}

    def invoke(self, args_string, from_tty):

        # print("invoke", args_string, from_tty)

        # gdb forwards all args as single string
        args = gdb.string_to_argv(args_string)

        cmd = gdb.execute('info threads', to_string=True)
        # print(repr(cmd))
        # print(type(cmd))

        threads = self.parseAllThreadId(cmd)

        if args:
            threads = { a:threads[a] for a in args if a in threads }
            for tid in [a for a in args if a not in threads]:
                print("!! could not find thread id %s - skip"%tid)

        # print(ids)

        print("="*40)
        for i,tid in enumerate(sorted(threads.keys(), key=int)):
            print('[%d/%d] Thread #%s name="%s"'%(i+1, len(threads.keys()), tid, threads[tid]))
            s = gdb.execute('thread %s'%tid, to_string=True).split()
            n = '15'
            print("-> (reduced) backtrace " + n + " :")
            # bt = gdb.execute('backtrace 5', to_string=True)
            print(gdb.execute('backtrace ' + n, to_string=True))
            # print(self.smallBacktrace(gdb.execute('backtrace ' + n, to_string=True)))
            print("-"*40)


# This registers our class to the gdb runtime at "source" time.
ThreadBackTraceCommand()




if __name__ == '__main__':

    thread_info_str = """
    (gdb) info threads           
      Id   Target Id         Frame 
    * 1    Thread 0x7ffff7f93c40 (LWP 11785) "blue-ski-Linux-" 0x00007ffff7181c32 in bluecime::matcher::sse_distance_transform (src_mat=..., dst=..., max_dist=10, value=255 '\377')
        at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/lib/bluevision/lib/bluevision/matcher/chamfer/chamfer_distance_map.cpp:181
      3    Thread 0x7fffd7035700 (LWP 12265) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      4    Thread 0x7fffd6834700 (LWP 12269) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      5    Thread 0x7fffd7836700 (LWP 12270) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      6    Thread 0x7fffd6033700 (LWP 12271) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      7    Thread 0x7fffd5832700 (LWP 12272) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      8    Thread 0x7fffd5031700 (LWP 12273) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      9    Thread 0x7fffd4830700 (LWP 12274) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      10   Thread 0x7fffcffff700 (LWP 12275) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      11   Thread 0x7fffcf7fe700 (LWP 12276) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      12   Thread 0x7fffceffd700 (LWP 12277) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      13   Thread 0x7fffce7fc700 (LWP 12278) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      14   Thread 0x7fffcdffb700 (LWP 12279) "ProcessPool::0" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      15   Thread 0x7fffcd7fa700 (LWP 12280) "ProcessPool::1" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      16   Thread 0x7fffccff9700 (LWP 12281) "ProcessPool::2" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      17   Thread 0x7fffcc7f8700 (LWP 12282) "ProcessPool::3" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      18   Thread 0x7fffc9527700 (LWP 12304) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      19   Thread 0x7fffc8d26700 (LWP 12305) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      21   Thread 0x7fffc7d24700 (LWP 12307) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      22   Thread 0x7fffc8525700 (LWP 12308) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      23   Thread 0x7fffc7523700 (LWP 12309) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      24   Thread 0x7fffc42c8700 (LWP 12310) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      25   Thread 0x7fffc3ac7700 (LWP 12311) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      26   Thread 0x7fffc32c6700 (LWP 12312) "blue-ski-Linux-" syscall ()
        at ../sysdeps/unix/sysv/linux/x86_64/syscall.S:38
      27   Thread 0x7fffc2ac5700 (LWP 12313) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      28   Thread 0x7fffc22c4700 (LWP 12317) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      29   Thread 0x7fffc1ac3700 (LWP 12318) "blue-ski-Linux-" syscall ()
        at ../sysdeps/unix/sysv/linux/x86_64/syscall.S:38
      34   Thread 0x7fffca7f4700 (LWP 12323) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      35   Thread 0x7fffcaff5700 (LWP 12324) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      36   Thread 0x7fffcb7f6700 (LWP 12325) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
      37   Thread 0x7fffcbff7700 (LWP 12326) "blue-ski-Linux-" pthread_cond_wait@@GLIBC_2.3.2 ()
        at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
    """

    backtrace_str = """
#0  0x00007fffe32ef9f5 in pthread_cond_wait@@GLIBC_2.3.2 () at ../nptl/sysdeps/unix/sysv/linux/x86_64/pthread_cond_wait.S:185
#1  0x00007ffff708dfaa in bluecime::thread::Pool::work(void*) (arg=0xef9e10) at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/lib/bluevision/lib/bluevision/core/thread/pool.cpp:65
#2  0x00007fffe32ebe65 in start_thread (arg=0x7fffcffff700) at pthread_create.c:307
#3  0x00007fffe380288d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:111
"""


    if "unittest" in sys.argv:
        print( thread_info_str )
        print( ThreadBackTraceCommand.parseAllThreadId(thread_info_str) )

        print( backtrace_str )
        print( ThreadBackTraceCommand.smallBacktrace(backtrace_str) )
