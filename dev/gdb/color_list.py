
# "pip install pygments
from pygments import highlight
from pygments.lexers import CppLexer as Lexer
from pygments.formatters import TerminalFormatter as Formatter

import gdb
class ColorListCommand(gdb.Command):

    def __init__(self):
        super(ColorListCommand, self).__init__("color_list", gdb.COMMAND_DATA)

    def invoke(self, arg, from_tty):

        print(repr(arg))
        code = gdb.execute('list ' + arg, to_string=True)
        print(highlight(code, Lexer(), Formatter()))


# This registers our class to the gdb runtime at "source" time.
ColorListCommand()


# code = """
# 125     {
# 126         bluecime::writeExceptionLogFile(e);
# 127         std::cerr << e.what() << std::endl;
# 128     }
# 129 
# 130 }
# 131 #endif
# """


# else : # '__main__'

#     bt = """
#     #0  0x00007fffebb1c64b in raise () at /lib64/libc.so.6
#     #1  0x00007fffebb1e450 in abort () at /lib64/libc.so.6
#     #2  0x00007fffebb62921 in __libc_message () at /lib64/libc.so.6
#     #3  0x00007fffebb6d7a9 in _int_free () at /lib64/libc.so.6
#     #4  0x00007fffebb7310e in free () at /lib64/libc.so.6
#     #5  0x00007ffff5e63c4b in AlgoIntegration::processFrame(bluecime::ImageView*) () at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib/libextra-eyes-gui.so.Linux-4.16.11-100.fc26.x86_64-rbg
#     #6  0x00007ffff5e82664 in bluecime::QueuedGrabber::frameLoop() () at /work-crypt/fdutrech/workspace/bluecime/extra-eyes/build/lib/libextra-eyes-gui.so.Linux-4.16.11-100.fc26.x86_64-rbg
#     #7  0x00007ffff48d33aa in QObject::event(QEvent*) (this=0xdb6978, e=<optimized out>) at kernel/qobject.cpp:1246
#     #8  0x00007ffff5125f2c in QApplicationPrivate::notify_helper(QObject*, QEvent*) (this=this@entry=0xcb82c0, receiver=receiver@entry=0xdb6978, e=e@entry=0x2f7fc80) at kernel/qapplication.cpp:3722
#     #9  0x00007ffff512d774 in QApplication::notify(QObject*, QEvent*) (this=0x7fffffffd940, receiver=0xdb6978, e=0x2f7fc80) at kernel/qapplication.cpp:3481
#     #10 0x00007ffff48a9d27 in QCoreApplication::notifyInternal2(QObject*, QEvent*) (receiver=0xdb6978, event=event@entry=0x2f7fc80) at kernel/qcoreapplication.cpp:1016
#     #11 0x00007ffff48ac4ab in QCoreApplication::sendEvent(QObject*, QEvent*) (event=0x2f7fc80, receiver=<optimized out>) at kernel/qcoreapplication.h:233
#     #12 0x00007ffff48ac4ab in QCoreApplicationPrivate::sendPostedEvents(QObject*, int, QThreadData*) (receiver=receiver@entry=0x0, event_type=event_type@entry=0, data=0x30336e0) at kernel/qcoreapplication.cpp:1676
#     #13 0x00007ffff48ac9c8 in QCoreApplication::sendPostedEvents(QObject*, int) (receiver=receiver@entry=0x0, event_type=event_type@entry=0) at kernel/qcoreapplication.cpp:1530
#     #14 0x00007ffff48faf83 in postEventSourceDispatch(GSource*, GSourceFunc, gpointer) (s=0x7fff7c0012d0) at kernel/qeventdispatcher_glib.cpp:276
#     #15 0x00007fffe3029257 in g_main_dispatch (context=0x7fff7c000990) at gmain.c:3234
#     #16 0x00007fffe3029257 in g_main_context_dispatch (context=context@entry=0x7fff7c000990) at gmain.c:3899
#     #17 0x00007fffe30295f8 in g_main_context_iterate (context=context@entry=0x7fff7c000990, block=block@entry=1, dispatch=dispatch@entry=1, self=<optimized out>) at gmain.c:3972
#     #18 0x00007fffe302968c in g_main_context_iteration (context=0x7fff7c000990, may_block=may_block@entry=1) at gmain.c:4033
#     #19 0x00007ffff48fad6f in QEventDispatcherGlib::processEvents(QFlags<QEventLoop::ProcessEventsFlag>) (this=0x7fff7c0008c0, flags=...) at kernel/qeventdispatcher_glib.cpp:423
#     """


#     for l in bt.split("\n"):
#         # print( l )
#         if l.strip():
#             m = backtrace_regex.match(l)
#             if m:
#                 g = m.groups()

#                 # print(g)

#                 termcolor.cprint( g[0] + g[1], color='yellow', end=' ')
#                 # termcolor.cprint( g[1], color='grey', end=' ')
#                 termcolor.cprint( g[2], end=' ')
#                 if g[3] is not None:
#                     termcolor.cprint( g[3], end=' ')
#                 termcolor.cprint( g[4], color='blue',  end='')
#                 # termcolor.cprint( " at ", end='')
#                 termcolor.cprint( " "+g[5], color='grey')

#             else:
#                 print("NO MATCH : ", repr(l))
