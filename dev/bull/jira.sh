#! /bin/bash

# Jira v8.9.1
# https://docs.atlassian.com/software/jira/docs/api/REST/8.9.1/
# https://developer.atlassian.com/cloud/jira/software/rest/api-group-board/#api-group-board
# https://developer.atlassian.com/cloud/jira/software/rest/api-group-board/#api-agile-1-0-board-post PYTHON EXMPLE

# curl --request POST \
#   --url 'https://your-domain.atlassian.net/rest/agile/1.0/board' \
#   --header 'Accept: application/json' \
#   --header 'Content-Type: application/json' \
#   --data '{
#   "name": "<string>",
#   "type": "<string>",
#   "filterId": 2154,
#   "location": {
#     "type": "<string>",
#     "projectKeyOrId": "<string>"
#   }
# }'

JIRA_URL=https://jirabdsfr.fsc.atos-services.net
USER=a787361
JIRA_TOKEN=qUoMHbVt38Dqn01EqiHQRrcrP8PONvutE8cHpY

# REQUEST=rest/api/2/issue/createmeta # OK
# DATA='maxResults=200&startAt=0'

# REQUEST=rest/api/2/dashboard # OK
# DATA='maxResults=200&startAt=0' # data='{ "filter" : "my" }'


# get Board ID
# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" ${URL}`
# REQUEST=rest/agile/1.0/board
# DATA='startAt=147&maxResults=2'
# URL="${JIRA_URL}/${REQUEST}?${DATA}"
# curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" ${URL} | jq
# VideoI : 848
# VideoI_bugs : 929





# create ISSUE
# https://developer.atlassian.com/server/jira/platform/jira-rest-api-example-create-issue-7897248/

# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" ${JIRA_URL}/rest/api/2/issue/createmeta`    # ALL projects, ALL issues
# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" "${JIRA_URL}/rest/api/2/issue/createmeta?projectKeys=BREBD"`   # BREBD project - SHORT
# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" "${JIRA_URL}/rest/api/2/issue/createmeta?projectKeys=BREBD&issuetypeNames=Bug"`   # BREBD project - BUG issue - SHORT
# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" "${JIRA_URL}/rest/api/2/issue/createmeta?projectKeys=BREBD&issuetypeNames=Bug&expand=projects.issuetypes.fields"` # BREBD project - BUG issue - LONG
stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" "${JIRA_URL}/rest/api/2/issue/createmeta?projectKeys=BREBD&issuetypeNames=User%20story&expand=projects.issuetypes.fields"` # BREBD project - BUG issue - LONG


# http://localhost:8090/rest/api/2/issue/createmeta?projectKeys=JRA&issuetypeNames=Bug&expand=projects.issuetypes.fields


# DATA='filter=my'

# URL="${JIRA_URL}/${REQUEST}?${DATA}"

# echo "URL = $URL"

# stdout=`curl --silent -u $USER:$JIRA_TOKEN -X GET -H "Content-Type: application/json" ${URL}`


# REQUEST=rest/api/2/issue/
# stdout=`curl -u $USER:$JIRA_TOKEN \
#      --request POST \
#      -H "Content-Type: application/json" \
#      --url "${JIRA_URL}/${REQUEST}" \
#      --data '{
#     "fields": {
#        "project":
#        {
#           "key": "BREBD"
#        },
#        "summary": "REST ye merry gentlemen.",
#        "components" : [{ "name" : "VI" }],
#        "labels" : ["CI"],
#        "description": "Creating of an issue using project keys and issue type names using the REST API",
#        "issuetype": {
#           "name": "Bug"
#        }
#    }
# }'`
# # https://jirabdsfr.fsc.atos-services.net/browse/BREBD-5089

# bug_key=`echo $stdout | jq '.key' | sed 's/"//g'`
# echo "https://jirabdsfr.fsc.atos-services.net/browse/${bug_key}"
# echo "https://jirabdsfr.fsc.atos-services.net/secure/RapidBoard.jspa?rapidView=929&view=detail&selectedIssue=${bug_key}"
# # echo $stdout | jq --indent 4 > /tmp/rep.json


if [ "$?" == 0 ]; then
    echo $stdout | jq
    echo $stdout | jq --indent 4 > /tmp/rep.json
else
    echo "curl fail"
    echo $stdout
fi

