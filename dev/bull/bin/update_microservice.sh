#! bash


KOIOS_HARDWARE=${KOIOS_HARDWARE:-"undefined"}
DOCKER_MICROSERVICE_NAME=`grep DOCKER_MICROSERVICE_NAME Makefile | cut -d '=' -f 2`
DOCKER_REGISTRY=`grep "DOCKER_REGISTRY?=" koios-shared-build-scripts/make/common.mk | cut -d '=' -f 2`

grep multiHardware scripts/jenkins/Jenkinsfile | grep -q true
[[ "$?" == "0" ]] && MY_KOIOS_HARDWARE="$KOIOS_HARDWARE" || MY_KOIOS_HARDWARE="generic"


GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`

DOCKER_IMAGE=${DOCKER_REGISTRY}xbd-vision/${DOCKER_MICROSERVICE_NAME}/${MY_KOIOS_HARDWARE}


PACKAGE_FILE="*.dist-info/top_level.txt"
[ ! -f $PACKAGE_FILE ] && python3 setup.py dist_info > /dev/null
PACKAGE_DIR=`cat $PACKAGE_FILE`

# docker images | grep $DOCKER_IMAGE | grep $GIT_BRANCH | awk '{print $3}'

echo " - KOIOS_HARDWARE=$MY_KOIOS_HARDWARE"
echo " - DOCKER_IMAGE=$DOCKER_IMAGE:$GIT_BRANCH"
echo " - PACKAGE_DIR=$PACKAGE_DIR"

# CONTAINER_NAME=`whoami`_${DOCKER_MICROSERVICE_NAME}_${GIT_BRANCH}

# echo " - step 1 : run docker -> $CONTAINER_NAME"
# docker run -t -d  -v `pwd`:/git -w /usr/local/lib/python3.6/dist-packages/$PACKAGE_DIR/ --name $CONTAINER_NAME $DOCKER_IMAGE:$GIT_BRANCH 

# echo " - step 2 : update $CONTAINER_NAME dist with local files"
# docker exec $CONTAINER_NAME find . -type f  -not -path "*/__pycache__/*" -exec cp -v /git/$PACKAGE_DIR/{} {} \;

# echo " - step 3 : commit changes"
# docker commit $CONTAINER_NAME $DOCKER_IMAGE:$GIT_BRANCH

# echo " - step 4 : rm docker"
# docker rm -f $CONTAINER_NAME 
