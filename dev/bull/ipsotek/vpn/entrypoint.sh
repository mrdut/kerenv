#! /bin/bash

echo "[entrypoint] Welcome (args=$1)"
/root/vpn_ipsotek_start.expect.sh

# run this script forever as it is the entrypoint 
echo "[entrypoint] sleep infinity..."
sleep infinity

echo "[entrypoint] Goodbye"
