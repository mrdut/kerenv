#! python3

import sys, os
import argparse
import json
import re
from logzero import logger
from pathlib import Path

def is_windows():
    with open("/proc/version") as f:
        return "Microsoft" in f.read()

def windowify(path):
    return path.replace("/mnt/c", "C:").replace("/", "\\") if is_windows() else path

ROOT=".sublime"


if __name__ == '__main__':

    logger.info("Hello !")

    parser = argparse.ArgumentParser()
    parser.add_argument("mode", help="which mode to use", choices=["project", "workspace", "session"])
    parser.add_argument("--shortname", help="koios project shortname", required=True)
    parser.add_argument("--name", help="koios project name", default=None)

    cwd=os.getcwd()
    logger.info(f" - cwd = '{cwd}'")


    if not os.path.exists(ROOT):
        logger.info(f" - create '{ROOT}' directory")
        os.mkdir(ROOT)
    else:
        logger.info(f" - '{ROOT}' directory already exists")

    args = parser.parse_args()

    if args.name is None:
        args.name = os.path.basename( cwd )

    basename = f"{args.shortname.upper()}-{args.name}" if args.shortname else args.name

    root = Path.cwd() / ROOT
    project_name = root / (basename +".sublime-project")
    workspace_name = root / (basename +".sublime-workspace")

    if is_windows():
        sublime_dir = "/mnt/c/Users/a787361/AppData/Roaming/Sublime Text 3/Local/" 
    else:
        
        sublime_dir = Path.home() / ".config/sublime-text-3/Local/"
            
    sublime_session = sublime_dir / "Session.sublime_session"


    logger.info(f" - project_name : {project_name}")
    logger.info(f" - workspace_name : {workspace_name}")
    logger.info(f" - sublime_session : {sublime_session}")

    if args.mode == "project":
        assert args.shortname != ""
        assert args.name != ""

        project = {
            "folders":
            [
                {
                    "file_exclude_patterns" : [".coverage", "unit-tests-results.xml"],
                    "folder_exclude_patterns" : [".git", "build", "htmlcolv", ".pytest_cache", "__py_cache__"],
                    "path": windowify(cwd)
                }
            ]
        }

        if not project_name.exists():
            logger.info(f" - write {project_name}")
            with project_name.open("w") as f:
                json.dump(project, f, indent=4)
        else:
            logger.info(f" - skip {project_name} (already exists)")

    elif args.mode == "workspace":
        assert args.name != ""

        workspace = {
                        "find_in_files":
                        {
                            "where_history":
                            [
                                windowify(os.path.join(cwd, args.name))
                            ]
                        }
                    }
        
        if not workspace_name.exists():
            logger.info(f" - write {workspace_name}")
            with workspace_name.open("w") as f:
                json.dump(workspace, f, indent=4)
        else:
            print(f" - skip {workspace_name} (already exists)")

    elif args.mode == "session":

        

        if sublime_session.exists():

            with sublime_session.open() as f:
                session = json.load(f)

            if "workspaces" in session:
                if "recent_workspaces" in session["workspaces"]:
                    if str(workspace_name) in session["workspaces"]["recent_workspaces"]:
                        logger.info(" - workspace already in sublime_session recent workspaces list")
                        sys.exit(0)
                    else:
                        session["workspaces"]["recent_workspaces"].append(str(workspace_name))

                else:
                    session["workspaces"]["recent_workspaces"] = [ str(workspace_name) ]
            else:
                session["workspaces"] = { "recent_workspaces" : [ str(workspace_name) ] }

            logger.info(" - append workspace to sublime_session recent workspaces list")

            # print(session)
            print( json.dumps(session["workspaces"]["recent_workspaces"], indent=4) )

            with sublime_session.open("w") as f:
                json.dump(session, f, indent='\t')

    sys.exit(0)

