#! /bin/bash

echo "[entrypoint] Welcome (args=$1)"

if [ "$1" != "" ]; then
    $1
else
    echo "[entrypoint] Connecting to vpn..."
    vpnc bull

    echo "[entrypoint] Test connection to nwadmin..."
    while ! ssh -o ConnectTimeout=1  -o StrictHostKeyChecking=no nwadmin exit; do
        echo "[entrypoint] Connection to nwadmin failed"
        echo " * disconnect vpn"
        vpnc-disconnect
        echo " * sleep..."
        sleep 0.5
        echo " * connect vpn"
        vpnc bull
    done

    echo "[entrypoint] Connection to nwadmin succeeded !! "

    /root/ssh_port_forwarding.sh connect
    # ps -ax
fi

# run this script forever as it is the entrypoint 
echo "sleep infinity..."
sleep infinity

echo "[entrypoint] Goodbye"
