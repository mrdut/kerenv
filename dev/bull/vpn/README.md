# Bull VPN connect

## Installation


    
```bash
 >> make build
 >> DOCKER_CMD="ps -ax" make exec

(vpn-bull) >> ssh-copy-id -i /root/.ssh/id_rsa.pub nwadmin
```

## Explanation

 - runs VPN within docker
 - forward port (tunnel) within docker
 - uses iptables to redirect "true" adresses to docker vpn

  Pros :
   - can use everywhere "true" adresses
   - vpn is not exposed by default




## Iptables

make iptables persistent

```bash
sudo apt-get install iptables-persistent

sudo iptables-save > /etc/iptables/rules


vim /etc/iptables/rules

# bull vpn rules
-A OUTPUT -d 129.183.22.27/32 -o wlp5s0 -p tcp -m tcp --dport 22 -j DNAT --to-destination 172.17.0.2:2222
-A OUTPUT -d 172.16.118.137/32 -o wlp5s0 -p tcp -m tcp --dport 94 -j DNAT --to-destination 172.17.0.2:1234
-A OUTPUT -d 172.16.118.137/32 -o wlp5s0 -p tcp -m tcp --dport 8081 -j DNAT --to-destination 172.17.0.2:1235
-A OUTPUT -d 172.16.118.137/32 -o wlp5s0 -p tcp -m tcp --dport 8080 -j DNAT --to-destination 172.17.0.2:1236


```
