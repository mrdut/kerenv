#! /bin/bash

YODA_URL=172.16.118.137
NWADMIN_URL=129.183.22.27

function yoda_connect() {
    if ps -ax | grep ssh | grep $4 | grep -q $2; then
        echo " -- $1 ssh tunnel already opened"
    else
        echo " -- open $1 ssh tunnel : from $4:$2 -> *:$3..."
        # -f : Requests ssh to go to background just before command execution
        # -N : Do not execute a remote command.  This is useful for just forwarding ports.
        # -T : Disable pseudo-tty allocation.
        # -L : Local port forwarding
        # -4 : Forces ssh to use IPv4 addresses only.
        ssh -4  -o "StrictHostKeyChecking accept-new" -fNT -L *:$3:$4:$2 nwadmin
    fi
}


function yoda_disconnect() {

    pid=`ps -ax | grep ssh | grep $4 | grep $2 | awk '{ print $1 }'`
    if [ "${pid}" -ne "" ]; then
        echo " -- close $1 ssh tunnel..."
        kill -9 ${pid}
    else
        echo " -- no opened $1 ssh tunnel"
    fi
}

case $1 in
connect)
    ssh -4  -o "StrictHostKeyChecking accept-new" -NT \
        -L *:1234:$YODA_URL:94 \
        -L *:1235:$YODA_URL:8081 \
        -L *:1236:$YODA_URL:8080 \
        -L *:2222:$NWADMIN_URL:22 \
        nwadmin
    # yoda_connect Jenkins 94 1234 $YODA_URL
    # yoda_connect Artifactory 8081 1235 $YODA_URL
    # yoda_connect SonarQube 8080 1236 $YODA_URL
    # yoda_connect Nwadmin 22 2222 $NWADMIN_URL
    ;;

disconnect)
    pid=`ps -ax | grep ssh | awk '{ print $1 }'`
    if [ "${pid}" -ne "" ]; then
        echo " -- close ssh tunnel..."
        kill -9 ${pid}
    else
        echo " -- no opened ssh tunnel"
    fi

    # yoda_disconnect Jenkins 94 1234 $YODA_URL
    # yoda_disconnect Artifactory 8081 1235 $YODA_URL
    # yoda_disconnect SonarQube 8080 1236 $YODA_URL
    # yoda_disconnect Nwadmin 22 2222 $NWADMIN_URL
    ;;

iptables)
    yoda_disconnect Jenkins 94 1234 $YODA_URL
    yoda_disconnect Artifactory 8081 1235 $YODA_URL
    yoda_disconnect SonarQube 8080 1236 $YODA_URL
    yoda_disconnect Nwadmin 22 2222 $NWADMIN_URL
    ;;



echo)
    ps -ax | grep ssh | grep $YODA_URL
    ;;
*)
  echo "$0 : Unhandled arg '$1' - expected either 'connect' or 'disconnect'"
  ;;
esac
