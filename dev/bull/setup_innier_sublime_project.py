#! python3

import sys, os
import argparse
import json
import re

def is_windows():
    with open("/proc/version") as f:
        return "Microsoft" in f.read()

def windowify(path):
    return path.replace("/mnt/c", "C:").replace("/", "\\") if is_windows() else path

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("mode", help="which mode to use", choices=["project", "workspace", "session"])
    parser.add_argument("--shortname", help="koios project shortname", default="")
    parser.add_argument("--name", help="koios project name", default="")
    parser.add_argument("--koios-root-dir", help="where all koios project are installed", default=None)
    parser.add_argument("--sublime-dir", help="sublime directory within koios root dir", default="sublime")

    args = parser.parse_args()

    assert args.koios_root_dir is not None
    args.koios_root_dir = os.path.realpath(args.koios_root_dir)

    basename = f"{args.shortname.upper()}-{args.name}"

    if args.mode == "project":
        assert args.shortname != ""
        assert args.name != ""

        project = {
            "folders":
            [
                {
                    "file_exclude_patterns" : [".coverage", "unit-tests-results.xml"],
                    "folder_exclude_patterns" : [".git", "build", "htmlcolv", ".pytest_cache", "__py_cache__"],
                    "path": windowify(os.path.join(args.koios_root_dir, args.name))
                }
            ]
        }
        filename = os.path.join(args.koios_root_dir, args.sublime_dir, basename +".sublime-project")

        if not os.path.exists(filename):
            print(" -- write",filename)
            with open(filename, "w") as f:
                json.dump(project, f)
        else:
            print(" -- skip",filename, "(already exists)")


    elif args.mode == "workspace":
        assert args.name != ""

        workspace = {
                        "find_in_files":
                        {
                            "where_history":
                            [
                                windowify(os.path.join(args.koios_root_dir, args.name))
                            ]
                        }
                    }
        
        filename = os.path.join(args.koios_root_dir, args.sublime_dir, basename +".sublime-workspace")

        if not os.path.exists(filename):
            print(" -- write",filename)
            with open(filename, "w") as f:
                json.dump(workspace, f)
        else:
            print(" -- skip",filename, "(already exists)")

    elif args.mode == "session":

        
        if is_windows():
            sublime_dir = "/mnt/c/Users/a787361/AppData/Roaming/Sublime Text 3/Local/" 
        else:
            sublime_dir = "~/.config/sublime-text-3/Local/"

        sessions = [f for f in os.listdir(sublime_dir) if f.endswith(".sublime_session")]

        if len(sessions)==1:
            session = sessions[0]
        elif len(sessions) > 1:
            index = sessions.index("Session.sublime_session")
            print(index)
            session = sessions[index]

        with open(os.path.join(sublime_dir, session)) as f:
            # print(f.read())
            data = json.loads(f.read())
            print(data)
            print(data["new_window_settings"]["select_project"])

        for d in os.listdir( os.path.join(args.koios_root_dir, args.sublime_dir) ):
            if d.endswith("sublime-project"):
                SHORTNAME, name = re.match(r"([A-Z\-]*)-([a-z\-]*)", d).groups()
                print(d, SHORTNAME.lower(), name)



        # session = {
        # }
        # print("Session.sublime_session")
        # print(session)

    sys.exit(0)



    # if not os.path.exists( os.path.join(root, "sublime", filename)  ):
    #     pass
    # else:
    #     print("file exuists", filename)
    # "settings": {
    #     "new_window_settings": {
    #         "select_project":
    #         {
    #             "height": 500.0,
    #             "last_filter": "db",
    #             "selected_items":
    #             [
    #                 [
    #                     "db",
    #                     "~/workspace/sublime/koios-db-benchmark.sublime-project"
    #                 ],
    #                 [
    #                     "",
    #                     "~/workspace/sublime/wip.sublime-project"
    #                 ],
    #                 [
    #                     "wip",
    #                     "~/workspace/sublime/wip.sublime-project"
    #                 ],
    #                 [
    #                     "comm",
    #                     "~/workspace/sublime/koios-common.sublime-project"
    #                 ],
    #                 [
    #                     "hmm",
    #                     "~/workspace/sublime/koios-HMM-human-model.sublime-project"
    #                 ],
    #                 [
    #                     "hm",
    #                     "~/workspace/sublime/koios-HMM-human-model.sublime-project"
    #                 ],
    #                 [
    #                     "ism",
    #                     "~/workspace/sublime/koios-ism.sublime-project"
    #                 ],
    #                 [
    #                     "ssm",
    #                     "~/workspace/sublime/koios-ssm.sublime-project"
    #                 ],
    #                 [
    #                     "ss",
    #                     "~/workspace/sublime/sshfs.sublime-project"
    #                 ],
    #                 [
    #                     "h",
    #                     "~/workspace/sublime/koios-HMM-human-model.sublime-project"
    #                 ],
    #                 [
    #                     "ker",
    #                     "~/workspace/sublime/kerenv.sublime-project"
    #                 ],
    #                 [
    #                     "keren",
    #                     "~/workspace/sublime/kerenv.sublime-project"
    #                 ],
    #                 [
    #                     "hum",
    #                     "~/workspace/sublime/koios-HMM-human-model.sublime-project"
    #                 ],
    #                 [
    #                     "ts",
    #                     "~/workspace/sublime/koios-tsg.sublime-project"
    #                 ]
    #             ],
    #             "width": 380.0
    #         },    }
