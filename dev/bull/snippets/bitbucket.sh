#! /bin/bash

PROJECT_KEY=BREBD

bitbucket_pull_request_create() {

    [ -z "$ATLASSIAN_ID" ] && echo "[ERROR] No ATLASSIAN_ID env" && return 1
    [ -z "$ATLASSIAN_TOKEN" ] && echo "[ERROR] No ATLASSIAN_TOKEN env" && return 1

    TEXT="\n"
    NEWT_COLORS='root=,blue'

    REPOSITORY_SLUG=$(basename "$(git rev-parse --show-toplevel)")
    if [ "$?" -ne 0 ]; then 
        echo "[ERROR] Not withing a git repo ? - exit"
        return 1
    fi
    echo " -- REPOSITORY_SLUG=$REPOSITORY_SLUG"

    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    echo " -- GIT_BRANCH=$GIT_BRANCH"
    [ "$GIT_BRANCH" = "master" ] && echo "[ERROR] Could not create PR from master branch" && return 1

    GIT_UPSTREAM_BRANCH=$(git rev-parse --abbrev-ref --symbolic-full-name "@{u}")
    if [ "$?" -ne 0 ]; then 
        echo "[WARN] No upstream branch - set to origin/$GIT_BRANCH"
        git push --set-upstream origin "$GIT_BRANCH"
        GIT_UPSTREAM_BRANCH=origin/$GIT_BRANCH
    fi
    echo " -- GIT_UPSTREAM_BRANCH=$GIT_UPSTREAM_BRANCH"

    # https://remarkablemark.org/blog/2017/10/12/check-git-dirty/
    if [[ $(git diff --stat) != '' ]]; then
        echo '[WARN] Your git working tree is dirty'
        NEWT_COLORS='root=,brightred'
        TEXT="$TEXT => $REPOSITORY_SLUG is DIRTY\n"
        git status
        # return 0
    fi

    echo " -- fetching from origin"
    git fetch

    LR=$(git rev-list --left-right --count   origin/master..."$GIT_BRANCH")
    left=$(echo "$LR" | awk '{ print $1 }')
    right=$(echo "$LR" | awk '{ print $2 }')
    echo " -- $left commit(s) are accessible only by origin/master"
    echo " -- $right commit(s) are accessible only by $GIT_BRANCH"
    if [[ $left -ne 0 ]]; then
        echo "[ERROR] Your local branch $GIT_BRANCH has diverged from origin/master -> rebase it !"
        return 1
    else
        TEXT="$TEXT => branch $GIT_BRANCH is rebased on origin/master\n"
        echo " => branch $GIT_BRANCH is rebased on origin/master"
    fi

    LR=$(git rev-list --left-right --count "$GIT_UPSTREAM_BRANCH"..."$GIT_BRANCH")
    left=$(echo "$LR" | awk '{ print $1 }')
    right=$(echo "$LR" | awk '{ print $2 }')
    echo " -- $left commit(s) are accessible only by $GIT_UPSTREAM_BRANCH"
    echo " -- $right commit(s) are accessible only by $GIT_BRANCH"
    if [[ $left -ne 0 ]]; then
        echo "[ERROR] Your local branch $GIT_BRANCH has diverged from $GIT_UPSTREAM_BRANCH -> push it (--force) !"
        return 1
    elif [[ $right -gt 0 ]]; then
        echo "[ERROR] Your local branch '$GIT_BRANCH' is ahead of upstream branch '$GIT_UPSTREAM_BRANCH' by $right commit(s) -> push it !"
        return 1
    else
        TEXT="$TEXT => branch $GIT_BRANCH is up-to-date with $GIT_UPSTREAM_BRANCH\n"
        echo " => branch $GIT_BRANCH is up-to-date with $GIT_UPSTREAM_BRANCH"
    fi

    add_reviewer() { echo "$REVIEWERS" | jq ". + [{\"user\":{\"name\" : \"${1}\"}}]"; }

    REVIEWERS="[]"
    for arg in $(echo "$@" | tr ' ' '\n' | sort | uniq)
    do
        case $arg in
            nico | n )     REVIEWERS=$(add_reviewer "a677483") ;;
            cecile | c )   REVIEWERS=$(add_reviewer "s227695") ;;
            baptiste | b ) REVIEWERS=$(add_reviewer "a760714") ;;
            genc | g )     REVIEWERS=$(add_reviewer "a779360") ;;
            maha | m )     REVIEWERS=$(add_reviewer "a806173") ;;
            torsten | t )  REVIEWERS=$(add_reviewer "a479384") ;;
            arek | a )     REVIEWERS=$(add_reviewer "a653811") ;;
            sandrine | s ) REVIEWERS=$(add_reviewer "a793763") ;;
            be )    REVIEWERS=$(add_reviewer "a677483")
                    REVIEWERS=$(add_reviewer "s227695")
                    REVIEWERS=$(add_reviewer "a760714")
                    REVIEWERS=$(add_reviewer "a779360")
                    REVIEWERS=$(add_reviewer "a806173") ;;

            fe )    REVIEWERS=$(add_reviewer "a653811")
                    REVIEWERS=$(add_reviewer "a479384") ;;

            all )   REVIEWERS=$(add_reviewer "a677483")
                    REVIEWERS=$(add_reviewer "s227695")
                    REVIEWERS=$(add_reviewer "a760714")
                    REVIEWERS=$(add_reviewer "a779360")
                    REVIEWERS=$(add_reviewer "a806173")
                    REVIEWERS=$(add_reviewer "a653811")
                    REVIEWERS=$(add_reviewer "a479384") ;;
            *)
                echo "[ERROR] Unknown reviewer '$arg'" && return 1
                ;;
        esac
    done

    # echo " -- REVIEWERS=$REVIEWERS"
    [ "$REVIEWERS" = "[]" ] && echo "[ERROR] Please add some reviewers !" && return 1

        # \"title\": \"Clean changelog\",
        # \"description\": \"Clean changelog\",
    DATA="{
        \"title\": \"$GIT_BRANCH\",
        \"state\": \"OPEN\",
        \"open\": true,
        \"closed\": false,
        \"toRef\": {
            \"id\": \"refs/heads/master\"
        },
        \"reviewers\": [],
        \"close_source_branch\": true
      }"
    DATA=$(echo "$DATA" | jq ". + {\"reviewers\":$REVIEWERS}")
    DATA=$(echo "$DATA" | jq ". + {\"fromRef\": { \"id\" : \"refs/heads/$GIT_BRANCH\" }}")
    DESCRIPTION=${DESCRIPTION:-$DESC} # alias for faster DESC
    [ -n "$DESCRIPTION" ] && DATA=$(echo "$DATA" | jq ". + {\"description\":\"${DESCRIPTION}\"}")
    echo "$DATA" | jq -r
    REQ="https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests"
    echo "$REQ"

    HEIGHT=15
    TEXT="$TEXT\n{"
    for key in $(echo "$DATA" | jq 'keys[]') ; do
        value=$(echo "$DATA" | jq -rc ".[$key]")
        TEXT="    $TEXT\n $key : $value"
        # echo "$key has value $value"
        HEIGHT=$((HEIGHT+1))
    done
    TEXT="$TEXT\n}"

    if [ -z "$NO_CONFIRM" ]; then
        # https://askubuntu.com/questions/776831/whiptail-change-background-color-dynamically-from-magenta
        NEWT_COLORS=$NEWT_COLORS whiptail --title "$REPOSITORY_SLUG : create Pull Request ?" --defaultno --yesno "$TEXT" $HEIGHT 150 
        [ $? -ne 0 ] && echo "whiptail exit" && return 0
    fi

    REP=$(curl --silent "$REQ" --request POST --header 'Content-Type: application/json' --data "$DATA")
    echo "$REP" | js || echo "$REP"

    bitbucket_pull_request
}


bitbucket_pull_request() {


    [ -z "$ATLASSIAN_ID" ] && echo "[ERROR] No ATLASSIAN_ID env" && return 1
    [ -z "$ATLASSIAN_TOKEN" ] && echo "[ERROR] No ATLASSIAN_TOKEN env" && return 1

    REPOSITORY_SLUG=$(basename "$(git rev-parse --show-toplevel)")

    REQ=https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests
    REP=$(curl --silent "$REQ")

    IFS=$'\n'
    for PR in $(echo "$REP" | jq -c '.values[]'); do
        # echo $PR | tr '\r\n' '$' | jq

        # pullRequestId=$(echo "$PR" | jq -r '.id')
        fromSlug=$(echo "$PR" | jq -r '.fromRef.repository.slug')
        fromRef=$(echo "$PR" | jq -r '.fromRef.displayId')
        toRef=$(echo "$PR" | jq -r '.toRef.displayId')
        title=$(echo "$PR" | jq -r '.title')
        outcome=$(echo "$PR" | jq -r '.properties.mergeResult.outcome')
        [[ "$outcome" == "CLEAN" ]] && OUTCOME_STATUS="\033[34m" || OUTCOME_STATUS="\033[31m"

        echo -e "# Pull-Request : $fromSlug ($title)"
        echo -e "## $fromRef -> $toRef"
        echo -e "## Link : $(echo "$PR" | jq -r '.links.self[0].href')"
        echo -e "## Author : $(echo "$PR" | jq -r '.author.user.displayName') ($(echo "$PR" | jq -r '.author.user.name'))"
        echo -e "## Outcome : ${OUTCOME_STATUS}${outcome}\033[0m"
        echo -e "## Reviewer(s) : "

        APPROVED=false
        for reviewer in $(echo "$PR" | jq -c '.reviewers[]'); do
            name=$(echo "$reviewer" | jq -r '.user.name' )
            displayName=$(echo "$reviewer" | jq -r '.user.displayName' )
            STATUS=$(echo "$reviewer" | jq -r '.status' )
            [[ "$STATUS" == "APPROVED" ]] && { APPROVED=true; COLOR_STATUS="\033[32m"; }  || COLOR_STATUS="\033[33m"
            echo -e " - $displayName ($name) : ${COLOR_STATUS}$STATUS\033[0m"
        done
        echo "APPROVED $APPROVED"
        # break


        # if MERGE && author.name == $ATLASSIAN_ID && Outcome==CLEAN && 1 

        echo " "

        # -----------
        # GET /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/merge
        # -----------
        # {
        #     "canMerge": false,
        #     "conflicted": true,
        #     "outcome": "CONFLICTED",
        #     "vetoes": [
        #         {
        #             "summaryMessage": "You may not merge after 6pm on a Friday.",
        #             "detailedMessage": "It is likely that your Blood Alcohol Content (BAC) exceeds the threshold for making sensible decisions regarding pull requests. Please try again on Monday."
        #         }
        #     ]
        # }
        # -----------
        # REQ=https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests/${pullRequestId}/merge
        # echo $REQ
        # curl --silent $REQ | jq

        # https://docs.atlassian.com/bitbucket-server/rest/6.6.3/bitbucket-rest.html#idp277
        # -----------
        # POST /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}/merge
        # -----------
        # curl -H "Content-Type:application/json" -H "Accept:application/json" --user admin:admin -X POST http://<Your_Stash_URL>//rest/api/1.0/projects/<PROJECT_ID>/repos/<REPO_ID>/pull-requests/<PR_ID>/merge?version=0
        # REQ="https://$ATLASSIAN_ID:$ATLASSIAN_TOKEN@bitbucketbdsfr.fsc.atos-services.net/rest/api/1.0/projects/${PROJECT_KEY}/repos/${REPOSITORY_SLUG}/pull-requests/${pullRequestId}/merge?version=0"
        # echo $REQ
        # REP=$(curl --silent --request POST $REQ -H "X-Atlassian-Token: no-check" )
        # echo $REP | jq

    done
}
