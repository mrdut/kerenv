YODA_URL=172.16.118.137

function yoda_connect() {
    if ps -ax | grep ssh | grep $YODA_URL | grep -q $2; then
        echo " -- ssh tunnel to $1 already opened"
    else
        echo " -- open ssh tunnel to $1..."
        # ssh -fNT -L $3:$YODA_URL:$2 dutrechf@nwadmin.frec.bull.fr

        if sudo iptables -t nat -C OUTPUT -p tcp --dport $2 -d $YODA_URL -j DNAT --to-destination 127.0.0.1:$3; then
            echo " -- iptables to $1 already set"
        else
            echo " -- set iptables to $1..."
            sudo iptables -t nat -A OUTPUT -p tcp --dport $2 -d $YODA_URL -j DNAT --to-destination 127.0.0.1:$3
        fi
    fi
}

function yoda_disconnect() {

    pid=`ps -ax | grep ssh | grep $YODA_URL | grep $2 | awk '{ print $1 }'`
    if [ "${pid}" -ne "" ]; then
        echo " -- close ssh tunnel to $1..."
        kill -9 ${pid}
    else
        echo " -- no ssh tunnel to $1"
    fi
}

# alias jenkins_disconnect='yoda_disconnect Jenkins 94 1234'
# alias artifactory_disconnect='yoda_disconnect Artifactory 8081 1235'
# alias sonarqube_disconnect='yoda_disconnect SonarQube 8080 1236'

# alias jenkins_connect='yoda_connect Jenkins 94 1234'
# alias artifactory_connect='yoda_connect Artifactory 8081 1235'
# alias sonarqube_connect='yoda_connect SonarQube 8080 1236'

# alias vpn-bull-start="usr-vpnc bull && { jenkins_connect; artifactory_connect; sonarqube_connect; notify-send 'VPN bull' 'VPN started' --icon=network-transmit-receive;  } || { notify-send 'VPN bull' 'VPN already started' --icon=dialog-warning; }"
# alias vpn-bull-stop="usr-vpnc-disconnect && { jenkins_disconnect; artifactory_disconnect; sonarqube_disconnect; notify-send 'VPN bull' 'VPN stopped' --icon=network-offline; }"


function vpn-bull-start {
    make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory run
    sudo make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory iptables-append
    sshfs edge:/home/dutrechf/ /home/dutrechf/Workspace/sshfs/edge  -o idmap=user,allow_other
    notify-send 'VPN bull' 'VPN started' --icon=network-transmit-receive;
    # notify-send 'VPN bull' 'VPN already started' --icon=dialog-warning;
}

function vpn-bull-status {
    make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory ps
    sudo make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory iptables-check
    # sudo make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory iptables-show
}

function vpn-bull-stop {
    make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory rm
    sudo make -C /home/dutrechf/Workspace/kerenv/dev/bull/vpn --no-print-directory iptables-delete
    notify-send 'VPN bull' 'VPN stopped' --icon=network-offline;
}
