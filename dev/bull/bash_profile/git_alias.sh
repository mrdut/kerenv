alias gco="git checkout";
alias gci="git commit";
alias gst="git status";
alias gadd="git add";
alias gdiff="git diff";
alias gb="git branch";
alias gba="git branch --all";

alias gpr="${DEV_ROOT}/koios-devrule-checker/git_pull_request.sh"
alias kpr="${DEV_ROOT}/koios-devrule-checker/koios_pull_request.sh"
