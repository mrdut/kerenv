function color_ssh() {
    echo "Using color_ssh"
    FG=$(xtermcontrol --get-fg)
    BG=$(xtermcontrol --get-bg)
    xtermcontrol --bg="#0C2539"
    /usr/bin/ssh "$@"
    xtermcontrol --fg="$FG"
    xtermcontrol --bg="$BG"
}
alias ssh="color_ssh"
