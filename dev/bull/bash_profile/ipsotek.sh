# #! /bin/bash

alias ,ipsotek="cd ~/Workspace/ipsotek"
alias ,ipsotek-mnt="cd ~/Workspace/ipsotek/mnt"
alias ,ipsotek-workspace="cd ~/Workspace/ipsotek/workspace"
alias ,ipsotek-visuite="cd ~/Workspace/ipsotek/visuite"

function vpn-ipsotek-start {
    ~/Workspace/kerenv/dev/bull/bash_profile/vpn_ipsotek_start.expect.sh
    notify-send 'VPN ipsotek' 'VPN started' --icon=network-transmit-receive;
}

function vpn-ipsotek-stop {
    echo "Was :"
    ps -ax | grep vpn
    echo "Kill :"
    sudo killall openvpn3-service-client
    echo "Is :"
    ps -ax | grep vpn
}

# gcloud compute start-iap-tunnel cvlab-fdutrech-windows 3389 --project=cvlab-20200319 --zone=europe-west4-c --local-host-port=localhost:13389

function ipsotek-mount {
    sudo mount --verbose -t cifs  -o credentials=/home/dutrechf/Workspace/ipsotek/smb.txt //192.168.10.5/Development /home/dutrechf/Workspace/ipsotek/mnt/
}

function ipsotek-umount {
    sudo umount /home/dutrechf/Workspace/ipsotek/mnt/
}



# flag=/tmp/kerenv_init.sh

# if [ ! -f $flag ]; then

#     echo "[kerenv] $0"
    
#     # avoid multiple init
#     echo "[kerenv] create $flag flag file"
#     touch $flag

#     echo "[kerenv] set us xeyboard layout"
#     setxkbmap us

#     if timeout 0.2 ping -c1 nwadmin ; then
#         echo "[kerenv] ATOS env detected - set flag"
#     else
#         echo "[kerenv] HOME env detected"
#         echo "[kerenv] configure screen"
#         #>> gtf 1920 1200 60
#         #
#         #  # 1920x1200 @ 60.00 Hz (GTF) hsync: 74.52 kHz; pclk: 193.16 MHz
#         #  Modeline "1920x1200_60.00"  193.16  1920 2048 2256 2592  1200 1201 1204 1242  -HSync +Vsync
#         #
#         xrandr --newmode "1920x1200_60.00"  193.16  1920 2048 2256 2592  1200 1201 1204 1242  -HSync +Vsync
#         xrandr --addmode DP-1 "1920x1200_60.00"
#         xrandr --output DP-1 --mode "1920x1200_60.00" --left-of eDP-1
#     fi

# # else
# #     echo "[kerenv] flag $flag detected - skip init"
# fi
