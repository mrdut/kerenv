/*******************************************************************************
 *
 * Lanceur du client VPN Cisco vpnc en user-space
 * Les options en ligne de commande sont passees directement a vpnc
 *
 * Le binaire resultant de la compilation de ce code doit appartenir a
 * l'utilisateur root et avoir son bit setuid a 1.
 *
 * == Rappels ==
 *
 * Pour compiler:                      gcc -Wall -o usvpnc usvpnc.c
 *
 * Pour changer les droits: (en root)  chown root:root usvpnc
 *                                     chmod u+s usvpnc
 *
 * Il est ensuite possible de placer l'exécutable dans /usr/sbin (pour faire
 * plus propre), puis de l'ajouter dans Système->Préférences->Session (sous
 * Gnome).
 *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>


// src : https://doc.ubuntu-fr.org/vpnc
//
// gcc -Wall -o usvpnc usvpnc.c
// chown root:root usvpnc
// chmod u=rws,g=rx,o=rx usvpnc

// cp usvpnc /usr/local/sbin
// chmod u=rws,g=rx,o=rx /usr/local/sbin/usvpnc

// alias vpn-bull-start="usr-vpnc bull && notify-send 'VPN bull' 'VPN started' --icon=network-transmit-receive || notify-send 'VPN bull' 'VPN already started' --icon=dialog-warning"
// alias vpn-bull-stop="usr-vpnc-disconnect && notify-send 'VPN bull' 'VPN stopped' --icon=network-offline"

int main(int argc, char **argv)
{
	// UID est deja a 0 (=root) avec le bit setuid sur le binaire

	// placement de UID, GID et EGID a 0 aussi
	setuid(0);
	setgid(0);
	setegid(0);

	// positionnement de argv[0] a la valeur "vpnc"
	argv[0] = "vpnc";

	// execution de vpnc
	execvp("vpnc", argv);

	// execute si le exec() s'est mal termine
	fprintf(stderr, "Impossible d'executer vpnc\n");

	return EXIT_FAILURE;
}
