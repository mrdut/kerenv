#! /bin/bash

function help() {
    echo "Setup Bull environment : "
    echo " - clone : clone all git repository"
    echo " - sublime : generate sublime project file for cloned git repositories"
    echo " - shortcut : create symbolic links to oios repo with shorter name in ROOT directory"
    echo " - bashrc : generate bash aliases for cloned git repositories"
    echo " - pyenv : create python venv for python cloned git repositories"
}

if [ $# -eq 0 ]; then
    echo "No arguments supplied"
    help
    exit 1
fi

# 1 - confirm root directory
# ROOT=${1:-$PWD}
ROOT=`realpath $PWD`
ROOT=${ROOT%/} # remove trailing slash

function confirm_root() {
    echo "Rootdir : $ROOT"
    whiptail --title "Bull setup env" --yesno "All files will be created from current directory as rootdir : $ROOT\nContinue ? " 10 110 --defaultno
    return $?
}

BITBUCKET=https://bitbucketbdsfr.fsc.atos-services.net/scm/brebd

# import repo MAP : repo slug -> shortcut
source $(realpath `dirname "$0"`)"/repo.sh"

function clone_git() {

    echo -e "\033[1mClone koios git repositories...\033[0m"

    here=$PWD
    cd $ROOT

    for key in "${!MAP[@]}"; do

        if [ ! -d "$ROOT/$key" ]; then
            echo " -- clone ${BITBUCKET}/${key}.git"
            git clone ${BITBUCKET}/${key}.git
        else
            echo " -- git repo ${key} already cloned - skip"
        fi

    done

    cd $here
}

function generate_bashrc() {

    BASH_DIR="$ROOT/bash_profile"
    BASHRC="$BASH_DIR/koios_alias.sh"

    mkdir -p $BASH_DIR
    echo "# Bull alias" > $BASHRC

    echo -e "\033[1mGenerate $BASHRC...\033[0m"

    for d in `ls $ROOT/`; do

        if [ ${MAP[$d]+_} ]; then
            key=${MAP[$d]}
            echo "alias ,${key}=\"cd \${DEV_ROOT}/${d}\"" >> $BASHRC
            echo " -- Append alias : ,${key} -> $d"
        else
            echo " -- Ignoring $d  (not registered as koios git repo)"
        fi
    done

    echo ""
    echo "Add following command to your shell profile (~/.bashrc, ~/.zshrc,...) :"
    echo "    source $BASHRC"
    echo ""

    return
}

function generate_sublime_project() {

    SUBLIME_DIR="$ROOT/sublime"
    mkdir -p $SUBLIME_DIR

    echo -e "\033[1mGenerate sublime project files...\033[0m"

    for d in `ls $ROOT/`; do

        if [ ${MAP[$d]+_} ]; then

            python3 ./kerenv/dev/bull/setup_sublime_project.py project --shortname=${MAP[$d]} --name=$d --koios-root-dir=$ROOT
            python3 ./kerenv/dev/bull/setup_sublime_project.py workspace --shortname=${MAP[$d]} --name=$d --koios-root-dir=$ROOT

        else
            echo " -- Ignoring $d (not registered as koios git repo)"
        fi
    done

    return

}


function generate_pyenv() {

    echo -e "\033[1mCreate koios git python virtual environment...\033[0m"

    VENV_DIR="$ROOT/python_venv"
    mkdir -p $VENV_DIR

    for d in `ls $ROOT/`; do
        if [ ${MAP[$d]+_} ]; then
            if [ -f "$ROOT/$d/setup.py" ]; then
                shortname=${MAP[$d]}
                VENV="python_venv/$shortname-venv"
                echo  " -- create venv : $d -> $VENV"
                mkdir -p $ROOT/$VENV
                python3 -m venv $ROOT/$VENV
                cd $d
                ln -sv $ROOT/$VENV .venv 
                cd - &> /dev/null
            fi
        fi
    done
}

function generate_shortcut() {

    echo -e "\033[1mCreate koios shortcuts...\033[0m"

    for d in `ls $ROOT/`; do
        if [ ${MAP[$d]+_} ]; then
            link="0___${MAP[$d]}"
            [ ! -f $link ] && ln -sv $d $link
        fi
    done
}

function fetch_projects() {

    user="jenkins-bdsrd"
    pwd="OTkxNjQzMzg3NzI2OiWwO7whXIIIZr2UJkHSNyk3%2FQn9"

    url="https://${user}:${pwd}@bitbucket.sdmc.ao-srv.com/rest/api/1.0/projects/BREBD/repos?limit=500"


    json=`curl $url --silent`

    # echo $json

    # pull_request=`echo $pull_requests_json | jq -r ".values[] | select( .fromRef.displayId == \"$git_branch\" )"`

    slugs=`echo $json | jq -r ".values[] | .slug"`

    for slug in $slugs; do

        if [[ "$slug" == koios* ]]; then

            if [ ! ${MAP[$slug]+_} ]; then

                echo "Missing key : $slug"
            fi


        fi
    done
    # for slug in echo $json | jq -r ".values[] | .slug"; do


    # done

    # if [ "$pull_request" != "" ]; then

    #     state=`echo $pull_request | jq .state | sed 's/"//g'`
    #     link=`echo $pull_request | jq .links.self[0].href | sed 's/"//g'`

    #     if [ "$state" == "APPROVED" ]; then
    #         log_success "Bitbucket pull requests state is $state"
    #     else
    #         log_warn "Bitbucket pull requests state is $state"
    #     fi

    #     log2 "pull request url $link"
    #     # echo $pull_request | jq
    #     return
    # fi


}

for var in "$@"
do
    case $var in
    fetch)
        fetch_projects
        ;;

    sublime)
        confirm_root || exit 0
        generate_sublime_project
      ;;
    bashrc)
        confirm_root || exit 0
        generate_bashrc
      ;;
    shortcut)
        confirm_root || exit 0
        generate_shortcut
        ;;
    pyenv)
        confirm_root || exit 0
        generate_pyenv
        ;;
    clone)
        confirm_root || exit 0
        clone_git
      ;;
    all)
        confirm_root || exit 0
        clone_git
        generate_bashrc
        generate_shortcut
        generate_sublime_project
        generate_pyenv
      ;;
    help)
        help
      ;;
    *)
      echo "Unhandled arg '$var'"
      help
      ;;
    esac
done
