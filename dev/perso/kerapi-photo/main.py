#! /usr/bin/env python3

# pip install --upgrade google-auth google-auth-httplib2 google-api-python-client google-auth-oauthlib



# GET https://photoslibrary.googleapis.com/v1/albums/{albumId}

# 695333203974-o2tqqt9emepi8qcnoaocqig9i7tqqjm6.apps.googleusercontent.com

# # /home/fdutrech/Downloads/client_secret_695333203974-o2tqqt9emepi8qcnoaocqig9i7tqqjm6.apps.googleusercontent.com.json
# {"installed":{"client_id":"695333203974-o2tqqt9emepi8qcnoaocqig9i7tqqjm6.apps.googleusercontent.com","project_id":"kerapi","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"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","redirect_uris":["http://localhost"]}}


# # th consent screen has been configured and your app has a publishing status of "Testing". This means it can be used by a limited set of test users, which you can s

if 1:
    # https://developers.google.com/identity/protocols/oauth2/web-server?hl=fr#python

    import google.oauth2.credentials
    import google_auth_oauthlib.flow
    import flask

    scopes=['https://www.googleapis.com/auth/photoslibrary.readonly']

    # Required, call the from_client_secrets_file method to retrieve the client ID from a
    # client_secret.json file. The client ID (from that file) and access scopes are required. (You can
    # also use the from_client_config method, which passes the client configuration as it originally
    # appeared in a client secrets file but doesn't access the file itself.)
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        'client_secret.json',
        scopes=scopes)

    # Required, indicate where the API server will redirect the user after the user completes
    # the authorization flow. The redirect URI is required. The value must exactly
    # match one of the authorized redirect URIs for the OAuth 2.0 client, which you
    # configured in the API Console. If this value doesn't match an authorized URI,
    # you will get a 'redirect_uri_mismatch' error.
    flow.redirect_uri = 'https://www.example.com/oauth2callback'

    # Generate URL for request to Google's OAuth 2.0 server.
    # Use kwargs to set optional request parameters.
    authorization_url, state = flow.authorization_url(
        # Recommended, enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Optional, enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true',
        # Recommended, state value can increase your assurance that an incoming connection is the result
        # of an authentication request.
        # state=sample_passthrough_value,
        # Optional, if your application knows which user is trying to authenticate, it can use this
        # parameter to provide a hint to the Google Authentication Server.
        # login_hint='hint@example.com',
        # Optional, set prompt to 'consent' will prompt the user for consent
        prompt='consent')

    print(authorization_url)
    flask.redirect(authorization_url)

elif 1:


    # # https://developers.google.com/photos/library/guides/overview?hl=fr

    #
    # https://max-coding.medium.com/loading-photos-and-metadata-using-google-photos-api-with-python-7fb5bd8886ef
    #

    import os
    import json


    from google.oauth2.credentials import Credentials
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request

    scopes=['https://www.googleapis.com/auth/photoslibrary.readonly']


    secret_filename = "/home/fdutrech/Workspace/kerenv/dev/perso/kerapi-photo/client_secret_695333203974-o2tqqt9emepi8qcnoaocqig9i7tqqjm6.apps.googleusercontent.com.json"

    # from gphotospy import authorize
    # service = authorize.init(secret_filename)


    # if os.path.exists(secret_filename):

    # scopes = [
    #     'https://www.googleapis.com/auth/photoslibrary',
    #     'https://www.googleapis.com/auth/photoslibrary.sharing'
    # ]

    # .../auth/photoslibrary  Consulter, importer et organiser des éléments dans votre bibliothèque Google Photos ￼
    # Photos Library API  .../auth/photoslibrary.readonly

    creds = Credentials.from_authorized_user_file(secret_filename, scopes)
    print(creds)

    # with open(secret_filename, "r", encoding="utf-8") as json_file:
    #     data = json.load(json_file)
    #     info = data["installed"]

    # print(type(info))
    # print(info)

    # info["refresh_token"] = False

    # creds = Credentials.from_authorized_user_info(info, scopes)

    # print(creds)
    # print(creds.valid)

    # flow = InstalledAppFlow.from_client_secrets_file(secret_filename, scopes)
    # creds = flow.run_local_server()


    # if not creds or not creds.valid:
    #     if creds and creds.expired and creds.refresh_token:
    #         creds.refresh(Request())
    #     else:
    #         flow = InstalledAppFlow.from_client_secrets_file(
    #             '_secrets_/client_secret.json', scopes)
    #         creds = flow.run_local_server()
    #     print(creds)
    #     # Save the credentials for the next run
    #     with open(secret_filename, 'w') as token:
    #         token.write(creds.to_json())

else:

    import os

    #
    # CHAT GPT
    #
    from google.oauth2.credentials import Credentials
    from google_auth_oauthlib.flow import InstalledAppFlow
    from google.auth.transport.requests import Request
    from googleapiclient.discovery import build

    SCOPES = ['https://www.googleapis.com/auth/photoslibrary.readonly']

    def authenticate():
        creds = None

        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first time.
        if os.path.exists('token.json'):
            creds = Credentials.from_authorized_user_file('token.json')

        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'client_secret.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(creds.to_json())

        return creds

    def list_photos():
        creds = authenticate()
        service = build('photoslibrary', 'v1', credentials=creds)

        results = service.mediaItems().list(pageSize=10).execute()
        items = results.get('mediaItems', [])

        if not items:
            print('No photos found.')
        else:
            print('Photos:')
            for item in items:
                print(f"{item['filename']} - {item['mediaMetadata']['creationTime']}")

    if __name__ == '__main__':
        list_photos()