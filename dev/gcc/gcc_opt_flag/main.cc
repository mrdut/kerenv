
#include <iostream>
#include <string>
#include <cstdint>
// #include "cpu_info.h"

// Otherwise use gcc-format assembler to implement the underlying instructions.
#define GETCPUID(a, b, c, d, a_inp, c_inp) \
  asm("mov %%rbx, %%rdi\n"                 \
      "cpuid\n"                            \
      "xchg %%rdi, %%rbx\n"                \
      : "=a"(a), "=D"(b), "=c"(c), "=d"(d) \
      : "a"(a_inp), "2"(c_inp))

int GetXCR0EAX() {
  int eax, edx;
  asm("XGETBV" : "=a"(eax), "=d"(edx) : "c"(0));
  return eax;
}

// Mostly ISA related features that we care about
enum CPUFeature {
  // Do not change numeric assignments.
  MMX = 0,
  SSE = 1,
  SSE2 = 2,
  SSE3 = 3,
  SSSE3 = 4,
  SSE4_1 = 5,
  SSE4_2 = 6,
  CMOV = 7,
  CMPXCHG8B = 8,
  CMPXCHG16B = 9,
  POPCNT = 10,
  AES = 11,
  AVX = 12,
  RDRAND = 13,
  AVX2 = 14,
  FMA = 15,
  F16C = 16,
  PCLMULQDQ = 17,
  RDSEED = 18,
  ADX = 19,
  SMAP = 20,

  // Prefetch Vector Data Into Caches with Intent to Write and T1 Hint
  // http://www.felixcloutier.com/x86/PREFETCHWT1.html.
  // You probably want PREFETCHW instead.
  PREFETCHWT1 = 21,

  BMI1 = 22,
  BMI2 = 23,
  HYPERVISOR = 25,  // 0 when on a real CPU, 1 on (well-behaved) hypervisor.

  // Prefetch Data into Caches in Anticipation of a Write (3D Now!).
  // http://www.felixcloutier.com/x86/PREFETCHW.html
  PREFETCHW = 26,

  // AVX-512: 512-bit vectors (plus masking, etc.) in Knights Landing,
  // Skylake
  // Xeon, etc.; each of these entries is a different subset of
  // instructions,
  // various combinations of which occur on various CPU types.
  AVX512F = 27,        // Foundation
  AVX512CD = 28,       // Conflict detection
  AVX512ER = 29,       // Exponential and reciprocal
  AVX512PF = 30,       // Prefetching
  AVX512VL = 31,       // Shorter vector lengths
  AVX512BW = 32,       // Byte and word
  AVX512DQ = 33,       // Dword and qword
  AVX512VBMI = 34,     // Bit manipulation
  AVX512IFMA = 35,     // Integer multiply-add
  AVX512_4VNNIW = 36,  // Integer neural network
  AVX512_4FMAPS = 37,  // Floating point neural network
};

// Structure for basic CPUID info
class CPUIDInfo {
 public:
  CPUIDInfo()
      : have_adx_(0),
        have_aes_(0),
        have_avx_(0),
        have_avx2_(0),
        have_avx512f_(0),
        have_avx512cd_(0),
        have_avx512er_(0),
        have_avx512pf_(0),
        have_avx512vl_(0),
        have_avx512bw_(0),
        have_avx512dq_(0),
        have_avx512vbmi_(0),
        have_avx512ifma_(0),
        have_avx512_4vnniw_(0),
        have_avx512_4fmaps_(0),
        have_bmi1_(0),
        have_bmi2_(0),
        have_cmov_(0),
        have_cmpxchg16b_(0),
        have_cmpxchg8b_(0),
        have_f16c_(0),
        have_fma_(0),
        have_mmx_(0),
        have_pclmulqdq_(0),
        have_popcnt_(0),
        have_prefetchw_(0),
        have_prefetchwt1_(0),
        have_rdrand_(0),
        have_rdseed_(0),
        have_smap_(0),
        have_sse_(0),
        have_sse2_(0),
        have_sse3_(0),
        have_sse4_1_(0),
        have_sse4_2_(0),
        have_ssse3_(0),
        have_hypervisor_(0) {}

  void Initialize() {

    std::cout << "CPUIDInfo::Initialize()" << std::endl;
    // Initialize cpuid struct
    // CHECK(cpuid == nullptr) << __func__ << " ran more than once";
    CPUIDInfo* cpuid = this;

    uint32_t eax, ebx, ecx, edx;

    // Get vendor string (issue CPUID with eax = 0)
    GETCPUID(eax, ebx, ecx, edx, 0, 0);
    cpuid->vendor_str_.append(reinterpret_cast<char *>(&ebx), 4);
    cpuid->vendor_str_.append(reinterpret_cast<char *>(&edx), 4);
    cpuid->vendor_str_.append(reinterpret_cast<char *>(&ecx), 4);

    // To get general information and extended features we send eax = 1 and
    // ecx = 0 to cpuid.  The response is returned in eax, ebx, ecx and edx.
    // (See Intel 64 and IA-32 Architectures Software Developer's Manual
    // Volume 2A: Instruction Set Reference, A-M CPUID).
    GETCPUID(eax, ebx, ecx, edx, 1, 0);

    cpuid->model_num_ = static_cast<int>((eax >> 4) & 0xf);
    cpuid->family_ = static_cast<int>((eax >> 8) & 0xf);

    cpuid->have_aes_ = (ecx >> 25) & 0x1;
    cpuid->have_cmov_ = (edx >> 15) & 0x1;
    cpuid->have_cmpxchg16b_ = (ecx >> 13) & 0x1;
    cpuid->have_cmpxchg8b_ = (edx >> 8) & 0x1;
    cpuid->have_mmx_ = (edx >> 23) & 0x1;
    cpuid->have_pclmulqdq_ = (ecx >> 1) & 0x1;
    cpuid->have_popcnt_ = (ecx >> 23) & 0x1;
    cpuid->have_rdrand_ = (ecx >> 30) & 0x1;
    cpuid->have_sse2_ = (edx >> 26) & 0x1;
    cpuid->have_sse3_ = ecx & 0x1;
    cpuid->have_sse4_1_ = (ecx >> 19) & 0x1;
    cpuid->have_sse4_2_ = (ecx >> 20) & 0x1;
    cpuid->have_sse_ = (edx >> 25) & 0x1;
    cpuid->have_ssse3_ = (ecx >> 9) & 0x1;
    cpuid->have_hypervisor_ = (ecx >> 31) & 1;

    const uint64_t xcr0_xmm_mask = 0x2;
    const uint64_t xcr0_ymm_mask = 0x4;
    const uint64_t xcr0_maskreg_mask = 0x20;
    const uint64_t xcr0_zmm0_15_mask = 0x40;
    const uint64_t xcr0_zmm16_31_mask = 0x80;

    const uint64_t xcr0_avx_mask = xcr0_xmm_mask | xcr0_ymm_mask;
    const uint64_t xcr0_avx512_mask = xcr0_avx_mask | xcr0_maskreg_mask |
                                    xcr0_zmm0_15_mask | xcr0_zmm16_31_mask;

    const bool have_avx =
        // Does the OS support XGETBV instruction use by applications?
        ((ecx >> 27) & 0x1) &&
        // Does the OS save/restore XMM and YMM state?
        ((GetXCR0EAX() & xcr0_avx_mask) == xcr0_avx_mask) &&
        // Is AVX supported in hardware?
        ((ecx >> 28) & 0x1);

    const bool have_avx512 =
        // Does the OS support XGETBV instruction use by applications?
        ((ecx >> 27) & 0x1) &&
        // Does the OS save/restore ZMM state?
        ((GetXCR0EAX() & xcr0_avx512_mask) == xcr0_avx512_mask);

    cpuid->have_avx_ = have_avx;
    cpuid->have_fma_ = have_avx && ((ecx >> 12) & 0x1);
    cpuid->have_f16c_ = have_avx && ((ecx >> 29) & 0x1);

    // Get standard level 7 structured extension features (issue CPUID with
    // eax = 7 and ecx= 0), which is required to check for AVX2 support as
    // well as other Haswell (and beyond) features.  (See Intel 64 and IA-32
    // Architectures Software Developer's Manual Volume 2A: Instruction Set
    // Reference, A-M CPUID).
    GETCPUID(eax, ebx, ecx, edx, 7, 0);

    cpuid->have_adx_ = (ebx >> 19) & 0x1;
    cpuid->have_avx2_ = have_avx && ((ebx >> 5) & 0x1);
    cpuid->have_bmi1_ = (ebx >> 3) & 0x1;
    cpuid->have_bmi2_ = (ebx >> 8) & 0x1;
    cpuid->have_prefetchwt1_ = ecx & 0x1;
    cpuid->have_rdseed_ = (ebx >> 18) & 0x1;
    cpuid->have_smap_ = (ebx >> 20) & 0x1;

    cpuid->have_avx512f_ = have_avx512 && ((ebx >> 16) & 0x1);
    cpuid->have_avx512cd_ = have_avx512 && ((ebx >> 28) & 0x1);
    cpuid->have_avx512er_ = have_avx512 && ((ebx >> 27) & 0x1);
    cpuid->have_avx512pf_ = have_avx512 && ((ebx >> 26) & 0x1);
    cpuid->have_avx512vl_ = have_avx512 && ((ebx >> 31) & 0x1);
    cpuid->have_avx512bw_ = have_avx512 && ((ebx >> 30) & 0x1);
    cpuid->have_avx512dq_ = have_avx512 && ((ebx >> 17) & 0x1);
    cpuid->have_avx512vbmi_ = have_avx512 && ((ecx >> 1) & 0x1);
    cpuid->have_avx512ifma_ = have_avx512 && ((ebx >> 21) & 0x1);
    cpuid->have_avx512_4vnniw_ = have_avx512 && ((edx >> 2) & 0x1);
    cpuid->have_avx512_4fmaps_ = have_avx512 && ((edx >> 3) & 0x1);
  }


std::string CPUVendorIDString() {
  return this->vendor_str();
}

    static CPUIDInfo* singleton()
    {
        static CPUIDInfo* s_cpuid = nullptr;
        if(s_cpuid==nullptr)
        {
            std::cout << "create singleton" << std::endl;
            s_cpuid = new CPUIDInfo();
            s_cpuid->Initialize();
        }
        return s_cpuid;
    }

  bool TestFeature(CPUFeature feature) {
    // InitCPUIDInfo();
    // clang-format off

    CPUIDInfo* cpuid = this;

    switch (feature) {
      case ADX:           return cpuid->have_adx_;
      case AES:           return cpuid->have_aes_;
      case AVX2:          return cpuid->have_avx2_;
      case AVX:           return cpuid->have_avx_;
      case AVX512F:       return cpuid->have_avx512f_;
      case AVX512CD:      return cpuid->have_avx512cd_;
      case AVX512PF:      return cpuid->have_avx512pf_;
      case AVX512ER:      return cpuid->have_avx512er_;
      case AVX512VL:      return cpuid->have_avx512vl_;
      case AVX512BW:      return cpuid->have_avx512bw_;
      case AVX512DQ:      return cpuid->have_avx512dq_;
      case AVX512VBMI:    return cpuid->have_avx512vbmi_;
      case AVX512IFMA:    return cpuid->have_avx512ifma_;
      case AVX512_4VNNIW: return cpuid->have_avx512_4vnniw_;
      case AVX512_4FMAPS: return cpuid->have_avx512_4fmaps_;
      case BMI1:          return cpuid->have_bmi1_;
      case BMI2:          return cpuid->have_bmi2_;
      case CMOV:          return cpuid->have_cmov_;
      case CMPXCHG16B:    return cpuid->have_cmpxchg16b_;
      case CMPXCHG8B:     return cpuid->have_cmpxchg8b_;
      case F16C:          return cpuid->have_f16c_;
      case FMA:           return cpuid->have_fma_;
      case MMX:           return cpuid->have_mmx_;
      case PCLMULQDQ:     return cpuid->have_pclmulqdq_;
      case POPCNT:        return cpuid->have_popcnt_;
      case PREFETCHW:     return cpuid->have_prefetchw_;
      case PREFETCHWT1:   return cpuid->have_prefetchwt1_;
      case RDRAND:        return cpuid->have_rdrand_;
      case RDSEED:        return cpuid->have_rdseed_;
      case SMAP:          return cpuid->have_smap_;
      case SSE2:          return cpuid->have_sse2_;
      case SSE3:          return cpuid->have_sse3_;
      case SSE4_1:        return cpuid->have_sse4_1_;
      case SSE4_2:        return cpuid->have_sse4_2_;
      case SSE:           return cpuid->have_sse_;
      case SSSE3:         return cpuid->have_ssse3_;
      case HYPERVISOR:    return cpuid->have_hypervisor_;
      default:
        break;
    }
    // clang-format on
    return false;
  }

  std::string vendor_str() const { return vendor_str_; }
  int family() const { return family_; }
  int model_num() { return model_num_; }

 private:
  int have_adx_ : 1;
  int have_aes_ : 1;
  int have_avx_ : 1;
  int have_avx2_ : 1;
  int have_avx512f_ : 1;
  int have_avx512cd_ : 1;
  int have_avx512er_ : 1;
  int have_avx512pf_ : 1;
  int have_avx512vl_ : 1;
  int have_avx512bw_ : 1;
  int have_avx512dq_ : 1;
  int have_avx512vbmi_ : 1;
  int have_avx512ifma_ : 1;
  int have_avx512_4vnniw_ : 1;
  int have_avx512_4fmaps_ : 1;
  int have_bmi1_ : 1;
  int have_bmi2_ : 1;
  int have_cmov_ : 1;
  int have_cmpxchg16b_ : 1;
  int have_cmpxchg8b_ : 1;
  int have_f16c_ : 1;
  int have_fma_ : 1;
  int have_mmx_ : 1;
  int have_pclmulqdq_ : 1;
  int have_popcnt_ : 1;
  int have_prefetchw_ : 1;
  int have_prefetchwt1_ : 1;
  int have_rdrand_ : 1;
  int have_rdseed_ : 1;
  int have_smap_ : 1;
  int have_sse_ : 1;
  int have_sse2_ : 1;
  int have_sse3_ : 1;
  int have_sse4_1_ : 1;
  int have_sse4_2_ : 1;
  int have_ssse3_ : 1;
  int have_hypervisor_ : 1;
  std::string vendor_str_;
  int family_;
  int model_num_;
};

// bool TestCPUFeature(CPUFeature feature) {
//   return CPUIDInfo::TestFeature(feature);
// }

// // If the CPU feature isn't present, log a fatal error.
// void CheckFeatureOrDie(CPUFeature feature, const std::string& feature_name) {
//   if (!TestCPUFeature(feature)) {
//     std::cerr
//         << "The TensorFlow library was compiled to use " << feature_name
//         << " instructions, but these aren't available on your machine.";
//   }
// }

// // Check if CPU feature is included in the TensorFlow binary.
// void CheckIfFeatureUnused(CPUFeature feature, const std::string& feature_name,
//                           std::string& missing_instructions) {
//   // if (TestCPUFeature(feature))
//   // {
//   //   std::cout << feature_name << " : "
//   // }
//   //  {
//   // if (TestCPUFeature(feature)) {
//   //   missing_instructions.append(" ");
//   //   missing_instructions.append(feature_name);
//   // }
//   // else
// }

// Raises an error if the binary has been compiled for a CPU feature (like AVX)
// that isn't available on the current machine. It also warns of performance
// loss if there's a feature available that's not being used.
// Depending on the compiler and initialization order, a SIGILL exception may
// occur before this code is reached, but this at least offers a chance to give
// // a more meaningful error message.
// class CPUFeatureGuard {
//  public:
//   CPUFeatureGuard() {
// #ifdef __SSE__
//     CheckFeatureOrDie(CPUFeature::SSE, "SSE");
// #endif  // __SSE__
// #ifdef __SSE2__
//     CheckFeatureOrDie(CPUFeature::SSE2, "SSE2");
// #endif  // __SSE2__
// #ifdef __SSE3__
//     CheckFeatureOrDie(CPUFeature::SSE3, "SSE3");
// #endif  // __SSE3__
// #ifdef __SSE4_1__
//     CheckFeatureOrDie(CPUFeature::SSE4_1, "SSE4.1");
// #endif  // __SSE4_1__
// #ifdef __SSE4_2__
//     CheckFeatureOrDie(CPUFeature::SSE4_2, "SSE4.2");
// #endif  // __SSE4_2__
// #ifdef __AVX__
//     CheckFeatureOrDie(CPUFeature::AVX, "AVX");
// #endif  // __AVX__
// #ifdef __AVX2__
//     CheckFeatureOrDie(CPUFeature::AVX2, "AVX2");
// #endif  // __AVX2__
// #ifdef __AVX512F__
//     CheckFeatureOrDie(CPUFeature::AVX512F, "AVX512F");
// #endif  // __AVX512F__
// #ifdef __FMA__
//     CheckFeatureOrDie(CPUFeature::FMA, "FMA");
// #endif  // __FMA__
//   }
// };

// void InfoAboutUnusedCPUFeatures() {

//     std::cout << "InfoAboutUnusedCPUFeatures()\n";

//     std::string missing_instructions;
// #if defined(_MSC_VER) && !defined(__clang__)

// #ifndef __AVX__
//     CheckIfFeatureUnused(CPUFeature::AVX, "AVX", missing_instructions);
// #endif  // __AVX__
// #ifndef __AVX2__
//     CheckIfFeatureUnused(CPUFeature::AVX2, "AVX2", missing_instructions);
// #endif  // __AVX2__

// #else  // if defined(_MSC_VER) && !defined(__clang__)

// #ifndef __SSE__
//     CheckIfFeatureUnused(CPUFeature::SSE, "SSE", missing_instructions);
// #endif  // __SSE__
// #ifndef __SSE2__
//     CheckIfFeatureUnused(CPUFeature::SSE2, "SSE2", missing_instructions);
// #endif  // __SSE2__
// // #ifndef __SSE3__
//     CheckIfFeatureUnused(CPUFeature::SSE3, "SSE3", missing_instructions);
// // #endif  // __SSE3__
// #ifndef __SSE4_1__
//     CheckIfFeatureUnused(CPUFeature::SSE4_1, "SSE4.1", missing_instructions);
// #endif  // __SSE4_1__
// #ifndef __SSE4_2__
//     CheckIfFeatureUnused(CPUFeature::SSE4_2, "SSE4.2", missing_instructions);
// #endif  // __SSE4_2__
// #ifndef __AVX__
//     CheckIfFeatureUnused(CPUFeature::AVX, "AVX", missing_instructions);
// #endif  // __AVX__
// #ifndef __AVX2__
//     CheckIfFeatureUnused(CPUFeature::AVX2, "AVX2", missing_instructions);
// #endif  // __AVX2__
// #ifndef __AVX512F__
//     CheckIfFeatureUnused(CPUFeature::AVX512F, "AVX512F", missing_instructions);
// #endif  // __AVX512F__
// #ifndef __FMA__
//     CheckIfFeatureUnused(CPUFeature::FMA, "FMA", missing_instructions);
// #endif  // __FMA__
// #endif  // else of if defined(_MSC_VER) && !defined(__clang__)
//     // if (!missing_instructions.empty()) {
// #ifndef INTEL_MKL
//       std::cout << "Your CPU supports instructions that this TensorFlow "
//                 << "binary was not compiled to use:" << missing_instructions << "\n";
// #else
//       std::cout << "This TensorFlow binary is optimized with Intel(R) MKL-DNN "
//                 << "to use the following CPU instructions in performance "
//                 << "critical operations: " << missing_instructions << std::endl
//                 << "To enable them in non-MKL-DNN operations, rebuild "
//                 << "TensorFlow with the appropriate compiler flags.\n";
// #endif


//     // }
// }


int main(int argc, char**argv)
{
    // using namespace tensorflow::port;


    // std::cout << " - NumSchedulableCPUs() : " << NumSchedulableCPUs() << std::endl;
    // std::cout << " - MaxParallelism() : " << MaxParallelism() << std::endl;
    // std::cout << " - MaxParallelism() : " << MaxParallelism(int numa_node) << std::endl;
    // std::cout << " - NumTotalCPUs() : " << NumTotalCPUs() << std::endl;
    // std::cout << " - GetCurrentCPU() : " << GetCurrentCPU() << std::endl;
    // std::cout << " - NumHyperthreadsPerCore() : " << NumHyperthreadsPerCore() << std::endl;

// Mostly ISA related features that we care about
// enum CPUFeature {
//   // Do not change numeric assignments.
//   MMX = 0,
//   SSE = 1,
//   SSE2 = 2,
//   SSE3 = 3,
//   SSSE3 = 4,
//   SSE4_1 = 5,
//   SSE4_2 = 6,
//   CMOV = 7,
//   CMPXCHG8B = 8,
//   CMPXCHG16B = 9,
//   POPCNT = 10,
//   AES = 11,
//   AVX = 12,
//   RDRAND = 13,
//   AVX2 = 14,
//   FMA = 15,
//   F16C = 16,
//   PCLMULQDQ = 17,
//   RDSEED = 18,
//   ADX = 19,
//   SMAP = 20,

//   // Prefetch Vector Data Into Caches with Intent to Write and T1 Hint
//   // http://www.felixcloutier.com/x86/PREFETCHWT1.html.
//   // You probably want PREFETCHW instead.
//   PREFETCHWT1 = 21,

//   BMI1 = 22,
//   BMI2 = 23,
//   HYPERVISOR = 25,  // 0 when on a real CPU, 1 on (well-behaved) hypervisor.

//   // Prefetch Data into Caches in Anticipation of a Write (3D Now!).
//   // http://www.felixcloutier.com/x86/PREFETCHW.html
//   PREFETCHW = 26,

//   // AVX-512: 512-bit vectors (plus masking, etc.) in Knights Landing,
//   // Skylake
//   // Xeon, etc.; each of these entries is a different subset of
//   // instructions,
//   // various combinations of which occur on various CPU types.
//   AVX512F = 27,        // Foundation
//   AVX512CD = 28,       // Conflict detection
//   AVX512ER = 29,       // Exponential and reciprocal
//   AVX512PF = 30,       // Prefetching
//   AVX512VL = 31,       // Shorter vector lengths
//   AVX512BW = 32,       // Byte and word
//   AVX512DQ = 33,       // Dword and qword
//   AVX512VBMI = 34,     // Bit manipulation
//   AVX512IFMA = 35,     // Integer multiply-add
//   AVX512_4VNNIW = 36,  // Integer neural network
//   AVX512_4FMAPS = 37,  // Floating point neural network
// };

// // Checks whether the current processor supports one of the features above.
// // Checks CPU registers to return hardware capabilities.
// bool TestCPUFeature(CPUFeature feature);

//   AVX512_4FMAPS = 37,  // Floating point neural network
// };

// Checks whether the current processor supports one of the features above.
// Checks CPU registers to return hardware capabilities.

    CPUIDInfo* cpuid = new CPUIDInfo();
    cpuid->Initialize();

    std::cout << " - __SSE__ ";
    if(cpuid->TestFeature(CPUFeature::SSE)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __SSE__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __SSE2__ ";
    if(cpuid->TestFeature(CPUFeature::SSE2)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __SSE2__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __SSE3__ ";
    if(cpuid->TestFeature(CPUFeature::SSE3)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __SSE3__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __SSE4_1__ ";
    if(cpuid->TestFeature(CPUFeature::SSE4_1)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __SSE4_1__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __SSE4_2__ ";
    if(cpuid->TestFeature(CPUFeature::SSE4_2)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __SSE4_2__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __AVX__ ";
    if(cpuid->TestFeature(CPUFeature::AVX)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __AVX__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __AVX2__ ";
    if(cpuid->TestFeature(CPUFeature::AVX2)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __AVX2__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __AVX512F_ ";
    if(cpuid->TestFeature(CPUFeature::AVX512F)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __AVX512F__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif

    std::cout << " - __FMA__ ";
    if(cpuid->TestFeature(CPUFeature::FMA)) { std::cout << "supported "; } else { std::cout << "unsupported "; }
    #ifdef __FMA__
        std::cout << "and used\n";
    #else
        std::cout << "and unused\n";
    #endif



    // std::cout << cpuid->TestFeature(SSE2) << std::endl;
    std::cout << "VendorIDString : '" << cpuid->CPUVendorIDString() << "'" << std::endl;

    delete cpuid;
    // InfoAboutUnusedCPUFeatures();

    // std::cout << " - CPUVendorIDString() : " << CPUVendorIDString() << std::endl;
    // std::cout << " - CPUFamily() : " << CPUFamily() << std::endl;
    // std::cout << " - CPUModelNum() : " << CPUModelNum() << std::endl;
    // // std::cout << " - NominalCPUFrequency() : " << NominalCPUFrequency() << std::endl;
    // std::cout << " - CPUIDNumSMT() : " << CPUIDNumSMT() << std::endl;
    return 0;
}