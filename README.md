# Kerenv

### setup

```
sudo dnf install mkdoc zsh vim tree
```




### What is this repository for?

* Fast dev setup :
    - documentation
    - sublime plugins
    - terminator plugins
    - zsh completion


### Docs
* use http://www.mkdocs.org/
* install : `sudo dnf install mkdocs`
* live : `mkdocs serve &` [link](http://127.0.0.1:8000/)
* static : `mkdocs build --clean` [link](file:///work-crypt/fdutrech/workspace/kerenv/site/index.html)
* theme : https://github.com/noraj/mkdocs-windmill-dark
* [multilevel documentation](http://www.mkdocs.org/user-guide/writing-your-docs/#multilevel-documentation)


### Service

```bash
# create service
sudo sh -c "make service > /etc/systemd/system/kerenv.service"

# run service
sudo systemctl daemon-reload
sudo systemctl start kerenv.service
sudo systemctl stop kerenv.service

# start service at bootup
sudo systemctl enable kerenv.service

# check logs
systemctl status kerenv.service
journalctl -u kerenv.service
```