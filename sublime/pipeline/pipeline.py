import sublime
import sublime_plugin
import os, re, sys
import subprocess
import traceback
import base64
import json, yaml

# https://www.sublimetext.com/docs/api_reference.html
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

class PipelineToAsciiCommand(sublime_plugin.TextCommand):

    def __init__(self, arg):
        super().__init__(arg)
        self.region = None


    def generate_html(self, filename):


        # self.view.run_command("show_panel",  {"panel": "console", "toggle": True})
        # self.view.run_command("show_panel",  {'panel': 'output.Test'})
        # print('\n' * newline_count)

        # /home/fdutrech/Workspace/kerenv/sublime/jsonify/jsonify.py:213:                view.run_command("pretty_pipeline", { "actions" : [item[0] for item in items[1:-1]] })
        #     { "keys": ["ctrl+`"], "command": "show_panel", "args": {"panel": "console", "toggle": true} },

        # get current context (line, cursor position)
        region = self.view.sel()[0]
        # region = sublime.Region(-1, -1)
        # point = region.begin()
        # _, col = self.view.rowcol(point)
        # line = self.view.substr(self.view.line(region))

        # self.view.erase_phantoms("pipelineviewer")

        # if self.region != region:

        # graph = self.generate_ascii(filename)
        graph = self.generate_png(filename)
        io = self.generate_io(filename)

        html = """
                <div style='padding: 0.4em;
                        margin-top: 0.2em;
                        border: 1px solid;
                        float: right;
                        white-space: pre-line;
                        border-radius: 10px;
                        background-color: dark-gray'><pre>
                """

        if graph:
            html += graph

        if graph and io:
            html += '<br><br>'

        if io:
            html += io

        if not graph and not io:
            html += "No HTML generated"
            with open(filename) as f:
                print(filename, "content:")
                print(f.readlines())

        html += "</pre></div>"

        return html

                # self.view.add_phantom("pipelineviewer", region, html, sublime.LAYOUT_BLOCK)
        #         self.region = region
        # else:
        #     self.region = None

    def run(self, edit, args = None):
        try:

            filename = self.view.file_name()
            if filename is None:
                sublime.error_message("PipelineToAsciiCommand()\nCould not run command - maybe file is unsaved ?")
                return

            filename = os.path.realpath(filename)

            with open(filename) as f:
                try:
                    data = json.load(f)
                except Exception as e:
                    f.seek(0)
                    data = yaml.load(f)

            if "pipelines" in data:

                html = ""
                for pipeline, value in data["pipelines"].items():
                    html += "<h1>" + pipeline + "</h1>"
                    filename = "/tmp/sublime_pipeline.json"
                    with open(filename, "w") as f:
                        json.dump(value, f, indent=2)

                    with open(filename) as f:
                        print(json.load(f))


                    html += self.generate_html(filename)

            else:
                html = self.generate_html(filename)

            self.view.show_popup(html, max_width=2000, max_height=1000)


        except Exception as e:
            traceback.print_exc()



    def generate_ascii(self, filename):
        cwd = os.path.join(os.environ.get("ATEME_WORKSPACE", "/home/fdutrech/Workspace/dev"), "TitanProcessing", "utils")
        cmd = ["python3", "pipelineviewer.py", filename, "--ascii"]
        p = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()

        print("PipelineToAsciiCommand() - ascii cmd:", " ".join(cmd))
        text = stdout.decode().strip("\n")

        if text:
            print(text)
        else:
            print("PipelineToAsciiCommand() - no stdout", )
        if stderr:
            print("stderr: ")
            print(stderr)

        return text.replace(' ', '&nbsp;').replace('\n', '<br>')

    def generate_png(self, filename):

        cwd = os.path.join(os.environ.get("ATEME_WORKSPACE", "/home/fdutrech/Workspace/dev"), "TitanProcessing", "utils")
        cmd = ["python3", "pipelineviewer.py", filename, "--png"]
        p = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()

        print("PipelineToAsciiCommand() - png cmd:", " ".join(cmd))

        if stdout:
            print("stdout: ")
            print(stdout)

        if stderr:
            print("stderr: ")
            print(stderr)

        filepath = os.path.join(cwd, "graph.png")

        if not os.path.exists(filepath):
            print("No png generated :(", filepath)
            return None



        binary_fc       = open(filepath, 'rb').read()  # fc aka file_content
        base64_utf8_str = base64.b64encode(binary_fc).decode('utf-8')
        ext     = filepath.split('.')[-1]
        dataurl = 'data:image/{};base64,{}'.format(ext, base64_utf8_str)

        os.remove(filepath)

        return '<img src="%s">'%dataurl


    def generate_io(self, filename):

        cwd = os.path.join(os.environ.get("ATEME_WORKSPACE", "/home/fdutrech/Workspace/dev"), "TitanProcessing", "utils")
        cmd = ["python3", "pipeline_tk.py", filename, "--io"]
        p = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()

        print("PipelineToAsciiCommand() - io cmd:", " ".join(cmd))
        text = stdout.decode().strip("\n")
        text = re.sub('\\u001b\[\d+m', "", text) # remove bash colors
        if text:
            print(text)
        else:
            print("PipelineToAsciiCommand() - no stdout", )
        if stderr:
            print("stderr:")
            print(stderr)

        text = text.split("\n")
        text = "<br>".join(text[1:]) # we dont want filename
        text = text.replace(' ', '&nbsp;')

        return text
