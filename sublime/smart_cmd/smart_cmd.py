import sublime
import sublime_plugin
import os, re, sys
import subprocess
import traceback

# https://www.sublimetext.com/docs/api_reference.html
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

class IncludeFdutrechCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        fdutrech = '#include "/home/fdutrech/Workspace/kerateme/jira_root/.jira/fdutrech.hpp"\n'
        view = self.view

        #
        # Ensure we are editing *.h, *.hpp, *c, *.cpp file
        #
        if view.file_name() and not view.file_name().endswith((".h", ".hpp", ".c", ".cpp")):
            sublime.error_message("Current file is not C/C++ file - skip insert fdutrech.hpp")
            return

        #
        # Find all #includes lines
        #
        include_regions = self.view.find_all("^#include .*$")
        include_lines = [self.view.substr(self.view.full_line(region)) for region in include_regions]

        if include_regions:
            for i, (region, line) in enumerate(zip(include_regions, include_lines)):
                if line == fdutrech:
                    sublime.error_message("Header fdutrech.hpp is already included (line %d) !"%(view.rowcol(region.a)[0]+1))
                    return
            pos = include_regions[-1].b + 1

        else:
            # insert at begin of file
            pos = 0

        # Get the whole document as a region.
        # region = sublime.Region(0, self.view.size())
        # for i, line_region in enumerate(self.view.lines(region)):
        #     line = self.view.substr(line_region)

        current_pos = view.sel()[0].b
        view.insert(edit, pos, fdutrech)
        view.sel().clear()
        view.sel().add(sublime.Region(current_pos + len(fdutrech)))
        sublime.message_dialog("Header fdutrech.hpp included (line %d) !"%(view.rowcol(pos)[0]+1))

class ClearFdutrechCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        # https://www.sublimetext.com/docs/api_reference.html#sublime.View.erase

        regions = []
        for pattern in ["^.*#include .*fdutrech.hpp\".*$\n", "^.*FD_TRACE.*$\n", "^.*FD_PRINT.*$\n"]:
            regions += self.view.find_all(pattern)

        if not regions:
            sublime.message_dialog("Document is clean !")
            return

        regions = sorted(regions)

        msg = "Do you want to remove:\n" + "\n".join(["Line %d : %s"%(self.view.rowcol(region.a)[0], repr(self.view.substr(region))) for region in regions])

        print(msg)
        if sublime.ok_cancel_dialog(msg, ok_title='Remove', title='Remove fdutrech.hpp...'):
            for region in reversed(regions):
                self.view.erase(edit, region)

class SmartCmdCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        items = []
        items += [["Include fdutrech.hpp", "Insert after last #include line"]]
        items += [["Clear fdutrech.hpp", "Remove #include, FD_PRINT, FD_TRACE"]]

        def on_select(index):
            if index==-1:
                return
            elif index==0:
                # sublime.set_timeout_async(lambda: self.view.run_command("sub"), 100)
                self.view.run_command("include_fdutrech")
            elif index==1:
                self.view.run_command("clear_fdutrech")
            else:
                sublime.error_message("Unhandled quick panel item #%d"%index)

        self.view.window().show_quick_panel(items, on_select=on_select)


