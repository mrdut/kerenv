import sublime
import sublime_plugin
import os, re
import json
import yaml
import time
import traceback

# https://www.youtube.com/watch?v=KN-EJ5JQ_fk&list=PLGfKZJVuHW90We0J0LjtEPJIaC4lFNeW3&ab_channel=OdatNurd-SublimeTextTutorials
# https://www.sublimetext.com/docs/api_reference.html#sublime.View
# https://bitbucket.org/mrdut/hexaparser-master/src/master/HexaParser.py
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

def pythonify(content):

    try:
        return json.loads(content), "json"
    except Exception as e:
        print("json.loads() Exception:", e)

    try:
        print("YAML")
        # return yaml.load(content, loader=yaml.CLoader)
        return yaml.safe_load(content), "yaml"
    except Exception as e:
        print("yaml.safe_load() Exception:", e)

    try:
        return eval(content), "pyhon"
    except Exception as e:
        print("python eval() Exception:", e)

    raise Exception("Unknown file type")


def jsonify(data):
    return json.dumps(data, indent=2, sort_keys=True)

def yamelify(data):
    return yaml.dump(data, Dumper=PrettyDumper, indent=2, sort_keys=True)

def dump(data, data_type):

    if data_type=="json":
        return jsonify(data)

    elif data_type=="yaml":
        return yamelify(data)

    raise Exception("Unknown data_type " + str(data_type))

class PrettyPipelineCommand(sublime_plugin.TextCommand):

    ACTION_REMOVE_ENDPOINTS = [["Remove endpoints", "Remove 'interface' keys"]]
    ACTION_REMOVE_INTERFACES = [["Remove interfaces", "Remove 'interface' keys"]]
    ACTION_CLEAN_MODULE_NAME = [["Clean modules name", "Make modules name pretty"]]
    ACTION_SHORT_LINK = [["Short links", "Convert to short link format"]]
    ACTION_SPLIT_PIPELINES = [["Split Pipelines", "Split multiple pipelines file"]]

    def run(self, edit, actions=[]):

        try:
            print("PrettyPipelineCommand", actions)
            view = self.view
            content = view.substr(sublime.Region(0, view.size()))
            data, data_type = pythonify(content)

            for action in actions:

            #     if action in "Remove interfaces":
            #         sublime.message_dialog("TODO : Remove interfaces")

                if action == PrettyPipelineCommand.ACTION_REMOVE_ENDPOINTS[0][0]:
                    data = self.remove_endpoints(data)

                if action == PrettyPipelineCommand.ACTION_CLEAN_MODULE_NAME[0][0]:
                    data = self.clean_modules_name(data)

                if action == PrettyPipelineCommand.ACTION_SHORT_LINK[0][0]:
                    data = self.short_links(data)

                if action == PrettyPipelineCommand.ACTION_SPLIT_PIPELINES[0][0]:
                    print("Split pipelines !!")

                    active_view = sublime.active_window().active_view()

                    if not "pipelines" in data:
                        sublime.status_message("No 'pipelines' on current document")
                        return

                    if active_view.file_name() is None:
                        sublime.error_message("Save file on disk before split")
                        return

                    name, ext = os.path.splitext(active_view.file_name())
                    dirname = os.path.dirname(name)

                    print(dirname, ext)

                    for pipeline, value in data.get("pipelines", {}).items():

                        filename = os.path.join(dirname, pipeline + ext)
                        with open(filename, "w") as f:
                            f.write(yamelify(value))
                            view.window().open_file(filename)
                            print("Write new file:", filename)

                    return


            print("Set new content")
            view.erase(edit, sublime.Region(0, view.size()))
            view.insert(edit, 0, dump(data, data_type))

        except Exception as e:
            print("run() Exception:")
            print(traceback.format_exc())


    @staticmethod
    def remove_endpoints(data):
        print("Remove Endpoints !!")
        if "pipelines" in data:
            return { "pipelines" : { pipeline : PrettyPipelineCommand.remove_endpoints(value) for pipeline, value in data["pipelines"].items() } }

        if "endpoints" in data:
            del data["endpoints"]
        return data

    @staticmethod
    def short_links(data):
        print("Short links !!")
        if "pipelines" in data:
            return { "pipelines" : { pipeline : PrettyPipelineCommand.short_links(value) for pipeline, value in data["pipelines"].items() } }

        if "links" in data:
            links = data["links"]
            # shorten links
            for i, item in enumerate(links):
                # print(i, item, type(item))
                if type(item) is dict:

                    def parse_item(item):
                        if type(item) is str:
                            return item
                        else:
                            link = item["module"]
                            if "connector" in item:
                                link += "/" + str(item["connector"])
                            return link
                    link = parse_item(item["source"]) + "," + parse_item(item["destination"])
                    print("Convert %s to %s"%(str(item), link))
                    links[i] = link

            # clean connectors
            for i, link in enumerate(links):
                links[i] = ",".join([l.replace("/1", "") for l in link.split(",")])

            # merge links
            i = 0
            while i < len(links):
                j = i + 1
                while j < len(links):
                    src = links[i].split(",")[-1]
                    dst = links[j].split(",")[0]
                    if src == dst:
                        links[i] += "," + ",".join(links[j].split(",")[1:])
                        del links[j]

                    else:
                        dst = links[i].split(",")[0]
                        src = links[j].split(",")[-1]
                        if src == dst:
                            links[i] = links[j] + "," + ",".join(links[i].split(",")[1:])
                            del links[j]
                        else:
                            j+=1
                i+=1

            data["links"] = links
        return data

    @staticmethod
    def clean_modules_name(data):
        print("clean_modules_name()")

        if "pipelines" in data:
            return { "pipelines" : { pipeline : PrettyPipelineCommand.clean_modules_name(value) for pipeline, value in data["pipelines"].items() } }

        lut = {}

        for module in list(data["modules"].keys()):

            if "name" in data["modules"][module]:
                print("Delete modules['%s']['name']='%s'"%(module, data["modules"][module]["name"]))
                del data["modules"][module]["name"]

            if len(data["modules"][module])==1:
                name_fmt = next(iter(data["modules"][module])) # take first key, ie unit name
                count=1
                name = name_fmt + "_%d"%count
                while name in data["modules"]:
                    count+=1
                    name = name_fmt + "_%d"%count

                print(module, "->", name)
                if name != module:
                    data["modules"][name] = data["modules"][module]
                    del data["modules"][module]

                lut[module] = name
            else:
                print("ERROR : module['%s'] has multiple keys %s"%(module, str(data["modules"][module].keys())))
                return data

        for i, link in enumerate(data["links"]):
            if type(link) is dict:
                for item in ["source", "destination"]:
                    if item in link:
                        link[item]["module"] = lut[link[item]["module"]]
            else:
                path = link.split(",")
                for j, p in enumerate(path):
                    module = p.split("/")[0]
                    path[j] = p.replace(module, lut[module.strip()])
                data["links"][i] = ", ".join(path)

        return data

    @staticmethod
    def show_quick_panel(view):

        items = []
        items += [["Do nothing", "Do not edit"]]
        items += PrettyPipelineCommand.ACTION_REMOVE_ENDPOINTS
        items += PrettyPipelineCommand.ACTION_REMOVE_INTERFACES
        items += PrettyPipelineCommand.ACTION_CLEAN_MODULE_NAME
        items += PrettyPipelineCommand.ACTION_SHORT_LINK
        items += PrettyPipelineCommand.ACTION_SPLIT_PIPELINES
        items += [["All", "Apply all actions"]]

        def on_select(index):

            print("on_select index=%d, item=%s"%(index, items[index][0]))

            if index==-1 or index==0:
                print("Do nothing (index=%d)"%index)

            elif index == len(items)-1:
                print("All (index=%d)"%index)
                view.run_command("pretty_pipeline", { "actions" : [item[0] for item in items[1:-1]] })

            else:
                view.run_command("pretty_pipeline", { "actions" : [ items[index][0] ] })

        def on_highlight(index):
            pass

        view.window().show_quick_panel(items, on_select=on_select, on_highlight=on_highlight, selected_index=len(items)-2)


def _convert(view, edit, SYNTAX, dump_func):

    try:

        content = view.substr(sublime.Region(0, view.size()))
        data, _ = pythonify(content)

        if not data:
            print("Empty data - content : ", repr(content))
            return

        filename = view.file_name()
        if not filename:
            filename = view.name()

        if filename:
            extension=os.path.splitext(filename)[1]

        if filename is None or filename and extension=="."+SYNTAX.lower():
            #
            # edit current file
            #
            view.erase(edit, sublime.Region(0, view.size()))
            view.assign_syntax("Packages/%s/%s.sublime-syntax"%(SYNTAX, SYNTAX))
        else:
            #
            # create new file
            #
            view = view.window().new_file(syntax="Packages/%s/%s.sublime-syntax"%(SYNTAX, SYNTAX), flags=sublime.TRANSIENT)
            if filename:
                view.set_name(os.path.basename(filename.replace(extension, "."+SYNTAX.lower())))


        view.erase(edit, sublime.Region(0, view.size()))
        view.insert(edit, 0, dump_func(data))

        if "links" in data or "pipelines" in data:
            print("show quick panel")
            PrettyPipelineCommand.show_quick_panel(view)

    except Exception as e:
        print("run() Exception:")
        print(traceback.format_exc())



class JsonifyCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        print("JsonifyCommand !!")
        _convert(self.view, edit, "JSON", jsonify)
        print("GoodBye")


import yaml.emitter
import yaml.serializer
import yaml.representer
import yaml.resolver


class IndentingEmitter(yaml.emitter.Emitter):
    def increase_indent(self, flow=False, indentless=False):
        """Ensure that lists items are always indented."""
        return super().increase_indent(
            flow=False,
            indentless=False,
        )


class PrettyDumper(
    IndentingEmitter,
    yaml.serializer.Serializer,
    yaml.representer.Representer,
    yaml.resolver.Resolver,
):
    def __init__(
        self,
        stream,
        default_style=None,
        default_flow_style=False,
        canonical=None,
        indent=None,
        width=None,
        allow_unicode=None,
        line_break=None,
        encoding=None,
        explicit_start=None,
        explicit_end=None,
        version=None,
        tags=None,
        sort_keys=True,
    ):
        IndentingEmitter.__init__(
            self,
            stream,
            canonical=canonical,
            indent=indent,
            width=width,
            allow_unicode=allow_unicode,
            line_break=line_break,
        )
        yaml.serializer.Serializer.__init__(
            self,
            encoding=encoding,
            explicit_start=explicit_start,
            explicit_end=explicit_end,
            version=version,
            tags=tags,
        )
        yaml.representer.Representer.__init__(
            self,
            default_style=default_style,
            default_flow_style=default_flow_style,
            sort_keys=sort_keys,
        )
        yaml.resolver.Resolver.__init__(self)
        
class YamlifyCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        print("YamlifyCommand !!")
        _convert(self.view, edit, "YAML", yamelify)
        print("GoodBye")

