import sublime
import sublime_plugin
import os, re
import json
import yaml
import traceback

# https://www.sublimetext.com/docs/api_reference.html#sublime.View
# https://o.org/mrdut/hexaparser-master/src/master/HexaParser.py
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

def pythonify(content):

	try:
		return json.loads(content)
	except Exception as e:
		print("json.loads() Exception:", e)

	try:
		# return yaml.load(content, loader=yaml.CLoader)
		return yaml.safe_load(content)
	except Exception as e:
		print("yaml.load() Exception:", e)

	try:
		return eval(content)
	except Exception as e:
		print("python eval() Exception:", e)

	raise Exception("Unknown file type")


def _convert(view, edit, SYNTAX, dump_func):

	try:

		content = view.substr(sublime.Region(0, view.size()))
		data = pythonify(content)

		filename = view.file_name()
		if not filename:
			filename = view.name()

		if filename:
			extension=os.path.splitext(filename)[1]

		if filename is None or extension=="."+SYNTAX.lower():
			#
			# edit current file
			#
			view.erase(edit, sublime.Region(0, view.size()))
			view.assign_syntax("Packages/%s/%s.sublime-syntax"%(SYNTAX, SYNTAX))
		else:
			#
			# create new file
			#
			view = view.window().new_file(syntax="Packages/%s/%s.sublime-syntax"%(SYNTAX, SYNTAX), flags=sublime.TRANSIENT)
			if filename:
				view.set_name(os.path.basename(filename.replace(extension, "."+SYNTAX.lower())))

		view.insert(edit, 0, dump_func(data))

	except Exception as e:
		print("run() Exception:")
		print(traceback.format_exc())

def jsonify(data):
	return json.dumps(data, indent=2, sort_keys=True)

class JsonifyCommand(sublime_plugin.TextCommand):
	def run(self, edit):


def yamelify(data):
	return yaml.dump(data, Dumper=PrettyDumper, indent=2, sort_keys=True)

import yaml.emitter
import yaml.serializer
import yaml.representer
import yaml.resolver


class IndentingEmitter(yaml.emitter.Emitter):
    def increase_indent(self, flow=False, indentless=False):
        """Ensure that lists items are always indented."""
        return super().increase_indent(
            flow=False,
            indentless=False,
        )


class PrettyDumper(
    IndentingEmitter,
    yaml.serializer.Serializer,
    yaml.representer.Representer,
    yaml.resolver.Resolver,
):
    def __init__(
        self,
        stream,
        default_style=None,
        default_flow_style=False,
        canonical=None,
        indent=None,
        width=None,
        allow_unicode=None,
        line_break=None,
        encoding=None,
        explicit_start=None,
        explicit_end=None,
        version=None,
        tags=None,
        sort_keys=True,
    ):
        IndentingEmitter.__init__(
            self,
            stream,
            canonical=canonical,
            indent=indent,
            width=width,
            allow_unicode=allow_unicode,
            line_break=line_break,
        )
        yaml.serializer.Serializer.__init__(
            self,
            encoding=encoding,
            explicit_start=explicit_start,
            explicit_end=explicit_end,
            version=version,
            tags=tags,
        )
        yaml.representer.Representer.__init__(
            self,
            default_style=default_style,
            default_flow_style=default_flow_style,
            sort_keys=sort_keys,
        )
        yaml.resolver.Resolver.__init__(self)
class YamlifyCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		print("YamlifyCommand !!")
		_convert(self.view, edit, "YAML", yamelify)
		print("GoodBye")
