# Sublime plugins


## Install

 - Reload plugin : restart sublime text or better, create plugin as directory

Plugin file:

 - Install plugin : `cd ~/.config/sublime-text/Packages/User && ln -s KERENV_PATH/sublime/PLUGIN.py .`

Plugin directory:

 - Install plugin directory : `cd ~/.config/sublime-text/Packages && ln -s KERENV_PATH/sublime/PLUGIN_DIR .`


## Links

 - https://www.sublimetext.com/docs/api_reference.html
 - https://github.com/SublimeText/sublime_lib
 - https://github.com/bradrobertson/sublime-packages/tree/master/Default

 - https://www.youtube.com/watch?v=KN-EJ5JQ_fk&list=PLGfKZJVuHW90We0J0LjtEPJIaC4lFNeW3&ab_channel=OdatNurd-SublimeTextTutorials	