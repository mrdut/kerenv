import os,sys, re

"""
TODO :
    - Prevent editing if git_diff_right != WORKDIR

TROBLESHOOTING:
 sudo apt install fonts-noto


LINKS
    - https://unicode-table.com/en/sets/arrow-symbols/
"""

import json
import time
import traceback
import subprocess
import html

# https://www.geeksforgeeks.org/broken-pipe-error-in-python/
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE,SIG_DFL)

from difflib import Differ, SequenceMatcher

from pynput import keyboard
from pyfiglet import figlet_format
import logging
import tty
import termios
from colorama import Fore, Back, Style
from functools import cmp_to_key
import inspect

ANSI_REGEX = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

TERMINAL_SIZE = os.get_terminal_size()

def ansi_string(blabla):
    """
    remove non-ansi characters from string
    """
    return ANSI_REGEX.sub("", blabla)

class ColorFormatter(logging.Formatter):

    COLOR_FMT = {
        logging.DEBUG: Fore.CYAN,
        logging.INFO: Fore.BLUE,
        logging.WARNING: Fore.YELLOW,
        logging.ERROR: Fore.RED,
        logging.CRITICAL: Fore.RED
    }

    INDENT = ""

    def format(self, record):
        # https://docs.python.org/3/library/logging.html#logrecord-attributes
        return logging.Formatter(self.COLOR_FMT[record.levelno] + "[%(asctime)s][%(levelname)s]" + Fore.RESET + self.INDENT + " %(message)s" + Fore.LIGHTYELLOW_EX + "\t[%(pathname)s:%(lineno)s]" + Fore.RESET).format(record)

    @staticmethod
    def indent():
        ColorFormatter.INDENT += " "*4

    @staticmethod
    def deindent():
        ColorFormatter.INDENT = ColorFormatter.INDENT[:-4]


LOGFILE = "/tmp/sublime_git_diff.log"

class ColorFormatterIndenter:
    def __enter__(self):
        ColorFormatter.indent()

    def __exit__(self, exc_type, exc_val, exc_tb):
        ColorFormatter.deindent()

def logit(func):
    """
    Decorator to print function call details.

    This includes parameters names and effective values.
    """
    # def wrapper(*args, **kwargs):
    #     func_args = inspect.signature(func).bind(*args, **kwargs).arguments
    #     func_args_str = ", ".join(map("{0[0]} = {0[1]!r}".format, func_args.items()))
    #     logging.info(f"***********{func.__module__}.{func.__qualname__} ( {func_args_str} )")
    #     return func(*args, **kwargs)
    # return wrapper

    def wrapper(*func_args, **func_kwargs):

        # print(func_args)
        # print(type(func_args))
        # print(len(func_args))

        argcount = func.__code__.co_argcount
        argnames = func.__code__.co_varnames
        argdefaults = func.__defaults__ or ()
        args = func_args[:len(argnames)]
        args = args + argdefaults[len(argdefaults) - (argcount - len(args)):]

        # arg_names = func.__code__.co_varnames[:argcount]
        # print(arg_names)
        # args = func_args[:len(arg_names)]
        # defaults = func.__defaults__ or ()
        # print(defaults)

        # args = args + defaults[len(defaults) - (argcount - len(args)):]
        # params = list(zip(arg_names, args))
        # args = func_args[len(arg_names):]
        # if args: params.append(('args', args))
        # if func_kwargs: params.append(('kwargs', func_kwargs))

        log = func.__name__
        log += "("
        log += ", ".join([f"{argnames[i]}={args[i]}" for i in range(argcount) if argnames[i]!='self'])
        if func_kwargs:
            log += str(func_kwargs)
        log += ")"
        logging.info(log)

        with ColorFormatterIndenter():
            return func(*func_args, **func_kwargs)

    return wrapper



STDOUT=0
STDERR=1
RETURNCODE=2
def bash_proc(cmd, *args, **kwargs):
    # Popen.stdin
    # Popen.stdout
    # Popen.stderr
    # Popen.pid
    # Popen.returncode
    # logging.log(loglevel, 'cmd="%s", args=%s, kwargs=%s', cmd, args, kwargs)

    logging.info(f"bash_proc: {cmd=}")

    bash_proc.print(cmd)

    return subprocess.Popen(cmd, shell=isinstance(cmd, str) , stdout=subprocess.PIPE, stderr=subprocess.PIPE, *args, **kwargs)

bash_proc.print = lambda x: print(x)

def bash_exec(proc):
    stdout, stderr = proc.communicate()
    stdout = stdout.decode().strip().strip("\n")
    stderr = stderr.decode().strip().strip("\n")
    # if proc.returncode != 0:
    #     logging.log(loglevel, "returncode=%d", proc.returncode)
    #     logging.log(loglevel, "stderr=%s", stderr)
    return stdout, stderr, proc.returncode

def bash_cmd(cmd, *args, **kwargs):
    proc = bash_proc(cmd, *args, **kwargs)
    return bash_exec(proc)

def git_sha1(name):
    if name == "WORKDIR":
        return ""
    sha1, _, has_name = bash_cmd("git rev-parse " + name)
    return sha1 if has_name==0 else None


class GitDiff:

    EXTRA_PARAMS=[]

    def __init__(self, left="origin/master", right="", cwd=None):

        logging.info(f"GitDiff({left=}, {right=}, {cwd=})")


        """
        Execute and parse git diff command
        """
        # proc = bash_cmd(f"git diff {left} {right} -U0 --diff-algorithm=minimal --ignore-space-change", cwd=cwd)
        proc = bash_cmd(f"git diff {left} {right} -U0 --diff-algorithm=minimal " + " ".join(GitDiff.EXTRA_PARAMS) , cwd=cwd)

        # diff --git a/utils/pipelineviewer.py b/utils/pipelineviewer.py
        # index ef50ed946..ef04c1b7c 100755
        # --- a/utils/pipeline_tk.py
        # +++ b/utils/pipeline_tk.py
        # @@ -348 +348 @@ def parse_io(filenames):
        # -                print(f"   └── {ioput.replace('puts','').upper():3}", end='')
        # +                print(f"   └── {ioput.replace('puts','').upper()} ", end='')

        self.DIFF = {} # (a,b) = { (start_line, start_count, end_line, end_count) : [[minus], [plus]] }

        for line in proc[STDOUT].split("\n"):

            logging.debug("[GitDiff] " + Fore.MAGENTA + line + Fore.RESET)

            if not line:
                continue

            if line.startswith("diff --git"):
                A, B, LINES, index = None, None, None, None
                logging.debug("[GitDiff] " + "\033[33mHEADER\033[0m" + line)

            elif A is None and line.startswith("---"):
                header = False
                A = line.replace("--- a/", "").replace("---", "")
                logging.debug("[GitDiff] " + f"\033[33m{A=}\033[0m")

            elif B is None and line.startswith("+++"):
                B = line.replace("+++ b/", "").replace("+++ ", "")
                logging.debug("[GitDiff] " + f"\033[33m{B=}\033[0m")
                self.DIFF[(A,B)] = {}

            elif line.startswith("@@"):
                # @@ -1,8 +1,9 @@
                # @@ -18,6 +19,8 @@ int cmd_http_fetch(int argc, const char **argv, ...
                # @@ -<start line>,<number of lines> +<start line>,<number of lines> @@ [header].

                LINES = tuple(int(i or 1) for i in re.match('@@ -(\d+),?(\d*) \+(\d+),?(\d*) @@.*', line).groups())
                logging.debug("[GitDiff] " + f"\033[33m{LINES=}\033[0m")

                self.DIFF[(A,B)][LINES] = [[], []]
                continue

            if not LINES:
                continue

            if line.startswith("-"):
                index = 0
                self.DIFF[(A,B)][LINES][index].append(line[1:])

            elif line.startswith("+"):
                index = 1
                self.DIFF[(A,B)][LINES][index].append(line[1:])

            elif line == "\ No newline at end of file":
                assert index is not None
                self.DIFF[(A,B)][LINES][index][-1] += "<NO_NEWLINE>"

            else:
                print("\033[31mUNKOWN LINE\033[0m", line)

    def files(self):
        return sorted(list(self.DIFF.keys()))

    def lines(self, files):
        return sorted(list(self.DIFF[files].keys()))

    def diff(self, files, lines):
        return self.DIFF[files][lines]

    @staticmethod
    def format_title(FILES, LINES, DIFF):

        ret = ""
        a,b = FILES
        start_line, start_count, end_line, end_count = LINES
        minus, plus = DIFF

        if a == "/dev/null":
            return Fore.BLUE + a + Fore.RESET + " -> " + Fore.GREEN + "New file" + Fore.RESET

        elif b == "/dev/null":
            return Fore.BLUE + a + Fore.RESET + " -> " + Fore.RED + "File deleted" + Fore.RESET

        elif a == b:
            ret = Fore.BLUE + b + Fore.RESET

        else:
            ret = Fore.RED + a + Fore.RESET + " -> " + Fore.GREEN + b + Fore.RESET

        ret += ":" + str(end_line) + Fore.RESET

        if start_count == 0:
            assert not minus
            assert len(plus) == end_count
            color, n, state = Fore.GREEN, len(plus), "added"

        elif end_count == 0:
            assert not plus
            assert len(minus) == start_count
            color, n, state = Fore.RED, len(minus), "deleted"

        else:
            assert len(plus) == end_count
            assert len(minus) == start_count
            color, n, state = Fore.LIGHTYELLOW_EX, max(len(minus), len(plus)), "modified"

        ret += " " + color + str(n) + " line%s "%('s' if n > 1 else '') + state + Fore.RESET + Back.RESET

        # ret += " " + str((start_line, start_count, end_line, end_count))

        return ret

    @staticmethod
    def format_diff(FILES, LINES, DIFF):
        """
        """

        ret = ""
        a,b = FILES
        start_line, start_count, end_line, end_count = LINES
        minus, plus = DIFF

        if start_count == end_count:

            """
            """

            assert len(minus) == len(plus)

            # https://docs.python.org/3/library/difflib.html#difflib.Differ

            stream = ["", ""]
            for m, p in zip(minus, plus):

                # if m.endswith("<NO_NEWLINE>"):
                #     m = m.replace("<NO_NEWLINE>", "")
                #     p += "<NEWLINE>"

                # if p.endswith("<NO_NEWLINE>"):
                #     p = p.replace("<NO_NEWLINE>", "")
                #     m += "<NEWLINE>"

                # ratio = SequenceMatcher(None, m, p).ratio()

                differ = list(Differ().compare(m, p))
                previous = [["", "", ""], ["", "", ""]] # [MINUS : (Fore, Back, Style), PLUS: (Fore, Back, Style)]
                for d in differ:
                    if d[:2] == "- ":
                        current = [Fore.RED, Back.RESET, Style.BRIGHT] if d[2] != ' ' else [Fore.RESET, Back.RED, Style.BRIGHT]
                        current = [current, None]
                    elif d[:2] == "+ ":
                        current = [Fore.GREEN, Back.RESET, Style.BRIGHT] if d[2] != ' ' else [Fore.RESET, Back.GREEN, Style.BRIGHT]
                        current = [None, current]
                    elif d[:2] == "  ":
                        # dark red / dark green
                        current = [["\033[38;5;52m", Back.RESET, Style.NORMAL], ["\033[38;5;22m", Back.RESET, Style.NORMAL]]
                    elif d[:2] == "? ":
                        assert False
                    else:
                        assert False, "Unhandled difference: %s"%repr(d)

                    for j in range(2):
                        curr, prev =  current[j], previous[j]

                        if curr is None:
                            continue

                        if d[2] == "\r":
                            curr[i] = Fore.YELLOW
                            d = (None, None, "\\r")

                        for i in range(3):
                            if curr[i] == prev[i]:
                                curr[i] = ""
                            else:
                                prev[i] = curr[i]
                        stream[j] += "".join(curr) + d[2]

                stream[0] += "\033[0m\n"
                stream[1] += "\033[0m\n"

            ret += stream[0]
            ret += stream[1]
        else:

            def _fmt_diff(lines, fore, back):

                ret = ""

                maxlines = TERMINAL_SIZE.lines - 4
                if len(lines) > maxlines:
                    lines = lines[:maxlines//2] + ["", back + Fore.WHITE + "... %d more line(s) ... "%(len(lines) - maxlines) + Back.RESET, ""] + lines[-maxlines//2:]

                # NB : we print line by line so that each line keeps color while using | less
                for l in lines:
                    if l == "":
                        l = Fore.YELLOW + "\\n"
                    elif l == "\t\n":
                        l = Fore.YELLOW + "\\t"

                    # print(repr(l))

                    if l.endswith("\r\n"):
                        l += Fore.YELLOW + "\\r"




                    # if l.endswith(0xD)

                    # else:
                        # l = l.replace(" ", Fore.YELLOW + "." + fore)
                    # l = repr(l)

                    # if l == "":
                    #     l = "<NEWLINE>"
                    # elif l == "\t\n":
                    #     l = "<TAB>"


                    ret += fore + l + "\033[0m\n"
                return ret

            # d = _fmt_diff(minus, Fore.RED, Back.RED)
            ret += _fmt_diff(minus, Fore.RED, Back.RED)
            ret += _fmt_diff(plus, Fore.GREEN, Back.GREEN)

        return ret.strip()



# https://code-maven.com/catch-control-c-in-python
# signal.signal(signal.SIGINT, handler)

class AlternateScreen:

    ENABLED = True
    # ENABLED = False

    def __enter__(self):
        self.old_attrs = termios.tcgetattr(sys.stdin)

        if AlternateScreen.ENABLED:
            tty.setcbreak(sys.stdin)
            print("\033[?1049h")
        self.x_split_hint = None
        self.clear()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):

        logging.info(f"{exc_type=} {exc_val=}")
        if exc_type:
            print(f"exc_val={exc_val}")

            logging.info(f"{exc_val=}")
            logging.info(f"{exc_tb=}")
            logging.info("".join(traceback.format_tb(exc_tb)))

            print(f"exc_tb={exc_tb}")

            print("".join(traceback.format_tb(exc_tb)))

            logging.error("".join(traceback.format_tb(exc_tb)))

            print("See logs at:", LOGFILE)

            os.read(sys.stdin.fileno(), 10)
        if AlternateScreen.ENABLED:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_attrs)
            print("\033[?1049l", end='')

    def clear(self):
        print("\033[H") # move TOP LEFT
        print("\033[2J", end='') # clear screen
        self.current_line = 0

        global TERMINAL_SIZE
        TERMINAL_SIZE = os.get_terminal_size()
        self.terminal_size = TERMINAL_SIZE

        self.x_split = self.terminal_size.columns // 2 - 10 if self.x_split_hint is None else self.x_split_hint

        # self.buffer = [""] * self.terminal_size.lines
        self.buffer_left = [""] * (self.terminal_size.lines - 1)
        self.buffer_right = [""] * (self.terminal_size.lines - 1)

    def __refresh__(self, name, old_buffer, new_buffer, x_left, x_right):

        logging.info(f"Screen {name} refresh {len(old_buffer)} / {len(new_buffer)}")

        if len(new_buffer) < len(old_buffer):
            logging.debug(f"Append buffer {name} {len(old_buffer) - len(new_buffer)} empty(lines)")
            new_buffer += [""] * (len(old_buffer)-len(new_buffer))

        elif len(new_buffer) > len(old_buffer):
            logging.warning(f"Shorten buffer {name} {len(new_buffer) - len(old_buffer)}")
            new_buffer = new_buffer[:len(old_buffer)]

        # printline = True
        printline = False

        maxwidth = x_right - x_left

        # with open("/tmp/" + name + "_buffer.txt", "w") as f:
        #     for i, (old, new) in enumerate(zip(old_buffer, new_buffer)):
        #         ansi_new = ansi_string(new)
        #         ansi_old = ansi_string(old)
        #         f.write(f"[{i:2}] ")
        #         # f.write(" len=")
        #         # f.write(str(len(ansi_old)))
        #         # f.write(" ")
        #         f.write(repr(ansi_old))
        #         f.write(" " * (maxwidth-len(ansi_old)))
        #         f.write(" -> ")
        #         f.write("len=")
        #         f.write(str(len(new)))
        #         f.write(" ")
        #         # f.write(repr(ansi_new))
        #         f.write(repr(new))
        #         f.write("\n")

        x_clearline_offset = x_left + (3 if x_left else 2) * printline

        for i, (old, new) in enumerate(zip(old_buffer, new_buffer)):

            if new == "<SCREEN_LINE>":
                new = Fore.BLACK + "─"*(maxwidth-printline*3) + Fore.RESET

            ansi_new = ansi_string(new)
            ansi_old = ansi_string(old)

            if old!=new:
                # logging.debug(f"{name} - write line {i} : {repr(ansi_new)}")

                if len(ansi_new) > maxwidth:
                    # new = ansi_new[:maxwidth]
                    # new = repr(new)[:maxwidth]
                    # new = new[:maxwidth-3] + "...\033[0m"

                    iutf = 0
                    iansi = 0
                    while iansi < maxwidth-3:
                        if new[iutf] == '\x1b':
                            while new[iutf] != 'm':
                                iutf+=1
                        iutf+=1
                        iansi+=1
                    # new = new[:iutf] + " " + Fore.LIGHTYELLOW_EX + " +" + str(len(ansi_new)-iansi+3) + "\033[0m"
                    new = new[:iutf] + Fore.BLACK + f" ...+{len(ansi_new)-maxwidth-3}...\033[0m"

                assert not "\n" in new
                old_buffer[i] = new
                linenumber = ("\033[0m" + Fore.CYAN + f"{i:2} " + Fore.RESET) if printline else ""

                print(f"\033[{i+1};{x_clearline_offset}H", " " * maxwidth, sep="") # clearline
                print(f"\033[{i+1};{x_left}H", linenumber, new, sep="") # move to line


            # else:
            #     logging.debug(f"{name} - skip line {i} : {repr(ansi_new)}")
        self.status(f"Terminal size : {self.terminal_size.columns} x {self.terminal_size.lines}, maxwidth = {maxwidth}")

    def refresh_left(self, buffer):
        self.__refresh__("left", self.buffer_left, buffer, x_left=0, x_right=self.x_split)

    def refresh_right(self, buffer):
        self.__refresh__("right", self.buffer_right, buffer, x_left=self.x_split+1, x_right=self.terminal_size.columns)
        print(f"\033[{self.terminal_size.lines};0H", end="") # move to BR
        sys.stdout.flush()

    # def refresh(self, buffer):
    #     logging.info("Screen refresh %d / %d", len(buffer), len(self.buffer))
    #     assert len(buffer) == len(self.buffer), f"Buffer size mismatch: {len(buffer)} / {len(self.buffer)}"

    #     for i, (a,b) in enumerate(zip(self.buffer, buffer)):
    #         if a!=b:
    #             assert not "\n" in b
    #             self.buffer[i] = b

    #             linenumber = "\033[0m" + Fore.CYAN + f"{i:2} " + Fore.RESET
    #             # linenumber = ""

    #             # if i> 15 and i < 30:
    #             # print(f"\033[{i}H\033[K", linenumber, b, "  ", repr(b), sep="") # move to line + clear line
    #             print(f"\033[{i}H\033[K", linenumber, b, sep="") # move to line + clear line



    def status(self, message):
        print(f"\033[{self.terminal_size.lines}H\033[K", message, sep="", end="") # move to line + clear line
        # print(f"\033[{self.terminal_size.lines};{self.terminal_size.columns}H", "@", sep="", end="") # move to line + clear line
        # print(f"\033[{self.terminal_size.lines};{self.terminal_size.columns // 2}H", "@", sep="", end="") # move to line + clear line
        # print(f"\033[{self.terminal_size.lines-1}H", "@"*(self.terminal_size.columns-1), sep="", end="") # move to line + clear line
        sys.stdout.flush()

    # https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html

    # # cursor positioning
    # ESC [ y;x H     # position cursor at x across, y down
    # ESC [ y;x f     # position cursor at x across, y down
    # ESC [ n A       # move cursor n lines up
    # ESC [ n B       # move cursor n lines down
    # ESC [ n C       # move cursor n characters forward
    # ESC [ n D       # move cursor n characters backward

    # # clear the screen
    # ESC [ mode J    # clear the screen

    # # clear the line
    # ESC [ mode K    # clear the line

class SublimeGitDiff:

    def __init__(self, left="origin/master", cwd=os.getcwd()):

        logging.info(f"SublimeGitDiff({left=}, {cwd=})")


        self.bash_cmd = ""
        def bash_print(cmd):
            if isinstance(cmd, list):
                return bash_print(" ".join(cmd))
            # self.bash_cmd = Fore.BLACK + cmd + Fore.RESET
            logging.debug("BASH CMD: %s", cmd)
            self.bash_cmd = cmd
        bash_proc.print = bash_print

        self.cwd = cwd

        #
        # Git root
        #
        self.git_root, _, returncode = bash_cmd("git rev-parse --show-toplevel", cwd=self.cwd)
        if returncode != 0:
            print(Fore.YELLOW + "Not in git repo - exit" + Fore.RESET)
            logging.warning("Not in git repo!")
            sys.exit(0)
        logging.info(f"Git root : {self.git_root} (returncode={repr(returncode)})")


        self.git_diff_left = left
        self.git_diff_left_sha1 = None

        self.git_diff_right = None
        self.git_diff_right_sha1 = None


        self.dirty = None # WORKDIR status

        self.DIFF = []
        self.LINES = None

        self.input_callback = None

        self.current_modified_files_index = None
        self.current_files_index = None
        self.current_lines_index = -1

        self.gitlab = None
        self.gitlab_url = None
        self.gitlab_pipeline_url = None

        # TODO : async file compare date  + self.last_cmd_date

    def find_files_index(self, filename):
        for i, (a,b) in enumerate(self.FILES):
            # logging.info(f"{a},{b}")
            if b == filename:
                return i
        return None

    def find_lines(self, filename):
        try:
            index = self.find_files_index(filename)
            if index is not None:
                FILES = self.FILES[index]
                return self.git_diff.lines(FILES)
            else:
                return []
        except Exception as e:

            logging.warning(f"An exception occured while trying to get LINES for {filename=}\n{e}")
            return []

    @logit
    def refresh(self):

        if self.screen is None:
            return

        # logging.info("\n".join(traceback.format_stack()))

        self.screen.clear()

        left, right = self.format()

        #
        # Left screen size check
        #

        a = len(left)
        b = len(self.screen.buffer_left)

        if a > b:

            if not self.short_modified:
                self.short_modified = True
                return self.refresh()

            if not self.short_title:
                self.short_title = True
                return self.refresh()

            if not self.short_gitlab:
                self.short_gitlab = True
                return self.refresh()




            # if not self.short_history:
            #     self.short_history = True
            #     return self.refresh()

        #
        # Right screen size check
        #

        a = len(right)
        b = len(self.screen.buffer_right)

        if a > b:
            logging.info(f"Truncate buffer[{a}] to {b}")
            last_line = self.git_diff_current_end
            if last_line < b:
                right = right[:b]
            else:
                right = right[last_line - b:last_line]


        self.screen.refresh_left(left)
        self.screen.refresh_right(right)

    @logit
    def init(self, current_modified_file_hint=None, current_modified_files_index_hint=None, current_lines_index_hint=None):

        self.input = None

        self.short_title = False
        self.short_status = False
        self.short_history = False
        self.short_gitlab = False
        self.short_modified = False

        #
        # Branches
        #

        self.git_branch = bash_cmd("git rev-parse --abbrev-ref HEAD")[STDOUT]
        self.git_origin_branch = "origin/" + self.git_branch

        #
        # Last common ancestor with origin/master
        #
        stdout, stderr, returncode = bash_cmd(f"git rev-list --left-right --count {self.git_diff_left}...HEAD", cwd=self.cwd)
        l, r = stdout.split("\t")
        logging.debug(f"{self.bash_cmd} -> {stdout}")
        self.commits = bash_cmd(f"git log --format=format:%H -{int(r)+1}", cwd=self.cwd)[STDOUT].split("\n")
        logging.debug(f"{self.bash_cmd} -> {self.commits}")

        #
        # Left/right
        #
        if self.git_diff_left is None:
            # self.git_diff_left = "HEAD"
            self.git_diff_left = self.commits[-1]

        if self.git_diff_right is None:
            self.git_diff_right = "WORKDIR"

        #
        # Git diff
        #

        self.git_diff = GitDiff(git_sha1(self.git_diff_left), git_sha1(self.git_diff_right), self.cwd)
        self.git_diff_cmd = self.bash_cmd
        logging.info(f"git diff: left={self.git_diff_left}, right={self.git_diff_right}")
        logging.info(f"git diff: {ansi_string(self.git_diff_cmd)}")
        self.FILES = self.git_diff.files()
        self.git_diff_cmd = self.bash_cmd
        logging.debug("Git FILES:\n" + "\n".join([str(f) for f in self.FILES]))

        #
        # Modified files
        #

        self.modified_files, self.dirty = self.set_modified_files()
        logging.debug("Modified files:\n" + "\n".join([str(f) for f in self.modified_files]))
        if self.modified_files:

            logging.debug(f"Init hint : {current_modified_file_hint=}, {current_modified_files_index_hint=}, {current_lines_index_hint=}")
            if current_modified_files_index_hint:
                index = current_modified_files_index_hint
            else:
                index = 0
                for i, (filename, _, _) in enumerate(self.modified_files):
                    if filename == current_modified_file_hint:
                        index = i
                        break
            logging.debug(f"Init hint : {index=}")
            self.set_modified_files_index(index, current_lines_index_hint=current_lines_index_hint)

    @logit
    def set_modified_files(self):
        """
        Parse git status output:
            - status for STAGED changes (git diff --name-status)
            - status for UNSTAGED changes (git status --porcelaine -> WORKDIR)

        Return:
            - [[filename, STATUS, status]]
            - WORKDIR dirty flag
        """

        ret = []
        dirty = None

        # git_diff_right = "HEAD" if self.git_diff_right == "WORKDIR" else self.git_diff_right

        #
        # Parse modified files between LEFT and RIGHT commits
        #
        staged_status = bash_cmd(f"git diff {git_sha1(self.git_diff_left)} {git_sha1(self.git_diff_right)} --name-status")[STDOUT].split("\n")
        logging.info(f"Staged status, count={len(staged_status)} ({self.bash_cmd}):\n" + "\n".join(staged_status))
        for line in staged_status:
            if not line:
                continue
            logging.debug("split %s", line)
            status, filename = line.split("\t", 1)
            # assert status.isupper()
            ret += [[filename, status, None]]

        #
        # Parse modified files not staged
        #
        if self.git_diff_right == "WORKDIR":
            dirty = 0
            unstaged_status = bash_cmd(f"git status --porcelain")[STDOUT].split("\n")
            logging.info(f"Untaged status, count={len(unstaged_status)} ({self.bash_cmd}):\n" + "\n".join(unstaged_status))
            for line in unstaged_status:

                if not line:
                    continue

                dirty += 1
                logging.debug(line)
                status, filename = re.match(r"(\s*[^\s]+)\s*(.*)", line).groups()
                line.strip().split(" ", 1)
                # assert status.isupper(), f"Expected UPPER status -> {status} ({repr(line)})"

                for i, (f, _, _) in enumerate(ret):
                    if f == filename:
                        ret[i][2] = status
                        filename = None
                        break

                if filename:
                    ret += [[filename, None, status]]


        def custom_cmp(a, b):
            A, B = a[0], b[0]
            splitA, splitB = A.split('/'), B.split('/')
            if len(splitA) < len(splitB) and (B.startswith("/".join(splitA[:-1])) or len(splitA)==1):
                return -1
            elif len(splitA) > len(splitB) and (A.startswith("/".join(splitB[:-1])) or len(splitB)==1):
                return 1
            else:
                return A < B

        #
        # NB : we want modified files to be sorted the same they will be in the Tree (eg __files__ are displayed before other subdirectories)
        #
        return sorted(ret, key=cmp_to_key(custom_cmp)), dirty

    @logit
    def set_modified_files_index(self, index, current_lines_index_hint=None):

        if not self.modified_files:
            self.current_modified_files_index = None
            return

        previous_modified_files_index = self.current_modified_files_index

        logging.info(f"{len(self.modified_files)=}")
        self.current_modified_files_index = index
        # logging.info(f"A {self.current_modified_files_index=}")
        self.current_modified_files_index += len(self.modified_files)
        # logging.info(f"B {self.current_modified_files_index=}")
        self.current_modified_files_index %= len(self.modified_files)
        # logging.info(f"C {self.current_modified_files_index=}")

        filename, _, _ = self.modified_files[self.current_modified_files_index]
        self.current_files_index = self.find_files_index(filename)

        logging.debug(f"{self.current_modified_files_index=}")
        logging.debug(f"{filename=}")
        logging.debug(f"{self.current_files_index=}")

        if len(self.FILES) == 0:
            return None

        if self.current_files_index is None:
            return self.set_modified_files_index(index + 1, current_lines_index_hint=current_lines_index_hint)

        FILES = self.FILES[self.current_files_index]

        self.LINES = self.git_diff.lines(FILES)

        # if not self.LINES:
        # print(self.LINES)

        if current_lines_index_hint:
            self.current_lines_index = min(current_lines_index_hint, len(self.LINES) -1)
            # self.current_lines_index = current_lines_index_hint % len(self.LINES)
        else:
            if previous_modified_files_index is not None and previous_modified_files_index > self.current_modified_files_index:
                self.current_lines_index = len(self.LINES) - 1
            else:
                self.current_lines_index = 0

        self.DIFF = []
        for i, lines in enumerate(self.LINES):
            diff = self.git_diff.diff(FILES, lines)
            self.DIFF += [(GitDiff.format_title(FILES, lines, diff), GitDiff.format_diff(FILES, lines, diff))]

        return previous_modified_files_index != self.current_modified_files_index

    @logit
    def update_left_right(self, hint=None):

        # LEFT          |   RIGHT
        # --------------|----------------
        # origin/master | WORKDIR
        # origin/branch | WORKDIR
        # HEAD          | WORKDIR
        # origin/master | HEAD
        # origin/branch | HEAD
        # HEAD~1        | HEAD
        # HEAD~2        | HEAD~1
        # ...

        logging.info(f" was -> left={self.git_diff_left}, right={self.git_diff_right}")

        if hint == 'B' :
            self.git_diff_left, self.git_diff_right = self.git_origin_branch, "WORKDIR"

        elif hint == 'H' :
            self.git_diff_left, self.git_diff_right = "HEAD", "WORKDIR"

        elif hint == 'O' :
            self.git_diff_left, self.git_diff_right = self.commits[-1], "WORKDIR"

        elif isinstance(hint, int):

            n = int(hint)

            choices = []
            choices += [("HEAD", "WORKDIR")]
            choices += [(self.commits[-1], "WORKDIR")]
            choices += [(self.commits[-1], "HEAD")]
            if git_sha1(self.git_origin_branch) is not None:
                choices += [(self.git_origin_branch, "WORKDIR")]
            for l, r in zip(self.commits[1:], self.commits[:-1]):
                choices += [(l, r)]

            # logging.info(choices)

            for i, c in enumerate(choices):
                if (self.git_diff_left, self.git_diff_right) == c:
                    logging.info(f"Found current left/right index: {i}")
                    self.git_diff_left, self.git_diff_right = choices[(i + n) % len(choices)]
                    break

        else:
            self.screen.status(f"Unknown update_left_right(hint={repr(hint)})")
            logging.warning(f"Unknown update_left_right(hint={repr(hint)})")
            return

        logging.info(f" is -> left={self.git_diff_left}, right={self.git_diff_right} ({self.current_modified_files_index=}, {self.current_lines_index=})")
        self.init(current_modified_files_index_hint=self.current_modified_files_index, current_lines_index_hint=self.current_lines_index)
        self.refresh()

        self.screen.status(f"Updated left={self.git_diff_left}, right={self.git_diff_right}")

    #
    # KEY LOOP METHODS
    #

    def run(self):

        with AlternateScreen() as screen:
            self.screen = screen
            self.init()
            self.refresh()

            # https://nitratine.net/blog/post/how-to-make-hotkeys-in-python/
            # https://pynput.readthedocs.io/en/latest/keyboard.html
            # https://stackoverflow.com/questions/15992651/python-filtered-line-editing-read-stdin-by-char-with-no-echo

            # import select
            # r, w, e = select.select([ f ], [], [], 0)
            # if f in r:
            #   print os.read(f.fileno(), 50)
            # else:
            #   print "nothing available!"  # or just ignore that case

            while True:
                char = os.read(sys.stdin.fileno(), 10)
                if not self.on_keypress(char.decode()):
                    break
            self.screen = None

    def on_keypress(self, key):

        # from getkey.unikeys import UnicodeAsciiKeys
        # /home/fdutrech/.local/lib/python3.8/site-packages/getkey/unikeys.py
        # /home/fdutrech/.local/lib/python3.8/site-packages/getkey/keynames.py

        ESCAPE = '\x1b'
        DEL = '\x1b[3~'
        UP = '\x1b[A'
        DOWN = '\x1b[B'
        RIGHT = '\x1b[C'
        LEFT = '\x1b[D'
        CTRL_UP = '\x1b[1;5A'
        CTRL_DOWN = '\x1b[1;5B'
        CTRL_RIGHT = '\x1b[1;5C'
        CTRL_LEFT = '\x1b[1;5D'

        CTRL_G = '\x07'
        CTRL_P = '\x10'
        CTRL_U = '\x15'

        ALT_UP = '\x1b[1;2A'
        ALT_DOWN = '\x1b[1;2B'
        ALT_RIGHT = '\x1b[1;2C'
        ALT_LEFT = '\x1b[1;2D'

        PAGE_UP = '\x1b[5~'
        PAGE_DOWN = '\x1b[6~'

        F5 = '\x1b[15~'

        if self.input_callback is not None:
            if self.input_callback(key) is False:
                self.input_callback = None

        elif key in ['h']:
            left, right = self.screen.buffer_left, self.screen.buffer_right
            self.screen.clear()

            fmt = []
            fmt += self.format_title()
            fmt += self.format_line()
            fmt += self.format_help()
            self.screen.refresh_left(fmt)

            def callback(key):
                print(f"{key=}")
                self.screen.refresh_left(left)
                self.screen.refresh_right(right)
                return False
            self.input_callback = callback

        elif key in ['q', ESCAPE]:
            return False

        elif key in [DOWN, RIGHT]:
            self.update_current_lines_index(1)

        elif key in [CTRL_DOWN, CTRL_RIGHT, PAGE_DOWN]:
            self.update_modified_files_index(1)

        elif key in [UP, LEFT]:
            self.update_current_lines_index(-1)

        elif key in [CTRL_UP, CTRL_LEFT, PAGE_UP]:
            self.update_modified_files_index(-1)

        elif key in [ALT_UP, ALT_LEFT]:
            self.update_left_right(-1)

        elif key in [ALT_DOWN, ALT_RIGHT]:
            self.update_left_right(1)

        elif key in ['B', 'O', 'H']:
            self.update_left_right(key)

        elif key in ['e', '\n']:
            self.edit()

        elif key in ['f']:
            # fullscreen
            self.screen.x_split_hint = 0 if self.screen.x_split_hint is None else None
            self.refresh()

        elif key in ['i']:

            """
            Test input mode
            """

            def callback(success):
                if key == "\n":
                    self.screen.status(Fore.GREEN + "SUCCESS " + self.input + Fore.RESET)
                    return False
                elif key == ESCAPE:
                    self.screen.status(Fore.RED + "CANCEL" + Fore.RESET)
                    return False
                elif len(key) == 1:
                    self.input += key
                    self.screen.status(Fore.BLUE + "INPUT: " + self.input + Fore.RESET)
                else:
                    self.screen.status(Fore.YELLOW + "INPUT: " + self.input + f" - ignoring {repr(key)}" + Fore.RESET)

                return True

                self.input = None
            self.input = ""
            self.input_callback = callback
            self.screen.status(Fore.BLUE + "INPUT: " + self.input + Fore.RESET)

        elif key in [CTRL_U]:
            b = self.unstage()
            self.init(current_modified_file_hint=b, current_lines_index_hint=self.current_lines_index)
            self.refresh()

        elif key in ['c']:
            self.copy()

        elif key in [CTRL_G]:
            if self.gitlab_url:
                bash_cmd(f"xdg-open {self.gitlab_url}")

        elif key in [CTRL_P]:
            if self.gitlab_pipeline_url:
                bash_cmd(f"xdg-open {self.gitlab_pipeline_url}")

        elif key in ['p']:
            self.copy_path()

        elif key in ['w']:
            param = "--ignore-space-change"
            if param in GitDiff.EXTRA_PARAMS:
                GitDiff.EXTRA_PARAMS.remove(param)
                self.screen.status(Fore.RED + f"Remove git diff '{param}'" + Fore.RESET)
            else:
                GitDiff.EXTRA_PARAMS.append(param)
                self.screen.status(Fore.GREEN + f"Append git diff '{param}'" + Fore.RESET)
            self.init(current_modified_files_index_hint=self.current_modified_files_index, current_lines_index_hint=self.current_lines_index)
            self.refresh()

        elif key in ['r', F5]:
            self.screen.status("Refreshing...")
            self.init(current_modified_files_index_hint=self.current_modified_files_index, current_lines_index_hint=self.current_lines_index)
            self.refresh()

        elif key in ['d', DEL]:
            b = self.undo()
            if b:
                self.init(current_modified_file_hint=b, current_lines_index_hint=self.current_lines_index)
                self.refresh()

        else:
            self.screen.status("KEY PRESS: " + repr(key))

            # print("KEY PRESS: " + repr(key))

        return True

    @logit
    def commit_all(self):
        pass

    @logit
    def unstage(self):
        """
        While developping you may want to move your changes from one commit to another

        The idea is to undo all changes, amend to previous commit, and redo the changes
        They can all be added to a new commit, that will be merged with the commit of interest using an interactive git rebase

        Exemple: Move all changes about topic #1 in same commit

            as53d43 <- HEAD
            4sad254 change about topic #1
            a42s5d5
            4ad5s47 change about topic #1

        """

        _, b = self.FILES[self.current_files_index]
        _, _, end_line, _ = self.LINES[self.current_lines_index]

        try:

            self.screen.status(f"Unstaging {b}:{end_line}...")

            #
            # stash everything to be in clean state
            #
            bash_cmd("git stash", cwd=self.cwd)

            #
            # backup the change...
            #
            with open(os.path.join(self.git_root, b), "r") as f:
                content = f.read()

            #
            # undo the change
            #
            self.undo()
            bash_cmd(f"git commit {b} --amend --no-edit", cwd=self.cwd)

            #
            # append to previous commit
            #

            #
            # restore the change... -> it is now unstaged !
            #
            with open(os.path.join(self.git_root, b), "w") as f:
                f.write(content)

        except Exception as e:
            raise e

        finally:
            bash_cmd("git stash apply", cwd=self.cwd)
            self.screen.status(f"Unstaged {b}:{end_line}!")
            return b

    @logit
    def undo(self):

        # if self.git_diff_right != 'WORKDIR':
        #     self.screen.status(Fore.RED + 'Cannot undo() if not comparing WORKDIR' + Fore.RESET)
        #     return

        FILES = self.FILES[self.current_files_index]
        LINES = self.LINES[self.current_lines_index]
        DIFF = self.git_diff.diff(FILES, LINES)

        a, b = FILES
        start_line, start_count, end_line, end_count = LINES
        minus, plus = DIFF

        minus = [m + "\n" for m in minus]
        plus = [p + "\n" for p in plus]

        logging.info(f"Undo at {b}:{end_line}")
        logging.info(f"{a}:{start_line},{start_count} {b}:{end_line},{end_count}")
        logging.info(f"{repr(minus)=}")
        logging.info(f"{repr(plus)=}")

        with open(os.path.join(self.git_root, b), "r+") as f:
            lines = f.readlines()

            f.seek(0)
            f.truncate()

            for i in range(end_count):
                logging.info("  - Remove line %d : %s", end_line+i-1, lines[end_line+i-1].rstrip("\n"))
                # assert lines[end_line+i-1] == plus[i], f"\n{repr(lines[end_line+i-1])}\n{repr(plus[i])}\n{b}:{end_line}"

            for i in range(start_count):
                logging.info("  - Add line %d : %s", end_line+i-1, minus[i])


            if plus:
                logging.info(f" => lines[:{end_line-1}] + minus (len({len(minus)})) + lines[{end_line + end_count - 1}:]")
                lines = lines[:end_line-1] + minus + lines[end_line + end_count - 1:]
            else:
                lines = lines[:end_line] + minus + lines[end_line:]


            if lines[-1].endswith("<NO_NEWLINE>\n"):
                lines[-1] = lines[-1].replace("<NO_NEWLINE>\n", "")

            f.writelines(lines)

        return b

    def edit(self):
        FILES = self.FILES[self.current_files_index]
        LINES = self.LINES[self.current_lines_index]
        DIFF = self.git_diff.diff(FILES, LINES)

        a, b = FILES
        start_line, start_count, end_line, end_count = LINES
        m, p = DIFF

        # import pyperclip
        # pyperclip.copy("\n".join(m))

        subprocess.run(["subl", f"{os.path.join(self.git_root, b)}:{end_line}"])

    def copy_path(self):
        FILES = self.FILES[self.current_files_index]
        LINES = self.LINES[self.current_lines_index]
        DIFF = self.git_diff.diff(FILES, LINES)

        a, b = FILES
        start_line, start_count, end_line, end_count = LINES
        m, p = DIFF

        import pyperclip
        pyperclip.copy(b)

        self.screen.status(f"Path '{b}' copied")

    def copy(self):
        FILES = self.FILES[self.current_files_index]
        LINES = self.LINES[self.current_lines_index]
        DIFF = self.git_diff.diff(FILES, LINES)

        a, b = FILES
        start_line, start_count, end_line, end_count = LINES
        m, p = DIFF

        import pyperclip
        pyperclip.copy("\n".join(m))

        self.screen.status(f"{len(m)} MINUS line(s) copied")

    def update_current_lines_index(self, offset):

        logging.info(f"update_current_lines_index({offset=})")

        self.current_lines_index += offset

        if self.LINES:

            if self.current_lines_index == len(self.LINES):
                return self.update_modified_files_index(1)

            elif self.current_lines_index == -1:
                return self.update_modified_files_index(-1)

        self.refresh()

    def update_modified_files_index(self, offset):
        logging.info(f"update_modified_files_index({offset=})")

        if self.current_modified_files_index is None:
            self.screen.status(Fore.RED + "No modified files" + Fore.RESET)
            return

        if self.set_modified_files_index(self.current_modified_files_index + offset):
            self.refresh()

    #
    # FORMAT METHODS
    #

    def format(self):

        self.fmt = []
        # self.fmt += self.format_line()           # ------------
        self.fmt += self.format_title()          # TITLE
        # self.fmt += self.format_line()           # ------------

        if self.git_root is None:
            return self.fmt + [Fore.RED + f"You're not in git repository (cwd={os.getcwd()})" + Fore.RESET], []

        self.fmt += self.format_history()        # HISTORY
        # self.fmt += self.format_line()           # ------------
        # self.fmt += self.format_status()         # STATUS
        # if not self.working_dir_clean:
        self.fmt += self.format_line()           # ------------
        self.fmt += self.format_modified_files()  # TREE
        self.fmt += self.format_line()           # ------------
        self.fmt += self.format_gitlab()         # GITLAB
        # self.fmt += self.format_line()           # ------------

        return self.fmt, self.format_line() + self.format_git_diff()

    def format_line(self):
        return ["<SCREEN_LINE>"]

    def format_title(self):

        if self.short_title:
            return []

        cwd = os.getcwd()
        title = figlet_format("Sublime Git Diff").split("\n")

        title = title[:-1]
        title += [self.git_diff_cmd]

        return self.format_line() + ["\t" + Fore.CYAN + t + Fore.RESET for t in title] + self.format_line()

    def format_gitlab(self):

        if self.short_gitlab:
            return []

        ret = [Fore.MAGENTA + "Gitlab :" + Fore.RESET]

        if self.gitlab is None:
            import threading

            def fetch_gitlab():
                # https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html
                import gitlab, ssl

                try:
                    gitlab_ret = []

                    git_branch, _, _ = bash_cmd("git rev-parse --abbrev-ref HEAD")
                    git_origin, _, _ = bash_cmd("git config --get remote.origin.url")

                    project_url = git_origin.split(":")[1].replace('.git', '') # git@gitlab.ateme.net:/sw/TitanProcessing -> /sw/TitanProcessing

                    GITLAB_TOKEN=os.environ.get('GITLAB_TOKEN', 'V5XKkR28zu3zooUkWgH7')
                    gl = gitlab.Gitlab(url="https://gitlab.ateme.net/", private_token=GITLAB_TOKEN)
                    gl.auth()

                    def pythonify(obj):
                        return eval(str(obj).split(" => ")[1])

                    def dump(obj):
                        print(json.dumps(pythonify(obj), indent=2))

                    # project = gl.projects.get("sw/TitanProcessing")


                    # for mr in project.mergerequests.list(iterator=True, source_branch="tp-7806"):
                    for mr in gl.mergerequests.list(iterator=True, state='opened', source_branch=git_branch):
                        # dump(mr)

                        project = gl.projects.get(mr.project_id, lazy=True)
                        editable_mr = project.mergerequests.get(mr.iid, lazy=True)

                        if project_url in mr.web_url:
                            self.gitlab_url = mr.web_url

                        web_url_color = [Fore.BLACK, Fore.YELLOW]
                        state_color = {"opened" : Fore.BLUE, "merged" : Fore.GREEN}

                        gitlab_ret += [f" * " + web_url_color[project_url in mr.web_url] + mr.web_url + Fore.BLACK + "  - " + state_color.get(mr.state, Fore.YELLOW + " ????? " ) + mr.state + Fore.RESET]

                        for pipeline in editable_mr.pipelines.list(iterator=True):

                            status_color = {"failed" : Fore.RED, "success" : Fore.GREEN, "running" : Fore.BLUE}

                            gitlab_ret += [Fore.BLACK + f"     - " + pipeline.web_url + "  - " + status_color.get(pipeline.status, Fore.YELLOW + " ????? " ) + pipeline.status + Fore.RESET + f" ({pipeline.sha[:7]})"]
                            if project_url in mr.web_url:
                                self.gitlab_pipeline_url = pipeline.web_url
                            # dump(pipeline)

                            # logging.debug( pythonify(pipeline) )
                            break

                    if not gitlab_ret:
                        gitlab_ret += [Fore.BLACK + f"No merge request" + Fore.RESET]


                # except ssl.SSLCertVerificationError:
                except Exception as e:
                    gitlab_ret += [Fore.RED + f"Not connected to VPN {e}" + Fore.RESET]

                self.gitlab = gitlab_ret
                self.refresh()

            self.gitlab = threading.Thread(target=fetch_gitlab)
            self.gitlab.start()
            self.gitlab_url = None
            self.gitlab_pipeline_url = None

            status = ["Not yet available..."]

        elif not isinstance(self.gitlab, list):
            status = ["Not yet available (running thread)..."]

        else:
            status = self.gitlab

        return ret + status + self.format_line()

    def format_history(self):

        if self.short_history:
            return [Fore.MAGENTA + "History : " + Fore.RESET + "..."]

        git_origin_master_sha1 = git_sha1(self.git_diff_left)

        history = bash_cmd(f"git log --decorate --color=always --oneline -{len(self.commits)}", cwd=self.cwd)[STDOUT]

        ret = [Fore.MAGENTA + "History :" + Fore.RESET]
        # ret += [Fore.BLACK + self.bash_cmd + Fore.RESET]

        left_sha1, right_sha1 = str(git_sha1(self.git_diff_left))[:7], str(git_sha1(self.git_diff_right))[:7]
        # ret.append(f"left={self.git_diff_left} ({left_sha1}), right={self.git_diff_right} ({right_sha1})")

        def set_prefix(target_sha1):
            target_sha1 = str(git_sha1(target_sha1))[:7]
            left = u"l ⮞ " if left_sha1 == target_sha1 else "    "
            right = u" ⮜ r " if right_sha1 == target_sha1 else "    "
            return left, right

        #
        # origin/master + origin/branch
        #
        for branch in [self.git_diff_left, self.git_origin_branch]:
            sha1 = git_sha1(branch)
            if sha1 and not sha1[:7] in history:
                lprefix, rprefix = set_prefix(sha1)
                stdout, _, _ = bash_cmd(f"git rev-list --left-right --count {branch}...HEAD", cwd=self.cwd)
                ret.append(f"{lprefix}{Fore.YELLOW}{sha1[:9]}{rprefix}({Fore.RED}{Style.BRIGHT}{branch}{Style.RESET_ALL}{Fore.YELLOW}) {Fore.RED}{self.bash_cmd} -> {stdout}{Style.RESET_ALL}")
                ret.append(Fore.BLACK + "-"*100 + Fore.RESET)

        #
        # WORKDIR
        #
        lprefix, rprefix = set_prefix("WORKDIR")
        if self.dirty is None:
            ret.append(lprefix + Fore.BLACK + "WORKDIR   -" + rprefix)
        else:
            if self.dirty != 0:
                ret.append(lprefix + Fore.YELLOW + "WORKDIR  " + Fore.RESET + rprefix + Fore.RED + f"dirty, {self.dirty} file(s) modified")

        for line in history.split("\n"):
            if not line:
                continue

            sha1, branches, msg = re.match(r"([^\s]*) (\([^)]*\)\x1b\[m )?(.*)", line).groups()
            branches = branches or ""

            lprefix, rprefix = set_prefix(ansi_string(sha1))
            if self.dirty == 0 and "(HEAD" in ansi_string(branches):
                branches = Fore.GREEN + "WORKDIR " + branches
                _, rprefix = set_prefix("WORKDIR")

            # if self.git_target_sha1[:7] in sha1:
            #     sha1 = Back.MAGENTA + ansi_string(sha1) + Back.RESET + (Fore.YELLOW if branches else "")
            #     ret.append(Fore.BLACK + "-"*100 + Fore.RESET)

            ret.append(lprefix + sha1 + Fore.RESET + rprefix + branches + msg)
            # ret.append(repr(line))

            if self.cwd.endswith("TitanProcessing") and not msg.startswith("Merge branch"):

                # search for invalid patterns
                invalid = []
                if not re.match("^.* : .*$", msg):
                    invalid.append("format")
                if "wip" in msg:
                    invalid.append("WIP")

                # apply format
                ret[-1] += "\r" + "\t"*12
                if invalid:
                    ret[-1] += Fore.RED + u'✗ invalid commit : ' + ", ".join(invalid)
                else:
                    ret[-1] += Fore.GREEN + u"✓ valid commit"
                ret[-1] += Fore.RESET

        return ret

    def format_git_diff(self):
        ret = []
        self.git_diff_begin = len(self.fmt)

        for i, (diff_title, diff) in enumerate(self.DIFF):

            if i == self.current_lines_index:
                prefix = Back.MAGENTA + " " + Back.RESET + " "
                self.git_diff_current_begin = len(ret)
            else:
                prefix = "  "

            ret.append(prefix + f"[{i+1}/{len(self.DIFF)}] " + diff_title)
            for line in diff.split("\n"):
                ret.append(prefix + line)

            ret += self.format_line()
            if i == self.current_lines_index:
                self.git_diff_current_end = len(ret)

        return ret

    def format_modified_files(self):

        class Tree:

            DATA = {}

            def __init__(self, data, prefix = ''):
                self.i = 0
                self.n = len(data) + len(data["__files__"]) - 1 if "__files__" in data else len(data)
                self.prefix = prefix

            def child(self, data):
                return Tree(data, self.prefix + [' │   ', '     '][self.i == self.n])

            def __str__(self):
                self.i += 1
                return Fore.BLACK + self.prefix + [' ├── ', ' └── '][self.i == self.n] + Fore.RESET

            @staticmethod
            def format_file(path, value):

                file, staged_status, unstaged_status = value
                filename = os.path.join(path, file)
                ret = ""

                if staged_status == 'M':
                    staged_status = Fore.BLUE + u'🖉 '
                elif staged_status == 'A':
                    staged_status = Fore.GREEN + u'new 🖉 '
                elif staged_status == None and unstaged_status == "??":
                    staged_status = Fore.GREEN + u'untracked 🖉 '
                    # return None
                else:
                    staged_status = Fore.RED + repr(staged_status) + " "

                staged_status += Fore.RESET

                changes = Fore.CYAN + " %d change(s)"%len(self.find_lines(filename)) + Fore.RESET

                current_modified_filename, _, _ = self.modified_files[self.current_modified_files_index]
                filename = file if filename != current_modified_filename else Back.MAGENTA + file + Back.RESET


                # X          Y     Meaning
                # -------------------------------------------------
                #          [AMD]   not updated
                # M        [ MD]   updated in index
                # A        [ MD]   added to index
                # D                deleted from index
                # R        [ MD]   renamed in index
                # C        [ MD]   copied in index
                # [MARC]           index and work tree matches
                # [ MARC]     M    work tree changed since index
                # [ MARC]     D    deleted in work tree
                # [ D]        R    renamed in work tree
                # [ D]        C    copied in work tree
                # -------------------------------------------------
                # D           D    unmerged, both deleted
                # A           U    unmerged, added by us
                # U           D    unmerged, deleted by them
                # U           A    unmerged, added by them
                # D           U    unmerged, deleted by us
                # A           A    unmerged, both added
                # U           U    unmerged, both modified
                # -------------------------------------------------
                # ?           ?    untracked
                # !           !    ignored
                # -------------------------------------------------

                if unstaged_status == None:
                    unstaged_status = Fore.BLACK + ' - ' + Fore.RESET
                    pass
                else:
                    unstaged_status = repr(unstaged_status)
                    # if unstaged_status == 'M':
                    #     unstaged_status += Fore.YELLOW + ' dirty'
                    # elif unstaged_status == 'UU':
                    #     unstaged_status += Fore.YELLOW + ' unmerged, both modified'

                return staged_status, filename, changes, unstaged_status

            @staticmethod
            def single_leaf(data, path=""):
                if len(data) > 1:
                    return False

                if isinstance(data, dict):
                    for key, value in data.items():
                        return Tree.single_leaf(value, os.path.join(path, key))
                elif isinstance(data, list):
                    assert os.path.basename(path) == "__files__"
                    path = os.path.dirname(path)
                    staged_status, filename, changes, unstaged_status = Tree.format_file(path, data[0])
                    return f"{staged_status} {Fore.BLACK}{path}/{Fore.RESET}{filename} {changes} {unstaged_status}"

                else:
                    assert False
                # return True

            @staticmethod
            def format(data=None, path="", tree = None):
                """return lines array of pretty tree print of dict"""
                data = Tree.DATA if data is None else data
                tree = Tree(data) if tree is None else tree.child(data)
                ret = []

                for value in sorted(data.get("__files__", [])):
                    fmt = Tree.format_file(path, value)
                    if fmt is not None:
                        ret += [str(tree) + "".join([str(f) for f in fmt])]

                for key in sorted(list(data.keys())):
                    if key == "__files__":
                        continue

                    single_leaf_path = Tree.single_leaf(data[key], os.path.join(path, key)) if self.short_modified else None
                    if single_leaf_path:
                        if path:
                            single_leaf_path = single_leaf_path.replace(path + '/', '')
                        ret += [str(tree) + f"{single_leaf_path}"]
                    else:
                        ret += [str(tree) + Fore.BLACK + key + Fore.RESET] + Tree.format(data[key], os.path.join(path, key), tree)

                return ret

            @staticmethod
            def populate(path, staged_status, unstaged_status, tree=None):
                """
                Recursively parse file path to populate dict
                If it is a leaf (ie a file), add it to '__file__' key
                """
                tree = Tree.DATA if tree is None else tree
                if len(path) == 1:
                    tree['__files__'] = tree.get('__files__', []) + [(path[0], staged_status, unstaged_status)]
                else:
                    tree[path[0]] = Tree.populate(path[1:], staged_status, unstaged_status, tree.get(path[0], {}))
                return tree

        for filename, staged_status, unstaged_status in self.modified_files:
            Tree.populate(filename.split("/"), staged_status, unstaged_status)

        return Tree.format()

    def format_help(self):

        data = {
            "HELP" : {
                "Navigation": {
                    "🠕🠗🠔🠖": "Next",
                    "⥣⥥": "Next"
                },
                "Edit" : {
                    "'e'" : "edit"
                }
            }
        }

        class Tree:

            DATA = {}

            def __init__(self, data, prefix = ''):
                self.i = 0
                self.n = len(data) + len(data["__files__"]) - 1 if "__files__" in data else len(data)
                self.prefix = prefix

            def child(self, data):
                return Tree(data, self.prefix + [' │   ', '     '][self.i == self.n])

            def __str__(self):
                self.i += 1
                return self.prefix + [' ├── ', ' └── '][self.i == self.n]

            @staticmethod
            def format(data=None, tree = None):
                ret = []
                tree = Tree(data) if tree is None else tree.child(data)
                for key in sorted(list(data.keys())):
                    value = data[key]
                    if isinstance(value, dict):
                        ret += [str(tree) + key + ":"]
                        ret += Tree.format(value, tree)
                    else:
                        ret += [str(tree) + key + ":" + value]
                return ret

        return Tree.format(data)

if __name__ == "__main__":
    logging.basicConfig(filename=LOGFILE,
                        filemode='w',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)
                        # level=logging.DEBUG)

    logging.getLogger().handlers[0].setFormatter(ColorFormatter())

    left = sys.argv[1] if len(sys.argv) > 1 else "origin/master"

    try:
        SublimeGitDiff(left).run()
    except Exception as e:
        print(f"An error occured while running SublimeGitDiff ({e})")

