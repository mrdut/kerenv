import os,sys, re

print("sys.version", sys.version)

import json
import time
import traceback
import subprocess
import html

# https://www.geeksforgeeks.org/broken-pipe-error-in-python/
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE,SIG_DFL)

from difflib import Differ, SequenceMatcher

try:
    import sublime
    import sublime_plugin

except ModuleNotFoundError:

    from colorama import Fore, Back, Style

# https://www.sublimetext.com/docs/api_reference.html
# https://github.com/SublimeText/WordHighlight/blob/master/word_highlight.py

STDOUT=0
STDERR=1
RETURNCODE=2
def bash_proc(cmd, *args, **kwargs):
    # Popen.stdin
    # Popen.stdout
    # Popen.stderr
    # Popen.pid
    # Popen.returncode
    # logging.log(loglevel, 'cmd="%s", args=%s, kwargs=%s', cmd, args, kwargs)

    bash_proc.print(cmd)

    return subprocess.Popen(cmd, shell=isinstance(cmd, str) , stdout=subprocess.PIPE, stderr=subprocess.PIPE, *args, **kwargs)

bash_proc.print = lambda x: print(x)

def bash_exec(proc):
    stdout, stderr = proc.communicate()
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()
    # if proc.returncode != 0:
    #     logging.log(loglevel, "returncode=%d", proc.returncode)
    #     logging.log(loglevel, "stderr=%s", stderr)
    return stdout, stderr, proc.returncode

def bash_cmd(cmd, *args, **kwargs):
    proc = bash_proc(cmd, *args, **kwargs)
    return bash_exec(proc)

class GitDiff:

    def __init__(self, target, cwd=None):

        proc = bash_cmd("git diff %s -U0"%target, cwd=cwd)

        # diff --git a/utils/pipelineviewer.py b/utils/pipelineviewer.py
        # index ef50ed946..ef04c1b7c 100755
        # --- a/utils/pipeline_tk.py
        # +++ b/utils/pipeline_tk.py
        # @@ -348 +348 @@ def parse_io(filenames):
        # -                print(f"   └── {ioput.replace('puts','').upper():3}", end='')
        # +                print(f"   └── {ioput.replace('puts','').upper()} ", end='')

        verbose = False

        self.DIFF = {} # (a,b) = { (start_line, start_count, end_line, end_count) : [[minus], [plus]] }

        for line in proc[STDOUT].split("\n"):

            verbose and print(line)

            if not line:
                continue

            if line.startswith("diff --git"):
                A, B, LINES, index = None, None, None, None
                verbose and print("\033[33mHEADER\033[0m")

            elif line.startswith("---"):
                header = False
                A = line.replace("--- a/", "").replace("---", "")
                verbose and print("\033[33mA\033[0m", A)

            elif line.startswith("+++"):
                B = line.replace("+++ b/", "").replace("+++ ", "")
                self.DIFF[(A,B)] = {}
                verbose and print("\033[33mB\033[0m", B)

            elif line.startswith("@@"):
                # @@ -1,8 +1,9 @@
                # @@ -18,6 +19,8 @@ int cmd_http_fetch(int argc, const char **argv, ...
                # @@ -<start line>,<number of lines> +<start line>,<number of lines> @@ [header].

                LINES = tuple(int(i or 1) for i in re.match('@@ -(\d+),?(\d*) \+(\d+),?(\d*) @@.*', line).groups())
                verbose and print("\033[33mLINES\033[0m", LINES)

                self.DIFF[(A,B)][LINES] = [[], []]
                continue

            if not LINES:
                continue

            if line.startswith("-"):
                index = 0
                self.DIFF[(A,B)][LINES][index].append(line[1:])

            elif line.startswith("+"):
                index = 1
                self.DIFF[(A,B)][LINES][index].append(line[1:])

            elif line == "\ No newline at end of file":
                assert index is not None
                self.DIFF[(A,B)][LINES][index][-1] += "<NO_NEWLINE>"

            else:
                print("\033[31mUNKOWN LINE\033[0m", line)

    def files(self):
        return sorted(list(self.DIFF.keys()))

    def lines(self, files):
        return sorted(list(self.DIFF[files].keys()))

    def diff(self, files, lines):
        return self.DIFF[files][lines]

    @staticmethod
    def format_title(FILES, LINES, DIFF):

        ret = ""
        a,b = FILES
        start_line, start_count, end_line, end_count = LINES
        minus, plus = DIFF

        if a == "/dev/null":
            return Fore.BLUE + a + Fore.RESET + " -> " + Fore.GREEN + "New file" + Fore.RESET

        elif b == "/dev/null":
            return Fore.BLUE + a + Fore.RESET + " -> " + Fore.RED + "File deleted" + Fore.RESET

        elif a == b:
            ret = Fore.BLUE + b + Fore.RESET

        else:
            ret = Fore.RED + a + Fore.RESET + " -> " + Fore.GREEN + b + Fore.RESET

        ret += ":" + str(end_line) + Fore.RESET

        if start_count == 0:
            assert not minus
            assert len(plus) == end_count
            color, n, state = Fore.GREEN, len(plus), "added"

        elif end_count == 0:
            assert not plus
            assert len(minus) == start_count
            color, n, state = Fore.RED, len(minus), "deleted"

        else:
            assert len(plus) == end_count
            assert len(minus) == start_count
            color, n, state = Fore.LIGHTYELLOW_EX, max(len(minus), len(plus)), "modified"

        ret += " " + color + str(n) + " line%s "%('s' if n > 1 else '') + state + Fore.RESET + Back.RESET

        # ret += " " + str((start_line, start_count, end_line, end_count))

        return ret

    @staticmethod
    def format_diff(FILES, LINES, DIFF):

        ret = ""
        a,b = FILES
        start_line, start_count, end_line, end_count = LINES
        minus, plus = DIFF

        if start_count == end_count:

            assert len(minus) == len(plus)

            # https://docs.python.org/3/library/difflib.html#difflib.Differ

            stream = ["", ""]
            for m, p in zip(minus, plus):

                # if m.endswith("<NO_NEWLINE>"):
                #     m = m.replace("<NO_NEWLINE>", "")
                #     p += "<NEWLINE>"

                # if p.endswith("<NO_NEWLINE>"):
                #     p = p.replace("<NO_NEWLINE>", "")
                #     m += "<NEWLINE>"

                # ratio = SequenceMatcher(None, m, p).ratio()

                differ = list(Differ().compare(m, p))
                previous = [["", "", ""], ["", "", ""]] # [MINUS : (Fore, Back, Style), PLUS: (Fore, Back, Style)]
                for d in differ:
                    if d[:2] == "- ":
                        current = [Fore.RED, Back.RESET, Style.BRIGHT] if d[2] != ' ' else [Fore.RESET, Back.RED, Style.BRIGHT]
                        current = [current, None]
                    elif d[:2] == "+ ":
                        current = [Fore.GREEN, Back.RESET, Style.BRIGHT] if d[2] != ' ' else [Fore.RESET, Back.GREEN, Style.BRIGHT]
                        current = [None, current]
                    elif d[:2] == "  ":
                        current = [[Fore.RED, Back.RESET, Style.NORMAL], [Fore.GREEN, Back.RESET, Style.NORMAL]]
                    elif d[:2] == "? ":
                        assert False
                    else:
                        assert False, "Unhandled difference: %s"%repr(d)

                    for j in range(2):
                        curr, prev =  current[j], previous[j]

                        if curr is None:
                            continue

                        if d[2] == "\r":
                            curr[i] = Fore.YELLOW
                            d = (None, None, "\\r")

                        for i in range(3):
                            if curr[i] == prev[i]:
                                curr[i] = ""
                            else:
                                prev[i] = curr[i]
                        stream[j] += "".join(curr) + d[2]

                stream[0] += "\033[0m\n"
                stream[1] += "\033[0m\n"

            ret += stream[0]
            ret += stream[1]
        else:

            def _fmt_diff(lines, fore, back):

                ret = ""

                maxlines = 20
                if len(lines) > maxlines:
                    lines = lines[:maxlines//2] + ["", back + Fore.WHITE + "... %d more line(s) ... "%(len(lines) - maxlines) + Back.RESET, ""] + lines[-maxlines//2:]

                # NB : we print line by line so that each line keeps color while using | less
                for l in lines:
                    if l == "":
                        l = Fore.YELLOW + "\\n"
                    elif l == "\t\n":
                        l = Fore.YELLOW + "\\t"

                    # print(repr(l))

                    if l.endswith("\r\n"):
                        l += Fore.YELLOW + "\\r"




                    # if l.endswith(0xD)

                    # else:
                        # l = l.replace(" ", Fore.YELLOW + "." + fore)
                    # l = repr(l)

                    # if l == "":
                    #     l = "<NEWLINE>"
                    # elif l == "\t\n":
                    #     l = "<TAB>"


                    ret += fore + l + "\033[0m\n"
                return ret

            d = _fmt_diff(minus, Fore.RED, Back.RED)
            ret += _fmt_diff(minus, Fore.RED, Back.RED)
            ret += _fmt_diff(plus, Fore.GREEN, Back.GREEN)


        return ret.strip()

    def print(self):

        total = sum([len(lines) for lines in self.DIFF.values()])
        count = 0

        print(Fore.MAGENTA, "Diff :", Fore.RESET, sep='', end = '')

        if not self.DIFF:
            print(" No diff")
            return
        print()

        for FILES, lines in self.DIFF.items():

            a,b = FILES

            for LINES, DIFF in lines.items():

                count and print(Fore.BLACK, "─"*120, Fore.RESET, sep='')
                count += 1
                print(Fore.CYAN, "[%d/%d]"%(count, total), Fore.RESET, sep='', end = ' ')

                print( self.format_title(FILES, LINES, DIFF) )
                print( self.format_diff(FILES, LINES, DIFF) )


if "sublime" in sys.modules:

    def bash_print(cmd):
        print("bash_proc(cmd=%s)"%str(cmd), args, kwargs)

    bash_proc.print = bash_print

    class NavigateGitDiffCommand(sublime_plugin.TextCommand):

        def __init__(self, args):
            print("*"*50)
            print("INIT NavigateGitDiffCommand")
            super().__init__(args)
            self.phantoms = set() # { git_root : { filename: { view: View, [pantoms] }}
            self.git_diff_index = 0

        def on_file_select(self, file_index):

            print("select file", file_index)

            if file_index == -1 :
                self.reset_phantoms()
                return

            self.current_file_index = file_index

            self.show_lines_panel(file_index)

        def on_file_highlight(self, file_index):
            print("on_file_highlight() file", self.file_keys[file_index])

            item, desc = self.file_items[file_index]
            if desc == "File deleted":
                self.show_files_panel(file_index + 1)
                return

            # self.line_keys = sorted(list(self.diff[self.file_keys[file_index]].keys()))
            line_index = -1
            print("on_file_highlight()", self.current_view.file_name())

            if self.current_view != self.selected_view:
                self.current_view.close()

            self.current_view = self.show_file(file_index, line_index, flags=sublime.SEMI_TRANSIENT)

        def on_line_select(self, line_index):

            if line_index == -1:
                print("on_line_select -> ESC")
                self.show_files_panel(self.current_file_index)
                return

            print("select line", line_index)
            self.current_line_index = line_index
            self.selected_view = self.on_line(line_index)

        def on_line_highlight(self, line_index):
            print("highlight line", line_index)
            self.on_line(line_index, flags=sublime.SEMI_TRANSIENT)

        def on_line(self, line_index, flags=0):
            # print("on_line", self.file_keys[self.current_file_index], self.line_keys[line_index], flags)
            return self.show_file(self.current_file_index, line_index, flags)

        def show_files_panel(self, selected_index=0):
            print("show_files_panel")
            sublime.active_window().show_quick_panel(self.file_items,
                                                        placeholder=self.cmd,
                                                        on_select=self.on_file_select,
                                                        on_highlight=self.on_file_highlight,
                                                        selected_index=selected_index)

        def show_lines_panel(self, file_index):
            print("show_lines_panel, file_index=", file_index)

            line_items = []

            a, b = self.file_keys[file_index]
            current_file = b

            self.line_keys = sorted(list(self.diff[self.file_keys[file_index]].keys()))
            line_index = 0
            min_line = None

            # print("show_lines_panel current_file ->", current_file)
            # print("show_lines_panel self.current_view.file_name() ->", self.current_view.file_name().replace(self.git_root, ""))

            for i, key in enumerate(self.line_keys):

                (start_line, start_count, end_line, end_count) = key
                (minus, plus) = self.diff[self.file_keys[file_index]][key]

                if start_count == 0:
                    assert not minus
                    assert len(plus) == end_count
                    n, state = len(plus), "added"

                elif end_count == 0:
                    assert not plus
                    assert len(minus) == start_count
                    n, state = len(minus), "deleted"

                else:
                    assert len(plus) == end_count
                    assert len(minus) == start_count
                    n, state = max(len(minus), len(plus)), "modified"

                line_items += [("%s - Line %d"%(current_file, end_line), "%d line%s %s"%(n, 's' if n > 1 else '', state))]

                if self.current_view.sel():
                    region = self.current_view.sel()[0]
                    point = region.begin()
                    row = self.view.rowcol(point)[0]+1 # 0-based line
                    if min_line is None or abs(end_line - row) < min_line:
                        min_line = abs(end_line - row)
                        line_index = i

            print("show_lines_panel line_index ->", i, "/", len(line_items))
            sublime.active_window().show_quick_panel(line_items,
                                                        placeholder=self.cmd,
                                                        on_select=self.on_line_select,
                                                        on_highlight=self.on_line_highlight,
                                                        selected_index=line_index)

        def on_git_diff_select(self, index):

            if index == -1:
                self.reset_phantoms()
                return

            filename = sublime.active_window().active_view().file_name()

            self.git_diff_index = index

            git_ref = self.diff_items[index][0]

            if git_ref == "custom":
                git_ref = "tp-6221"

            self.diff = GitDiff(git_ref, cwd=self.git_root).DIFF

            self.cmd = "git diff %s -U0"%git_ref

            self.file_keys = sorted(list(self.diff.keys()))

            self.file_items = []
            self.current_file_index = 0
            self.line_keys = None

            for key in self.file_keys:

                (a,b) = key
                lines = self.diff[key]

                if a == "/dev/null":
                    self.file_items += [(b, "New file")]

                elif b == "/dev/null":
                    self.file_items += [(a, "File deleted")]

                elif a == b:
                    self.file_items += [(a, "File modified (%d)"%len(lines))]

                else:
                    self.file_items += [(b, "File renamed (%s) and modified (%d)"%(a, len(lines)))]


            for i, item in enumerate(self.file_items):
                if filename.replace(self.git_root, "") == item[0]:
                    self.current_file_index = i
                    self.show_lines_panel(i)
                    return

            self.show_files_panel()

        def show_git_diff_panel(self):
            print("show_git_diff_panel")

            self.diff_items = []

            def add_git_ref(ref):
                self.diff_items += [(ref, bash_cmd("git rev-list --left-right --count %s...HEAD"%ref, cwd=self.git_root)[STDOUT])]

            add_git_ref("origin/master")
            add_git_ref("HEAD^")
            add_git_ref(bash_cmd("git merge-base origin/master HEAD", cwd=self.git_root)[STDOUT])

            self.diff_items += [("custom", "")]

            sublime.active_window().show_quick_panel(self.diff_items,
                                                        placeholder="Git diff <...> -U0",
                                                        on_select=self.on_git_diff_select,
                                                        selected_index=self.git_diff_index)

        def show_file(self, file_index, line_index, flags=0):

            a, b = self.file_keys[file_index]
            if b == "/dev/null":
                return

            filename = b

            print("show file", filename, "line_index =", line_index)

            print("Open files:")
            print("-", "\n - ".join([v.file_name() for v in sublime.active_window().views()]))

            filename = os.path.join(self.git_root, filename)

            if filename!=sublime.active_window().active_view().file_name():
                self.current_view = sublime.active_window().open_file(filename, flags=flags)
            else:
                self.current_view = sublime.active_window().active_view()

            def inner_show_file():

                if self.current_view.is_loading():
                    print("view is loading...")
                    sublime.set_timeout(lambda: inner_show_file(), 10)
                    return

                self.show_line(self.current_view, line_index)
                self.show_diff(self.current_view, file_index)

            inner_show_file()
            return self.current_view

        def reset_phantoms(self):
            print("reset phantoms")

            print(sublime.active_window().views())

            print(self.phantoms)
            print(self.current_view)

            if self.current_view.id() in self.phantoms:
                print("REMOVE PHANTOMS")
                self.phantoms.remove(self.current_view.id())
                self.current_view.erase_phantoms("gitdiff")

            print("self.current_view", self.current_view, self.current_view.file_name())
            print("self.selected_view", self.selected_view, self.selected_view.file_name())
            if self.current_view != self.selected_view:
                print(" -> CLOSE")
                self.current_view.close()

        def show_diff(self, view, file_index):

            print("show diff")

            file_key = self.file_keys[file_index]
            a, b = file_key

            print(view)
            if view.id() in self.phantoms:
                print("REMOVE PHANTOMS")
                self.phantoms.remove(view.id())
                view.erase_phantoms("gitdiff")

            for (start_line, start_count, end_line, end_count), (minus, plus) in self.diff[file_key].items():

                content = """
                        <div style='background-color: #2F0B3A'>
                        """

                def escape(txt):
                    if txt == "":
                        return "<NEWLINE>"
                    return html.escape(txt).replace(" ", "&nbsp;")


                if start_count == end_count:

                    # print("Smart Diff !!")

                    # https://docs.python.org/3/library/difflib.html#difflib.Differ
                    for m, p in zip(minus, plus):

                        if m.endswith("<NO_NEWLINE>"):
                            m = m.replace("<NO_NEWLINE>", "")
                            p += "<NEWLINE>"

                        if p.endswith("<NO_NEWLINE>"):
                            p = p.replace("<NO_NEWLINE>", "")
                            m += "<NEWLINE>"

                        current_color = None
                        current_text = ""

                        for differ in list(Differ().compare(m, p)):
                            # print(repr(differ), end="")
                            if differ[:2] == "- ":
                                color = "background-color: red;"
                            elif differ[:2] == "+ ":
                                color = "background-color: green;"
                            elif differ[:2] == "  ":
                                color = "color: dark-gray;"
                            elif differ[:2] == "? ":
                                color = "background-color: yellow;"
                            else:
                                assert False, "Unhandled difference: %s"%repr(differ)

                            if color != current_color:
                                if current_text:
                                    content += "<code style='%s display: inline;'>%s</code>"%(current_color, escape(current_text))

                                current_color = color
                                current_text = ""

                            current_text += differ[2]

                        if current_text:
                            content += "<code style='%s display: inline;'>%s</code><br>"%(current_color, escape(current_text))
                        # print()

                else:

                    content += "<pre><code style='color: red;'>"
                    content += "<br>".join([escape(m) for m in minus])
                    content += "</code></pre>"
                    # if minus and plus:
                    #     content += "<br>"
                    # content += "<pre><code style='color: green;'>"
                    # content += "<br>".join([escape(p) for p in plus])
                    # content += "</code></pre>"

                    for i,p in enumerate(plus):
                        row = end_line + i - 1
                        col = 0
                        pt = view.text_point(row, col)
                        view.add_phantom("gitdiff", sublime.Region(pt, pt), "<div style='background-color: green; display: inline;'>+</div>", sublime.LAYOUT_INLINE)

                content += "</div>"

                # print(content)

                row = end_line + end_count - 1 - 1
                col = 0
                pt = view.text_point(row, col)
                view.add_phantom("gitdiff", sublime.Region(pt, pt), content, sublime.LAYOUT_BLOCK)

                if view.id() not in self.phantoms:
                    print("ADD PHANTOMS", view.id())
                    self.phantoms.add(view.id())

        def show_line(self, view, line_index):

            if self.line_keys:
                print("show line", view, "line_index=", line_index, "self.line_keys[line_index] ->", self.line_keys[line_index])
            else:
                print("show line", view, "line_index=", line_index, "self.line_keys=", self.line_keys)

            if view is None:
                print("NO VIEW")

            if line_index == -1 and self.current_view.sel():
                region = self.current_view.sel()[0]
                point = region.begin()
                row = self.current_view.rowcol(point)[0]+1 # 0-based line
                print("show_line()", "view row =", row)
                # print("show_line()", "view row =", row, "diff row =", end_line + end_count - 1)

                print(self.current_file_index)
                print(self.file_keys[self.current_file_index])

                key = self.file_keys[self.current_file_index]

                line_keys = sorted(list(self.diff[key].keys()))
                line_index = 0
                min_diff_row = None
                for i, (start_line, start_count, end_line, end_count) in enumerate(line_keys):
                    # current_diff_row = end_line + end_count -1
                    current_diff_row = end_line -1
                    # print("Try", current_diff_row)
                    if min_diff_row is None or abs(current_diff_row - row) < min_diff_row:
                        # print("New row match !!")
                        min_diff_row = abs(current_diff_row - row)
                        diff_row = current_diff_row
            else:
                start_line, start_count, end_line, end_count = self.line_keys[line_index]
                # diff_row = end_line + end_count - 1
                diff_row = end_line - 1

            print("show line - diff_row=", diff_row)

            self.current_view.sel().clear()
            pt=self.current_view.text_point(diff_row, 0)
            self.current_view.sel().add(pt)
            self.current_view.show_at_center(pt, animate=True)

        def run(self, edit):

            try:
                print("-"*50)
                print("NavigateGitDiffCommand : RUN")

                filename = sublime.active_window().active_view().file_name()

                self.selected_view = self.view
                self.current_view = self.selected_view

                proc = bash_cmd("git rev-parse --show-toplevel", cwd=os.path.dirname(filename))
                if proc[RETURNCODE]!=0:
                    sublime.message_dialog("Not in git repo !")
                    return
                self.git_root = proc[STDOUT] + "/"

                print("git_root=%s"%self.git_root)
                self.show_git_diff_panel()

            except Exception as e:
                print("run() Exception:")
                print(traceback.format_exc())


else:

    from pynput import keyboard
    from pyfiglet import figlet_format
    import logging
    from colorama import Fore
    import tty
    import termios

    class ColorFormatter(logging.Formatter):

        COLOR_FMT = {
            logging.DEBUG: Fore.CYAN,
            logging.INFO: Fore.BLUE,
            logging.WARNING: Fore.YELLOW,
            logging.ERROR: Fore.RED,
            logging.CRITICAL: Fore.RED
        }

        def format(self, record):
            # https://docs.python.org/3/library/logging.html#logrecord-attributes
            return logging.Formatter(self.COLOR_FMT[record.levelno] + "[%(asctime)s][%(levelname)s]" + Fore.RESET + " %(message)s" + Fore.LIGHTYELLOW_EX + "\t[%(pathname)s:%(lineno)s]" + Fore.RESET).format(record)

    LOGFILE = "/tmp/sublime_git_diff.log"

    # https://code-maven.com/catch-control-c-in-python
    # signal.signal(signal.SIGINT, handler)

    class AlternateScreen:

        ANSI_REGEX = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

        def __enter__(self):
            self.old_attrs = termios.tcgetattr(sys.stdin)
            tty.setcbreak(sys.stdin)
            print("\033[?1049h")
            self.clear()

            return self

        def __exit__(self, exc_type, exc_val, exc_tb):

            print(f"exc_type={exc_type}")
            if exc_type:
                print(f"exc_val={exc_val}")
                print(f"exc_tb={exc_tb}")

                print("".join(traceback.format_tb(exc_tb)))

                logging.error("".join(traceback.format_tb(exc_tb)))

                print("See logs at:", LOGFILE)

                os.read(sys.stdin.fileno(), 10)

            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_attrs)
            print("\033[?1049l", end='')

        def clear(self):
            print("\033[H") # move TOP LEFT
            print("\033[2J", end='') # clear screen
            self.current_line = 0
            self.terminal_size = os.get_terminal_size()
            # self.buffer = [""] * self.terminal_size.lines
            self.buffer_left = [""] * (self.terminal_size.lines - 1)
            self.buffer_right = [""] * (self.terminal_size.lines - 1)

        def __refresh__(self, name, old_buffer, new_buffer, printline=False, x_offset=0):

            logging.info(f"Screen {name} refresh {len(old_buffer)} / {len(new_buffer)}")

            if len(new_buffer) < len(old_buffer):
                logging.debug(f"Append buffer {name} {len(old_buffer) - len(new_buffer)} empty(lines)")
                new_buffer += [""] * (len(old_buffer)-len(new_buffer))

            elif len(new_buffer) > len(old_buffer):
                logging.warning(f"Shorten buffer {name} {len(new_buffer) - len(old_buffer)}")
                new_buffer = new_buffer[:len(old_buffer)]

            maxwidth = self.terminal_size.columns // 2 - printline*3

            # with open("/tmp/" + name + "_buffer.txt", "w") as f:
            #     for i, (old, new) in enumerate(zip(old_buffer, new_buffer)):
            #         ansi_new = self.ANSI_REGEX.sub("", new)
            #         ansi_old = self.ANSI_REGEX.sub("", old)
            #         f.write(f"[{i:2}] ")
            #         # f.write(" len=")
            #         # f.write(str(len(ansi_old)))
            #         # f.write(" ")
            #         f.write(repr(ansi_old))
            #         f.write(" " * (maxwidth-len(ansi_old)))
            #         f.write(" -> ")
            #         f.write("len=")
            #         f.write(str(len(new)))
            #         f.write(" ")
            #         # f.write(repr(ansi_new))
            #         f.write(repr(new))
            #         f.write("\n")

            x_clearline_offset = x_offset + (3 if x_offset else 2) * printline

            for i, (old, new) in enumerate(zip(old_buffer, new_buffer)):

                if new == "<SCREEN_LINE>":
                    new = Fore.BLACK + "─"*(maxwidth-printline*3) + Fore.RESET

                ansi_new = self.ANSI_REGEX.sub("", new)
                ansi_old = self.ANSI_REGEX.sub("", old)

                if old!=new:
                    # logging.debug(f"{name} - write line {i} : {repr(ansi_new)}")

                    if len(ansi_new) > maxwidth:
                        # new = ansi_new[:maxwidth]
                        # new = repr(new)[:maxwidth]
                        # new = new[:maxwidth-3] + "...\033[0m"

                        iutf = 0
                        iansi = 0
                        while iansi < maxwidth-3:
                            if new[iutf] == '\x1b':
                                while new[iutf] != 'm':
                                    iutf+=1
                            iutf+=1
                            iansi+=1
                        new = new[:iutf] + " " + Fore.LIGHTYELLOW_EX + " +" + str(len(ansi_new)-iansi+3) + "\033[0m"

                    assert not "\n" in new
                    old_buffer[i] = new
                    linenumber = ("\033[0m" + Fore.CYAN + f"{i:2} " + Fore.RESET) if printline else ""

                    # print(f"\033[{i}H\033[K", linenumber, b, sep="") # move to line + clear line
                    print(f"\033[{i+1};{x_clearline_offset}H", " " * maxwidth, sep="") # clearline
                    print(f"\033[{i+1};{x_offset}H", linenumber, new, sep="") # move to line


                # else:
                #     logging.debug(f"{name} - skip line {i} : {repr(ansi_new)}")
            self.status(f"Terminal size : {self.terminal_size.columns} x {self.terminal_size.lines}, maxwidth = {maxwidth}")

        def refresh_left(self, buffer):
            self.__refresh__("left", self.buffer_left, buffer, printline=True)

        def refresh_right(self, buffer):
            # x_offset = max([len(self.ANSI_REGEX.sub("", line)) for line in self.buffer_left])
            x_offset = self.terminal_size.columns // 2 + self.terminal_size.columns % 2
            self.__refresh__("right", self.buffer_right, buffer, printline=True, x_offset=x_offset)
            print(f"\033[{self.terminal_size.lines};0H", end="") # move to BR
            sys.stdout.flush()

        # def refresh(self, buffer):
        #     logging.info("Screen refresh %d / %d", len(buffer), len(self.buffer))
        #     assert len(buffer) == len(self.buffer), f"Buffer size mismatch: {len(buffer)} / {len(self.buffer)}"

        #     for i, (a,b) in enumerate(zip(self.buffer, buffer)):
        #         if a!=b:
        #             assert not "\n" in b
        #             self.buffer[i] = b

        #             linenumber = "\033[0m" + Fore.CYAN + f"{i:2} " + Fore.RESET
        #             # linenumber = ""

        #             # if i> 15 and i < 30:
        #             # print(f"\033[{i}H\033[K", linenumber, b, "  ", repr(b), sep="") # move to line + clear line
        #             print(f"\033[{i}H\033[K", linenumber, b, sep="") # move to line + clear line



        def status(self, message):
            print(f"\033[{self.terminal_size.lines}H\033[K", message, sep="", end="") # move to line + clear line
            print(f"\033[{self.terminal_size.lines};{self.terminal_size.columns}H", "@", sep="", end="") # move to line + clear line
            print(f"\033[{self.terminal_size.lines};{self.terminal_size.columns // 2}H", "@", sep="", end="") # move to line + clear line
            print(f"\033[{self.terminal_size.lines-1}H", "@"*(self.terminal_size.columns-1), sep="", end="") # move to line + clear line
            sys.stdout.flush()

        # https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html

        # # cursor positioning
        # ESC [ y;x H     # position cursor at x across, y down
        # ESC [ y;x f     # position cursor at x across, y down
        # ESC [ n A       # move cursor n lines up
        # ESC [ n B       # move cursor n lines down
        # ESC [ n C       # move cursor n characters forward
        # ESC [ n D       # move cursor n characters backward

        # # clear the screen
        # ESC [ mode J    # clear the screen

        # # clear the line
        # ESC [ mode K    # clear the line

    class SublimeGitDiff:

        def __init__(self, git_target, cwd=os.getcwd()):

            self.git_target = git_target
            self.cwd = cwd

            self.reverted_diff = []

            self.ctrl_modifier = False
            self.DIFF = []

            self.current_modified_files_index = None
            self.current_files_index = None
            self.current_lines_index = -1
            self.buffer_git_diff_line = None
            self.gitlab = None
            self.gitlab_url = None

            self.short_title = False
            self.short_status = False
            self.short_history = False
            self.short_gitlab = False
            self.short_modified = False

            # TODO : async file compare date  + self.last_cmd_date

            self.bash_cmd = ""
            def bash_print(cmd):
                if isinstance(cmd, list):
                    return bash_print(" ".join(cmd))
                self.bash_cmd = Fore.BLACK + cmd + Fore.RESET
            bash_proc.print = bash_print

        def run(self):

            with AlternateScreen() as screen:
                self.screen = screen
                self.init()

                # https://nitratine.net/blog/post/how-to-make-hotkeys-in-python/
                # https://pynput.readthedocs.io/en/latest/keyboard.html
                # https://stackoverflow.com/questions/15992651/python-filtered-line-editing-read-stdin-by-char-with-no-echo

                # import select
                # r, w, e = select.select([ f ], [], [], 0)
                # if f in r:
                #   print os.read(f.fileno(), 50)
                # else:
                #   print "nothing available!"  # or just ignore that case

                while True:
                    char = os.read(sys.stdin.fileno(), 10)
                    if not self.on_keypress(char.decode()):
                        break

        def on_keypress(self, key):

            # from getkey.unikeys import UnicodeAsciiKeys
            # /home/fdutrech/.local/lib/python3.8/site-packages/getkey/unikeys.py
            # /home/fdutrech/.local/lib/python3.8/site-packages/getkey/keynames.py

            ESCAPE = '\x1b'
            DEL = '\x1b[3~'
            UP = '\x1b[A'
            DOWN = '\x1b[B'
            RIGHT = '\x1b[C'
            LEFT = '\x1b[D'
            CTRL_UP = '\x1b[1;5A'
            CTRL_DOWN = '\x1b[1;5B'
            CTRL_RIGHT = '\x1b[1;5C'
            CTRL_LEFT = '\x1b[1;5D'
            PAGE_UP = '\x1b[5~'
            PAGE_DOWN = '\x1b[6~'

            F5 = '\x1b[15~'

            if key in ['q', ESCAPE]:
                return False

            elif key in [DOWN, RIGHT]:
                self.update_current_lines_index(1)

            elif key in [CTRL_DOWN, CTRL_RIGHT, PAGE_DOWN]:
                self.update_modified_files_index(1)

            elif key in [UP, LEFT]:
                self.update_current_lines_index(-1)

            elif key in [CTRL_UP, CTRL_LEFT, PAGE_UP]:
                self.update_modified_files_index(-1)

            elif key in ['e', '\n']:
                self.edit()

            elif key in ['c', '\n']:
                self.copy()

            elif key in ['p']:
                self.copy_path()

            elif key in ['r', F5]:
                self.init(self.modified_files[self.current_modified_files_index], self.current_lines_index)

            elif key in ['d', DEL]:
                self.undo()

            elif key in ['g']:
                if self.gitlab_url:
                    bash_cmd(f"xdg-open {self.gitlab_url}")

            else:
                self.screen.status("KEY PRESS: " + repr(key))
                # print("KEY PRESS: " + repr(key))

            return True

        def update_current_lines_index(self, offset):

            logging.info(f"update_current_lines_index({offset=})")

            self.current_lines_index += offset

            if self.LINES:

                if self.current_lines_index == len(self.LINES):
                    return self.update_modified_files_index(1)

                elif self.current_lines_index == -1:
                    return self.update_modified_files_index(-1)

            self.refresh()

        def update_modified_files_index(self, offset):
            logging.info(f"update_modified_files_index({offset=})")
            self.set_modified_files_index(self.current_modified_files_index + offset)

        def find_files_index(self, filename):
            for i, (a,b) in enumerate(self.FILES):
                if b == filename:
                    return i
            return None

        def find_lines(self, filename):
            try:
                index = self.find_files_index(filename)
                FILES = self.FILES[index]
                return self.git_diff.lines(FILES)
            except Exception as e:

                logging.warning(f"An exception occured while trying to get LINES for {filename=}")
                return []

                # print(f"{self.FILES}")
                # print(f"{filename=}")
                # print(f"{index=}")
                # raise e

        def set_modified_files_index(self, index, current_lines_index_hint=None):

            logging.info(f"set_modified_files_index({index=}, {current_lines_index_hint=})")

            current_modified_files_index = self.current_modified_files_index

            self.current_modified_files_index = index
            self.current_modified_files_index += len(self.modified_files)
            self.current_modified_files_index %= len(self.modified_files)

            self.current_files_index = self.find_files_index(self.modified_files[self.current_modified_files_index])

            FILES = self.FILES[self.current_files_index]

            self.LINES = self.git_diff.lines(FILES)

            if current_lines_index_hint:
                self.current_lines_index = min(current_lines_index_hint, len(self.LINES) -1)
                # self.current_lines_index = current_lines_index_hint % len(self.LINES)
            else:
                if current_modified_files_index is not None and current_modified_files_index > self.current_modified_files_index:
                    self.current_lines_index = len(self.LINES) - 1
                else:
                    self.current_lines_index = 0

            self.DIFF = []
            for i, lines in enumerate(self.LINES):
                diff = self.git_diff.diff(FILES, lines)
                self.DIFF += [(GitDiff.format_title(FILES, lines, diff), GitDiff.format_diff(FILES, lines, diff))]

            if current_modified_files_index != self.current_modified_files_index:
                self.refresh()

        def undo(self):
            FILES = self.FILES[self.current_files_index]
            LINES = self.LINES[self.current_lines_index]
            DIFF = self.git_diff.diff(FILES, LINES)

            a, b = FILES
            start_line, start_count, end_line, end_count = LINES
            minus, plus = DIFF

            minus = [m + "\n" for m in minus]
            plus = [p + "\n" for p in plus]

            logging.info(f"Undo at {b}:{end_line}")

            logging.info(f"{a}:{start_line},{start_count} {b}:{end_line},{end_count}")
            logging.info(f"{repr(minus)=}")
            logging.info(f"{repr(plus)=}")

            with open(os.path.join(self.git_root, b), "r+") as f:
                lines = f.readlines()

                f.seek(0)
                f.truncate()

                for i in range(end_count):
                    logging.info("  - Remove line %d : %s", end_line+i-1, lines[end_line+i-1].rstrip("\n"))
                    # assert lines[end_line+i-1] == plus[i], f"\n{repr(lines[end_line+i-1])}\n{repr(plus[i])}\n{b}:{end_line}"

                for i in range(start_count):
                    logging.info("  - Add line %d : %s", end_line+i-1, minus[i])


                if plus:
                    logging.info(f" => lines[:{end_line-1}] + minus (len({len(minus)})) + lines[{end_line + end_count - 1}:]")
                    lines = lines[:end_line-1] + minus + lines[end_line + end_count - 1:]
                else:
                    lines = lines[:end_line] + minus + lines[end_line:]


                if lines[-1].endswith("<NO_NEWLINE>\n"):
                    lines[-1] = lines[-1].replace("<NO_NEWLINE>\n", "")

                f.writelines(lines)

            self.init(current_modified_file_hint=b, current_lines_index_hint=self.current_lines_index)

        def edit(self):
            FILES = self.FILES[self.current_files_index]
            LINES = self.LINES[self.current_lines_index]
            DIFF = self.git_diff.diff(FILES, LINES)

            a, b = FILES
            start_line, start_count, end_line, end_count = LINES
            m, p = DIFF

            # import pyperclip
            # pyperclip.copy("\n".join(m))

            subprocess.run(["subl", f"{os.path.join(self.git_root, b)}:{end_line}"])

        def copy_path(self):
            FILES = self.FILES[self.current_files_index]
            LINES = self.LINES[self.current_lines_index]
            DIFF = self.git_diff.diff(FILES, LINES)

            a, b = FILES
            start_line, start_count, end_line, end_count = LINES
            m, p = DIFF

            import pyperclip
            pyperclip.copy(b)

            self.screen.status(f"Path '{b}' copied")

        def copy(self):
            FILES = self.FILES[self.current_files_index]
            LINES = self.LINES[self.current_lines_index]
            DIFF = self.git_diff.diff(FILES, LINES)

            a, b = FILES
            start_line, start_count, end_line, end_count = LINES
            m, p = DIFF

            import pyperclip
            pyperclip.copy("\n".join(m))

            self.screen.status(f"{len(m)} MINUS line(s) copied")

        def refresh(self):
            logging.info("Refresh()")

            double_buffer = True
            # double_buffer = False

            if double_buffer:

                left, right = self.format2()

                #
                # Left screen size check
                #

                a = len(left)
                b = len(self.screen.buffer_left)

                if a > b:

                    if not self.short_title:
                        self.short_title = True
                        return self.refresh()

                    if not self.short_history:
                        self.short_history = True
                        return self.refresh()

                    if not self.gitlab:
                        self.gitlab = True
                        return self.refresh()

                    if not self.gitlab:
                        self.gitlab = True
                        return self.refresh()

                #
                # Right screen size check
                #

                a = len(right)
                b = len(self.screen.buffer_right)

                if a > b:
                    logging.info(f"Truncate buffer[{a}] to {b}")
                    last_line = self.git_diff_current_end
                    if last_line < b:
                        right = right[:b]
                    else:
                        right = right[last_line - b:last_line]
                # elif a < b:

                #     if self.current_modified_files_index is not None:
                #         right = [""]*((b-a)//2) \
                #                 + [Fore.BLACK + "<- " + self.modified_files[self.current_modified_files_index-1] + Fore.RESET] \
                #                 + right \
                #                 + [Fore.BLACK + "-> " + self.modified_files[(self.current_modified_files_index+1)%len(self.modified_files)] + Fore.RESET]
                #     else:
                #         right = [""]*((b-a)//2) + right

                self.screen.refresh_left(left)
                self.screen.refresh_right(right)

            else:


                lines = self.format()

                a = len(lines)
                b = len(self.screen.buffer)

                if a > b:
                    logging.info(f"Truncate buffer[{a}] to {b}")

                    last_line = self.git_diff_begin + self.git_diff_current_end

                    if last_line < b:
                        lines = lines[:b]
                    else:
                        lines = lines[last_line - b:last_line]

                elif a < b:
                    logging.info(f"Append {b-a} empty line(s)")
                    lines += [""] * (b-a)

                self.screen.refresh(lines)

        def init(self, current_modified_file_hint=None, current_lines_index_hint=None):
            self.screen.clear()

            self.git_diff = GitDiff(self.git_target, self.cwd)

            stdout, stderr, returncode = bash_cmd("git rev-parse --show-toplevel", cwd=self.cwd)
            self.git_root = stdout if returncode == 0 else None
            logging.info(f"Git root : {self.git_root} (returncode={repr(returncode)})")

            self.FILES = self.git_diff.files()

            self.set_modified_files()

            if self.modified_files:
                try:
                    logging.debug(f"Init hint : {current_modified_file_hint=}, {current_lines_index_hint=}")
                    index = self.modified_files.index(current_modified_file_hint)
                    logging.debug(f"Init hint : {index=}")
                except Exception as e:
                    logging.debug(str(e))
                    index = 0
                self.set_modified_files_index(index, current_lines_index_hint=current_lines_index_hint)

            self.refresh()

        def set_modified_files(self):

            def populate_tree(status, keys, tree = {}):
                """recursively parse file path to populate dict"""
                if len(keys) == 1:
                    tree['__files__'] = tree.get('__files__', []) + [(keys[0], status)]
                else:
                    tree[keys[0]] = populate_tree(status, keys[1:], tree.get(keys[0], {}))
                return tree

            self.modified_tree = {}

            git_diff_status = bash_cmd(f"git diff {self.git_target} --name-status")[STDOUT]

            logging.info("set_modified_files()")
            logging.info(git_diff_status)

            for line in git_diff_status.split("\n"):

                if not line:
                    continue

                fields = line.split("\t")
                if fields[0] in ['M', 'A']:
                    populate_tree(fields[0], fields[1].split('/'), self.modified_tree)
                elif fields[0].startswith("R"):
                    # populate_tree(fields[0], fields[2].split('/'), self.modified_tree)
                    pass # IGNORE RENAMED FILES
                elif fields[0].startswith("D"):
                    # deleted files
                    pass
                else:
                    assert False, f"Unhandled git status line : {repr(line)}"

            def populate_list(data, path = ""):
                ret = []
                for file, status in sorted(data.get("__files__", [])):
                    if status in ['M', 'A']:
                        ret.append(os.path.join(path, file))
                for key in sorted(list(data.keys())):
                    if key != "__files__":
                        ret += populate_list(data[key], os.path.join(path, key))
                return ret


            self.modified_files = populate_list(self.modified_tree)

            # logging.debug(json.dumps(self.modified_tree, indent=2))
            logging.debug("self.modified_files : \n - " + "\n - ".join([l for l in self.modified_files]) )
        # def format(self):

        #     self.fmt = []
        #     self.fmt += self.format_line()           # ------------
        #     self.fmt += self.format_title()          # TITLE
        #     self.fmt += self.format_line()           # ------------
        #     self.fmt += self.format_history()        # HISTORY
        #     self.fmt += self.format_line()           # ------------
        #     self.fmt += self.format_modified_files() # MODIFIED FILES
        #     self.fmt += self.format_line()           # ------------
        #     self.fmt += self.format_git_diff()       # GIT DIFF

        #     return self.fmt

        def format2(self):

            self.fmt = []
            self.fmt += self.format_line()           # ------------
            self.fmt += self.format_title()          # TITLE
            self.fmt += self.format_line()           # ------------

            if self.git_root is None:
                return self.fmt + [Fore.RED + f"You're not in git repository (cwd={os.getcwd()})" + Fore.RESET], []

            self.fmt += self.format_history()        # HISTORY
            self.fmt += self.format_line()           # ------------
            self.fmt += self.format_status()         # STATUS
            # if not self.working_dir_clean:
            self.fmt += self.format_line()           # ------------
            self.fmt += self.format_modified_files() # MODIFIED FILES
            self.fmt += self.format_line()           # ------------
            self.fmt += self.format_gitlab()         # GITLAB
            self.fmt += self.format_line()           # ------------
            self.fmt += self.format_pipeline()       # PIPELINE
            self.fmt += self.format_line()           # ------------

            # self.fmt += self.format_git_diff()       # GIT DIFF

            return self.fmt, self.format_line() + self.format_git_diff()

        def format_line(self):
            return ["<SCREEN_LINE>"]

        def format_title(self):

            if self.short_title:
                return []

            cwd = os.getcwd()
            title = figlet_format("Sublime Git Diff").split("\n")

            title = title[:-1]

            return ["\t" + Fore.CYAN + t + Fore.RESET for t in title]

        def format_status(self):

            if self.short_status:
                return [Fore.MAGENTA + "Status : " + Fore.RESET + "..."]

            git_branch = bash_cmd("git rev-parse --abbrev-ref HEAD")[STDOUT]
            git_origin_branch = "origin/" + git_branch

            git_head_sha1 = bash_cmd("git rev-parse HEAD")[STDOUT]
            git_branch_sha1 = bash_cmd("git rev-parse " + git_branch)[STDOUT]
            git_origin_branch_sha1, _, has_origin_branch = bash_cmd("git rev-parse " + git_origin_branch)

            # targets = []
            # targets += [("origin/master", "master", 0, 0)]
            # targets += [("origin/master", "HEAD", 0, 1)]
            # targets += [("origin/master", git_branch, 0, 1)]
            # if self.git_target != "origin/master":
            #     targets += [(self.git_target, "HEAD", 0, 1)]

            ret = [f" * Branch : {git_branch} ({git_branch_sha1[:8]})" ]

            if git_head_sha1 == git_branch_sha1:
                ret[0] += Fore.GREEN + " HEAD" + Fore.RESET
            else:
                ret += [f" * HEAD : {git_branch} ({git_branch_sha1[:8]})"  + Back.RED + " ????? " + Back.RESET ]

            if has_origin_branch == 0:

                if git_origin_branch_sha1 == git_branch_sha1:
                    ret[0] += Fore.GREEN + " " + git_origin_branch + Fore.RESET
                else:
                    # targets += [(git_origin_branch, "HEAD", 0, 0)]
                    # targets += [("origin/master", git_origin_branch, 0, 1)]
                    l, r = bash_cmd(f"git rev-list --left-right --count {git_origin_branch}...{git_branch}", cwd=self.cwd)[STDOUT].split("\t")
                    ret += [f" * Origin: {git_origin_branch} ({git_origin_branch_sha1[:8]}) " + Back.RED + f"DIVERGED ({l}/{r})" + Back.RESET ]

            else:
                ret += [f" * Origin: " + Fore.BLACK + "None " + Fore.RESET]

            # for left, right, expected_l, expected_r in targets:
            #     l, r = bash_cmd(f"git rev-list --left-right --count {left}...{right}", cwd=self.cwd)[STDOUT].split("\t")
            #     ret += [self.bash_cmd]
            #     ret[-1] += " -> " + (Fore.GREEN if (int(l), int(r)) == (expected_l, expected_r) else Back.RED) + f"{l} {r}" + "\033[0m"

            # l, r = bash_cmd(f"git rev-list --left-right --count origin/master...{git_branch}", cwd=self.cwd)[STDOUT].split("\t")
            # if l == 0:
            #     ret[0] += Back.GREEN + "REBASED" + Back.RESET
            # else:
            #     ret[0] += Back.RED + "DIVERGED origin/master" + Back.RESET



            stdout, _, _ = bash_cmd("git status --porcelain")

            self.untracked_files = []
            self.dirty_files = []
            self.renamed_files = []
            unknown_files = []


            if not stdout:
                self.working_dir_clean = True
                ret += [Fore.GREEN + " Working directory is clean ! " + Fore.RESET + " (everything is commited)"]

            else:

                for status in stdout.split("\n"):

                    if not status:
                        continue

                    split = status.split()
                    flag, file = split[0], split[-1]

                    if flag == "??":
                        self.untracked_files.append(file)
                    elif flag == "M":
                        self.dirty_files.append(file)
                    elif flag == "R":
                        self.renamed_files.append(file)
                    else:
                        unknown_files.append(status)

                self.working_dir_clean = False
                color = Fore.RED if self.untracked_files else Fore.YELLOW
                ret += [color + " Working directory is dirty !" + Fore.RESET]
                if self.untracked_files:
                    ret += [Fore.MAGENTA + "  Untracked files :" + Fore.RESET]
                    ret += [ "  - " + f for f in self.untracked_files]
                if self.dirty_files:
                    ret += [Fore.MAGENTA + "  Modified files :" + Fore.RESET]
                    ret += [ "  - " + f for f in self.dirty_files]
                if self.renamed_files:
                    ret += [Fore.MAGENTA + "  Renamed files :" + Fore.RESET]
                    ret += [ "  - " + f for f in self.renamed_files]

            if unknown_files:
                ret += [Fore.RED + "  Untracked files :" + Fore.RESET]
                ret += [ Fore.RED + "  - " + f + Fore.RESET for f in unknown_files]


            return [Fore.MAGENTA + "Status :" + Fore.RESET] + ret

        def format_gitlab(self):

            if self.short_gitlab:
                return [Fore.MAGENTA + "Gitlab : " + Fore.RESET + "..."]

            ret = [Fore.MAGENTA + "Gitlab :" + Fore.RESET]

            if self.gitlab is None:
                import threading

                def fetch_gitlab():

                    # git_branch, _, _ = bash_cmd("git rev-parse --abbrev-ref HEAD")
                    # git_origin, _, _ = bash_cmd("git config --get remote.origin.url")

                    # import gitlab
                    # GITLAB_TOKEN=os.environ.get('GITLAB_TOKEN', 'V5XKkR28zu3zooUkWgH7')
                    # gl = gitlab.Gitlab(url="https://gitlab.ateme.net/", private_token=GITLAB_TOKEN)
                    # gl.auth()

                    # def pythonify(obj):
                    #     return eval(str(obj).split(" => ")[1])

                    # def dump(obj):
                    #     print(json.dumps(pythonify(obj), indent=2))

                    # project = gl.projects.get("sw/TitanProcessing")

                    # for mr in project.mergerequests.list(iterator=True, source_branch="tp-7806"):



                    #     dump(mr)
                    # # mr = project.mergerequests.get(mr_iid)

                    #     for pipeline in mr.pipelines.list(iterator=True):
                    #         dump(pipeline)
                    #         break

                    # sys.exit(0)





                    import time
                    GITLAB_TOKEN=os.getenv('GITLAB_TOKEN')

                    git_branch, _, _ = bash_cmd("git rev-parse --abbrev-ref HEAD")
                    git_origin, _, _ = bash_cmd("git config --get remote.origin.url")

                    git_origin = git_origin.split(":")[1]

                    # curl --silent --connection-timeout --header "PRIVATE-TOKEN: 44zLZ8rn-1GGTjN1bSsY" "https://gitlab.ateme.net/api/v4/merge_requests?author_username=fdutrech&source_branch=tp-5622"

                    start = time.time()
                    request = f"https://gitlab.ateme.net/api/v4/merge_requests?author_username={os.getenv('USER')}&source_branch={git_branch}"
                    header = "PRIVATE-TOKEN: " + os.getenv('GITLAB_TOKEN')
                    raw, _, _ = bash_cmd(["curl", "--silent", "--header", header, request])

                    gitlab = [f"Fetch time: {time.time()-start:.02} seconds"]

                    try:
                        merge_requests = json.loads(raw)
                    except json.decoder.JSONDecodeError:
                        gitlab += [Back.RED + f"Could not connect to Gitlab - no connected to VPN ? \033[0m"]
                        merge_requests = []

                    if not merge_requests:
                        gitlab += ["No Merge Request"]
                    else:
                        for i, mr in enumerate(merge_requests):
                            color = Fore.BLACK
                            if git_origin in mr['web_url']:
                                color = Fore.YELLOW
                                self.gitlab_url = mr['web_url']
                            gitlab += [f" * [{i+1}/{len(merge_requests)}] " + color + mr['web_url'] + Fore.RESET + "  - " + mr['state']]

                    self.gitlab = gitlab
                    self.refresh()

                self.gitlab = threading.Thread(target=fetch_gitlab)
                self.gitlab.start()
                status = ["Not yet available..."]

            elif not isinstance(self.gitlab, list):
                status = ["Not yet available (running thread)..."]

            else:
                status = self.gitlab

            return ret + status

        def format_pipeline(self):

            ret = [Fore.MAGENTA + "Gitlab pipeline :" + Fore.RESET]

            if self.gitlab is None:
                import threading

                def fetch_pipeline():
                    import time
                    GITLAB_TOKEN=os.getenv('GITLAB_TOKEN')

                    git_branch, _, _ = bash_cmd("git rev-parse --abbrev-ref HEAD")
                    git_origin, _, _ = bash_cmd("git config --get remote.origin.url")

                    git_origin = git_origin.split(":")[1]

                    # curl --silent --connection-timeout --header "PRIVATE-TOKEN: 44zLZ8rn-1GGTjN1bSsY" "https://gitlab.ateme.net/api/v4/merge_requests?author_username=fdutrech&source_branch=tp-5622"

                    start = time.time()
                    request = f"https://gitlab.ateme.net/api/v4/merge_requests?author_username={os.getenv('USER')}&source_branch={git_branch}"
                    header = "PRIVATE-TOKEN: " + os.getenv('GITLAB_TOKEN')
                    raw, _, _ = bash_cmd(["curl", "--silent", "--header", header, request])

                    gitlab = [f"Fetch time: {time.time()-start:.02} seconds"]

                    try:
                        merge_requests = json.loads(raw)
                    except json.decoder.JSONDecodeError:
                        gitlab += [Back.RED + f"Could not connect to Gitlab - no connected to VPN ? \033[0m"]
                        merge_requests = []

                    if not merge_requests:
                        gitlab += ["No Merge Request"]
                    else:
                        for i, mr in enumerate(merge_requests):
                            color = Fore.BLACK
                            if git_origin in mr['web_url']:
                                color = Fore.YELLOW
                                self.gitlab_url = mr['web_url']
                            gitlab += [f" * [{i+1}/{len(merge_requests)}] " + color + mr['web_url'] + Fore.RESET + "  - " + mr['state']]

                    self.gitlab = gitlab
                    self.refresh()

                self.gitlab = threading.Thread(target=fetch_gitlab)
                self.gitlab.start()
                status = ["Not yet available..."]

            elif not isinstance(self.gitlab, list):
                status = ["Not yet available (running thread)..."]

            else:
                status = self.gitlab

            return ret + status

        def format_modified_files(self):

            if self.short_modified:
                return [Fore.MAGENTA + "Modified Tree : " + Fore.RESET + "..."]

            class Tree:
                def __init__(self, data, prefix = ''):
                    self.i = 0
                    self.n = len(data) + len(data["__files__"]) - 1 if "__files__" in data else len(data)
                    self.prefix = prefix

                def child(self, data):
                    return Tree(data, self.prefix + [' │   ', '     '][self.i == self.n])

                def __str__(self):
                    self.i += 1
                    return self.prefix + [' ├── ', ' └── '][self.i == self.n]

                @staticmethod
                def format(data, path="", tree = None):
                    """return lines array of pretty tree print of dict"""
                    tree = Tree(data) if tree is None else tree.child(data)
                    ret = []

                    for file, status in sorted(data.get("__files__", [])):
                        filename = os.path.join(path, file)
                        ret += [""]
                        ret[-1] += str(tree)
                        ret[-1] += file if filename != self.modified_files[self.current_modified_files_index] else Back.MAGENTA + file + Back.RESET
                        if filename in self.dirty_files:
                            ret[-1] += Fore.YELLOW + " *" + Fore.RESET
                        ret[-1] += Fore.CYAN + " %d change(s)"%len(self.find_lines(filename)) + Fore.RESET


                        ret[-1] += " " + {
                            'A' : Fore.BLACK + "New",
                            'M' : Fore.BLACK + "Modified"
                        }.get(status, Back.RED + status + Back.RESET)

                        # ret[-1] += " " + status

                    for key in sorted(list(data.keys())):
                        if key != "__files__":
                            ret += [str(tree) + key] + Tree.format(data[key], os.path.join(path, key), tree)

                    return ret

            return [self.bash_cmd] + Tree.format(self.modified_tree)

        def format_history(self):

            if self.short_history:
                return [Fore.MAGENTA + "History : " + Fore.RESET + "..."]

            history = bash_cmd("git log --decorate --color=always --oneline %s^.."%self.git_target, cwd=self.cwd)[STDOUT]

            ret = [Fore.MAGENTA + "History :" + Fore.RESET]

            for line in history.split("\n"):
                if not line:
                    continue

                if "origin/master" in line:
                    ret.append(Fore.BLACK + "-"*100 + Fore.RESET)

                msg = line.split(" ", 1)[1]
                if msg.startswith("("):
                    msg = msg.split(")", 1)[1]
                msg = msg.strip("\x1b[m ")

                ret.append(line)

                if self.cwd.endswith("TitanProcessing") and not msg.startswith("Merge branch"):

                    ret[-1] += " "

                    invalid = "INVALID COMMIT FORMAT" if not re.match("^.* : .*$", msg) else ""
                    invalid = invalid + "WIP COMMIT" if "wip" in msg else invalid
                    if invalid:
                        ret[-1] += Back.RED + invalid
                    else:
                        ret[-1] += Back.GREEN + "VALID"

                    ret[-1] += "\033[0m"

            return [self.bash_cmd] + ret

        def format_git_diff(self):
            ret = []
            self.git_diff_begin = len(self.fmt)

            for i, (diff_title, diff) in enumerate(self.DIFF):

                if i == self.current_lines_index:
                    prefix = Back.MAGENTA + " " + Back.RESET + " "
                    self.git_diff_current_begin = len(ret)
                else:
                    prefix = "  "

                ret.append(prefix + f"[{i+1}/{len(self.DIFF)}] " + diff_title)
                for line in diff.split("\n"):
                    ret.append(prefix + line)

                ret += self.format_line()
                if i == self.current_lines_index:
                    self.git_diff_current_end = len(ret)

            return ret

def print_status(lr_om_m, lr_om_b, lr_om_ob, lr_ob_b, workdir_clean):


    print("origin/master / master : ", lr_om_m)
    print("origin/master / branch : ", lr_om_b)
    print("origin/master / origin/branch : ", lr_om_ob)
    print("origin/branch / branch : ", lr_ob_b)

    left = []
    left += [[lr_om_m[0], "master"]]
    left += [[lr_om_ob[0], "origin/branch-ancestor"]]
    left += [[lr_om_b[0], "branch-ancestor"]]
    left.sort()
    for i in range(1, len(left)):
        left[i][0] -= left[i-1][0]
    print(left)


    left_rows = [ ["origin/master", "*", ""] ]
    for cumdist, branch in left:
        if cumdist == 0:
            left_rows[-1][0] += " " + branch

        elif cumdist == 1:
            left_rows += [["", "|  ", ""]]
            left_rows += [[branch, "*  ", ""]]

        else:
            left_rows += [["", "|  ", ""]]
            left_rows += [["...+" + str(cumdist-1) + " commit(s)...","*", ""]]
            left_rows += [["", "|  ", ""]]
            left_rows += [[branch, "*  ", ""]]
    left_rows += [["", "|  ", ""]]


    workdir_dirty = not workdir_clean

    right = [[0 + workdir_dirty, "branch"]]
    right += [[0, "workdir"]]

    # branch and origin/branch on same branch ?
    if lr_ob_b[0] == 0:
        right += [[lr_ob_b[1] + workdir_dirty , "origin/branch"]]

    right.sort()
    for i in range(1, len(right)):
        right[i][0] -= right[i-1][0]

    print(f"{workdir_dirty=}")
    print(right)

    right_rows = []
    for cumdist, branch in right:
        if cumdist == 0:
            if right_rows:
                right_rows[-1][0] += " " + branch
            else:
                right_rows += [["", "*", right[0][1]]]

        elif cumdist == 1:
            right_rows += [["", "|", ""]]
            right_rows += [["", "*", branch]]

        else:
            right_rows += [["", "|  ", ""]]
            right_rows += [["", "*", "...+" + str(cumdist-1) + " commit(s)..."]]
            right_rows += [["", "|", ""]]
            right_rows += [["", "*", branch]]
    right_rows += [["", "|  ", ""]]



    for col1, col2, col3 in left_rows:
        print(f"{col1:>35} {col2} {col3:<13}")
    for col1, col2, col3 in right_rows:
        print(f"{col1} {col2} {col3}")







if __name__ == "__main__":

    # if 1:
    if 0:
        s = [
                #lr_om_m,    lr_om_b,    lr_om_ob,   lr_ob_b,   workdir_clean
                [[4,0],      [0,6],      [12,1],     [0,4],    False],
                # [[4,0],      [0,1],      [12,1],     [13,1],    True],
                # [[4,0],      [2,1],      [12,1],     [13,1],    False],
        ]
        for lr_om_m, lr_om_b, lr_om_ob, lr_ob_b, workdir_clean in s:
            print_status(lr_om_m, lr_om_b, lr_om_ob, lr_ob_b, workdir_clean)

        sys.exit(0)

    if "--cli" in sys.argv:

        logging.basicConfig(filename=LOGFILE,
                            filemode='w',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)
        # logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        #                     datefmt='%H:%M:%S',
        #                     level=logging.DEBUG)

        # logging.basicConfig(level=logging.DEBUG)
        logging.getLogger().handlers[0].setFormatter(ColorFormatter())


        sys.argv.remove("--cli")

        target = sys.argv[1] if len(sys.argv) > 1 else "origin/master"

        try:
            SublimeGitDiff(target).run()
        except Exception as e:
            print(f"An error occured while running SublimeGitDiff ({e})")

        sys.exit(0)

    def bash_print(cmd):
        print(Fore.BLACK,cmd, Fore.RESET, sep='')
    bash_proc.print = bash_print

    target = sys.argv[1] if len(sys.argv) > 1 else "origin/master"
    cwd = os.getcwd()

    from pyfiglet import figlet_format
    l,r = bash_cmd("git rev-list --left-right --count %s...HEAD"%target, cwd=cwd)[STDOUT].split("\t")

    title = figlet_format("Sublime Git Diff").split("\n")
    title[-3] += " Comparing against " + target
    title[-3] += " (Left/right=" + l + "/" + r + ")"

    print(Fore.BLACK, "─"*120, Fore.RESET, sep='')

    print()
    for t in title:
        print("\t", Fore.CYAN, t, Fore.RESET, sep='')

    print(Fore.BLACK, "─"*120, Fore.RESET, sep='')

    if l != '0':
        if sys.stdout.isatty():
            desc = "You're not rebased with {} (left/right = {}/{})\n\nDo you want to continue ?".format(target, l, r)
            p = subprocess.Popen(['whiptail', '--title', "Sublime Git Diff", '--backtitle', '', '--defaultno', '--yesno', '--', desc, '10', '100'], stderr=subprocess.PIPE)
            p.communicate()
            if p.returncode != 0:
                print("Goodbye !")
                sys.exit(0)
        else:
            print()
            print("!! You are not rebased against %s !!"%target, repr(l))
            print()

    #
    # History
    #
    history = bash_cmd("git log --decorate --color=always --oneline %s^.."%target, cwd=cwd)[STDOUT]
    print(Fore.MAGENTA, "History :", Fore.RESET, sep='')

    for line in history.split("\n"):
        if "origin/master" in line:
            print(Fore.BLACK, "-"*120, Fore.RESET, sep='')

        msg = line.split(" ", 1)[1]
        if msg.startswith("("):
            msg = msg.split(")", 1)[1]
        msg = msg.strip("\x1b[m ")

        print(line, end=" ")

        if cwd.endswith("TitanProcessing") and not msg.startswith("Merge branch"):

            invalid = "INVALID COMMIT FORMAT" if not re.match("^.* : .*$", msg) else ""
            invalid = invalid + "WIP COMMIT" if "wip" in msg else invalid
            if invalid:
                print(Back.RED, invalid, sep="", end="")
            else:
                print(Back.GREEN, "VALID", Back.RESET, sep="", end="")

        print()



    print(Fore.BLACK, "─"*120, Fore.RESET, sep='')

    #
    # Modified files
    #
    modified = bash_cmd("git diff %s --name-only"%target, cwd=cwd)[STDOUT].split("\n")
    modified = [m for m in modified if m]
    print(Fore.MAGENTA, "Modified files :", Fore.RESET, sep='', end = '')
    if modified:
        print()
        for m in modified:
            print(" -", repr(m))
    else:
        print(" No modified file")

    print(Fore.BLACK, "─"*120, Fore.RESET, sep='')

    # print(Fore.MAGENTA, modified, Fore.RESET)
    #             count and print(Fore.BLACK, "─"*120, Fore.RESET, sep='')

    #
    # Diff
    #
    GitDiff(target, cwd).print()
