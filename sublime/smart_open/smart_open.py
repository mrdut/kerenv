import sublime
import sublime_plugin
import os, re
import html
import time
import traceback
import subprocess

import sys

print(sys.version)

# https://www.sublimetext.com/docs/api_reference.html
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

VERSION="1.0.0"

class SmartOpenCommand(sublime_plugin.TextCommand):

    def run(self, edit, args = None):

        try:

            # get current context (line, cursor position)
            region = self.view.sel()[0]
            point = region.begin()
            _, col = self.view.rowcol(point)
            line = self.view.substr(self.view.line(region))

            # if cursor is at the end of the line, select previous substr
            if col == len(line) and len(line) != 0:
                col -= 1

            # parse current selection (find previous and next whitespace in current line)
            left = line[:col].split(' ')[-1]
            right = line[col:].rsplit(' ')[0]
            substr = left + right
            substr = substr.strip("\"'")
            substr = substr.strip(":")

            print("="*50)
            print("SmartOpenCommand: VERSION", VERSION)
            print("SmartOpenCommand: cwd", os.getcwd())
            print("SmartOpenCommand: filename", self.view.file_name())
            print("SmartOpenCommand: current line", repr(line))
            print("SmartOpenCommand: substr", repr(substr))

            #
            # Empty line
            #
            if not line:
                self.message("current line is empty, skip")
                return

            #
            # Browser file (http://, https://, file:///)
            #  - https://www.sublimetext.com/docs/api_reference.html
            #  - file:///home/fdutrech/Documents/dolby/Dolby_Pro_Audio_Transcoder_Software_Development_Kit_v1.5/Code/Libraries/dlb_padmx/api_docs/docs/html/index.html

            if re.match(r"https?://.*\b", substr) or re.match(r"file:///.*", substr):
                os.system("xdg-open " + substr)
                return

            #
            # PDF file
            #
            match = re.match(r"(.+\.pdf):?(\d*)?", substr)
            if match:
                substr = os.path.join(os.path.dirname(self.view.file_name()), substr)
                print("SmartOpenCommand: PDF", repr(substr))
                cmd = "evince " + match.group(1)
                if match.group(2):
                    cmd += " -i " + match.group(2)
                print(cmd)
                os.system(cmd + " &")
                return

            #
            # Python
            #  - File "/home/fdutrech/.config/terminator/plugins/sublime_wrapper_url.py", line 154
            #
            match=re.search(r'File \"([\w\-_/.]+)\", line (\d+)', line)
            if match:
                filename, row = match.groups()
                return self.open(filename, row)
            #
            # Gdb
            #  - #24 0x0000000011996906 in Titan::FileInputProcessingUnit::main (this=0x1fe87d00) at /home/fdutrech/Workspace/dev/TitanProcessing/backbone/src/titan_backbone_processing_unit_input.cpp:324
            #
            # Valgrind
            #   ==45246==    by 0x4479A8: gaac::paec::Encoder::encode(GAAC_Input const&) (src/paec/gaac_paec_encoder.cpp:257)

            match = re.search(r'([\w_\-./]+)(?::([0-9]+))?(?::([0-9]+))?', substr)
            if match:
                filename, row, col = match.groups()
                print("SmartOpenCommand: default match filename=%s, row=%s, col=%s"%(str(filename), str(row), str(col)))
                return self.open(filename, row, col)

            #
            # Pytest
            #  - tp_test "units/TitanAudioEncoder/tests/test_audio_encoder.py::TestAudioEncoder::test_audio_encoder_generic[250hz-aac-he]"
            #
            match=re.match(r"\"?(units/Titan[\w]+/tests/[\w_]+\.py)(::[\w_]+)?(::[\w_]+)?(\[.*\])?\"?", substr)
            if match:
                print("SmartOpenCommand: pytest match=%s"%(str(match.groups())))
                filename, classname, functionname, idname = match.groups()
                filename = os.path.join(os.environ["ATEME_WORKSPACE"], "TitanProcessing", filename)
                if os.path.exists(filename):
                    if functionname:
                        row = 1
                        print("SmartOpenCommand: functionname=%s"%functionname)

                        functionname = functionname[2:] # remove leading "::"
                        with open(filename) as f:
                            for irow, line in enumerate(f):
                                if functionname in line:
                                    row = irow + 1

                    print("SmartOpenCommand: pytest match filename=%s, row=%s, col=%s"%(str(filename), str(row), str(col)))
                    return self.open(filename, row, col)

                else:
                    sublime.error_dialog("SmartOpenCommand: no such Pytest %s"%repr(filename))
                    return


            #
            # No pattern found, goodbye !
            #
            self.message("no match for current selection %s"%repr(substr))

        except Exception as e:
            print("SmartOpenCommand::run() Exception:")
            print(traceback.format_exc())

        except Exception as e:
            raise e


    def open(self, filename, row = None, col = None):
        print("SmartOpenCommand: open filename=%s, row=%s, col=%s"%(str(filename), str(row), str(col)))

        if filename:

            if not os.path.exists(filename) and self.view.file_name() is not None:
                current_directory_filename = os.path.join(os.path.dirname(self.view.file_name()), filename)
                self.message("SmartOpenCommand: try open in current directory (%s) does not exists"%current_directory_filename)
                if os.path.exists(current_directory_filename):
                    filename = current_directory_filename

            if not os.path.exists(filename):
                extension = os.path.splitext(filename)[1]
                root = os.environ.get("ATEME_WORKSPACE", "/home/fdutrech/Workspace/dev")
                if not os.path.exists(root):
                    self.message("ERROR: fzf root path (%s) does not exists"%root)
                    return
                cmd = 'find %s -name "*%s" | /home/fdutrech/Workspace/third_parties/fzf/bin/fzf --no-sort --filter %s'%(root, extension, filename)
                self.message(cmd)
                ps = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = ps.communicate()

                if stderr:
                    self.message("Stderr: %s"%stderr.decode())

                filenames=stdout.decode().strip('\n').split("\n")

                if len(filenames)!=1:
                    self.message("fzf multiple matches !! filenames=%s"%(str(filenames)))
                    filenames = [f for f in filenames if f.endswith("/" + filename)]

                if filenames:
                    filename = filenames[0]
                    self.message("fzf -> %s"%filename)

            view = self.view.window().open_file(filename)
            if view and row:
                def wait_is_loading():
                    if view.is_loading():
                        sublime.set_timeout(wait_is_loading, 250)
                    else:
                        view.run_command("goto_line", {"line": int(row) } )
                wait_is_loading()


    def message(self, msg):
        sublime.status_message("SmartOpenCommand: " + msg)
        print("SmartOpenCommand: " + msg)
