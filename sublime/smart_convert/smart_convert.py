import sublime
import sublime_plugin
import os, re, sys
import math
import traceback

# https://www.sublimetext.com/docs/api_reference.html
# https://github.com/bradrobertson/sublime-packages/tree/master/Default

class SmartConvertCommand(sublime_plugin.TextCommand):

    def __init__(self, arg):
        super().__init__(arg)
        self.region = None
        self.substr = None

    def parse(self, value):
        try:
            if value.startswith("0x") or value.startswith("0X"):
                return "hexa", int(value, 16)
            if value.startswith("0b"):
                return "binary", int(value, 2)
            return "decimal", int(value, 10)
        except Exception as e:
            self.message("ERROR: could not parse %s"%value)
            return None, None

    def run(self, edit, args = None):

        try:

            # get current context (line, cursor position)
            region = self.view.sel()[0]
            point = region.begin()
            _, col = self.view.rowcol(point)
            line = self.view.substr(self.view.line(region))

            # if cursor is at the end of the line, select previous substr
            if col == len(line) and len(line) != 0:
                col -= 1

            # parse current selection (find previous and next whitespace in current line)

            regex = re.compile(r'([0-9a-fA-FxXb]*)(.*)')

            # print(sublime.version())

            # reverse left side, and then re-reverse it...
            left = regex.match(line[::-1][len(line)-col:]).group(1)[::-1]
            right = regex.match(line[col:]).group(1)

            # print("left", left)
            # print("right", right)

            # left = line[:col].split(' ')[-1]
            # right = line[col:].rsplit(' ')[0]
            substr = left + right

            # self.message("current line: " + repr(line))
            # self.message("substr: " + repr(substr))

            fmt, value = self.parse(substr)

            if fmt is not None:
                html = "decimal: %s, hexa: %s, binary: %s"%(str(value), hex(value), bin(value))
            # elif fmt == "hexa":
            #     html = "decimal: %s, binary: %s"%(str(value), bin(value))
            # elif fmt == "binary":
            #     html = "decimal: %s, hexa: %s"%(str(value), hex(value))

                lut = {
                    "decimal" : ("font-weight: normal; color: cornflowerblue;", str(value), 3, None),
                    "hexa" : ("font-weight: normal; color: darkseagreen;", hex(value), 2, 0),
                    "binary" : ("font-weight: normal; color: orange;", bin(value), 4, 0)
                }

                # http://davidbau.com/colors/
                html = """
                        <div style='padding: 0.4em;
                                margin-top: 0.2em;
                                border: 1px solid;
                                border-radius: 10px;
                                background-color: dark-gray'>
                        """

                for key, (style, value, sep, pad) in lut.items():
                    if key == fmt:
                        style = style.replace("normal", "bold")

                    if pad is not None:
                        prefix, value = value[:2], value[2:]
                        value = value.zfill(math.ceil(len(value)/sep)*sep)
                    else:
                        prefix = ""

                    idx = sorted(list(set([0] + [idx for idx in range(len(value)%sep, len(value), sep)] + [len(value)])))
                    print(value, idx)
                    value = ".".join([ value[i:j] for i,j in zip(idx[::1], idx[1::1]) ])
                    html += "<span style='color: gray'>{}</span><span style='{}'>{} </span>".format(prefix, style, value)

                html += "</div>"

            # au milieu d'une phrase 123456789
            # int:1234
            # binary:0b0101110
            # hex:0xbca10
            # HEX: 0x0123ABC

            else:
                html = None

            self.view.erase_phantoms("smart-convert")


            if html:
                if self.region != region and self.substr != substr:
                    self.view.add_phantom("smart-convert", region, html, sublime.LAYOUT_BELOW)
                    self.region = region
                    self.substr = substr
                else:
                    self.region = None
                    self.substr = None


        except Exception as e:
            print("SmartOpenCommand::run() Exception:")
            print(traceback.format_exc())

        except Exception as e:
            raise e

    def message(self, msg):
        sublime.status_message("SmartConvertCommand: " + msg)
        print("SmartConvertCommand: " + msg)
