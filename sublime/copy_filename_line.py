import sublime
import sublime_plugin
import os, re

# https://github.com/miguelgraz/FocusFileOnSidebar/blob/master/FocusFileOnSidebar.py
# https://github.com/xunker/AutoHideSidebar/blob/212d71b7837fa63a637c42587bdec46a326294e3/AutoHideSidebar.py#L32
class RevealAndFocusSideBarCommand(sublime_plugin.WindowCommand):
    def run(self):
        print("RevealAndFocusSideBarCommand : self.window.is_sidebar_visible() : ", self.window.is_sidebar_visible())
        if self.window.is_sidebar_visible():
            print("RevealAndFocusSideBarCommand : toggle (hide)")
            self.window.set_sidebar_visible(False)
            view=sublime.active_window().active_view()
            if view.sheet().is_transient() or view.sheet().is_semi_transient():
                sublime.active_window().open_file( view.file_name() )
        else:
            print("RevealAndFocusSideBarCommand : reveal + focus")
            self.window.run_command("reveal_in_side_bar")
            self.window.run_command("focus_side_bar")

class HideSideBarOrMoveCommand(sublime_plugin.WindowCommand):
    def run(self):
        if self.window.is_sidebar_visible():
            print("HideSideBarOrMoveCommand : hide sidebar")
            self.window.set_sidebar_visible(False)
            view=sublime.active_window().active_view()
            if view.sheet().is_transient() or view.sheet().is_semi_transient():
                sublime.active_window().open_file( view.file_name() )
        else:
            self.window.run_command("move", args = {"by": "word_ends", "forward": True} )

class CustomContextEventListener(sublime_plugin.EventListener):
    """
    This event is raised when Sublime sees a key binding context key it does not
    recognize.
    """
    # def on_load(self, view):
    #     print("on_load !!", view.file_name())

    def on_activated(self, view):

        if sublime.active_window().is_sidebar_visible():

            print("on_activated !!", sublime.active_window().active_sheet(), view.file_name(), view.sheet().is_transient(), view.sheet().is_semi_transient(), " - hide sidebar")
            # sublime.active_window().set_sidebar_visible(False)
            return True
        return False

    # def on_query_context(self, view, key, operator, operand, match_all):
    #     print("on_query_context")
    #     print("key", key)
    #     print("operator", operator)
    #     print("operand", operand)
    #     print("match_all", match_all)
    #     return None

    # def on_text_command(self, view, command_name, args):
    #     print("on_text_command")
    #     print("command_name", command_name)
    #     print("args", args)
    #     return None

    # def on_window_command(self, window, command_name, args):
    #     print("on_window_command")
    #     print("command_name", command_name)
    #     print("args", args)
    #     return None

        # # Determine if we understand the key. If we don't we should return None
        # # to allow Subliem to ask another listener.
        # if not key.startswith('view_setting.'):
        #     return None

        # # Obtain the actual setting by removing the common key prefix, and then
        # # get the value of the setting for us in our comparison.
        # setting = key[len('view_setting.'):]
        # lhs = view.settings().get(setting)

        # # The operator tells us what sort of comparison to do and the operand
        # # is the information that comes from the key binding.
        # #
        # # The full list of options is:
        # #   OP_EQUAL, OP_NOT_EQUAL, OP_REGEX_MATCH, OP_NOT_REGEX_MATCH,
        # #   OP_REGEX_CONTAINS, OP_NOT_REGEX_CONTAINS
        # if operator == sublime.OP_EQUAL:
        #     return lhs == operand
        # elif operator == sublime.OP_NOT_EQUAL:
        #     return lhs != operand

        # # We understand the key, but someone used an operator that we don't
        # # understand, so default to saying that the binding doesn't apply.
        # return False

# https://www.sublimetext.com/docs/api_reference.html#sublime.View
# https://bitbucket.org/mrdut/hexaparser-master/src/master/HexaParser.py
# https://github.com/bradrobertson/sublime-packages/tree/master/Default
class CopyFilenameLineCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sel = self.view.sel()
        if sel:
            region = sel[0]
            point = region.begin()
            row = self.view.rowcol(point)[0]+1 # 0-based line
            file_line = "%s:%d"%(os.path.basename(self.view.file_name()),row)
            print("'%s' copied to clipboard !"%file_line)
            sublime.set_clipboard(file_line)

