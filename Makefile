


.PHONY: user_vpnc user_vpnc_disconnect

install:
	apt-get install mkdocs





# https://stackoverflow.com/questions/649246/is-it-possible-to-create-a-multi-line-string-variable-in-a-makefile
define SERVICE_STRING
[Unit]
Description=Kerenv service

[Service]
ExecStart=$(shell which mkdocs) serve
WorkingDirectory=${PWD}
# Restart=always
# User=$(shell whoami)

[Install]
WantedBy=multi-user.target
endef

export SERVICE_STRING
# sudo make service > /etc/systemd/system/kerenv.service
service:
	@echo "$$SERVICE_STRING"